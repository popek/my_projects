#pragma once
#include "GMMA_Signal.h"

class IPointer;
class AssetContainer;


class GMMA_NearBuySignal : public GMMA_Signal
{
public:

   GMMA_NearBuySignal(shared_ptr<AssetContainer> a_spAssetContainer);
   virtual ~GMMA_NearBuySignal(void);


   virtual bool VerifyAssetConditions();

private:

   // check if week signal exists (week signal is when short term means are below long term means, and last kandle value is near 
   // smallest lont term mean)
   bool WeekSignalExists();

   // check if strong signal exists (this appears when we are after week signal, but still true signal is not generated. In this state, few last 
   // short term means maximm value is between long term means boundries, and before that short term means are below long term means)
   bool StrongSignalExists();
   
   bool ShortMeansBelowLongMeans(const size_t a_offsetStart, const size_t a_offsetEnd) const;

private:
   size_t m_shortAndLongMeansNearEachOtherLongestPeriod;
   size_t m_shortMeansGettingToLongMeansPeriod;
   double m_dLongMeanMinimumValueOffset;
};
