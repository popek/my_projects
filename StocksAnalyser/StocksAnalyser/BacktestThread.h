#pragma once
#include "ThreadBase.h"
#include "IThreadMessageHandler.h"


class TradeStrategyBase;
class StrategyResult;
class StrategyParams;
class KandleDate;


class BacktestThread : public ThreadBase, public IThreadMessageHandler
{
public:
   class IBacdtestObserver
   {
   public:
      virtual ~IBacdtestObserver(){}

      virtual void onAssetsAmountEstablished(int assetsToBacktestAmount) = 0;
      virtual void onAssetTested(const CString& assetFilePath, shared_ptr<StrategyResult> result) = 0;
      virtual void onBactestFinished() = 0;
   };

public:
   BacktestThread(void);
   virtual ~BacktestThread(void);

   virtual BOOL ProcessThreadMessage(UINT a_messageId);

   void cancelBacktest();

   void startBacktest(int strategyId, 
                      CString databaseDir, 
                      IBacdtestObserver* bactestObserver,
                      shared_ptr<KandleDate> testStartDate,
                      shared_ptr<KandleDate> testFinishDate);

private:

   void processAllAssets();
   void processAsset(CString filePath);

   shared_ptr<TradeStrategyBase> createStrategy(const CString& assetFilePath);

private:
   bool _stopProcessing;
   shared_ptr<StrategyParams> _params;

   int _strategyIndex;
   CString _databaseDir;
   IBacdtestObserver* _backtestObserver;
   shared_ptr<KandleDate> _startDate;
   shared_ptr<KandleDate> _finishDate;
};
