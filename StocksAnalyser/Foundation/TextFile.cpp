#include "StdAfx.h"
#include "TextFile.h"

TextFile::TextFile(void)
: m_hFile(NULL)
{
}
TextFile::~TextFile(void)
{
}

BOOL TextFile::OpenForWritting(CString a_strFilePath)
{
   m_hFile = CreateFile(a_strFilePath, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

   return m_hFile!=NULL;
}

void TextFile::Write(CString a_strText)
{
   USES_CONVERSION;
   LPCWSTR pszWideText = T2W(a_strText.GetBuffer());

   DWORD bytesWritten = 0;
   WriteFile(m_hFile, (void*)pszWideText, a_strText.GetLength()*sizeof(TCHAR), &bytesWritten, NULL);
}

void TextFile::Close()
{
   CloseHandle(m_hFile);
}