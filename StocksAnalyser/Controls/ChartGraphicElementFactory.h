#pragma once
#include "EChartElementId.h"


class GraphicObjectBase;
class GraphicObjectPainterBase;
class GrxStyle;


class ChartGraphicElementFactory
{
   NONCOPYABLE_TYPE(ChartGraphicElementFactory)

private:
   ChartGraphicElementFactory(void);
   virtual ~ChartGraphicElementFactory(void);

public:
   static ChartGraphicElementFactory* Instance();

   shared_ptr<GraphicObjectBase> CreateGraphicObject(EChartElementId a_objectId, shared_ptr<GrxStyle> a_style) const;

   shared_ptr<GrxStyle> CreateDefaultGrxStyle(EChartElementId a_objectId) const;

private:

   shared_ptr<GraphicObjectPainterBase> CreateGraphicObjectPainter(EChartElementId a_objectId) const;

private:
   static ChartGraphicElementFactory* Sm_pSingleInstance;
};
