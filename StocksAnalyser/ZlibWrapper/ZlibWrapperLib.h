#pragma once

#ifdef EXPORT_ZLIBWRAPPER_LIB
#  define ZLIBWRAPPER_LIB      __declspec(dllexport)
#else
#  define ZLIBWRAPPER_LIB      __declspec(dllimport)
#endif