#include "stdafx.h"
#include "ScrollBarExt.h"
#include "MemoryDC.h"
#include "IDataRangeObserver.h"
#include "ControlsLibUtils.h"


IMPLEMENT_DYNAMIC(ScrollBarExt, CWnd)

ScrollBarExt::ScrollBarExt(IDataRangeObserver* a_pRangeObserver)
: m_pRangeObserver(a_pRangeObserver)
, m_minThumbLen(5)
{
   m_bkColor = RGB(226, 225, 227);
   //m_thumbColor = RGB(152, 158, 167);
   m_thumbColor = RGB(60, 60, 60);
}
ScrollBarExt::~ScrollBarExt()
{
}

void ScrollBarExt::Reset()
{
   CRect rcClient;
   if(GetSafeHwnd())
   {
      GetClientRect(&rcClient);
   }

   m_thumbStart = 0.0;
   m_thumbFinish = 1.0;

   m_thumbStartPos = 0;
   m_thumbFinishPos = rcClient.right;


   m_dragBorderEnabled = false;
   m_dragBorderOnGoing = false;

   m_dragThumbEnabled = false;
   m_dragThumbOnGoing = false;
}

double ScrollBarExt::GetStartNormalisedPos() const
{
   return m_thumbStart;
}

double ScrollBarExt::GetFinishNormalisedPos() const
{
   return m_thumbFinish;
}

void ScrollBarExt::ForceSetRange(double a_thumbStart, double a_thumbStop)
{
   ASSERT(a_thumbStart>=0.0);
   ASSERT(a_thumbStop<=1.0);

   m_thumbStart  = a_thumbStart;
   m_thumbStartPos = (int)(m_thumbStart*m_windowWidth);

   m_thumbFinish = a_thumbStop;
   m_thumbFinishPos = (int)(m_thumbFinish*m_windowWidth);

   Invalidate();

   // notify observer
   m_pRangeObserver->OnDataRangeChanged(m_thumbStart, m_thumbFinish);
}
void ScrollBarExt::Handle_DragBorder(const CPoint& a_mousePos)
{
   if(m_dragingLeft)
   {
      m_thumbStartPos = (a_mousePos.x < 0 ? 0 : a_mousePos.x);
      m_thumbStartPos = min(m_thumbStartPos, (m_thumbFinishPos - m_minThumbLen));

      m_thumbStart = (double)m_thumbStartPos/m_windowWidth;
   }
   else
   {
      m_thumbFinishPos = (a_mousePos.x > (int)m_windowWidth ? (int)m_windowWidth : a_mousePos.x);
      m_thumbFinishPos = max((m_thumbStartPos + m_minThumbLen), m_thumbFinishPos);

      m_thumbFinish = (double)m_thumbFinishPos/m_windowWidth;
   }

   Invalidate();
   SetCursor(ControlsLibUtils::Cursor(IDC_SIZEWE));
   // notify observer
   m_pRangeObserver->OnDataRangeChanged(m_thumbStart, m_thumbFinish);
}

void ScrollBarExt::Handle_DragThumb(const CPoint& a_mousePos)
{
   int thumbLenBuffer = m_thumbFinishPos - m_thumbStartPos;
   int diff           = a_mousePos.x - m_thumbDragStartPos;

   m_thumbStartPos += diff;
   m_thumbStart    = (double)m_thumbStartPos/m_windowWidth;

   m_thumbFinishPos += diff;
   m_thumbFinish = (double)m_thumbFinishPos/m_windowWidth;

   // if position is in range then notify observer
   if(m_thumbFinish <=1.0 && m_thumbStart >=0.0)
   {
      // update "drag start position"
      m_thumbDragStartPos = a_mousePos.x;
   }
   // if position is out of range then correct position
   else
   {
      if(m_thumbFinish > 1.0)
      {
         m_thumbFinish = 1.0;
         m_thumbFinishPos = (int)m_windowWidth;

         m_thumbStartPos = m_thumbFinishPos - thumbLenBuffer;
         m_thumbStart    = (double)m_thumbStartPos/m_windowWidth;
      }
      else
      {
         m_thumbStart    = 0.0;
         m_thumbStartPos = 0;

         m_thumbFinishPos = thumbLenBuffer;
         m_thumbFinish = (double)m_thumbFinishPos/m_windowWidth;
      }
   }

   Invalidate();
   // notify observer
   m_pRangeObserver->OnDataRangeChanged(m_thumbStart, m_thumbFinish);
}

BEGIN_MESSAGE_MAP(ScrollBarExt, CWnd)
   ON_WM_MOUSEMOVE()
   ON_WM_LBUTTONDOWN()
   ON_WM_LBUTTONUP()
   ON_WM_PAINT()
   ON_WM_SIZE()
   ON_WM_CREATE()
   ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()



void ScrollBarExt::OnMouseMove(UINT nFlags, CPoint point)
{
   const int dragArea = 4;

   if(m_dragBorderOnGoing)
   {
      Handle_DragBorder(point);
   }
   else
   {
      if(m_dragThumbOnGoing)
      {
         Handle_DragThumb(point);
      }
      else
      {
         if( point.x > m_thumbStartPos - dragArea && point.x <= m_thumbStartPos + dragArea )
         {
            SetCursor(ControlsLibUtils::Cursor(IDC_SIZEWE));   
            m_dragBorderEnabled = true;
            m_dragingLeft = true;
         }
         else if(point.x > m_thumbFinishPos - dragArea && point.x <= m_thumbFinishPos + dragArea)
         {
            SetCursor(ControlsLibUtils::Cursor(IDC_SIZEWE));   
            m_dragBorderEnabled = true;
            m_dragingLeft = false;
         }
         else
         {
            m_dragBorderEnabled = false;
            SetCursor(ControlsLibUtils::Cursor(IDC_ARROW));

            if(m_thumbStartPos<point.x && point.x<=m_thumbFinishPos)
            {
               m_dragThumbEnabled = true;
            }
            else
            {
               m_dragThumbEnabled = false;
            }
         }
      }
   }

   CWnd::OnMouseMove(nFlags, point);
}

void ScrollBarExt::OnLButtonDown(UINT nFlags, CPoint point)
{
   if(m_dragBorderEnabled)
   {
      if(false == m_dragBorderOnGoing)
      {
         if(this != GetCapture())
         {
            SetCapture();
         }

         m_dragBorderOnGoing = true;
      }
   }
   else if(m_dragThumbEnabled)
   {
      if(false == m_dragThumbOnGoing)
      {
         if(this != GetCapture())
         {
            SetCapture();
         }

         m_dragThumbOnGoing = true;

         m_thumbDragStartPos = point.x;
      }
   }

   CWnd::OnLButtonDown(nFlags, point);
}

void ScrollBarExt::OnLButtonUp(UINT nFlags, CPoint point)
{
   m_dragBorderOnGoing = false;
   m_dragThumbOnGoing = false;

   if(this == GetCapture())
   {
      ReleaseCapture();
   }

   CWnd::OnLButtonUp(nFlags, point);
}

void ScrollBarExt::OnPaint()
{
   const int minimumThumbWidth = 6;
   CPaintDC dc(this); 
   CRect rcClient;
   CRect rcThumb;
   
   GetClientRect(&rcClient);
   rcThumb.top = rcClient.top;
   rcThumb.bottom = rcClient.bottom;
   rcThumb.left = m_thumbStartPos;
   rcThumb.right = m_thumbFinishPos;

   if(rcThumb.Width() < minimumThumbWidth)
   {
      int inflateOffset = (minimumThumbWidth - rcThumb.Width())/2;

      rcThumb.InflateRect(inflateOffset, 0, inflateOffset, 0);
   }

   MemoryDC memDC(&dc, this);

   memDC.BufferedDC().FillSolidRect(&rcClient, m_bkColor);


   CRgn thumb;
   thumb.CreateRoundRectRgn(rcThumb.left+1, rcThumb.top+1, rcThumb.right, rcThumb.bottom, rcClient.Height()/3, rcClient.Height()/3);
   memDC.BufferedDC().SelectClipRgn(&thumb);

   memDC.BufferedDC().FillSolidRect(&rcThumb, m_thumbColor);   

   thumb.DeleteObject();
}

void ScrollBarExt::OnSize(UINT nType, int cx, int cy)
{
   CWnd::OnSize(nType, cx, cy);
   
   m_windowWidth = (double)cx;

   m_thumbStartPos = (int)(m_thumbStart * m_windowWidth);
   m_thumbFinishPos = (int)(m_thumbFinish * m_windowWidth);

   Invalidate();
}

int ScrollBarExt::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
   if (CWnd::OnCreate(lpCreateStruct) == -1)
      return -1;

   Reset();

   return 0;
}

void ScrollBarExt::OnLButtonDblClk(UINT nFlags, CPoint point)
{
   if(m_thumbStart > 0.0 && m_thumbFinish < 1.0)
   {
      if(point.x <= m_thumbFinishPos && point.x >= m_thumbStartPos)
      {
         ForceSetRange(0.0, 1.0);
      }
   }

   __super::OnLButtonDblClk(nFlags, point);
}
