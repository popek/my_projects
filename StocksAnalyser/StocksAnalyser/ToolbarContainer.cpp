#include "stdafx.h"
#include "StocksAnalyser.h"
#include "ToolbarContainer.h"
#include "ClientRect.h"
#include "WindowRect.h"
#include "WindowUtils.h"


IMPLEMENT_DYNAMIC(ToolbarContainer, CWnd)

const LPCTSTR  TOOLBAR_WINDOW_CLAS_NAME = _T("STOCK_ANALYZE_TOOLS");

WNDPROC ToolbarContainer::S_originalProc = NULL;

ToolbarContainer::ToolbarContainer()
{
   VERIFY( WindowUtils::RegisterWindowClass(TOOLBAR_WINDOW_CLAS_NAME, NULL, CS_HREDRAW|CS_DBLCLKS|CS_VREDRAW) );
}
ToolbarContainer::~ToolbarContainer()
{
}

BOOL ToolbarContainer::Create(CWnd* a_parent)
{
   //return m_toolbar.Create(StockAnalyserToolbar::IDD, G_stockAnalyserApp.GetMainWnd());

   DWORD style = WS_CAPTION|WS_CHILD|WS_OVERLAPPED|WS_CLIPCHILDREN|WS_VISIBLE|WS_THICKFRAME;
   CRect rcWindow(0, 0, 100, 100);
   
   //CWnd::CreateEx(WS_EX_TOOLWINDOW|WS_EX_WINDOWEDGE, NULL, _T("Narz�dzia"), style, rcWindow, G_stockAnalyserApp.GetMainWnd(), 0)
   if(CWnd::Create(TOOLBAR_WINDOW_CLAS_NAME, _T("Narz�dzia"), style, rcWindow, a_parent, 0))
   {
      ClientRect rcParent(a_parent);
      CPoint     ptMiddle = rcParent.CenterPoint();
      SetWindowPos(&CWnd::wndTopMost, ptMiddle.x, ptMiddle.y, 0, 0, SWP_NOSIZE|SWP_NOZORDER);

      S_originalProc = (WNDPROC) SetWindowLong(GetSafeHwnd(), GWL_WNDPROC, (LONG)ToolsWndProc); 

      return TRUE;

      if(m_toolbar.Create(StockAnalyserToolbar::IDD, this))
      {
         ClientRect rcParent(a_parent);
         CPoint     ptMiddle = rcParent.CenterPoint();

         WindowRect rcContainerWindow(this);
         ClientRect rcContainerClient(this);
         ClientRect rcToolbar(m_toolbar);

         int xOffset = rcContainerWindow.Width() - rcContainerClient.Width();
         int yOffset = rcContainerWindow.Height() - rcContainerClient.Height();
         
         rcToolbar.right += xOffset;
         rcToolbar.bottom += yOffset;

         SetWindowPos(&CWnd::wndTopMost, ptMiddle.x, ptMiddle.y, rcToolbar.Width(), rcToolbar.Height(), 0);

         return TRUE;
      }
   }

   return FALSE;
}

void ToolbarContainer::Show()
{
   if(GetSafeHwnd())
   {
      ShowWindow(SW_SHOW);
   }
}

BOOL ToolbarContainer::Visible() const
{
   return GetSafeHwnd() ? IsWindowVisible() : FALSE;
}

LRESULT ToolbarContainer::ToolsWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
   return CallWindowProc(S_originalProc, hWnd, Msg, wParam, lParam); 
}

BEGIN_MESSAGE_MAP(ToolbarContainer, CWnd)
   ON_WM_PAINT()
   ON_WM_NCLBUTTONDOWN()
   ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()



void ToolbarContainer::OnPaint()
{
   CPaintDC dc(this);

   ClientRect  rcClient(this);

   dc.FillSolidRect(&rcClient, RGB(200, 0,0));
   int oldBkMode = dc.SetBkMode(TRANSPARENT);
   dc.DrawText(_T("test"), rcClient, DT_VCENTER|DT_CENTER|DT_END_ELLIPSIS);

   dc.SetBkMode(oldBkMode);
}

void ToolbarContainer::OnNcLButtonDown(UINT nHitTest, CPoint point)
{

   // TODO: Add your message handler code here and/or call default

   CWnd::OnNcLButtonDown(nHitTest, point);
}

void ToolbarContainer::OnLButtonDown(UINT nFlags, CPoint point)
{
   // TODO: Add your message handler code here and/or call default

   CWnd::OnLButtonDown(nFlags, point);
}
