#include "stdafx.h"
#include "StocksAnalyser.h"
#include "MainFrm.h"
#include "ChildFrm.h"
#include "StocksAnalyserDoc.h"
#include "StocksAnalyserView.h"
#include "AssetsListDownloader.h"
#include "InstrumentListUpdateProgressDlg.h"
#include "Misc.h"
#include "ChartElementsCollection.h"
#include "OHLCElement.h"
#include "Asset.h"

#include "MarketInfo.h"
#include "GlobalSettings.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


BEGIN_MESSAGE_MAP(CStocksAnalyserApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CStocksAnalyserApp::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()


CStocksAnalyserApp::CStocksAnalyserApp()
{
}


CStocksAnalyserApp G_stockAnalyserApp;


BOOL CStocksAnalyserApp::InitInstance()
{
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
	LoadStdProfileSettings(4);  // Load standard INI file options (including MRU)

   CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(IDR_StocksAnalyserTYPE,
		RUNTIME_CLASS(CStocksAnalyserDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CStocksAnalyserView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
	{
		delete pMainFrame;
		return FALSE;
	}
	m_pMainWnd = pMainFrame;
	m_pMainWnd->DragAcceptFiles();

	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

	CCommandLineInfo cmdInfo;
   cmdInfo.m_nShellCommand = CCommandLineInfo::FileNothing;
	ParseCommandLine(cmdInfo);

	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

   pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

   // Initialize GDI+.
   Gdiplus::GdiplusStartup(&_gdiplusToken, &_gdiplusStartupInput, NULL);

   AssetsListDownloader::Init();

   ChartDocument::SetBeforeShowChartEvent(this);

   MarketInfo::instance();

   return TRUE;
}

ToolbarContainer& CStocksAnalyserApp::ToolsWindow()
{
   return ((CMainFrame*)m_pMainWnd)->ToolsWnd();
}

void CStocksAnalyserApp::ShowGMMADlg()
{
   if( !m_GMMAsignalDlg.GetSafeHwnd() )
   {
      m_GMMAsignalDlg.Create(SearchGMMAsignalsDlg::IDD, NULL);
   }

   m_GMMAsignalDlg.ShowWindow(SW_SHOW);
}

BOOL CStocksAnalyserApp::GMMADlgVisible() const
{
   return m_GMMAsignalDlg.GetSafeHwnd() ? m_GMMAsignalDlg.IsWindowVisible() : FALSE;
}

void CStocksAnalyserApp::ShowTradeStrategyTesterDlg()
{
   if( !m_strategyTesterDlg.GetSafeHwnd() )
   {
      m_strategyTesterDlg.Create(TradeStrategiesTesterDlg::IDD, NULL);
   }

   m_strategyTesterDlg.ShowWindow(SW_SHOW);
}

BOOL CStocksAnalyserApp::TradeStrategyTesterDlgVisible() const
{
   return m_strategyTesterDlg.GetSafeHwnd() ? m_strategyTesterDlg.IsWindowVisible() : FALSE;
}

void CStocksAnalyserApp::ShowMarketScannerDlg()
{
   if( !m_marketScannerDlg.GetSafeHwnd() )
   {
      m_marketScannerDlg.Create(MarketScannerDlg::IDD, NULL);
   }

   m_marketScannerDlg.ShowWindow(SW_SHOW);
}

BOOL CStocksAnalyserApp::MarketScannerDlgVisible() const
{
   return m_marketScannerDlg.GetSafeHwnd() ? m_marketScannerDlg.IsWindowVisible() : FALSE;
}


void CStocksAnalyserApp::UpdateCSVFiles()
{
   InstrumentListUpdateProgressDlg  instrumentListUpdateDlg;
   instrumentListUpdateDlg.DoModal();
}

UINT CStocksAnalyserApp::AssetsListUpdateSuccessMsg()
{
   return WM_USER + 23;
}

UINT CStocksAnalyserApp::AssetsListUpdateErrorMsg()
{
   return AssetsListUpdateSuccessMsg()+1;
}

void CStocksAnalyserApp::OnBeforeShowingAsset(Asset& a_assetToShow) const
{
   Misc::TryToAttachStantardCharElementsSet(a_assetToShow);

   a_assetToShow.ElementsCollection()->PushFront( shared_ptr<ChartElement>(new OHLCElement(a_assetToShow)) );
}

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// App command to run the dialog
void CStocksAnalyserApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}
int CStocksAnalyserApp::ExitInstance()
{
   Gdiplus::GdiplusShutdown(_gdiplusToken);

   if(m_GMMAsignalDlg.GetSafeHwnd())
   {
      m_GMMAsignalDlg.DestroyWindow();
   }

   MarketInfo::release();
   GlobalSettings::release();

   return CWinApp::ExitInstance();
}

BOOL CStocksAnalyserApp::PreTranslateMessage(MSG* pMsg)
{
   if(AssetsListUpdateSuccessMsg() == pMsg->message)
   {
      AfxMessageBox(_T("Llista instrument�w zaktualizowana"));
      return TRUE;
   }
   else if(AssetsListUpdateErrorMsg() == pMsg->message)
   {
      AfxMessageBox(_T("Wyst�pi� b��d podczas aktualizacji listy instrument�w !!!"));
      return TRUE;
   }

   return CWinApp::PreTranslateMessage(pMsg);
}