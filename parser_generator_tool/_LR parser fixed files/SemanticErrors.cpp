#include "StdAfx.h"
#include "SemanticErrors.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


SemanticErrors::SemanticErrors(void)
{
}
SemanticErrors::~SemanticErrors(void)
{
}

bool SemanticErrors::errorOccured() const
{
   return m_errors.size() > 0;
}

void SemanticErrors::registerError(SemanticErrors::EDetails a_details)
{
   m_errors.push_back(Entity(EPEL_error, a_details));
}

void SemanticErrors::registerWarning(SemanticErrors::EDetails a_details)
{
   m_warnings.push_back(Entity(EPEL_warning, a_details));
}