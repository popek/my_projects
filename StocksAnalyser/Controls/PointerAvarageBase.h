#pragma once
#include "ControlsLib.h"
#include "IPointer.h"


class CONTROLS_LIB PointerAvarageBase : public IPointer
{
   NONCOPYABLE_TYPE(PointerAvarageBase)

public:
   PointerAvarageBase(int a_nMeanValueCalculationSamplesAmount);
   virtual ~PointerAvarageBase(void);

   virtual const DataSeries& Series(size_t a_seriesId) const;

   const size_t period() const;

protected:

   int m_nMeanValueCalculationSamplesAmount;
   shared_ptr<DataSeries> m_spDataSeries; // introduced to satisfy Series method
};
