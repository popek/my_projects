#include "stdafx.h"
#include "StocksAnalyser.h"
#include "ChannelAlgorithParametersDlg.h"
#include "ParallelChannelFinder.h"
#include "NumericStringValidator.h"
#include "Misc.h"


IMPLEMENT_DYNAMIC(ChannelAlgorithParametersDlg, PropertyDialogBase)

ChannelAlgorithParametersDlg::ChannelAlgorithParametersDlg()
	: PropertyDialogBase(ChannelAlgorithParametersDlg::IDD)
   , m_spNumericStringValidator(new NumericStringValidator())
{
   m_algorithmType = m_context.AlgType();
   m_strPeriod = Misc::ToString(m_context.Period());
   m_strMinimumLinePointsAmount = Misc::ToString(m_context.MinLinePointsCount());
   m_strMaxDeviation2LineValueInPercents = Misc::ToString(m_context.MaxDev2LineValue()*100);
   m_strMaxDataSource2LineValueDistanceInPercents = Misc::ToString(m_context.MaxData2LineValueDistance()*100);

//    m_editMaxDev2Value.SetValidator(m_spNumericStringValidator);
//    m_editMaxData3LineDistance.SetValidator(m_spNumericStringValidator);
}
ChannelAlgorithParametersDlg::~ChannelAlgorithParametersDlg()
{
}

void ChannelAlgorithParametersDlg::DoDataExchange(CDataExchange* pDX)
{
   PropertyDialogBase::DoDataExchange(pDX);
   DDX_Radio(pDX, IDC_CHANNEL, m_algorithmType);

   DDX_Text(pDX, IDC_EDIT2, m_strPeriod);
   DDX_Control(pDX, IDC_EDIT_MAX_DEVIATION_TO_LINE_VALUE, m_editMaxDev2Value);
   DDX_Control(pDX, IDC_EDIT_MAX_DATA_TO_LINE_DISTANCE, m_editMaxData3LineDistance);
   DDX_Text(pDX, IDC_EDIT_MIN_POINTS_ON_LINE, m_strMinimumLinePointsAmount);
   DDX_Text(pDX, IDC_EDIT_MAX_DEVIATION_TO_LINE_VALUE, m_strMaxDeviation2LineValueInPercents);
   DDX_Text(pDX, IDC_EDIT_MAX_DATA_TO_LINE_DISTANCE, m_strMaxDataSource2LineValueDistanceInPercents);
   DDX_Control(pDX, IDC_CONFIRMED_SIGNAL, m_butShowConfirmedSignal);

   if(TRUE == pDX->m_bSaveAndValidate)
   {
      m_context.Update(Misc::ToInt(m_strPeriod),
         m_algorithmType, 
         Misc::ToInt(m_strMinimumLinePointsAmount),
         Misc::ToDouble(m_strMaxDeviation2LineValueInPercents)/100,
         Misc::ToDouble(m_strMaxDataSource2LineValueDistanceInPercents)/100,
         m_butShowConfirmedSignal.GetCheck() ? true : false);
   }
}


BEGIN_MESSAGE_MAP(ChannelAlgorithParametersDlg, PropertyDialogBase)
END_MESSAGE_MAP()


// ChannelAlgorithParametersDlg message handlers

const ChannelFinderContext& ChannelAlgorithParametersDlg::Context() const
{
   return m_context;
}
