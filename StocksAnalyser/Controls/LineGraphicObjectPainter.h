#pragma once
#include "GraphicObjectPainterBase.h"


class GraphicObjectBase;


class LineGraphicObjectPainter : public GraphicObjectPainterBase
{
public:
   LineGraphicObjectPainter(bool a_drawPoints);
   virtual ~LineGraphicObjectPainter(void);

   virtual void DrawObject(const GraphicObjectBase& a_object, CDC* a_pDC);

private:
   void DrawPoint(const CPoint& a_point, CDC* a_pDC, COLORREF a_color);

private:
   bool m_drawPoints;
};
