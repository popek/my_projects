// test_client.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#pragma comment(lib, "Ws2_32.lib")

#include <sdkddkver.h>
#include <WinSock2.h>
#include <iostream>
#include <string>

using namespace std;

void exitProgram(int code)
{
   system("PAUSE");
   exit(code);
}

int _tmain(int argc, _TCHAR* argv[])
{
   int res = 0;
   WSAData wsd;
   WORD dllVer = MAKEWORD(2, 1);

   if(WSAStartup(dllVer, &wsd))
   {
      cout << "failed to init socket lib";
      exitProgram(1);
   }

   SOCKADDR_IN address;
   address.sin_family = AF_INET;
   address.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
   address.sin_port = htons(444);

   SOCKET sock;
   sock = socket(AF_INET, SOCK_STREAM, NULL);
   if(INVALID_SOCKET == sock)
   {
      cout << "fail when creating client socket. Error code: " << WSAGetLastError() << endl;
      WSACleanup();
      exitProgram(1);
   }

   res = connect(sock, (SOCKADDR*)&address, sizeof(address));
   if(SOCKET_ERROR == res)
   {
      cout << "connection failed" << endl;
      WSACleanup();
      exitProgram(1);
   }

   char request[512];
   char response[512];
   int requestsCount = 7;
   while(requestsCount--)
   {
      sprintf_s(request, "this is the client request. Requests still to come count: %d", requestsCount); 

      res = send(sock, request, strlen(request), NULL);
      if(SOCKET_ERROR == res)
      {
         cout << "sending failed" << endl;
         continue;
      }
         
      res = recv(sock, response, sizeof(response), NULL);
      if(SOCKET_ERROR == res)
      {
         cout << "receive failed" << endl;
         continue;
      }
      response[res] = '\0';

      cout << "request:  " << request << endl
           << "response: " << response << endl;

      Sleep(2000);
   }
   cout << "closing after sending all requests" << endl;
   closesocket(sock);

   WSACleanup();
   
   exitProgram(0);
}

