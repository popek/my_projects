#include "StdAfx.h"
#include "BackgroundPainter.h"

BackgroundPainter::BackgroundPainter(void)
//: m_BackgroundColor(RGB(189, 194, 196))
: m_BackgroundColor(RGB(255, 255, 255))
{
}
BackgroundPainter::~BackgroundPainter(void)
{
}

void BackgroundPainter::Draw(CDC& a_dc, CRect& a_rcBackGroundArea)
{
   a_dc.FillSolidRect(&a_rcBackGroundArea, m_BackgroundColor);
}
