#include "StdAfx.h"
#include "OHLCElement.h"
#include "OHLCGraphicObject.h"
#include "Asset.h"
#include "TimeScaleContext.h"
#include "ValueScaleContext.h"


OHLCElement::OHLCElement(const Asset& a_dataSource)
: DataSeriesChartElement(EOHLC, a_dataSource.Size())
{
   m_openSeries.reserve(a_dataSource.Size());
   m_lowSeries.reserve(a_dataSource.Size());
   m_highSeries.reserve(a_dataSource.Size());

   for(size_t sampleIndex=0; sampleIndex<a_dataSource.Size(); ++sampleIndex)
   {
      const Kandle& kandle = a_dataSource[sampleIndex];

      AddPoint(kandle.Date(), kandle.Close());

      m_openSeries.push_back(kandle.Open());
      m_lowSeries.push_back(kandle.Low());
      m_highSeries.push_back(kandle.High());
   }
}
OHLCElement::~OHLCElement(void)
{
}

shared_ptr<GraphicObjectBase> OHLCElement::CreateGraphicObject(const TimeScaleContext& a_timeScaleContext, const ValueScaleContext& a_valueScaleContext) const
{
   size_t firstPointIndex = FindFirstIndex(a_timeScaleContext);
   size_t lastPointIndex  = FindLastIndex(a_timeScaleContext);

   shared_ptr<OHLCGraphicObject> spGraphicObject(new OHLCGraphicObject());

   spGraphicObject->Reserve(lastPointIndex - firstPointIndex + 1);

   size_t graphicPointIndex = 0;
   for(size_t pointIndex = firstPointIndex; pointIndex <= lastPointIndex; ++pointIndex, ++ graphicPointIndex)
   {
      spGraphicObject->UpdatePoint( graphicPointIndex, 
                                    a_timeScaleContext.ToCoordinate( m_points[pointIndex].Timestamp()), 
                                    a_valueScaleContext.ToCoordinate( m_points[pointIndex].Value()) );

      spGraphicObject->UpdateOHLCoordinates(graphicPointIndex, a_valueScaleContext.ToCoordinate( m_openSeries[pointIndex]),
                                                               a_valueScaleContext.ToCoordinate( m_highSeries[pointIndex]),
                                                               a_valueScaleContext.ToCoordinate( m_lowSeries[pointIndex]));
   }

   return spGraphicObject;
}
