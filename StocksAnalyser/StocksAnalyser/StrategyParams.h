#pragma once


class Kandle;


class StrategyParams
{
public:

   enum paramId
   {
      EMaxChange,
      EMaxDropDown,
      EValueStability,
      EMinTurnover
   };

public:
   StrategyParams(double a_maxChange, double a_maxDropDown, double a_valueStability, double a_minTurnover);
   ~StrategyParams(void);

   bool isParamFulfilled(paramId a_paramId, const Kandle& a_candle) const;
   bool isParamFulfilled(paramId a_paramId, const Kandle& a_canlde1, const Kandle& a_kandle2) const;

private:
   bool isParamFulfilledInternal(paramId a_paramId, const Kandle& a_candle) const;
   bool isParamFulfilledInternal(paramId a_paramId, const Kandle& a_canlde1, const Kandle& a_kandle2) const;

private:

   double _maxChange;            // maximum change (in percents) between kandle n and n+1
   double _maxDropDown;          // maximum loss between kandle n and n+1 (means: (C(n) - O(n+1))/C(n)
   double _valueStability;       // (H - L)/((C+O)/2)
   double _minTurnover;
};

