#include "StdAfx.h"
#include "AssetContainer.h"
#include "Asset.h"

AssetContainer::AssetContainer(void)
{
}
AssetContainer::~AssetContainer(void)
{
}

CString AssetContainer::Type2String(EAggregationType a_aggregationType)
{
   switch(a_aggregationType)
   {
   case eDay:
      return _T("(day)");

   case eWeek:
      return _T("(week)");

   case eMonth:
      return _T("(month)");

   default:
      ASSERT(FALSE);
      return _T("");
   }

}

void AssetContainer::LoadDaysAssetFromFile(LPCTSTR a_CSVfileName)
{
   shared_ptr<Asset> spDayAsset = shared_ptr<Asset>(new Asset());

   spDayAsset->ReadFromFile(a_CSVfileName);

   m_mapAssets[eDay] = spDayAsset;
}

shared_ptr<Asset> AssetContainer::operator[](const EAggregationType& a_aggregationType)
{
   TAssetsMap::iterator itAsset = m_mapAssets.find(a_aggregationType);

   if(itAsset != m_mapAssets.end())
   {
      return itAsset->second;
   }

   shared_ptr<Asset> spNewAssetType = CreateFromDayAsset(a_aggregationType);

   m_mapAssets[a_aggregationType] = spNewAssetType;

   return spNewAssetType;
}

shared_ptr<Asset> AssetContainer::CreateFromDayAsset(const EAggregationType& a_aggregationType)
{
   switch(a_aggregationType)
   {
   case eWeek:
      return DayAsset2WeekAsset();

   case eMonth:
      return DayAsset2MonthAsset();

   default:
      ASSERT(FALSE);
      return shared_ptr<Asset>();
   }
}

shared_ptr<Asset> AssetContainer::DayAsset2WeekAsset() const
{
   ASSERT(m_mapAssets.find(eDay)!= m_mapAssets.end());

   shared_ptr<Asset> spDayAsset = m_mapAssets.find(eDay)->second;

   shared_ptr<Asset> spWeekAsset = shared_ptr<Asset>(new Asset());

   size_t weekIdx = 0;
   spWeekAsset->AppendKandle( (*spDayAsset)[0].Clone(weekIdx) );

   for(size_t kandleIdx=1; kandleIdx<spDayAsset->Size(); ++kandleIdx)
   {
      Kandle& prevDayKandle = (*spDayAsset)[kandleIdx-1];
      Kandle& dayKandle     = (*spDayAsset)[kandleIdx];

      if(prevDayKandle.Date()->DaysOffset( *dayKandle.Date() ) < 7 &&
         dayKandle.Date()->ToWeekDay() > prevDayKandle.Date()->ToWeekDay())
      {
         (*spWeekAsset)[weekIdx].Concatenate(dayKandle);
      }
      else  // it's a new week
      {
         ++weekIdx;
         spWeekAsset->AppendKandle( dayKandle.Clone(weekIdx) );
      }
   }

   spWeekAsset->Loaded(true);
   
   // update label
   spWeekAsset->Label() = spDayAsset->Label();
   spWeekAsset->Label().AssetAggreagation(eWeek);

   return spWeekAsset;
}

shared_ptr<Asset> AssetContainer::DayAsset2MonthAsset() const
{
   ASSERT(m_mapAssets.find(eDay)!= m_mapAssets.end());

   shared_ptr<Asset> spDayAsset = m_mapAssets.find(eDay)->second;

   shared_ptr<Asset> spMonthAsset = shared_ptr<Asset>(new Asset());

   size_t monthIdx = 0;
   spMonthAsset->AppendKandle( (*spDayAsset)[0].Clone(monthIdx) );

   for(size_t kandleIdx=1; kandleIdx<spDayAsset->Size(); ++kandleIdx)
   {
      Kandle& prevDayKandle = (*spDayAsset)[kandleIdx-1];
      Kandle& dayKandle     = (*spDayAsset)[kandleIdx];

      if(prevDayKandle.Date()->Month() == dayKandle.Date()->Month())
      {
         (*spMonthAsset)[monthIdx].Concatenate(dayKandle);
      }
      else  // it's a new week
      {
         ++monthIdx;
         spMonthAsset->AppendKandle( dayKandle.Clone(monthIdx) );
      }
   }

   spMonthAsset->Loaded(true);

   // update label
   spMonthAsset->Label() = spDayAsset->Label();
   spMonthAsset->Label().AssetAggreagation(eMonth);

   return spMonthAsset;
}
