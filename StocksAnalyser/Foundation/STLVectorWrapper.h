#pragma once
#include "FoundationLib.h"
#include "IDoubleVector.h"


class FOUNDATION_LIB STLVectorWrapper : public IDoubleVector
{
public:
   STLVectorWrapper(vector<double>& a_vector);
   virtual ~STLVectorWrapper(void);

   virtual double operator [](size_t a_elementIndex) const;
   virtual size_t Size() const;

private:
   vector<double>& _vector;
};
