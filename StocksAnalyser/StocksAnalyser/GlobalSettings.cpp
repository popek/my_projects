#include "StdAfx.h"
#include "GlobalSettings.h"
#include "Misc.h"
#include "PathUtils.h"
#include "FoundationUtils.h"
#include "IXMLObjectsFactory.h"
#include "IXMLSerialiser.h"


const LPCTSTR STR_SETTINGS_FOLDER_NAME = _T("settings");
const LPCTSTR STR_STRATEGY_PARAMS_CONFIGFILE_NAME = _T("backtest_parameters.xml");


GlobalSettings* GlobalSettings::_instance = nullptr;


GlobalSettings::GlobalSettings(void)
{
   _databaseFolderPath = PathUtils::concatenateDirAndFile(Misc::GetBinFolder(), _T("DB"));
   _settingsFolder = PathUtils::concatenateDirAndFile(Misc::GetBinFolder(), STR_SETTINGS_FOLDER_NAME);

   CreateDirectory(_settingsFolder, nullptr);
   CreateDirectory(_databaseFolderPath, nullptr);
}
GlobalSettings::~GlobalSettings(void)
{
}

GlobalSettings* GlobalSettings::instance()
{
   if(!_instance)
   {
      _instance = new GlobalSettings();
   }

   return _instance;
}

void GlobalSettings::release()
{
   if(_instance)
   {
      delete _instance;
      _instance = nullptr;
   }
}

const CString& GlobalSettings::DBFolderPath() const
{
   return _databaseFolderPath;
}
