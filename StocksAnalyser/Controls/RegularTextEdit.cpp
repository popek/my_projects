#include "stdafx.h"
#include "RegularTextEdit.h"
#include "StringValidator.h"


set<TCHAR> RegularTextEdit::Sm_specialChars;

IMPLEMENT_DYNAMIC(RegularTextEdit, CEdit)

RegularTextEdit::RegularTextEdit()
{
   if(0 == Sm_specialChars.size())
   {
      Sm_specialChars.insert(VK_SHIFT);
      Sm_specialChars.insert(VK_HOME);
      Sm_specialChars.insert(VK_END);
      Sm_specialChars.insert(VK_LEFT);
      Sm_specialChars.insert(VK_RIGHT);
      Sm_specialChars.insert(VK_UP);
      Sm_specialChars.insert(VK_DOWN);
      Sm_specialChars.insert(VK_CONTROL);
      Sm_specialChars.insert(VK_ACCEPT);
   }
}
RegularTextEdit::~RegularTextEdit()
{
}

void RegularTextEdit::SetValidator(shared_ptr<StringValidator> a_spValidator)
{
   m_spStringValidator = a_spValidator;
}

bool RegularTextEdit::SpecialChar(const TCHAR a_char) const
{
   return Sm_specialChars.find(a_char) != Sm_specialChars.end();
}

CString RegularTextEdit::GetClipboardText() const
{
   CString	strText;

   if (::IsClipboardFormatAvailable(CF_TEXT))
   {
      if (::OpenClipboard(m_hWnd))
      {
         HANDLE	hClipBrdData = NULL;
         if ((hClipBrdData = ::GetClipboardData(CF_TEXT)) != NULL)
         {
            LPTSTR	lpClipBrdText = (LPTSTR)::GlobalLock(hClipBrdData);
            if (lpClipBrdText)
            {
               strText = lpClipBrdText;
               ::GlobalUnlock(hClipBrdData);
            }
         }

         VERIFY(::CloseClipboard());
      }
   }

   return strText;
}

CString RegularTextEdit::TextAfterTextInsert(CString a_textToAdd)
{
   CString currentText;
   GetWindowText(currentText);

   int startSel = 0;
   int endSel   = 0;
   GetSel(startSel, endSel);

   CString resultingText = currentText.Mid(0, startSel);
   resultingText.Append(a_textToAdd);
   resultingText.Append(currentText.Mid(endSel + 1, currentText.GetLength() - endSel));

   return resultingText;
}

CString RegularTextEdit::TextAfterCharInsert(TCHAR a_char) const
{
   CString resultingText;
   CString currentText;
   GetWindowText(currentText);

   int startSel = 0;
   int endSel   = 0;
   GetSel(startSel, endSel);

   if(VK_DELETE == a_char)
   {
      int endOffset = startSel == endSel ? 1 : 0;

      resultingText = currentText.Mid(0, startSel);
      resultingText.Append(currentText.Mid(endSel+endOffset, currentText.GetLength() - endSel));
   }
   else if(VK_BACK == a_char)
   {
      int startOffset = startSel == endSel ? -1 : 0;

      resultingText = currentText.Mid(0, startSel + startOffset);
      resultingText.Append(currentText.Mid(endSel, currentText.GetLength() - endSel));
   }
   else
   {
      resultingText = currentText.Mid(0, startSel);
      resultingText.AppendChar(a_char);
      resultingText.Append(currentText.Mid(endSel + 1, currentText.GetLength() - endSel));
   }

   return resultingText;
}

BOOL RegularTextEdit::PreTranslateMessage(MSG* a_pMSG)
{
   if( NULL == m_spStringValidator.get() || WM_KEYDOWN != a_pMSG->message )
   {
      return __super::PreTranslateMessage(a_pMSG);
   }

   UINT	keyCode = a_pMSG->wParam;

   if ( SpecialChar((TCHAR)keyCode) || ((keyCode == _T('C') || keyCode == _T('V')) && (::GetKeyState(VK_CONTROL) & 0x8000)) )
   {
      return __super::PreTranslateMessage(a_pMSG);
   }

   if( keyCode == _T('X') && (::GetKeyState(VK_CONTROL) & 0x8000) )
   {
      keyCode = VK_DELETE;
   }

   if(false == m_spStringValidator->ProperLeftSubstring( TextAfterCharInsert((TCHAR)keyCode)) )
   {
      return TRUE;   
   }

   return __super::PreTranslateMessage(a_pMSG);
}


BEGIN_MESSAGE_MAP(RegularTextEdit, CEdit)
   ON_MESSAGE(WM_PASTE, &RegularTextEdit::OnPaste)
   ON_WM_CHAR()
END_MESSAGE_MAP()


LRESULT RegularTextEdit::OnPaste(WPARAM , LPARAM )
{
   if(NULL == m_spStringValidator.get() || 
      true == m_spStringValidator->ProperLeftSubstring( TextAfterTextInsert(GetClipboardText())))
   {
      return __super::Default();
   }

   return 0;
}


void RegularTextEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
//    if(NULL != m_spStringValidator.get() && false == m_spStringValidator->ProperLeftSubstring( TextAfterCharInsert((TCHAR)nChar)) )
//    {
//       return;   
//    }

   __super::OnChar(nChar, nRepCnt, nFlags);
}