#pragma once
#include "FoundationLib.h"
#include "IXMLSerialiser.h"

class FOUNDATION_LIB XercescXMLSerializer : public IXMLSerialiser
{
public:
   XercescXMLSerializer(void);
   virtual ~XercescXMLSerializer(void);

   virtual bool serialise(LPCTSTR outputFilePath, const XMLNode& node);

   virtual shared_ptr<XMLNode> unserialise(LPCTSTR inputFilePath);
};

