#pragma once
#include "IPointer.h"


class CONTROLS_LIB MIN_pointer : public IPointer
{
public:
   MIN_pointer(size_t period);
   virtual ~MIN_pointer(void);

   virtual void   Create(const DataSeries& inputData);

   virtual const DataSeries& Series(size_t a_seriesId) const;

private:
   shared_ptr<DataSeries> _spDataSeries;
   size_t _period;
};

