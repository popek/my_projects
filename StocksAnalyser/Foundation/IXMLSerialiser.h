#pragma once
#include "FoundationLib.h"


class XMLNode;


class FOUNDATION_LIB IXMLSerialiser
{
public:
   virtual ~IXMLSerialiser(void);

   virtual bool serialise(LPCTSTR outputFilePath, const XMLNode& node) = 0;

   virtual shared_ptr<XMLNode> unserialise(LPCTSTR inputFilePath) = 0;
};

