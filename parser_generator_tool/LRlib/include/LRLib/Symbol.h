#pragma once
#include <string>

#define INVALID_SYMBOL_ID (-666)

namespace lrlib
{

class Symbol
{
public:

   Symbol(void);
   Symbol(std::string, int, bool, int);
   virtual ~Symbol(void);

   bool operator < (const Symbol&) const;
   bool operator == (const Symbol&) const;
   bool operator != (const Symbol&) const;

   std::string name() const;
   bool term() const;

   int id() const;
   void id(int a_nID);

   void used(bool flag);
   bool used();

   void defined(bool flag);
   bool defined();

   int  lineNo();
   void lineNo(int a_nLineNo);

   void rootSymbol(bool a_bValue);
   bool rootSymbol();

protected:
   void Init(std::string, int, bool, int);

protected:
   std::string m_strName;
   int  m_iID;
   bool m_bTerm;
   bool m_bUsed;
   bool m_bDefined;

   bool m_bGrammarRootSymbol;

   int  m_nLineNo;              // line number in which symbol definition appeared first time
};

} // namespace lrlib