#include "GrammarFileParser/Scanner.h"
#include <stack>

#define ERROR_STRING_LENGTH   256
#define _LEX_PREBUFFERSIZE 1024

namespace lrlib
{

static int LexDFA[22][13] =
{
   //	-9	-8	-7	-6	-5	-4	-3	-2	-1	44	45	62	95
   4,	2,	3,	1,	5,	8,	12,	-1,	10,	6,	7,	9,	11,
   -1,	-1,	-1,	13,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	14,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	-1,	-1,	15,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	16,	-1,
   -1,	-1,	-1,	-1,	17,	18,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	19,	20,	-1,	-1,	-1,	21,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	19,	20,	-1,	-1,	-1,	21,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	13,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,	-1,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	19,	20,	-1,	-1,	-1,	21,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	19,	20,	-1,	-1,	-1,	21,
   -1,	-1,	-1,	-1,	-1,	-1,	-1,	19,	20,	-1,	-1,	-1,	21,
};

static short Char2ColID[256] =
{
   -1, -1, -1, -1, -1, -1, -1, -1, -1, 
   3, 1, -1, -1, 2, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, 3, 
   0, 0, 0, 0, 0, 0, 0, 0, 
   0, 4, 0, 9, 10, 0, 5, 7, 
   7, 7, 7, 7, 7, 7, 7, 7, 
   7, 0, 0, 0, 0, 11, 0, 0, 
   8, 8, 8, 8, 8, 8, 8, 8, 
   8, 8, 8, 8, 8, 8, 8, 8, 
   8, 8, 8, 8, 8, 8, 8, 8, 
   8, 8, 0, 0, 0, 0, 12, -1, 
   8, 8, 8, 8, 8, 8, 8, 8, 
   8, 8, 8, 8, 8, 8, 8, 8, 
   8, 8, 8, 8, 8, 8, 8, 8, 
   8, 8, 0, 6, 0, 0, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, -1, 
   -1, -1, -1, -1, -1, -1, -1, 
};

static int AcceptedStates[22] =
{
   -1, e_blank, e_eol, -1, e_single_char_token, e_single_char_token, e_single_char_token, e_single_char_token, e_single_char_token, 
   e_single_char_token, e_identifier, e_identifier, e_or, e_blank, e_eol, e_multi_line_comment_finish, e_eval_to, 
   e_multi_line_comment_start, e_single_line_comment_start, e_identifier, e_identifier, e_identifier, 
};


Scanner::Scanner()
   : _bInMultiLineComments(false)
   , _bInSingleLineComments(false)
   , _bAuxReadingFileAllowed(true)
   , _pszErrDesc(NULL)
{
}

Scanner::~Scanner()
{
   Cleanup();
}

bool Scanner::Init(const char* pszFilePath)
{
   _pszErrDesc = new char[ERROR_STRING_LENGTH];
   _InBuffer[0] = new char[_LEX_PREBUFFERSIZE];
   _InBuffer[1] = new char[_LEX_PREBUFFERSIZE];
   _bEOF = false;

   fopen_s(&_InputFile, pszFilePath, "r");

   if (NULL == _InBuffer[0] || NULL == _InBuffer[1] || NULL == _pszErrDesc || NULL == _InputFile)
   {
      strcpy(_pszErrDesc, "could not initialize lexical analyzer");
      return false;
   }

   _pszErrDesc[0] = '\0';
   _uiBytesRed[0] = fread(_InBuffer[0], 1, _LEX_PREBUFFERSIZE, _InputFile);

   _uiCurrBuff = 0;
   _uiCurrPos = 0;
   _uiAuxLineNo = 0;


   return true;
}

unsigned int Scanner::LineNo()
{
   return _uiAuxLineNo;
}

void Scanner::ShowError()
{
   if (NULL != _pszErrDesc && '\0' != _pszErrDesc[0])
   {
      printf("lexical error: %s\b", _pszErrDesc);
   }
}

eTermID Scanner::getAtom(LexAtom* pLexAtom)
{
   int iState = 0;
   unsigned char ucChar = 0;
   int           nAuxCnt = 0;
   int           nCharRed = 0;
   std::stack<int>    Steps;

   // check if end of file
   if (_bEOF)
   {
      pLexAtom->m_strText = "";
      pLexAtom->m_ID = e_eof;
      return e_eof;
   }

   while (iState > -1)
   {
      Steps.push(iState);
      ucChar = readNextChar();

      _szAuxAtomName[nAuxCnt] = ucChar; // save atom name
      nAuxCnt++;

      if (Char2ColID[ucChar] > -1)
      {
         iState = LexDFA[iState][Char2ColID[ucChar]];
      }
      else
      {
         if (_bEOF)
         {
            break;
         }
         else
         {
            if (false == inCommentBlock())
            {
               sprintf_s(_pszErrDesc, ERROR_STRING_LENGTH, "character '%c'(0x%x) is forbidden", ucChar, ucChar);
            }

            return e_error;
         }
      }
   }

   nCharRed = nAuxCnt;

   //remember last atom text (in case atom will be invalid)
   pLexAtom->m_strText = _szAuxAtomName;

   while (Steps.size())
   {
      writeBackChar();
      nAuxCnt--;
      _szAuxAtomName[nAuxCnt] = '\0';

      iState = Steps.top();
      Steps.pop();

      if (AcceptedStates[iState] > -1)
      {
         pLexAtom->m_strText = _szAuxAtomName;
         pLexAtom->m_ID = (eTermID)AcceptedStates[iState];
         handleCommentsState(pLexAtom);
         return pLexAtom->m_ID;
      }

   }

   if (true == inCommentBlock())
   {
      while (nCharRed)
      {
         readNextChar();
         --nCharRed;
      }
   }
   else
   {
      sprintf_s(_pszErrDesc, ERROR_STRING_LENGTH, "invalid token ('%s')", pLexAtom->m_strText.c_str());
   }

   return e_error;
}

void Scanner::recognizeSingleChar(LexAtom* a_pSingleCharToken)
{
   if (';' == a_pSingleCharToken->m_strText[0])
   {
      a_pSingleCharToken->m_ID = e_semicolon;         // if single char is semicolon, then it have to be marked as semicolon token
   }
}

void Scanner::handleCommentsState(LexAtom* a_pLexToken)
{
   switch (a_pLexToken->m_ID)
   {
   case e_single_line_comment_start:
      if (false == _bInMultiLineComments)
      {
         _bInSingleLineComments = true;
      }
      break;

   case e_eol:
      _bInSingleLineComments = false;
      break;

   case e_multi_line_comment_start:
      if (false == _bInSingleLineComments)
      {
         _bInMultiLineComments = true;
      }
      break;

   case e_multi_line_comment_finish:
      if (false == _bInSingleLineComments)
      {
         if (false == _bInMultiLineComments)
         {
            sprintf_s(_pszErrDesc, ERROR_STRING_LENGTH, "comments block had not been started");
            a_pLexToken->m_ID = e_error;
         }
         else
         {
            _bInMultiLineComments = false;
            getAtom(a_pLexToken);            // calling getAtom second time to ommit end multiline comment token
         }

      }
      break;
   }
}

void Scanner::GetAtom(LexAtom* pLexAtom)
{
   // filtering blanks, end of line, and comments
   while (((pLexAtom->m_ID = getAtom(pLexAtom)) == e_eol) || (pLexAtom->m_ID == e_blank) || inCommentBlock())
   {
      if (e_eof == pLexAtom->m_ID && true == _bInMultiLineComments)
      {
         strcpy(_pszErrDesc, "comment block wasn't closed");
         pLexAtom->m_ID = e_error;
         return;
      }

      if (pLexAtom->m_ID == e_eol)
      {
         _uiAuxLineNo++;
      }
   }
   pLexAtom->m_iLine = _uiAuxLineNo;

   if (e_single_char_token == pLexAtom->m_ID)
   {
      recognizeSingleChar(pLexAtom);
   }
}

unsigned char Scanner::readNextChar()
{
   unsigned char ret = 0;
   if (!_bEOF)
   {
      if (_uiCurrPos == _uiBytesRed[_uiCurrBuff])//if buffer boundry is reached
      {
         if (true == _bAuxReadingFileAllowed)
         {
            _uiBytesRed[_uiCurrBuff ? 0 : 1] = fread(_uiCurrBuff ? _InBuffer[0] : _InBuffer[1], 1, _LEX_PREBUFFERSIZE, _InputFile);
         }
         else
         {
            _bAuxReadingFileAllowed = true; // just toggle auxiliary flag
         }

         _uiCurrBuff = _uiCurrBuff ? 0 : 1;           // change current buffer
         _uiCurrPos = 0;                              // set current position to 0
         if (_uiBytesRed[_uiCurrBuff] == 0)            // if buffer is empty it means there is nothing to read -> whole file had been red
         {
            fclose(_InputFile);
            _bEOF = true;
            return ret;
         }
      }

      ret = _InBuffer[_uiCurrBuff][_uiCurrPos];       // get char 

      ++_uiCurrPos;                                   // increment counter
   }

   return ret;
}

void Scanner::writeBackChar()
{
   if (_uiCurrPos)
   {
      _uiCurrPos--;
   }
   else
   {
      //_bEOF = false;// in case eof was reached
      _uiCurrBuff = _uiCurrBuff ? 0 : 1;           // change current buffer
      _uiCurrPos = _uiBytesRed[_uiCurrBuff] - 1;    //point to last character in previously used buffer
      _bAuxReadingFileAllowed = false;                          // set auxiliary flag so file will not be read second time
   }
}

bool Scanner::inCommentBlock()
{
   return (true == _bInSingleLineComments || true == _bInMultiLineComments);
}

void Scanner::Cleanup()
{
   if (_InBuffer[0])
   {
      delete[] _InBuffer[0];
      _InBuffer[0] = NULL;
   }

   if (_InBuffer[1])
   {
      delete[] _InBuffer[1];
      _InBuffer[1] = NULL;
   }

   if (_pszErrDesc)
   {
      delete[] _pszErrDesc;
      _pszErrDesc = NULL;
   }
}

} // namespace lrlib