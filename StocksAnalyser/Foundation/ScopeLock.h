#pragma once
#include "CriticalSection.h"
#include "FoundationLib.h"

class FOUNDATION_LIB ScopeLock
{
public:
   ScopeLock(CriticalSection& a_criticalSection);
   virtual ~ScopeLock(void);

protected:
   CriticalSection&   m_criticalSection;
};



#define CONCAT__(x, y) x##y
#define CONCAT_(x, y) CONCAT__(x, y)
#define LOCK_SCOPE(cs)  ScopeLock CONCAT_(__sl__,__LINE__)(cs)

