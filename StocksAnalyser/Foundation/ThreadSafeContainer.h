#pragma once
#include "ScopeLock.h"

template<class _DATA>
class ThreadSafeContainer
{
public:
   ThreadSafeContainer(void)
   {
   }

   virtual ~ThreadSafeContainer(void)
   {
      Release();
   }

   void   AddTail(_DATA& a_newData)
   {
      LOCK_SCOPE(m_csLock);
      m_deqDataContainer.push_back(a_newData);
   }

   _DATA GetFront()
   {
      LOCK_SCOPE(m_csLock);
      return m_deqDataContainer.front();
   }
   
   void ReleaseFront()
   {
      LOCK_SCOPE(m_csLock);
      m_deqDataContainer.pop_front();
   }

   void   Release()
   {
      LOCK_SCOPE(m_csLock);
      m_deqDataContainer.clear();
   }

   size_t Size() const
   {
      LOCK_SCOPE(m_csLock);
      return m_deqDataContainer.size();
   }

protected:
   mutable CriticalSection   m_csLock;
   deque<_DATA>      m_deqDataContainer;
};