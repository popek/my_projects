#pragma once


class KandleDate;
class SingleMarketEntryDetails;
class TransactionDetails;


class StrategyResult
{
public:
   class ISubIncomeObserver
   {
   public:
      virtual ~ISubIncomeObserver(){}

      virtual void Notify(double a_bankrollAfterTrade) = 0;
   };

public:
   StrategyResult();
   virtual ~StrategyResult(void);

   void RegisterEntry(shared_ptr<TransactionDetails> a_spBuy, 
                      shared_ptr<TransactionDetails> a_spSell);

   double Income(const double& a_initialBankroll, ISubIncomeObserver* a_pSingleEntriesObserver) const;

   bool EntriesCount() const;

private:
   vector<shared_ptr<SingleMarketEntryDetails>> _marketEntries;
   double _brokerFee;      // value 0.0038 means 0.38%
};
