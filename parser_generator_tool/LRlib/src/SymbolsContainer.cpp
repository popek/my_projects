#include "SymbolsContainer.h"
#include "Utils.h"

namespace lrlib
{

void SymbolsContainer::Insert(Symbol& symbol)
{
   auto iterator_and_insertionDone =
      m_setSymbols.insert(std::make_pair(SymbolUniqueKey(symbol), symbol));
   if (iterator_and_insertionDone.second)
   {
      auto symbolPtr = Utils::SymbolPtr(iterator_and_insertionDone.first);
      m_mapName2Symbol[symbolPtr->name()] = symbolPtr;
   }
}

size_t SymbolsContainer::Size()
{
   return m_setSymbols.size();
}

Symbol* SymbolsContainer::FindSymbol(std::string a_strSymbolName)
{
   auto itName2Symbol = m_mapName2Symbol.find(a_strSymbolName);

   return itName2Symbol != m_mapName2Symbol.end() ? itName2Symbol->second : NULL;
}

Symbol* SymbolsContainer::FindSymbol(int a_nSymbolID)
{
   auto itID2Symbol = m_mapID2Symbol.find(a_nSymbolID);

   return itID2Symbol != m_mapID2Symbol.end() ? itID2Symbol->second : NULL;
}

void SymbolsContainer::GenerateTerminalsIDs(int a_nFirstID)
{
   t_setSymbol::iterator itSymbol = m_setSymbols.begin();

   while (itSymbol != m_setSymbols.end())
   {
      if (true == Utils::SymbolPtr(itSymbol)->term())
      {
         Utils::SymbolPtr(itSymbol)->id(a_nFirstID);
         ++a_nFirstID;
      }
      ++itSymbol;
   }
}
t_setSymbol::iterator SymbolsContainer::Begin()
{
   return m_setSymbols.begin();
}

t_setSymbol::iterator SymbolsContainer::End()
{
   return m_setSymbols.end();
}

t_setSymbol& SymbolsContainer::symbols()
{
   return m_setSymbols;
}

} // namespace lrlib