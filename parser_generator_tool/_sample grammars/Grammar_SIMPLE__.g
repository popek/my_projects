;
; TASL GRAMMAR
; ------------------------
;    Author: Tomasz Bielak
;

; script definition

;		SCRIPT -> ASSIGN_STMT
;			| SCRIPT ASSIGN_STMT    

		;BLOCK -> ASSIGN_STMT

; assignment statement

;		ASSIGN_STMT -> ASSIGN_ID EXPR

;		ASSIGN_ID -> IDENTIFIER =

; arithmetic/string expressions

;		EXPR -> ALL_NUMBERS
;			| string
;			| IDENTIFIER

;		ALL_NUMBERS -> int_number
;			| float_number


;		IDENTIFIER -> identifier
;			| IDENTIFIER . identifier
;			| IDENTIFIER [ EXPR ]
		
; eof


S -> L = R | R
L -> * R | a
R -> L
