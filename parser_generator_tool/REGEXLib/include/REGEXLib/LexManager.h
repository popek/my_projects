#pragma once
#include "LexFileReader.h"
#include "RegExManager.h"
#include "CodeGenerator.h"
#include "CommonDefs.h"

namespace regexlib
{
class LexManager : public RegExManager, public LexFileReader
{
public:
   LexManager(eRegExModel modelType);
   virtual ~LexManager(void);
   bool  CreateLexer(const char* LexFile);

   short	SymbolName2SymbolID(std::string strSymName);
   short Char2SymbolID(char cChar);
   bool GenerateCode(const char* path);

protected:
   std::map<unsigned char, short>& GetChar2SymbolIDMap();

private:
   void BuildRegExFromDefinition(std::vector<std::string>& vecWords, std::vector<RegEx*>& vecRegEx, unsigned int& uiPos);
   CodeGenerator* m_pCodeGen;
};

} // namespace regexlib