#pragma once
#include "GrxStyle.h"


class CONTROLS_LIB LineStyle : public GrxStyle
{
public:
   enum EParam
   {
      EStyle,
      EWidth,
      EColor,
      EPointColor
   };

public:
   LineStyle(void);
   virtual ~LineStyle(void);

   virtual LONG_PTR GetNumericParam(paramId_t a_paramId) const;

   virtual void SetNumericParam(paramId_t a_paramId, LONG_PTR a_value);

private:
   int _style;    // dotted, solid etc
   int _width;
   COLORREF _lineColor;
   COLORREF _pointColor;
};
