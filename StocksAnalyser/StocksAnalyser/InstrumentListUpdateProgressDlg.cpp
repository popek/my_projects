#include "stdafx.h"
#include "StocksAnalyser.h"
#include "InstrumentListUpdateProgressDlg.h"
#include "UIStrings.h"
#include "AssetsListDownloader.h"

const UINT G_SetRangeMsg        = WM_USER + 1;
const UINT G_UpdateProgressMsg  = WM_USER + 2;

const UINT G_UnzippingStartedMsg   = WM_USER + 3;
const UINT G_UnzippingFinishedMsg  = WM_USER + 4;
const UINT G_ErrorDuringUpdate     = WM_USER + 5;

const UINT G_UpdateCanceled        = WM_USER + 6;

const UINT G_SetZippedFilesCountMsg = WM_USER + 7;
const UINT G_NextFileUnzippedMsg = WM_USER + 8;

InstrumentListUpdateProgressDlg::ProgressDescription::ProgressDescription(CString a_description, int a_progressBarPos)
{
   _description      = a_description;
   _numeric   = a_progressBarPos;
}

IMPLEMENT_DYNAMIC(InstrumentListUpdateProgressDlg, CDialog)
InstrumentListUpdateProgressDlg::InstrumentListUpdateProgressDlg(CWnd* pParent)
	: CDialog(InstrumentListUpdateProgressDlg::IDD, pParent)
   , m_zipFileSize(0)
   , m_zippedFilesCount(0)
{
}
InstrumentListUpdateProgressDlg::~InstrumentListUpdateProgressDlg()
{
}

void InstrumentListUpdateProgressDlg::SetRange(int a_assetListSize)
{
   m_instrumentListUpdateProgressData.AddTail(shared_ptr<ProgressDescription>
                                              (new ProgressDescription(_T(""), a_assetListSize)) );
   PostMessage(G_SetRangeMsg, 0, 0);
}

void InstrumentListUpdateProgressDlg::SetZippedFilesCount(int a_assetsCount)
{
   m_instrumentListUpdateProgressData.AddTail(shared_ptr<ProgressDescription>
                                              (new ProgressDescription(_T(""), a_assetsCount)) );
   PostMessage(G_SetZippedFilesCountMsg, 0, 0);
}

void InstrumentListUpdateProgressDlg::NextFileUnzippedNotification(CString a_strFileName)
{
   m_instrumentListUpdateProgressData.AddTail(shared_ptr<ProgressDescription>
                                             (new ProgressDescription(a_strFileName, -1)) );
   PostMessage(G_NextFileUnzippedMsg, 0, 0);
}

void InstrumentListUpdateProgressDlg::OnDataDownloaded(int a_totalDataDownloaded)
{
   m_instrumentListUpdateProgressData.AddTail(shared_ptr<ProgressDescription>
      (new ProgressDescription(_T(""), a_totalDataDownloaded)) );

   PostMessage(G_UpdateProgressMsg, 0, 0);
}

void InstrumentListUpdateProgressDlg::UnzippingStarted()
{
   PostMessage(G_UnzippingStartedMsg);
}

void InstrumentListUpdateProgressDlg::UnzippingFinished()
{
   PostMessage(G_UnzippingFinishedMsg);
}

void InstrumentListUpdateProgressDlg::UpdateCanceled()
{
   PostMessage(G_UpdateCanceled, 0, 0);
}

void InstrumentListUpdateProgressDlg::NotifyError(CString a_strErrorDesc)
{
   m_instrumentListUpdateProgressData.AddTail(shared_ptr<ProgressDescription>
                                             (new ProgressDescription(a_strErrorDesc, 0)) );

   PostMessage(G_ErrorDuringUpdate, 0, 0);
}

void InstrumentListUpdateProgressDlg::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_INSTRUMENT_LIST_UPDATE_PROGRESS, m_progressBar);
   DDX_Control(pDX, IDC_INSTRUMENTS_LIST_UPDATE_DESCRIPTION, m_progressDescription);
}

LRESULT InstrumentListUpdateProgressDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{

   switch(message)
   {
      case G_SetRangeMsg:
         {
            shared_ptr<ProgressDescription> spDesc = m_instrumentListUpdateProgressData.GetFront();
            m_instrumentListUpdateProgressData.ReleaseFront();

            m_zipFileSize = spDesc->_numeric;

            GetDlgItem(IDOK)->EnableWindow(FALSE);
            GetDlgItem(IDCANCEL)->EnableWindow(TRUE);

            m_progressBar.SetRange32(0, spDesc->_numeric);
            m_progressBar.SetPos(0);

            CString strDesc;
            strDesc.Format(STR_INSTRUMENT_LIST_DOWNLOAD_STATE, 0, m_zipFileSize);
            m_progressDescription.SetWindowText(strDesc);

            return 0;
         }
      case G_UpdateProgressMsg:
         {
            shared_ptr<ProgressDescription> spDesc = m_instrumentListUpdateProgressData.GetFront();
            m_instrumentListUpdateProgressData.ReleaseFront();

            m_progressBar.SetPos(spDesc->_numeric);

            CString strDesc;
            strDesc.Format(STR_INSTRUMENT_LIST_DOWNLOAD_STATE, spDesc->_numeric, m_zipFileSize);
            m_progressDescription.SetWindowText(strDesc);

            return 0;
         }
      case G_UnzippingStartedMsg:
         m_progressDescription.SetWindowText(STR_INSTRUMENT_LIST_UNZIPPING);
         GetDlgItem(IDCANCEL)->EnableWindow(FALSE); //  cancel is not available during unzipping

         return 0;

      case G_UnzippingFinishedMsg:
         m_progressDescription.SetWindowText(STR_INSTRUMENT_LIST_UPDATE_SUCCESS);
         GetDlgItem(IDOK)->EnableWindow(TRUE);
         return 0;

      case G_ErrorDuringUpdate:
         {
            shared_ptr<ProgressDescription> spDesc = m_instrumentListUpdateProgressData.GetFront();
            m_instrumentListUpdateProgressData.ReleaseFront();

            CString strErroDesc;
            strErroDesc.Format(STR_INSTRUMENT_LIST_UPDATE_ERROR, spDesc->_description);
            m_progressDescription.SetWindowText(strErroDesc);
            GetDlgItem(IDOK)->EnableWindow(TRUE);
            return 0;
         }

      case G_UpdateCanceled:
         m_progressDescription.SetWindowText(STR_INSTRUMENT_LIST_UPDATE_CANCELED);
         GetDlgItem(IDOK)->EnableWindow(TRUE);
         GetDlgItem(IDCANCEL)->EnableWindow(FALSE);
         return 0;

      case G_SetZippedFilesCountMsg:
         {
            shared_ptr<ProgressDescription> spDesc = m_instrumentListUpdateProgressData.GetFront();
            m_instrumentListUpdateProgressData.ReleaseFront();

            GetDlgItem(IDOK)->EnableWindow(FALSE);
            GetDlgItem(IDCANCEL)->EnableWindow(TRUE);

            m_zippedFilesCount = spDesc->_numeric;
            m_progressBar.SetRange32(0, m_zippedFilesCount);
            m_progressBar.SetPos(0);
         }
         return 0;

      case G_NextFileUnzippedMsg:
         {
            shared_ptr<ProgressDescription> spDesc = m_instrumentListUpdateProgressData.GetFront();
            m_instrumentListUpdateProgressData.ReleaseFront();

            int nextPos = m_progressBar.GetPos() + 1;

            CString strDesc;
            strDesc.Format(STR_INSTRUMENT_UNZIPPING_NEXT_INSTRUMENT, nextPos, m_zippedFilesCount, spDesc->_description);
            m_progressDescription.SetWindowText(strDesc);

            m_progressBar.SetPos(nextPos);
         }
         return 0;
   }

   return __super::WindowProc(message, wParam, lParam);
}


BEGIN_MESSAGE_MAP(InstrumentListUpdateProgressDlg, CDialog)
   ON_BN_CLICKED(IDCANCEL, &InstrumentListUpdateProgressDlg::OnBnClickedCancel)
   ON_BN_CLICKED(IDOK, &InstrumentListUpdateProgressDlg::OnBnClickedOk)
END_MESSAGE_MAP()



BOOL InstrumentListUpdateProgressDlg::OnInitDialog()
{
   __super::OnInitDialog();

   GetDlgItem(IDOK)->EnableWindow(FALSE);
   GetDlgItem(IDCANCEL)->EnableWindow(FALSE);

   m_spAssetsListUpdater = make_shared<AssetsListDownloader>(this);

   m_spAssetsListUpdater->UpdateAssetsList();

   return TRUE;
}

void InstrumentListUpdateProgressDlg::OnBnClickedCancel()
{
   m_spAssetsListUpdater->CancelUpdateProcess();
}

void InstrumentListUpdateProgressDlg::OnBnClickedOk()
{
   OnOK();
}
