#pragma once
#include "ETerminalId.h"
#include "typeDefs.h"


class LexAtom
{
public:
   LexAtom(void);
   virtual ~LexAtom(void);

   int         line() const;
   ETerminalId id() const;
   string_t    text() const;
   double      doubleValue() const;
   int         intValue() const;

   void line(int a_line);
   void id(ETerminalId a_id);
   void text(const string_t& a_text);
   void doubleValue(double a_value);
   void intValue(int a_value);

protected:
  int         _line;
  ETerminalId _id;
  string_t    _text;
  double      _dValue;
  int         _iValue;
};
