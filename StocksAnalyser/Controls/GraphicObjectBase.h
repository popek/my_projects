#pragma once
#include "ControlsLib.h"
#include "Point2D.h"
#include "GrxStyle.h"


class GraphicObjectPainterBase;


class CONTROLS_LIB GraphicObjectBase
{
   NONCOPYABLE_TYPE(GraphicObjectBase)

public:
   GraphicObjectBase();
   GraphicObjectBase(shared_ptr<GraphicObjectPainterBase> a_spPainter);
   virtual ~GraphicObjectBase(void);

   virtual void Draw(CDC* a_pDC);

   virtual void Reserve(size_t a_pointsCount);

   void UpdatePoint(size_t a_pointIndex, Coordinate a_xCoordinate, Coordinate a_yCoordinate);

   size_t PointsCount() const;

   const Point2D& GetAt(size_t a_pointIndex) const;

   void Style(shared_ptr<GrxStyle> a_style);
   shared_ptr<GrxStyle> Style() const;
   
protected:
   vector<Point2D> m_objectPoints;

   shared_ptr<GraphicObjectPainterBase> m_spPainter;

   shared_ptr<GrxStyle> _grxStyle;
};
