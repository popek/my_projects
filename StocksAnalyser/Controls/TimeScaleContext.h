#pragma once
#include "controlsLibTypes.h"
#include "ControlsLib.h"


class KandleDate;
class Asset;


class CONTROLS_LIB TimeScaleContext
{
   NONCOPYABLE_TYPE(TimeScaleContext)

public:
   TimeScaleContext(void);
   virtual ~TimeScaleContext(void);

   void InitiateTimestampsContainer(const Asset& a_assetContainingTimestamps);

   void UpdateCoordinates(double a_rangeStart, double a_rangeEnd, Coordinate a_availableWidth);


   Coordinate ToCoordinate(shared_ptr<KandleDate>& a_spDate) const;

   Coordinate GetCoordinate(size_t a_timestampIndex) const;

   shared_ptr<KandleDate> GetTimestamp(size_t a_timestampIndex) const;

   size_t FirstVisibleTimestampIndex() const;

   size_t LastVisibleTimestampIndex() const;

   Coordinate NeighbourDistance() const;

protected:

   vector<shared_ptr<KandleDate>>  m_datesContainer;
   vector<Coordinate>              m_datesCoordinatesContainer; 

   size_t                          m_1stVisibleTimestampIndex;
   size_t                          m_lastVisibleTimestampIndex;
};
