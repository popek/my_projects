#include "StdAfx.h"
#include "MarketInfo.h"
#include "XMLNode.h"
#include "FoundationUtils.h"
#include "IXMLSerialiser.h"


const LPCTSTR STR_ROOT = _T("MarketInfo");
const LPCTSTR STR_WIG20 = _T("WIG20");
const LPCTSTR STR_WIG40 = _T("WIG40");
const LPCTSTR STR_WIG80 = _T("WIG80");
const LPCTSTR STR_INDEXES = _T("Indexes");


MarketInfo* MarketInfo::s_instance = nullptr;


MarketInfo::MarketInfo(void)
{
   _marketInfoNode = make_shared<XMLNode>(STR_ROOT);
   auto wig20node = make_shared<XMLNode>(STR_WIG20);
   auto wig40node = make_shared<XMLNode>(STR_WIG40);
   auto wig80node = make_shared<XMLNode>(STR_WIG80);
   auto IndexesNode = make_shared<XMLNode>(STR_INDEXES);

   wig20node->addNode(_T("ASSECOPOL"), nullptr);
   wig20node->addNode(_T("BOGDANKA"), nullptr);
   wig20node->addNode(_T("BRE"), nullptr);
   wig20node->addNode(_T("BZWBK"), nullptr);
   wig20node->addNode(_T("EUROCASH"), nullptr);
   wig20node->addNode(_T("GTC"), nullptr);
   wig20node->addNode(_T("HANDLOWY"), nullptr);
   wig20node->addNode(_T("JSW"), nullptr);
   wig20node->addNode(_T("KERNEL"), nullptr);
   wig20node->addNode(_T("KGHM"), nullptr);
   wig20node->addNode(_T("LOTOS"), nullptr);
   wig20node->addNode(_T("PEKAO"), nullptr);
   wig20node->addNode(_T("PGE"), nullptr);
   wig20node->addNode(_T("PGNIG"), nullptr);
   wig20node->addNode(_T("PKNORLEN"), nullptr);
   wig20node->addNode(_T("PKOBP"), nullptr);
   wig20node->addNode(_T("PZU"), nullptr);
   wig20node->addNode(_T("SYNTHOS"), nullptr);
   wig20node->addNode(_T("TAURONPE"), nullptr);
   wig20node->addNode(_T("TPSA"), nullptr);

   wig40node->addNode(_T("AGORA"), nullptr);
   wig40node->addNode(_T("ALCHEMIA"), nullptr);
   wig40node->addNode(_T("ALIOR"), nullptr);
   wig40node->addNode(_T("AMREST"), nullptr);
   wig40node->addNode(_T("APATOR"), nullptr);
   wig40node->addNode(_T("ASTARTA"), nullptr);
   wig40node->addNode(_T("AZOTYTARNOW"), nullptr);
   wig40node->addNode(_T("BORYSZEW"), nullptr);
   wig40node->addNode(_T("BUDIMEX"), nullptr);
   wig40node->addNode(_T("CCC"), nullptr);
   wig40node->addNode(_T("CCIINT"), nullptr);
   wig40node->addNode(_T("CDPROJEKT"), nullptr);
   wig40node->addNode(_T("CIECH"), nullptr);
   wig40node->addNode(_T("CORMAY"), nullptr);
   wig40node->addNode(_T("CYFRPLSAT"), nullptr);
   wig40node->addNode(_T("ECHO"), nullptr);
   wig40node->addNode(_T("EMPERIA"), nullptr);
   wig40node->addNode(_T("ENEA"), nullptr);
   wig40node->addNode(_T("GETIN"), nullptr);
   wig40node->addNode(_T("GETINOBLE"), nullptr);
   wig40node->addNode(_T("GPW"), nullptr);
   wig40node->addNode(_T("HAWE"), nullptr);
   wig40node->addNode(_T("IDMSA"), nullptr);
   wig40node->addNode(_T("INGBSK"), nullptr);
   wig40node->addNode(_T("INTEGERPL"), nullptr);
   wig40node->addNode(_T("INTERCARS"), nullptr);
   wig40node->addNode(_T("KETY"), nullptr);
   wig40node->addNode(_T("KOPEX"), nullptr);
   wig40node->addNode(_T("KRUK"), nullptr);
   wig40node->addNode(_T("LPP"), nullptr);
   wig40node->addNode(_T("MIDAS"), nullptr);
   wig40node->addNode(_T("MILLENNIUM"), nullptr);
   wig40node->addNode(_T("NETIA"), nullptr);
   wig40node->addNode(_T("ORBIS"), nullptr);
   wig40node->addNode(_T("PETROLINV"), nullptr);
   wig40node->addNode(_T("POLIMEXMS"), nullptr);
   wig40node->addNode(_T("ROVESE"), nullptr);
   wig40node->addNode(_T("SERINUS"), nullptr);
   wig40node->addNode(_T("TVN"), nullptr);
   wig40node->addNode(_T("ZEPAK"), nullptr);

   wig80node->addNode(_T("ABPL"), nullptr);
   wig80node->addNode(_T("ACE"), nullptr);
   wig80node->addNode(_T("ACTION"), nullptr);
   wig80node->addNode(_T("AGROTON"), nullptr);
   wig80node->addNode(_T("AMICA"), nullptr);
   wig80node->addNode(_T("ASSECOBS"), nullptr);
   wig80node->addNode(_T("ASSECOSEE"), nullptr);
   wig80node->addNode(_T("ATLANTIS"), nullptr);
   wig80node->addNode(_T("ATM"), nullptr);
   wig80node->addNode(_T("AVIASG"), nullptr);
   wig80node->addNode(_T("BANKBPH"), nullptr);
   wig80node->addNode(_T("BBIDEVNFI"), nullptr);
   wig80node->addNode(_T("BENEFIT"), nullptr);
   wig80node->addNode(_T("BOS"), nullptr);
   wig80node->addNode(_T("CITYINTER"), nullptr);
   wig80node->addNode(_T("COLIAN"), nullptr);
   wig80node->addNode(_T("COMARCH"), nullptr);
   wig80node->addNode(_T("COMP"), nullptr);
   wig80node->addNode(_T("DEBICA"), nullptr);
   wig80node->addNode(_T("DOMDEV"), nullptr);
   wig80node->addNode(_T("DUDA"), nullptr);
   wig80node->addNode(_T("EKOEXPORT"), nullptr);
   wig80node->addNode(_T("ELBUDOWA"), nullptr);
   wig80node->addNode(_T("FAMUR"), nullptr);
   wig80node->addNode(_T("FARMACOL"), nullptr);
   wig80node->addNode(_T("FORTE"), nullptr);
   wig80node->addNode(_T("GANT"), nullptr);
   wig80node->addNode(_T("GRAJEWO"), nullptr);
   wig80node->addNode(_T("IMCOMPANY"), nullptr);
   wig80node->addNode(_T("IMPEL"), nullptr);
   wig80node->addNode(_T("IMPEXMET"), nullptr);
   wig80node->addNode(_T("IPOPEMA"), nullptr);
   wig80node->addNode(_T("JWCONSTR"), nullptr);
   wig80node->addNode(_T("KOFOLA"), nullptr);
   wig80node->addNode(_T("KOGENERA"), nullptr);
   wig80node->addNode(_T("KREDYTIN"), nullptr);
   wig80node->addNode(_T("KREZUS"), nullptr);
   wig80node->addNode(_T("LCCORP"), nullptr);
   wig80node->addNode(_T("LENTEX"), nullptr);
   wig80node->addNode(_T("LUBAWA"), nullptr);
   wig80node->addNode(_T("MAGELLAN"), nullptr);
   wig80node->addNode(_T("MCI"), nullptr);
   wig80node->addNode(_T("MENNICA"), nullptr);
   wig80node->addNode(_T("MERCOR"), nullptr);
   wig80node->addNode(_T("MILKILAND"), nullptr);
   wig80node->addNode(_T("MIRBUD"), nullptr);
   wig80node->addNode(_T("MNI"), nullptr);
   wig80node->addNode(_T("MONNARI"), nullptr);
   wig80node->addNode(_T("MOSTALZAB"), nullptr);
   wig80node->addNode(_T("NEUCA"), nullptr);
   wig80node->addNode(_T("NFIEMF"), nullptr);
   wig80node->addNode(_T("OPENFIN"), nullptr);
   wig80node->addNode(_T("ORZBIALY"), nullptr);
   wig80node->addNode(_T("PCGUARD"), nullptr);
   wig80node->addNode(_T("PELION"), nullptr);
   wig80node->addNode(_T("PEP"), nullptr);
   wig80node->addNode(_T("PHN"), nullptr);
   wig80node->addNode(_T("PLAZACNTR"), nullptr);
   wig80node->addNode(_T("POLICE"), nullptr);
   wig80node->addNode(_T("POLNORD"), nullptr);
   wig80node->addNode(_T("RADPOL"), nullptr);
   wig80node->addNode(_T("RAFAKO"), nullptr);
   wig80node->addNode(_T("RANKPROGR"), nullptr);
   wig80node->addNode(_T("SADOVAYA"), nullptr);
   wig80node->addNode(_T("SANOK"), nullptr);
   wig80node->addNode(_T("SKOTAN"), nullptr);
   wig80node->addNode(_T("SNIEZKA"), nullptr);
   wig80node->addNode(_T("STALEXP"), nullptr);
   wig80node->addNode(_T("STALPROD"), nullptr);
   wig80node->addNode(_T("SYGNITY"), nullptr);
   wig80node->addNode(_T("TRAKCJA"), nullptr);
   wig80node->addNode(_T("URSUS"), nullptr);
   wig80node->addNode(_T("VISTULA"), nullptr);
   wig80node->addNode(_T("WAWEL"), nullptr);
   wig80node->addNode(_T("WIELTON"), nullptr);
   wig80node->addNode(_T("WORKSERV"), nullptr);
   wig80node->addNode(_T("ZETKAMA"), nullptr);
   wig80node->addNode(_T("ZPUE"), nullptr);

   _marketInfoNode->addNode(wig20node);
   _marketInfoNode->addNode(wig40node);
   _marketInfoNode->addNode(wig80node);
   _marketInfoNode->addNode(IndexesNode);
   
   auto serialiser = FoundationUtils::xmlFactory()->createSerialiser();
   
   serialiser->serialise(_T("c:\\_dane\\_testNode.xml"), *_marketInfoNode);

   auto node = serialiser->unserialise(_T("c:\\_dane\\_testNode.xml"));

   serialiser->serialise(_T("c:\\_dane\\_testNode_2.xml"), *node);
}
MarketInfo::~MarketInfo(void)
{
}

MarketInfo* MarketInfo::instance()
{
   if(!s_instance)
   {
      s_instance = new MarketInfo();
   }

   return s_instance;
}

void MarketInfo::release()
{
   if(!s_instance)
   {
      delete s_instance;
      s_instance = nullptr;
   }
}
