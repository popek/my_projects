#include "stdafx.h"
#include "StocksAnalyser.h"
#include "MarketScannerDlg.h"
#include "Misc.h"
#include "PathUtils.h"


const int g_setProgressBarMaxMessageId = 0;
const int g_nextAssetScannedMessageId = 1;

const UINT g_updatUiMessageId = WM_USER + 1;
const UINT g_scanFinishedMessageId = WM_USER + 2;

IMPLEMENT_DYNAMIC(MarketScannerDlg, CDialog)

MarketScannerDlg::MarketScannerDlg()
	: CDialog(MarketScannerDlg::IDD, nullptr)
   , m_state(EFinishedScanning)
{
}
MarketScannerDlg::~MarketScannerDlg()
{
}

void MarketScannerDlg::NotifyTotalAssetsToScanCount(size_t a_assetsToScanCount)
{
   _updateUIMessagesQueue.AddTail(make_shared<ui_message>(a_assetsToScanCount, g_setProgressBarMaxMessageId));
   PostMessage(g_updatUiMessageId);
}

void MarketScannerDlg::NextAssetScanned(CString a_scannedAssetName, bool a_addToWatchList)
{
   _updateUIMessagesQueue.AddTail(make_shared<ui_message>(a_scannedAssetName, a_addToWatchList, g_nextAssetScannedMessageId));
   PostMessage(g_updatUiMessageId);
}

void MarketScannerDlg::MarketScanFinished()
{
   PostMessage(g_scanFinishedMessageId);
}

void MarketScannerDlg::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_ASSETS_PATH, m_editAssetsDirectory);
   DDX_Control(pDX, IDC_PROGRESS, m_progressScan);
   DDX_Control(pDX, IDC_ASSETS_LIST, m_listAssetsList);
   DDX_Control(pDX, IDC_TRADE_STRATEGIES_LIST, m_listStrategiesList);
   DDX_Control(pDX, IDC_PROCESSED_ASSET_DESCRIPTION, m_staticAssetDescription);
}

bool MarketScannerDlg::ReadyToProcessAssets()
{
   if(!PathUtils::doesDirectoryExist(m_editAssetsDirectory))
   {
      AfxMessageBox(_T("niepoprawny katalog z plikami instrumentów"), MB_OK);
      return false;
   }

   return true;
}

void MarketScannerDlg::ProcessUiMessages()
{
   while(_updateUIMessagesQueue.Size())
   {
      shared_ptr<ui_message> spMessage = _updateUIMessagesQueue.GetFront();

      switch(spMessage->_messageId)
      {
      case g_nextAssetScannedMessageId:
         {
            m_progressScan.OffsetPos(1);
            if(spMessage->_addToWatchlist)
            {
               _assetsListMeetingRequirements.push_back(spMessage->_assetFilePath);
               m_listAssetsList.InsertItem(m_listAssetsList.GetItemCount(), PathUtils::extractFileName(spMessage->_assetFilePath));
            }

            int lowerRange, upperRange;
            m_progressScan.GetRange(lowerRange, upperRange);
            m_progressScan.GetPos();
            CString description;
            description.Format(_T("[%d/%d] %s"), m_progressScan.GetPos(), upperRange, PathUtils::extractFileName(spMessage->_assetFilePath));
            m_staticAssetDescription.SetWindowText(description);
         }
         break;

      case g_setProgressBarMaxMessageId:
         m_progressScan.SetRange32(0, spMessage->_value);
         break;
      }
      _updateUIMessagesQueue.ReleaseFront();
   }
}

void MarketScannerDlg::SetState(state a_state)
{
   m_state = a_state;

   if(EScanning == m_state)
   {
      _assetsListMeetingRequirements.clear();
      m_listAssetsList.DeleteAllItems();
      m_progressScan.SetPos(0);
   }

   GetDlgItem(IDC_START_SCAN)->EnableWindow(EScanning == m_state ? FALSE : TRUE);
   GetDlgItem(IDC_STOP_SCAN)->EnableWindow(EScanning == m_state ? TRUE : FALSE);
}

BEGIN_MESSAGE_MAP(MarketScannerDlg, CDialog)
   ON_BN_CLICKED(IDC_CHOOSE_ASSETS_PATH, &MarketScannerDlg::OnBnClickedChooseAssetsPath)
   ON_BN_CLICKED(IDC_START_SCAN, &MarketScannerDlg::OnBnClickedStartScan)
   ON_BN_CLICKED(IDC_STOP_SCAN, &MarketScannerDlg::OnBnClickedStopScan)
   ON_NOTIFY(LVN_ITEMCHANGED, IDC_ASSETS_LIST, &MarketScannerDlg::OnLvnItemchangedAssetsList)
END_MESSAGE_MAP()


// MarketScannerDlg message handlers


void MarketScannerDlg::OnBnClickedChooseAssetsPath()
{
   m_editAssetsDirectory.SetWindowText(Misc::ChooseAssetsFilesDirectory());   
}


void MarketScannerDlg::OnBnClickedStartScan()
{
   UpdateData(TRUE);

   if(ReadyToProcessAssets())
   {
      SetState(EScanning);

      CString filesPath;
      m_editAssetsDirectory.GetWindowText(filesPath);
      _workerThread.RunMarketScan(this, StrategiesUtils::EMacd, filesPath);
   }
}


void MarketScannerDlg::OnBnClickedStopScan()
{
   _workerThread.StopMarketScan();
}


BOOL MarketScannerDlg::OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
   if(g_updatUiMessageId == message)
   {
      ProcessUiMessages();
   }
   else if(g_scanFinishedMessageId == message)
   {
      SetState(EFinishedScanning);
   }

   return __super::OnWndMsg(message, wParam, lParam, pResult);
}


void MarketScannerDlg::OnLvnItemchangedAssetsList(NMHDR *pNMHDR, LRESULT *pResult)
{
   LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

   if(pNMItemActivate->iItem > -1)
   {
      G_stockAnalyserApp.OpenDocumentFile( _assetsListMeetingRequirements[pNMItemActivate->iItem] );
   }

   *pResult = 0;
}
