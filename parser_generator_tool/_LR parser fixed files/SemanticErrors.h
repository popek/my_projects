#pragma once


class SemanticErrors
{
public:
   // error levels
   enum ELevel
   {
      EPEL_error,
      EPEL_warning,
   };

   // error detialed identifier
   enum EDetails
   {
      ESED_E_unknownSemanticError,
      ESED_E_SemanticRuleNotImplementedYet
   };

   // error entity calss
   class Entity
   {
   public:
      Entity(ELevel a_level, EDetails a_id)
         : _level(a_level)
         , _id(a_id)
      {
      }
      ~Entity()
      {
      }

      ELevel   _level;
      EDetails _id;
   };

public:
   SemanticErrors(void);
   virtual ~SemanticErrors(void);

   bool errorOccured() const;

   void registerError(EDetails a_details);
   
   void registerWarning(EDetails a_details);

private:
   vector<Entity> m_errors;
   vector<Entity> m_warnings;
};

