#pragma once
#include "ThreadSafeContainer.h"
#include "IMarketScannerObserver.h"
#include "MarketScannerWorkerThread.h"
#include "afxwin.h"


class MarketScannerDlg : public CDialog, public IMarketScannerObserver
{
	DECLARE_DYNAMIC(MarketScannerDlg)

private:
   
   enum state
   {
      EScanning,
      EFinishedScanning
   };

   struct ui_message
   {
      ui_message(CString a_assetFilePath, bool a_addToWatchlist, int a_messageId)
         : _assetFilePath(a_assetFilePath)
         , _addToWatchlist(a_addToWatchlist)
         , _value(0)
         , _messageId(a_messageId)
      {
      }

      ui_message(size_t a_value, int a_messageId)
         : _addToWatchlist(false)
         , _value(a_value)
         , _messageId(a_messageId)
      {
      }

      CString _assetFilePath;
      bool _addToWatchlist;
      int _messageId;
      size_t _value;
   };

public:
	MarketScannerDlg();
	virtual ~MarketScannerDlg();

	enum { IDD = IDD_MARKET_SCANNER };

   virtual void NotifyTotalAssetsToScanCount(size_t a_assetsToScanCount);

   virtual void NextAssetScanned(CString a_scannedAssetName, bool a_addToWatchList);

   virtual void MarketScanFinished();

protected:
   
   bool ReadyToProcessAssets();

   void ProcessUiMessages();

   void SetState(state a_state);

	virtual void DoDataExchange(CDataExchange* pDX);

private:
   ThreadSafeContainer<shared_ptr<ui_message>> _updateUIMessagesQueue;

	DECLARE_MESSAGE_MAP()

   CEdit m_editAssetsDirectory;
   CProgressCtrl m_progressScan;
   CListCtrl m_listAssetsList;
   CListCtrl m_listStrategiesList;

   vector<CString> _assetsListMeetingRequirements;

   MarketScannerWorkerThread  _workerThread;

   state m_state;
   
   afx_msg void OnBnClickedChooseAssetsPath();
   afx_msg void OnBnClickedStartScan();
   afx_msg void OnBnClickedStopScan();
   virtual BOOL OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult);
public:
   afx_msg void OnLvnItemchangedAssetsList(NMHDR *pNMHDR, LRESULT *pResult);
   CStatic m_staticAssetDescription;
};
