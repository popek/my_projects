#pragma once
#include "controlsLibTypes.h"


class Point2D
{
public:
   Point2D(void);
   virtual ~Point2D(void);

public:
   Coordinate _x;
   Coordinate _y;
};
