#include "GrammarFileParser/ParserErrors.h"

namespace lrlib
{

ParserErrors::ParserErrors(void)
{
}

ParserErrors::~ParserErrors(void)
{
}

ParserErrors::CError::CError(std::string a_strErrorDescription, int a_nLineNo)
{
   m_strErrorDescription = a_strErrorDescription;
   m_nLineNo = a_nLineNo;
}

void ParserErrors::AddError(std::string a_strErrorDescription, int a_nLineNo)
{
   m_deqErrors.push_back(CError(a_strErrorDescription, a_nLineNo));
}

void ParserErrors::AddFormatedError0(std::string a_strErrorDescription, int a_nLineNo, std::string a_AuxMiddleString)
{
   char sErrorDescription[1024];
   sprintf_s(sErrorDescription, 1024, a_strErrorDescription.c_str(), a_AuxMiddleString.c_str());
   m_deqErrors.push_back(CError(sErrorDescription, a_nLineNo));
}
bool ParserErrors::DoesErrorExist()
{
   return (m_deqErrors.size() > 0);
}

void ParserErrors::Clear()
{
   m_deqErrors.clear();
}

void ParserErrors::ShowErrors()
{
   for (size_t nErrorIdx = 0; nErrorIdx < m_deqErrors.size(); ++nErrorIdx)
   {
      printf("parse error in line %d: %s\n", m_deqErrors[nErrorIdx].m_nLineNo + 1, m_deqErrors[nErrorIdx].m_strErrorDescription.c_str());
   }
}

} // namespace lrlib