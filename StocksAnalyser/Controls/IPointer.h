#pragma once
#include "ControlsLib.h"
#include "IDoubleVector.h"


class DataSeries;


class CONTROLS_LIB IPointer
{
public:
   virtual ~IPointer(void);

   virtual void   Create(const DataSeries& a_inputData)     = 0;

   virtual const DataSeries& Series(size_t a_seriesId) const = 0;
};
