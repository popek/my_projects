#pragma once
#include "Symbol.h"
#include "SymbolUniqueKey.h"
#include "LessString.h"
#include "StateActions.h"
#include "CPlusCodeGenerator.h"
#include "SymbolsContainer.h"
#include "Rules.h"
#include "GrammarFileParser\GrammarParser.h"
#include "auxiliary_types.h"

#include <vector>
#include <set>
#include <map>
#include <functional>

namespace lrlib
{

namespace helper
{
class IGrammarGenerationObserver;
}

/*------------------------------------------------------
class name: Grammar
-------------------------------------------------------*/
/**
* class representing grammar (SLR(1), in the future also LALR(1)).
* Important information is that grammar processed by this class
* can't have an empty productions (for empty productions FIRST and FOLLOW
* methods have to be modified)
*//*--------------------------------------------------*/
class Grammar
{
   friend CPlusCodeGenerator;

public:
   Grammar(const char* pGrammarDefinitionFile, const char* pPathForCodeGeneration);
   virtual ~Grammar(void);

   std::string GetSymbolName(int iSymID);

   void ShowErrors();

   static int m_iEofID;

private:
   std::deque<std::string> m_deqErrors;            // errors container

   CPlusCodeGenerator      m_oCodeGenerator;       // object which generates c++ code
   GrammarParser       grammarFileParser_;   // grammar definition file parser

   t_setSymbol				    m_setGrammarSymbols;	 // grammar symbols
   t_mmapInt2VecInt			 m_mmapRules;			    // rules (productions)
   std::vector<t_mmapInt2VecInt> m_vecLR0States;			 // states (groups of LR(0) items)


   t_transitionGraph        m_vecStateActions;      //variable representing transition graph (index of each element is start state ID)
   t_mapInt2SetInt          m_mapSymbol2Follow;     //map for storing FOLLOW symbols 

   //--- auxiliary containers ----------
   t_mapStr2Sym m_mapAuxSymbName2Sym;
   t_mapInt2Sym m_mapAuxSymbID2Sym;
   std::set<int> m_setAuxRuleAlreadyChecked;
   //-----------------------------------

   SymbolsContainer symbolsContainer_;
   Rules rulesContainer_;

private:
   void PropagateSymbols();
   void PropagateSymbol(Symbol& symbol);

   void PropagateRules();

   void ExtendGrammar();
   void GenerateParsers();
   bool CheckAcceptance(t_mmapInt2VecInt& mmapState);

   t_mmapInt2VecInt Closure(int, std::vector<int>, int iDotIdx = -1, bool bClearAuxSet = true);
   t_mmapInt2VecInt Closure(t_mmapInt2VecInt mmapProductions);
   t_mmapInt2VecInt Succ(t_mmapInt2VecInt&, Symbol&);
   t_mmapInt2VecInt Succ(t_mmapInt2VecInt&, int);
   t_mmapInt2VecInt Goto(t_mmapInt2VecInt&, Symbol&);
   t_mmapInt2VecInt Goto(t_mmapInt2VecInt&, int);
   std::vector<int> FindSuccElement(std::vector<int>&, int);

   void             FIRST(Symbol&, std::set<int>&, bool bJustStarted = true);
   void             FOLLOW(Symbol&, std::set<int>&, bool bJustStarted = true);

   bool             CheckSLR1Reduction(t_mmapInt2VecInt& mmapState, std::vector<int>& vecReductionRulesIDs, Symbol& LookAheadSymbol);
   bool             CheckIfSymbolIsInFOLLOW(int iLookAheadID, int iSymbolToFoolowID);
   void             GenerateFOLLOW();

   int              GetRuleIdx(t_mmapInt2VecInt::iterator it);
   int              Iter2Index(t_mmapInt2VecInt::iterator);
   int              GetDotIdx(t_mmapInt2VecInt::iterator);
   int              GetDotIdx(std::vector<int>&);

   static int m_iDotID;
   static int m_iExtNontermID;

   std::function<std::unique_ptr<helper::IGrammarGenerationObserver>()> createObserver_;
};

} // namespace lrlib