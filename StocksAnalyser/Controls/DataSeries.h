#pragma once
#include "IDoubleVector.h"
#include "ControlsLib.h"


//
class CONTROLS_LIB DataSeries abstract
{
public:
   DataSeries(size_t a_offset);
   DataSeries(const DataSeries* a_pParent, size_t a_offset);
   virtual ~DataSeries(void);

   double GetDataBasedOnRootSeriesIndex(size_t a_rootSeriesIndex) const;

   virtual double operator [](size_t a_elementIndex) const = 0;

   virtual size_t size() const = 0;

   // returns difference between size of parent and this (flat = true), or 
   //         difference between size of root and this (flat = false)
   size_t offset(bool a_flatOffset) const;

protected:
   size_t m_offset;
   size_t m_parentToRootOffset;
};

//
class VectorBasedSeries : public DataSeries
{
public:
   VectorBasedSeries(shared_ptr<vector<double>> a_spData, size_t a_offset);
   VectorBasedSeries(const DataSeries* a_pParent, shared_ptr<vector<double>> a_spData, size_t a_offset);
   virtual ~VectorBasedSeries(void);

   virtual double operator [](size_t a_elementIndex) const;

   virtual size_t size() const;

private:
   shared_ptr<vector<double>> m_spData;
};
