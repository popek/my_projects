#pragma once
#include "ThreadBase.h"
#include "IThreadMessageHandler.h"
#include "BeginingOfTrendParamsDlg.h"
#include "ChannelAlgorithParametersDlg.h"
#include "DoubleBottomContextDlg.h"
#include "ResultsLog.h"
#include "UpdateUIMessageHandler.h"
#include "Resource.h"

class IPointer;
class Asset;

class SearchGMMAsignalsDlg : public CDialog, public IThreadMessageHandler
{
	DECLARE_DYNAMIC(SearchGMMAsignalsDlg)

public:
	SearchGMMAsignalsDlg(CWnd* pParent = NULL);
	virtual ~SearchGMMAsignalsDlg();

	enum { IDD = IDD_SEARCH_GMMA_SIGNALS_DIALOG };

   virtual BOOL    ProcessThreadMessage(UINT a_messageId);

   void            UpdateProgress(CString a_strLabel, int a_progressPos);

   void            SetProgressRange(int a_range);

   void            AddNewAssetToTheList(CString a_assetFilePath);

   void            AfterProcessing();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);

   virtual BOOL OnInitDialog();

   virtual BOOL OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult);

   void         AbortProcessing();

   void         InitialiseNextResult();

   void         OnAlgorithgTypeChanged();

   void         ProcessAllAssets();

   void         ProcessAsset(CString a_fileFullPath);

   bool         AssetTurnoverOK(shared_ptr<Asset> a_spAsset) const;

   void         BeforeProcessing();

   void         ResetTurnOver();

   bool         TurnoverDefinedProperly() const;

   bool         AssetsFilesPathDefinedProperly() const;

   bool         ReadyToProcessAssets();

	DECLARE_MESSAGE_MAP()
   afx_msg void OnBnClickedChooseAssetsPath();
   afx_msg void OnBnClickedSearchBuy();
   afx_msg void OnBnClickedStopProcessing();
   afx_msg void OnNMDblclkAssetsList(NMHDR *pNMHDR, LRESULT *pResult);
   afx_msg void OnBnClickedBeginingOfTrend();
   afx_msg void OnBnClickedNearBuySignal();
   afx_msg void OnBnClickedParallelChannel();
   afx_msg void OnBnClickedDoubleBottom();
   afx_msg void OnDestroy();

protected:

   CString        m_strTurnoverKandlesAmount;

   double         m_turnoverKandleAmount;

   CString        m_strMinTurnover;
   CString        m_strMaxTurnover;

   double         m_minTurnoverTotal;
   double         m_maxTurnoverTotal;

   CListCtrl         m_assetsList;
   vector<CString>   m_assetsListItemsData;
   
   CProgressCtrl  m_progress;
   
   CEdit          m_assetsFilesPath;
   CString        m_strAssetFilePath;

   ThreadBase     m_thread;

   bool           m_stopProcessing;

   int            m_algorithmType;

   ResultsLog     m_results;
   shared_ptr<ResultsLog::SingleResult> m_spTemporarySingleResult;

   BeginingOfTrendParamsDlg   m_beginingOfTrendParams;
   
   ChannelAlgorithParametersDlg m_parallelChannelParams;

   DoubleBottomContextDlg       m_doubleBottomContextDlg;


   UpdateUIMessageHandler    m_UIRefreshHandler;

   vector<UINT> m_algorithmTypeList;
};
