#pragma once

#define NONCOPYABLE_TYPE(_type)     private: \
                                       _type::_type(const _type& a_ref); \
                                       _type& operator = (const _type& a_ref);
