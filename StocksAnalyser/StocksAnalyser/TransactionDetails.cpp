#include "StdAfx.h"
#include "TransactionDetails.h"
#include "KandleDate.h"


TransactionDetails::TransactionDetails(shared_ptr<KandleDate> a_spDate, double a_price)
: _spDate(a_spDate)
, m_price(a_price)
{
}
TransactionDetails::~TransactionDetails(void)
{
}
