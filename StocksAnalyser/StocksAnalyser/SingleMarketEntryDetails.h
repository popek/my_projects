#pragma once


class TransactionDetails;


class SingleMarketEntryDetails
{
public:
   SingleMarketEntryDetails(shared_ptr<TransactionDetails> a_buy, 
                            shared_ptr<TransactionDetails> a_sell);
   virtual ~SingleMarketEntryDetails(void);

   double Income(double a_bankroll, double a_brokerFee) const;

private:
   shared_ptr<TransactionDetails> _buy;
   shared_ptr<TransactionDetails> _sell;
};
