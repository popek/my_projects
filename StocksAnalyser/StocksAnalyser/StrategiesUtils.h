#pragma once


class Asset;
class TradeStrategyBase;


class StrategiesUtils
{
public:
   enum strategyId
   {
      EMacd,

      EInvalidStrategyId
   };

public:
   static shared_ptr<TradeStrategyBase> CreateStrategy(strategyId a_id, shared_ptr<Asset> a_asset);
};

