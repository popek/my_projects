#pragma once


class GraphicObjectBase;


class GraphicObjectPainterBase abstract
{
public:
   virtual ~GraphicObjectPainterBase(void);

   virtual void DrawObject(const GraphicObjectBase& a_object, CDC* a_pDC) = 0;
};
