#pragma once
#include "Scanner.h"
#include "SemActions.h"


class LRParser
{
public:
   LRParser();

   bool Compile(string_t a_scriptFile);

private:
	void ErrorRecovery();
	
protected:
   shared_ptr<SemActions> _spActions;
   shared_ptr<Scanner>	  _spScaner;
};
