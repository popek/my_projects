#include "StdAfx.h"
#include "GMMA_Signal.h"
#include "AssetContainer.h"
#include "Asset.h"
#include "IPointer.h"
#include "DataSeries.h"


GMMA_Signal::GMMA_Signal(shared_ptr<AssetContainer> a_spAssetContainer)
: m_shortPeriodsPointersAmount(0)
{
   SetAsset(a_spAssetContainer);
}
GMMA_Signal::~GMMA_Signal(void)
{
}

double GMMA_Signal::CalcShortTermPointersMean(size_t a_sampleOffset) const
{
   double shortPointersMean = 0;
   size_t pointerIdx        = 0;

   // calculate "short" pointers mean
   for(pointerIdx=0; pointerIdx<m_shortPeriodsPointersAmount; ++pointerIdx)
   {
      IPointer* pPointer = m_vTemporaryPointers[pointerIdx].get();

      shortPointersMean += pPointer->Series(0)[pPointer->Series(0).size()- a_sampleOffset];
   }

   return shortPointersMean /= m_shortPeriodsPointersAmount;
}

double GMMA_Signal::CalcLongTermPointersMean(size_t a_sampleOffset) const
{
   double longPointersMean = 0;
   size_t pointerIdx        = 0;

   // calculate "short" pointers mean
   for(pointerIdx=m_shortPeriodsPointersAmount; pointerIdx<m_vTemporaryPointers.size(); ++pointerIdx)
   {
      IPointer* pPointer = m_vTemporaryPointers[pointerIdx].get();

      longPointersMean += pPointer->Series(0)[pPointer->Series(0).size()- a_sampleOffset];
   }

   return longPointersMean /= (m_vTemporaryPointers.size() - m_shortPeriodsPointersAmount);
}

double GMMA_Signal::GetShortTermsMeansMinimum(const size_t& a_offset) const
{
   bool   minValueInitialised = false;
   double minValue            = 0;

   for(size_t pointerIdx=0; pointerIdx<m_shortPeriodsPointersAmount; ++pointerIdx)
   {
      IPointer* pPointer = m_vTemporaryPointers[pointerIdx].get();

      if(false == minValueInitialised)
      {
         minValueInitialised = true;
         minValue            = pPointer->Series(0)[pPointer->Series(0).size()- a_offset];
      }
      else
      {
         minValue = min(minValue, pPointer->Series(0)[pPointer->Series(0).size()- a_offset]);
      }
   }

   return minValue;
}

double GMMA_Signal::GetShortTermsMeansMaximum(const size_t& a_offset) const
{
   bool   maxValueInitialised = false;
   double maxValue            = 0;

   for(size_t pointerIdx=0; pointerIdx<m_shortPeriodsPointersAmount; ++pointerIdx)
   {
      IPointer* pPointer = m_vTemporaryPointers[pointerIdx].get();

      if(false == maxValueInitialised)
      {
         maxValueInitialised = true;
         maxValue            = pPointer->Series(0)[pPointer->Series(0).size()- a_offset];
      }
      else
      {
         maxValue = max(maxValue, pPointer->Series(0)[pPointer->Series(0).size()- a_offset]);
      }
   }

   return maxValue;
}

double GMMA_Signal::GetLongTermsMeansMinimum(const size_t& a_offset) const
{
   bool   minValueInitialised = false;
   double minValue            = 0;

   for(size_t pointerIdx=m_shortPeriodsPointersAmount; pointerIdx<m_vTemporaryPointers.size(); ++pointerIdx)
   {
      IPointer* pPointer = m_vTemporaryPointers[pointerIdx].get();

      if(false == minValueInitialised)
      {
         minValueInitialised = true;
         minValue            = pPointer->Series(0)[pPointer->Series(0).size()- a_offset];
      }
      else
      {
         minValue = min(minValue, pPointer->Series(0)[pPointer->Series(0).size()- a_offset]);
      }
   }

   return minValue;
}

double GMMA_Signal::GetLongTermsMeansMaximum(const size_t& a_offset) const
{
   bool   maxValueInitialised = false;
   double maxValue            = 0;

   for(size_t pointerIdx=m_shortPeriodsPointersAmount; pointerIdx<m_vTemporaryPointers.size(); ++pointerIdx)
   {
      IPointer* pPointer = m_vTemporaryPointers[pointerIdx].get();

      if(false == maxValueInitialised)
      {
         maxValueInitialised = true;
         maxValue            = pPointer->Series(0)[pPointer->Series(0).size()- a_offset];
      }
      else
      {
         maxValue = max(maxValue, pPointer->Series(0)[pPointer->Series(0).size()- a_offset]);
      }
   }

   return maxValue;
}

bool GMMA_Signal::CheckPointersSamplesAmount(const size_t a_minSamplesAmount) const
{
   for(size_t pointerIdx=0; pointerIdx<m_vTemporaryPointers.size(); ++pointerIdx)
   {
      if( m_vTemporaryPointers[pointerIdx]->Series(0).size() < a_minSamplesAmount ) 
      {
         return false;
      }
   }

   return true;
}

size_t GMMA_Signal::MinimalSamplesCount() const
{
   size_t minimalSamplesCount = m_vTemporaryPointers[0]->Series(0).size();

   for(size_t pointerIdx=1; pointerIdx<m_vTemporaryPointers.size(); ++pointerIdx)
   {
      minimalSamplesCount = min(minimalSamplesCount, m_vTemporaryPointers[pointerIdx]->Series(0).size());
   }

   return minimalSamplesCount;
}