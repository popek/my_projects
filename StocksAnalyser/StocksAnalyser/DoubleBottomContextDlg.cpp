#include "stdafx.h"
#include "StocksAnalyser.h"
#include "DoubleBottomContextDlg.h"
#include "Misc.h"


IMPLEMENT_DYNAMIC(DoubleBottomContextDlg, PropertyDialogBase)

DoubleBottomContextDlg::DoubleBottomContextDlg()
	: PropertyDialogBase(DoubleBottomContextDlg::IDD)
   , m_spContext(new DoubleBottomFinder::Context())
{
   m_period = Misc::ToString((int)m_spContext->_searchArea);
   m_neigboursCount = Misc::ToString((int)m_spContext->_bottomNeigboursMinCount);
   m_bottomDepth = Misc::ToString(m_spContext->_bottomDepth*100);
   m_bottomsDifference = Misc::ToString(m_spContext->_bottomsValueDiff*100);
}
DoubleBottomContextDlg::~DoubleBottomContextDlg()
{
}

void DoubleBottomContextDlg::DoDataExchange(CDataExchange* pDX)
{
   PropertyDialogBase::DoDataExchange(pDX);
   DDX_Text(pDX, IDC_EDIT_SEARCH_AREA, m_period);
   DDX_Text(pDX, IDC_EDIT_NEIGHBOURS_COUNT, m_neigboursCount);
   DDX_Text(pDX, IDC_EDIT_DEPTH, m_bottomDepth);
   DDX_Text(pDX, IDC_EDIT_LEVELS_DIFF, m_bottomsDifference);

   if(TRUE == pDX->m_bSaveAndValidate)
   {
      m_spContext->_searchArea = (size_t)Misc::ToInt(m_period);
      m_spContext->_bottomNeigboursMinCount = (size_t)Misc::ToInt(m_neigboursCount);
      m_spContext->_bottomDepth = Misc::ToDouble(m_bottomDepth)/100;
      m_spContext->_bottomsValueDiff = Misc::ToDouble(m_bottomsDifference)/100;
   }
}

shared_ptr<DoubleBottomFinder::Context> DoubleBottomContextDlg::Context()
{
   return m_spContext;
}

BEGIN_MESSAGE_MAP(DoubleBottomContextDlg, PropertyDialogBase)
END_MESSAGE_MAP()


// DoubleBottomContextDlg message handlers
