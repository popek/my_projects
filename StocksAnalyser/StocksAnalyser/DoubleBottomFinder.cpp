#include "StdAfx.h"
#include "DoubleBottomFinder.h"
#include "IDoubleVector.h"
#include "AssetContainer.h"
#include "Asset.h"
#include "SMA_pointer.h"
#include "AssetAuxiliaryWrappers.h"
#include "ChartElementsCollection.h"
#include "ChartElement.h"
#include "Kandle.h"
#include "ControlsLibUtils.h"
#include "DataSeries.h"


static const size_t s_invalidIndex = (size_t)-1;


DoubleBottomFinder::DoubleBottomFinder(shared_ptr<DoubleBottomFinder::Context> a_spContext)
: m_spContext(a_spContext)
{
}
DoubleBottomFinder::~DoubleBottomFinder(void)
{
}

bool DoubleBottomFinder::VerifyAssetConditions()
{
   static const size_t s_secondBottomIndexMaxOffset = 3;

   shared_ptr<Asset> spDaily = (*m_spAsset)[eDay];
   
   size_t smaPeriod = 3;
   size_t offset = smaPeriod - 1;

   if(spDaily->Size() < 3 * m_spContext->_searchArea + smaPeriod)
   {
      return false;
   }

   SMA_pointer sma(smaPeriod);
   sma.Create( AssetLowValueGetter(*spDaily) );

   size_t secondBottomLeftBoundryIndex = s_invalidIndex;
   size_t firstBottomLeftBoundryIndex = s_invalidIndex;

   size_t secondBottomIndex = s_invalidIndex;

   // check border condition (in case last sample could be a bottom). Must be done here since FindBottom is looking for
   // bottom supposing that it will start from right branch (not from bottom itself)
   if(IsBottom(sma.Series(0).size()-1, secondBottomLeftBoundryIndex, sma.Series(0), false))
   {
      secondBottomIndex = sma.Series(0).size()-1;
   }
   else
   {
      secondBottomIndex = FindBottom(sma.Series(0), secondBottomLeftBoundryIndex, false, sma.Series(0).size()-1);
   }

   // if bottom is far away from recent sample
   if(secondBottomIndex < sma.Series(0).size() - s_secondBottomIndexMaxOffset)
   {
      return false;
   }
   
   size_t firstBottomIndex = s_invalidIndex != secondBottomIndex ? 
                              FindBottom(sma.Series(0), firstBottomLeftBoundryIndex, true, secondBottomLeftBoundryIndex) : s_invalidIndex;

   // if indexes were not found then return false
   if(s_invalidIndex == secondBottomIndex || s_invalidIndex == firstBottomIndex)
   {
      return false;
   }

   // if bottom values are not on "similiar level" then return false
   size_t graterValueBottomIndex = sma.Series(0)[firstBottomIndex] > sma.Series(0)[secondBottomIndex] ? firstBottomIndex : secondBottomIndex;
   size_t smallerValueBottomIndex = firstBottomIndex == graterValueBottomIndex ? secondBottomIndex : firstBottomIndex;
   
   if( (sma.Series(0)[graterValueBottomIndex] - sma.Series(0)[smallerValueBottomIndex])/sma.Series(0)[smallerValueBottomIndex] > m_spContext->_bottomsValueDiff)
   {
      return false;
   }

   // else attach graphic elements and return true
   shared_ptr<ChartElementsCollection> spElementsCollection(new ChartElementsCollection());

   shared_ptr<ChartElement> spDoubleBottomLine(new ChartElement(ELineWithPoints));
   {
      Kandle& kandle = (*spDaily)[firstBottomLeftBoundryIndex + offset];
      spDoubleBottomLine->AddPoint( ChartPoint(kandle.Date(), sma.Series(0)[firstBottomLeftBoundryIndex]) );
   }
   {
      Kandle& kandle = (*spDaily)[firstBottomIndex + offset];
      spDoubleBottomLine->AddPoint( ChartPoint(kandle.Date(), sma.Series(0)[firstBottomIndex]) );
   }
   {
      Kandle& kandle = (*spDaily)[secondBottomLeftBoundryIndex + offset];
      spDoubleBottomLine->AddPoint( ChartPoint(kandle.Date(), sma.Series(0)[secondBottomLeftBoundryIndex]) );
   }
   {
      Kandle& kandle = (*spDaily)[secondBottomIndex + offset];
      spDoubleBottomLine->AddPoint( ChartPoint(kandle.Date(), sma.Series(0)[secondBottomIndex]) );
   }
   spElementsCollection->PushBack(spDoubleBottomLine);

   spElementsCollection->PushFront( ControlsLibUtils::CreatePointerSeriesElement(sma.Series(0), *spDaily) );
   spDaily->AttachElementsCollection(spElementsCollection);

   return true;
}

size_t DoubleBottomFinder::FindBottom(const DataSeries& a_data, size_t& a_bottomLeftBoundry, bool a_checkBothSidesDepth, size_t a_mostRightIndex)
{
   size_t firstSmaple = a_mostRightIndex - m_spContext->_searchArea;
   size_t lastSample = a_mostRightIndex;

   double prevSample = a_data[lastSample];

   for(size_t sampleIdx=lastSample-1; sampleIdx>=firstSmaple; --sampleIdx)
   {
      double currentValue = a_data[sampleIdx];

      if(a_data[sampleIdx] < prevSample)
      {
         prevSample = a_data[sampleIdx];
      }
      else if (a_data[sampleIdx] > prevSample)
      {
         if(IsBottom(sampleIdx+1, a_bottomLeftBoundry, a_data, a_checkBothSidesDepth))
         {
            return sampleIdx+1;
         }
      }
   }

   return s_invalidIndex;
}

bool DoubleBottomFinder::IsBottom(size_t a_potentialBotomIndex, size_t& a_doubleBottomBoundryPoint, const DataSeries& a_data, bool a_checkBothSidesDepth)
{
   size_t firstSample = (a_potentialBotomIndex > m_spContext->_bottomNeigboursMinCount ? a_potentialBotomIndex - m_spContext->_bottomNeigboursMinCount : 0);
   size_t lastSample = a_potentialBotomIndex + m_spContext->_bottomNeigboursMinCount;
   size_t sampleIdx = 0;
   
   if(lastSample >= a_data.size())
   {
      lastSample = a_data.size() - 1;
   }

   if(CheckPotentialBottomRightSide(a_potentialBotomIndex, lastSample, a_data, a_checkBothSidesDepth))
   {
      return CheckPotentialBottomLeftSide(a_potentialBotomIndex, firstSample, a_data, a_doubleBottomBoundryPoint);
   }

   return false;
}

bool DoubleBottomFinder::CheckPotentialBottomRightSide(size_t a_potentialBottomIndex, size_t a_lastIndex,
                                                       const DataSeries& a_data, bool a_checkDepth)
{
   size_t sampleIdx = a_potentialBottomIndex;

   // check neighbours on the right
   while(sampleIdx <= a_lastIndex)
   {
      sampleIdx = DoesItFitToRightBranch(sampleIdx, a_data);

      if(sampleIdx == s_invalidIndex)
      {
         return false;
      }

      if(sampleIdx == a_data.size()-1)
      {
         break;
      }
   }

   // check if bottom line "to the right" is deep enough
   if(true == a_checkDepth)
   {
      while(true)
      {
         if(sampleIdx >= a_data.size())
         {
            break;
         }

         size_t index = DoesItFitToRightBranch(sampleIdx, a_data);

         if(s_invalidIndex == index)
         {
            break;
         }
         else
         {
            sampleIdx = index;
         }
      }
      
      return IsDeepEnough(a_potentialBottomIndex, sampleIdx, a_data);
   }
   else
   {
      return true;
   }
}

bool DoubleBottomFinder::CheckPotentialBottomLeftSide(size_t a_potentialBottomIndex, size_t a_firstIndex,
                                                      const DataSeries& a_data, size_t& a_leftBoundryIndex)
{
   size_t sampleIdx = a_potentialBottomIndex-1;
   // check neighbours on the left
   while(sampleIdx > a_firstIndex)
   {
      sampleIdx = DoesItFitToLeftBranch(sampleIdx, a_data);

      if(sampleIdx == s_invalidIndex)
      {
         return false;
      }
   }

   // check if bottom line "to the left" is deep enough
   while(true)
   {
      if(0 == sampleIdx)
      {
         break;
      }

      size_t index = DoesItFitToLeftBranch(sampleIdx, a_data);

      if(s_invalidIndex == index)
      {
         a_leftBoundryIndex = sampleIdx;
         break;
      }
      else
      {
         sampleIdx = index;
      }
   }

   return IsDeepEnough(a_potentialBottomIndex, a_leftBoundryIndex, a_data);
}

bool DoubleBottomFinder::IsDeepEnough(size_t a_bottomBottomIndex, size_t a_bottomTopIndex, const DataSeries& a_data)
{
   return (a_data[a_bottomTopIndex] - a_data[a_bottomBottomIndex])/a_data[a_bottomBottomIndex] >= m_spContext->_bottomDepth;
}

size_t DoubleBottomFinder::DoesItFitToLeftBranch(size_t a_sampleIndex, const DataSeries& a_data)
{
   if(0 == a_sampleIndex)
   {
      return s_invalidIndex;
   }

   static const size_t s_sampleLeftOffset = 3;

   double sampleValue = a_data[a_sampleIndex];

   size_t lastIndexToCheck = a_sampleIndex - 1;
   size_t firstIndexToCheck =  a_sampleIndex > s_sampleLeftOffset ? a_sampleIndex - s_sampleLeftOffset : 0;

   // check if "on the left from requested sample" there is at least one grater value
   for(size_t index=lastIndexToCheck; index >= firstIndexToCheck; --index)
   {
      if(a_data[index] > sampleValue)
      {
         return index;
      }

      if(0 == index)
      {
         break;
      }
   }

   return s_invalidIndex;
}

size_t DoubleBottomFinder::DoesItFitToRightBranch(size_t a_sampleIndex, const DataSeries& a_data)
{
   if(a_sampleIndex >= a_data.size()-1)
   {
      return a_data.size()-1;
   }

   static const size_t s_sampleRightOffset = 3;

   double sampleValue = a_data[a_sampleIndex];

   size_t firstIndexToCheck =  min(a_sampleIndex + 1, a_data.size()-1);
   size_t lastIndexToCheck = min(a_sampleIndex + s_sampleRightOffset, a_data.size()-1);


   // check if "on the right from requested sample" there is at least one grater value
   for(size_t index=firstIndexToCheck; index <= lastIndexToCheck; ++index)
   {
      if(a_data[index] > sampleValue)
      {
         return index;
      }
   }

   return s_invalidIndex;
}