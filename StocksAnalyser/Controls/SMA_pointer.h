#pragma once
#include "ControlsLib.h"
#include "PointerAvarageBase.h"


class CONTROLS_LIB SMA_pointer : public PointerAvarageBase
{
public:
   SMA_pointer(int a_nMeanValueCalculationSamplesAmount);
   virtual ~SMA_pointer(void);

   virtual void Create(const DataSeries& a_inputData);
};
