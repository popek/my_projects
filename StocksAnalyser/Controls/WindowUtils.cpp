#include "StdAfx.h"
#include "WindowUtils.h"


BOOL WindowUtils::RegisterWindowClass(LPCTSTR a_className, HINSTANCE a_moduleInstance, UINT a_style)
{
   WNDCLASS wndcls;
   memset(&wndcls, 0, sizeof(WNDCLASS));

   wndcls.style = a_style;
   wndcls.lpfnWndProc   = ::DefWindowProc; 
   wndcls.hInstance     = a_moduleInstance;
   wndcls.hIcon         = NULL;
   wndcls.hCursor       = LoadCursor(wndcls.hInstance, MAKEINTRESOURCE(IDC_ARROW));
   wndcls.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
   wndcls.lpszMenuName  = NULL;
   wndcls.lpszClassName = a_className;

   return AfxRegisterClass(&wndcls);
}

void WindowUtils::SetWindowPos(CWnd* a_pWindowToMove, const CWnd* a_pWndInsertAfter, const CRect& a_rect, UINT a_flags)
{
   if(a_pWindowToMove->GetSafeHwnd())
   {
      a_pWindowToMove->SetWindowPos(a_pWndInsertAfter, a_rect.left, a_rect.top, a_rect.Width(), a_rect.Height(), a_flags);
   }
}