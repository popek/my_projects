#include "LexManager.h"
#include "Exceptions.h"

namespace regexlib
{

LexManager::LexManager(eRegExModel modelType) :
   LexFileReader(modelType),
   m_pCodeGen(NULL)
{
}

LexManager::~LexManager(void)
{
}

bool LexManager::CreateLexer(const char* LexFile)
{
   if (Read(LexFile))
   {
      //generate main regex from definition
      unsigned int uiAux;
      for (unsigned int i = 0; i < m_RegExDefs.size(); i++)
      {
         CompoundRegEx* pcre = new CompoundRegEx();
         uiAux = 0;
         BuildRegExFromDefinition(m_RegExDefs[i].second, pcre->GetRegExContainer(), uiAux);
         m_RegExRoot.AddOR(pcre);
      }
      GenerateDFAdiagram();
      return true;
   }
   else
      return false;
}

short LexManager::SymbolName2SymbolID(std::string strSymName)
{
   if (strSymName.size() == 1)
      return strSymName[0];

   return m_mapComplexSymbolName2SymbolID.find(strSymName)->second;
}

short LexManager::Char2SymbolID(char cChar)
{
   if (m_mapChar2SymbolID.find(cChar) == m_mapChar2SymbolID.end())
      throw CharNotFoundExc();

   return m_mapChar2SymbolID[cChar];
}

t_Char2SymbolID& LexManager::GetChar2SymbolIDMap()
{
   return m_mapChar2SymbolID;
}

bool LexManager::GenerateCode(const char* path)
{
   if (!m_pCodeGen)
      m_pCodeGen = new CodeGenerator(m_mapChar2SymbolID, m_DFAdiagram, m_DFAacceptStates, m_RegExDefs, m_uiDFAStatesAmount);

   return m_pCodeGen->GenerateCode(path);

}

void LexManager::BuildRegExFromDefinition(std::vector<std::string>& vecWords, std::vector<RegEx*>& vecRegEx, unsigned int& uiPos)
{
   unsigned int uiSize;
   uiSize = vecWords.size();
   while (uiPos < uiSize)
   {
      if (strcmp(vecWords[uiPos].c_str(), "(") == 0)
      {
         CompoundRegEx *pre = new regexlib::CompoundRegEx();

         if (vecRegEx.size() && vecRegEx[vecRegEx.size() - 1]->Type() == eRE_OR)
            static_cast<ORRegEx*>(vecRegEx[vecRegEx.size() - 1])->AddOR(pre);
         else
            vecRegEx.push_back(pre);

         BuildRegExFromDefinition(vecWords, pre->GetRegExContainer(), ++uiPos);
         continue;
      }

      if (strcmp(vecWords[uiPos].c_str(), ")") == 0)
      {
         uiPos++;
         return;
      }

      if (strcmp(vecWords[uiPos].c_str(), "|") == 0)
      {

         if (vecRegEx.size() && vecRegEx[vecRegEx.size() - 1]->Type() != eRE_OR)
         {
            ORRegEx *porre = new ORRegEx();
            porre->AddOR(vecRegEx[vecRegEx.size() - 1]);
            vecRegEx[vecRegEx.size() - 1] = porre;
         }
         uiPos++;
         continue;
      }

      if (strcmp(vecWords[uiPos].c_str(), "*") == 0)
      {
         MULRegEx *pmulre = new MULRegEx();

         if (vecRegEx[vecRegEx.size() - 1]->Type() == eRE_OR)
         {
            pmulre->SetBaseRegEx(static_cast<ORRegEx*>(vecRegEx[vecRegEx.size() - 1])->GetLastOR());
            static_cast<ORRegEx*>(vecRegEx[vecRegEx.size() - 1])->SetLastOR(pmulre);
         }
         else
         {
            pmulre->SetBaseRegEx(vecRegEx[vecRegEx.size() - 1]);
            vecRegEx[vecRegEx.size() - 1] = pmulre;
         }
         uiPos++;
         continue;
      }


      short SymbolID = SymbolName2SymbolID(vecWords[uiPos]);
      bool bExtendLastOr = false;
      if (vecRegEx.size() && vecRegEx[vecRegEx.size() - 1]->Type() == eRE_OR)
         bExtendLastOr = true;

      if (SymbolID < 0 && m_ModelType == eREM_SimpleRegEx)//symbolID<0 means a complex symbol
      {
         auto& allChars = m_ComplexSymbolsDefs[vecWords[uiPos]];
         auto it = allChars.begin();
         while (it != allChars.end())
         {
            SimpleRegEx *psre = new SimpleRegEx(*it, *it);
            if (bExtendLastOr)
               static_cast<ORRegEx*>(vecRegEx[vecRegEx.size() - 1])->AddOR(new SimpleRegEx(*it, *it));
            else
               vecRegEx.push_back(psre);
            it++;
         }
      }
      else
      {
         SimpleRegEx *psre = new SimpleRegEx(vecWords[uiPos], SymbolID);
         if (bExtendLastOr)
            static_cast<ORRegEx*>(vecRegEx[vecRegEx.size() - 1])->AddOR(psre);
         else
            vecRegEx.push_back(psre);
      }

      uiPos++;
   }
}

} // namespace regexlib