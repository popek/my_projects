#include "RegExClasses.h"

namespace regexlib
{

void SNFADiagram::clear()
{
   _SymbolTranDiag.clear();
   _EmptyTranDiag.clear();
}

unsigned int SimpleRegEx::GenerateNFA(unsigned int uiNextStartStateNo)
{
   /*
   (uiNSN) --(token)-- (uiNSN+1)
   */
   m_pNFA->_SymbolTranDiag[uiNextStartStateNo].insert(t_Token2State(m_iSymbolID, uiNextStartStateNo + 1));
   return ++uiNextStartStateNo;
}

unsigned int ORRegEx::GenerateNFA(unsigned int uiNextStartStateNo)
{
   /*	                                                                                              (optionally - if not main reg ex)
                     null													                          null
                  |------ {(uiNSN+1) RegEx(0) (uiNSN+x)}--------------------------------------|
                  |	null																					  null |
                  |------ {(uiNSN+x+1) RegEx(1) (uiNSN+x+1+y1)}-------------------------------|
                  | null																					  null
   (uiNSN) --|------ {(uiNSN+x+1+y1 +1) RegEx(2) (uiNSN+x+1+y1+1+y2)}-------------------------| --- (uiNSN+x+n+y1+y2+...+y(n+1)+1)
                  .																									 .
                  .																									 .
                  .	null																					  null .
                  |------	{(uiNSN+x+n+y1+y2+...+yn) RegEx(n) (uiNSN+x+n+y1+y2+...+y(n+1))}---|
   */

   unsigned int uiStart = uiNextStartStateNo, i;
   std::vector<unsigned int> vecBranchesEnd;

   for (i = 0; i < m_vRE.size(); i++)
   {
      m_pNFA->_EmptyTranDiag[uiStart].insert(t_Token2State(0, ++uiNextStartStateNo));
      uiNextStartStateNo = m_vRE[i]->GenerateNFA(uiNextStartStateNo);

      if (m_bMainRegEx)//if it is main reg ex then all ends are accepting states
         m_pNFA->_uiNFAacceptStates.push_back(uiNextStartStateNo);
      else
         vecBranchesEnd.push_back(uiNextStartStateNo);
   }

   if (!m_bMainRegEx) // if it is not main regular expression (form then 
   {
      uiNextStartStateNo++;
      for (i = 0; i < vecBranchesEnd.size(); i++)
         m_pNFA->_EmptyTranDiag[vecBranchesEnd[i]].insert(t_Token2State(0, uiNextStartStateNo));
   }

   return uiNextStartStateNo;
}

unsigned int CompoundRegEx::GenerateNFA(unsigned int uiNextStartStateNo)
{
   for (unsigned int i = 0; i < m_vRE.size(); i++)
      uiNextStartStateNo = m_vRE[i]->GenerateNFA(uiNextStartStateNo);
   return uiNextStartStateNo;
}

unsigned int MULRegEx::GenerateNFA(unsigned int uiNextStartStateNo)
{
   /*													null
                          |--------------------|
                          |						  |
               null		 \|/						  |		  null
   (uiNSN)------ {(uiNSN+1) BaseRegEx (uiNSN+x)}------------(uiNSN+x+1)
               |																	|
               |										null						|
               |--------------------------------------------------|
   */

   unsigned int uiGoBack, uiStart = uiNextStartStateNo;
   m_pNFA->_EmptyTranDiag[uiStart].insert(t_Token2State(0, ++uiNextStartStateNo));
   uiGoBack = uiNextStartStateNo;

   uiNextStartStateNo = m_BaseRegEx->GenerateNFA(uiNextStartStateNo);
   m_pNFA->_EmptyTranDiag[uiNextStartStateNo].insert(t_Token2State(0, uiGoBack));
   m_pNFA->_EmptyTranDiag[uiNextStartStateNo].insert(t_Token2State(0, uiNextStartStateNo + 1));
   m_pNFA->_EmptyTranDiag[uiStart].insert(t_Token2State(0, uiNextStartStateNo + 1));

   return ++uiNextStartStateNo;
}


void RegEx::SetNFA(SNFADiagram* pnfa)
{
   m_pNFA = pnfa;
}

void SimpleRegEx::SetNFA(SNFADiagram* pnfa)
{
   RegEx::SetNFA(pnfa);
}

void ORRegEx::SetNFA(SNFADiagram* pnfa)
{
   RegEx::SetNFA(pnfa);
   for (unsigned int i = 0; i < m_vRE.size(); i++)
      m_vRE[i]->SetNFA(pnfa);
}

void CompoundRegEx::SetNFA(SNFADiagram* pnfa)
{
   RegEx::SetNFA(pnfa);
   for (unsigned int i = 0; i < m_vRE.size(); i++)
      m_vRE[i]->SetNFA(pnfa);
}

void MULRegEx::SetNFA(SNFADiagram* pnfa)
{
   RegEx::SetNFA(pnfa);
   m_BaseRegEx->SetNFA(pnfa);
}


SimpleRegEx::SimpleRegEx(unsigned char c, short SymbolID) :
   RegEx(eRE_Simple),
   m_iSymbolID(SymbolID)
{
   m_sSymbolName = " ";
   m_sSymbolName[0] = c;
}

} // namespace regexlib