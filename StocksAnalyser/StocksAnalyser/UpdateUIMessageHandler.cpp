#include "StdAfx.h"
#include "UpdateUIMessageHandler.h"
#include "SearchGMMAsignalsDlg.h"


const UINT G_updateUIMessageID   = (WM_USER + 1);

const UINT G_updateProgress      = 1;
const UINT G_setProgressRange    = 2;
const UINT G_addNewAssetToList   = 3;
const UINT G_afterProcessing     = 4;


UpdateUIMessageHandler::UpdateUIMessageHandler(SearchGMMAsignalsDlg* a_pOwner)
: m_pOwner(a_pOwner)
{
}
UpdateUIMessageHandler::~UpdateUIMessageHandler(void)
{
}

void UpdateUIMessageHandler::HandleQueuedMessages()
{
   UINT       messageID = 0;
   QueueData  data;
   for(size_t dataIdx=0; dataIdx<m_deqMessages.size(); ++dataIdx)
   {
      m_csQueueLock.Enter();

      data = m_deqMessages.front();
      m_deqMessages.pop_front();

      m_csQueueLock.Leave();

      HandleData(data);
   }
}

BOOL UpdateUIMessageHandler::TryToHandle(const UINT& a_msgId)
{
   if(a_msgId == G_updateUIMessageID)
   {
      HandleQueuedMessages();
      return TRUE;
   }

   return FALSE;
}

void UpdateUIMessageHandler::PostUpdateProgress(CString a_strLabel, int a_progressPos)
{
   m_csQueueLock.Enter();

   QueueData newData;
   newData.m_messageID = G_updateProgress;
   newData.m_text      = a_strLabel;
   newData.m_number    = a_progressPos;
   m_deqMessages.push_back(newData);

   m_csQueueLock.Leave();

   m_pOwner->PostMessage(G_updateUIMessageID, 0, 0);

}

void UpdateUIMessageHandler::PostSetProgressRange(int a_range)
{
   m_csQueueLock.Enter();

   QueueData newData;
   newData.m_messageID = G_setProgressRange;
   newData.m_number    = a_range;
   m_deqMessages.push_back(newData);

   m_csQueueLock.Leave();

   m_pOwner->PostMessage(G_updateUIMessageID, 0, 0);
}

void UpdateUIMessageHandler::PostAddNewAsset(CString a_assetFilePath)
{
   m_csQueueLock.Enter();

   QueueData newData;
   newData.m_messageID = G_addNewAssetToList;
   newData.m_text      = a_assetFilePath;
   m_deqMessages.push_back(newData);

   m_csQueueLock.Leave();

   m_pOwner->PostMessage(G_updateUIMessageID, 0, 0);
}

void UpdateUIMessageHandler::PostAfterProcessing()
{
   m_csQueueLock.Enter();

   QueueData newData;
   newData.m_messageID = G_afterProcessing;
   m_deqMessages.push_back(newData);

   m_csQueueLock.Leave();

   m_pOwner->PostMessage(G_updateUIMessageID, 0, 0);
}

void UpdateUIMessageHandler::HandleData(UpdateUIMessageHandler::QueueData& a_dataToHandle)
{
   switch(a_dataToHandle.m_messageID)
   {
   case G_updateProgress:
      m_pOwner->UpdateProgress(a_dataToHandle.m_text, a_dataToHandle.m_number);
      break;

   case G_setProgressRange:
      m_pOwner->SetProgressRange(a_dataToHandle.m_number);
      break;

   case G_addNewAssetToList:
      m_pOwner->AddNewAssetToTheList(a_dataToHandle.m_text);
      break;

   case G_afterProcessing:
      m_pOwner->AfterProcessing();
      break;
   }
}