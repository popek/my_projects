#pragma once

class ValueScalePainter
{
public:
   ValueScalePainter(void);
   virtual ~ValueScalePainter(void);

   int   Width() const;
   void  Width(int a_nNewPaintAreadWidth);

   int   Height() const;
   void  Height(int a_nNewPaintAreadHeight);

   int   CalculateMinWidth();

   void  UpdateMinMax(double a_dNewMin, double a_dNewMax);

   virtual void Draw(CDC& a_rDC, int a_nXOffset);


protected:
   
   int m_nPaintAreaWidth;
   int m_nPaintAreaHeight;

   double m_dMinValue;
   double m_dMaxValue;

   double m_dValueModule;

   double m_dValue2Pixel;

   CRect  m_rcValue;
   CPen   m_Pen;
   CFont  m_font;
};
