#pragma once
#include <string>

namespace lrlib
{

enum eTermID 
{
   e_eval_to ,
   e_identifier ,
   e_or ,
   e_semicolon ,
   e_single_char_token ,
   e_eof, 
  
   e_blank, 
   e_eol, 
   e_single_line_comment_start, 
   e_multi_line_comment_start, 
   e_multi_line_comment_finish, 

   e_error
};

class LexAtom
{
public:
  LexAtom(void);
  virtual ~LexAtom(void);

  int     m_iLine;
  eTermID m_ID;
  std::string  m_strText;
  double  m_dValue;
  int     m_iValue;
};

} // namespace lrlib