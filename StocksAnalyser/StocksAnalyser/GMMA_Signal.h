#pragma once
#include "AssetFinderBase.h"

class IPointer;
class AssetContainer;
class Asset;

class GMMA_Signal : public AssetFinderBase
{
public:
   GMMA_Signal(shared_ptr<AssetContainer> a_spAssetContainer);
   virtual ~GMMA_Signal(void);

protected:

   double   CalcShortTermPointersMean(size_t a_sampleOffset) const;

   double   CalcLongTermPointersMean(size_t a_sampleOffset) const;

   double   GetShortTermsMeansMinimum(const size_t& a_offset) const;

   double   GetShortTermsMeansMaximum(const size_t& a_offset) const;

   double   GetLongTermsMeansMinimum(const size_t& a_offset) const;

   double   GetLongTermsMeansMaximum(const size_t& a_offset) const;

   bool     CheckPointersSamplesAmount(const size_t a_minSamplesAmount) const;

   size_t   MinimalSamplesCount() const;

protected:

   mutable shared_ptr<Asset>            m_spTemporaryAsset;
   mutable vector<shared_ptr<IPointer>> m_vTemporaryPointers;
   mutable size_t                       m_shortPeriodsPointersAmount;
};

