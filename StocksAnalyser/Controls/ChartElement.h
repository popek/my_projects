
#pragma once
#include "ControlsLib.h"
#include "ChartPoint.h"
#include "EChartElementId.h"


class GraphicObjectBase;
class ValueScaleContext;
class TimeScaleContext;
class GrxStyle;


class CONTROLS_LIB ChartElement
{
   NONCOPYABLE_TYPE(ChartElement)

public:
   ChartElement(EChartElementId a_elementId);
   ChartElement(EChartElementId a_elementId, size_t a_totalPointsCount);
   virtual ~ChartElement(void);

   virtual shared_ptr<GraphicObjectBase> CreateGraphicObject(const TimeScaleContext& a_timeScaleContext, const ValueScaleContext& a_valueScaleContext) const;

   void AddPoint(const ChartPoint& a_chartPoint);
   
   void AddPoint(shared_ptr<KandleDate> a_spDate, double a_value);

   void Style(shared_ptr<GrxStyle> a_style);

   shared_ptr<GrxStyle> Style() const;

   void OwnValueScaleContext(shared_ptr<ValueScaleContext> a_spOwneValueScaleContext);
   shared_ptr<ValueScaleContext> OwnValueScaleContext();

protected:

   shared_ptr<GraphicObjectBase> CreateGraphicObject(const TimeScaleContext& a_timeScaleContext, const ValueScaleContext& a_valueScaleContext, 
                                                     size_t a_startPointIndex, size_t a_endPointIndex) const;

protected:
   vector<ChartPoint> m_points;
   EChartElementId m_elementId;
   shared_ptr<GrxStyle> _elementStyle;

   shared_ptr<ValueScaleContext> _spOwnValueScaleContex;

private:
   mutable shared_ptr<GraphicObjectBase> m_spGraphicObject;
};
