#include "stdafx.h"
#include "Scanner.h"
#include "LexAtom.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


static int LexDFA[][] =
{
	// copy from generated file !!!
};

static short Char2ColID[256] =
{
	// copy from generated file !!!
};

static int AcceptedStates[] =
{
	// copy from generated file !!!
};


//
Scanner::Scanner(string_t a_scriptFilePath)
{
}

ETerminalId Scanner::GetAtom(LexAtom* a_pLexAtom)
{
   int     state     = 0;
   char_t  character = 0;
   int     charsCnt  = 0;
   stack<int> steps;

   if(eof())
   {
      a_pLexAtom->text(_T(""));
      a_pLexAtom->id(Eeof);
      return a_pLexAtom->id();
   }

   while(state>-1)
   {
      character = readCharacter();

      // save next token character
      _token[charsCnt] = character;
      ++charsCnt;

      steps.push(state);

      // if character is eof then break
      if(0 == character)
      {
         break;
      }

      // test if this is forbidden character
      if(-1 == Char2ColID[character])
      {
         break;
      }

      // move to another state
      state = LexDFA[state][Char2ColID[character]];
   }

   //remember last atom text (in case atom will be invalid)
   _token[charsCnt] = _T('\0');
   a_pLexAtom->text(_token);

   size_t forwardOffsetInCaseOfError = 0;

   while(steps.size())
   {
      // "give back" last red character
      moveToPrevChar();
      ++forwardOffsetInCaseOfError;
      --charsCnt;
      _token[charsCnt] = _T('\0');

      state = steps.top();
      steps.pop();

      // if "remembered" state is accepting state then return it
      if(AcceptedStates[state]>-1)
      {
         a_pLexAtom->text(_token);
         a_pLexAtom->id((ETerminalId)AcceptedStates[state]);

         FixIfNecessary(a_pLexAtom);

         return a_pLexAtom->id();
      }
   }
   
   while(forwardOffsetInCaseOfError)
   {
      readCharacter();
      --forwardOffsetInCaseOfError;
   }

   if(eof())
   {
      a_pLexAtom->text(_T(""));
      a_pLexAtom->id(Eeof);
   }
   else
   {
      // when we are here it means that error character occured in script file and 
      ASSERT(charsCnt == 0);
      _token[charsCnt]   = character; 
      _token[charsCnt+1] = _T('\0');
      a_pLexAtom->text(_token);
      a_pLexAtom->id(Eerror);
   }

   return a_pLexAtom->id();
}

