#include "StdAfx.h"
#include "AssetLabel.h"
#include <Shlwapi.h>
#include <atlpath.h>

AssetLabel::AssetLabel(void)
: m_aggreagationType(eDay)
{
}
AssetLabel::~AssetLabel(void)
{
}

void AssetLabel::AssetAggreagation(EAggregationType a_aggregation)
{
   m_aggreagationType = a_aggregation;
}

EAggregationType AssetLabel::AssetAggreagation() const
{
   return m_aggreagationType;
}

void AssetLabel::SetName(CString a_assetFilePath)
{
   CPath path(a_assetFilePath);

   int   fileNameIdx = path.FindFileName();

   m_name = a_assetFilePath.Mid(fileNameIdx, path.FindExtension() - fileNameIdx);
}

CString AssetLabel::AssetName() const
{
   return m_name;
}

