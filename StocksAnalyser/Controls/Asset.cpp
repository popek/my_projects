#include "StdAfx.h"
#include "Asset.h"
#include "CSVFileReader.h"
#include "KandleDate.h"
#include <algorithm>



Asset::Asset(void)
: m_bLoaded(false)
, m_dMinVolume(-1)
, m_dMaxVolume(-1)
, m_dMinVal(-1)
, m_dMaxVal(-1)
{
}

Asset::~Asset(void)
{
}

void Asset::ReadFromFile(const MY_CHAR* pcFilePath)
{
   CCSVFileReader oCsvReader;
   m_vKandles.clear();
   oCsvReader.ReadFromFile(pcFilePath);
   oCsvReader.Erase(0);

   for(size_t nKandleIdx = 0; nKandleIdx < oCsvReader.Size(); nKandleIdx++)
   {
      m_vKandles.push_back(shared_ptr<Kandle>(new Kandle(oCsvReader[nKandleIdx], nKandleIdx)));
   }

   m_label.SetName(pcFilePath);
   m_label.AssetAggreagation(eDay);

   m_bLoaded = true;
}

vector<shared_ptr<Kandle>> Asset::GrabbKandles(size_t a_nFirstIdx, size_t a_nLastIdx)
{
   vector<shared_ptr<Kandle>> vRet;                       // return vector is empty on begin
   
   size_t nCnt;

   a_nLastIdx = __min(m_vKandles.size()-1, a_nLastIdx); // if last index is greater then size of asset then use last index of asset

   if(a_nFirstIdx < m_vKandles.size() &&      // if first index is lower then size of asset
      a_nFirstIdx <= a_nLastIdx )               // if first index is lower or equal last index
   {
      for(nCnt = a_nFirstIdx; nCnt <= a_nLastIdx; nCnt++ ) 
      {
         vRet.push_back(m_vKandles[nCnt]);
      }
   }  

   return vRet;
}

void Asset::GetMinMax(double& a_rdMin, double& a_rdMax) const
{
   if(m_dMinVal < 0 || m_dMaxVal < 0)
   {
      GetMinMax(0, Size()-1, m_dMinVal, m_dMaxVal);
   }

   a_rdMin = m_dMinVal;
   a_rdMax = m_dMaxVal;
}

void Asset::GetMinMax(size_t a_nFirstIdx, size_t a_nLastIdx, double& a_rdMin, double& a_rdMax) const
{
   size_t nCnt;
   a_nLastIdx = __min(m_vKandles.size()-1, a_nLastIdx); // if last index is greater then size of asset then use last index of asset

   if(a_nFirstIdx < m_vKandles.size() &&      // if first index is lower then size of asset
      a_nFirstIdx <= a_nLastIdx )               // if first index is lower or equal last index
   {
      a_rdMin = m_vKandles[a_nFirstIdx]->Low();
      a_rdMax = m_vKandles[a_nFirstIdx]->High();

      for(nCnt = a_nFirstIdx + 1; nCnt <= a_nLastIdx; nCnt++ ) 
      {
         a_rdMax = __max(m_vKandles[nCnt]->High(), a_rdMax);
         a_rdMin = __min(m_vKandles[nCnt]->Low(), a_rdMin);
      }
   }  
}

void Asset::GetVolumeMinMax(double& a_rdMin, double& a_rdMax) const
{
   if(m_dMinVolume < 0 || m_dMaxVolume < 0)
   {
      GetVolumeMinMax(0, Size()-1, m_dMinVolume, m_dMaxVolume);
   }

   a_rdMin = m_dMinVolume;
   a_rdMax = m_dMaxVolume;
}

void Asset::GetVolumeMinMax(size_t a_nFirstIdx, size_t a_nLastIdx, double& a_rdMin, double& a_rdMax) const
{
   size_t nCnt;
   a_nLastIdx = __min(m_vKandles.size()-1, a_nLastIdx); // if last index is greater then size of asset then use last index of asset

   if(a_nFirstIdx < m_vKandles.size() &&      // if first index is lower then size of asset
      a_nFirstIdx <= a_nLastIdx )               // if first index is lower or equal last index
   {
      a_rdMin = (double)m_vKandles[a_nFirstIdx]->Volume();
      a_rdMax = a_rdMax;

      for(nCnt = a_nFirstIdx + 1; nCnt <= a_nLastIdx; nCnt++ ) 
      {
         a_rdMax = __max(m_vKandles[nCnt]->Volume(), a_rdMax);
         a_rdMin = __min(m_vKandles[nCnt]->Volume(), a_rdMin);
      }
   }  
}

size_t Asset::Size() const
{
   return m_vKandles.size();
}

Kandle& Asset::operator [] (size_t a_nIdx)
{
   return *m_vKandles[a_nIdx];
}

const Kandle& Asset::operator [] (size_t a_nIdx) const
{
   return *m_vKandles[a_nIdx];
}

const Kandle& Asset::GetKandle(size_t a_kandleIndex) const
{
   return *m_vKandles[a_kandleIndex];
}

bool Asset::Loaded() const
{
   return m_bLoaded;
}

void Asset::Loaded(bool a_loaded)
{
   m_bLoaded = true;
}

// finds nearest kandle (grater or equal to reqested one)
size_t Asset::FindNearestKandle(const KandleDate& a_dateToFind)
{
   struct pred
   {
      bool operator()(const shared_ptr<Kandle>& a_kandle, const shared_ptr<Kandle>& a_baseKandle) const
      {	
         return (*(a_kandle->Date()) < *(a_baseKandle->Date()));
      }
   };

   vector<shared_ptr<Kandle>>::iterator itKandle = 
      lower_bound(m_vKandles.begin(), m_vKandles.end(), 
                  Kandle::CreateNullObject(a_dateToFind), pred());

   return (itKandle != m_vKandles.end() ? (*itKandle)->Date()->Index() : m_vKandles.size()-1);   
}


void Asset::AppendKandle(shared_ptr<Kandle> a_spKandle)
{
   m_vKandles.push_back(a_spKandle);
}

void Asset::ExtractSamplesDates(vector<shared_ptr<KandleDate>>& a_datesVector) const
{
   a_datesVector.clear();
   a_datesVector.reserve(m_vKandles.size());

   for(size_t sampleIndex=0; sampleIndex<m_vKandles.size(); ++sampleIndex)
   {
      a_datesVector.push_back( m_vKandles[sampleIndex]->Date() );
   }
}

AssetLabel& Asset::Label()
{
   return m_label;
}

void Asset::AttachElementsCollection(shared_ptr<ChartElementsCollection> a_spElementsCollection)
{
   m_spChartElementsCollection = a_spElementsCollection;
}

shared_ptr<ChartElementsCollection> Asset::ElementsCollection() const
{
   return m_spChartElementsCollection;
}