#include "StdAfx.h"
#include "ParallelChannelFinder.h"
#include "AssetContainer.h"
#include "Asset.h"
#include "CommonTypes.h"
#include "EChartElementId.h"
#include "ChartElementsCollection.h"
#include "ChartElement.h"
#include "AssetAuxiliaryWrappers.h"
#include "MathUtils.h"
#include "SMA_pointer.h"
#include "ChannelFinderContext.h"
#include "ControlsLibUtils.h"
#include "DataSeries.h"


static const size_t s_invalidIndex = -1;


ParallelChannelFinder::LineFinder::LineFinder(const ChannelFinderContext& a_context,
                                              shared_ptr<Asset>  a_spAsset,
                                              const DataSeries& a_sourceData,
                                              size_t             a_sourceDataIndexOfsset,
                                              size_t             a_channelPeriod)
: m_context(a_context)
, m_spAsset(a_spAsset)
, m_sourceData(a_sourceData)
, m_sourceDataIndexOfsset(a_sourceDataIndexOfsset)
, m_channelPeriod(a_channelPeriod)
{
}

void ParallelChannelFinder::LineFinder::FillGraphicElements(shared_ptr<ChartElementsCollection> a_spElementsCollection)
{
   // attach extremum points
   for(size_t extremumIndex=0; extremumIndex<m_extremumsIndexes.size(); ++extremumIndex)
   {
      if(s_invalidIndex == m_extremumsIndexes[extremumIndex])
      {
         continue;
      }

      const Kandle& kandle = (*m_spAsset)[ m_extremumsIndexes[extremumIndex] + m_sourceDataIndexOfsset ];

      shared_ptr<ChartElement> spPoint(new ChartElement(EPoint));
      spPoint->AddPoint( ChartPoint(kandle.Date(), m_sourceData[m_extremumsIndexes[extremumIndex]]) );

      a_spElementsCollection->PushBack(spPoint);
   }

   shared_ptr<ChartElement> spAuxOrgLine(new ChartElement(ELine));
   
   Kandle& auxKandle1 = (*m_spAsset)[m_firstFoundExtremumIndex + m_sourceDataIndexOfsset];
   spAuxOrgLine->AddPoint( ChartPoint(auxKandle1.Date(), m_calculator.A()*(double)m_firstFoundExtremumIndex + m_calculator.B()) );
   
   Kandle& auxKandle2 = (*m_spAsset)[m_lastFoundExtremumIndex + m_sourceDataIndexOfsset];
   spAuxOrgLine->AddPoint( ChartPoint(auxKandle2.Date(), m_calculator.A()*(double)m_lastFoundExtremumIndex + m_calculator.B()) );

   a_spElementsCollection->PushBack(spAuxOrgLine);
}

bool ParallelChannelFinder::LineFinder::Find()
{
   size_t offsetFromEnd = 0;
   size_t localMinimumIndex = 0;
   while( true == FindLocalExtremum(offsetFromEnd, localMinimumIndex) )
   {
      // update offset to be used during next loop
      offsetFromEnd = m_sourceData.size() - localMinimumIndex;

      m_extremumsIndexes.push_front(localMinimumIndex);
   } 

   if( m_extremumsIndexes.size() > (size_t)m_context.MinLinePointsCount() )
   {
      m_firstFoundExtremumIndex = *m_extremumsIndexes.begin();
      m_lastFoundExtremumIndex = *(m_extremumsIndexes.end()-1);

      if( true == IsLineAvailable() )
      {
         size_t extremumIndex = 0;
         while(extremumIndex < m_extremumsIndexes.size())
         {
            if(s_invalidIndex == m_extremumsIndexes[extremumIndex])
            {
               m_extremumsIndexes.erase(m_extremumsIndexes.begin() + extremumIndex);
            }
            else
            {
               ++extremumIndex;
            }
         }

         return true;
      }
   }

   return false;
}

size_t ParallelChannelFinder::LineFinder::LastExtremumIndex() const
{
   return m_extremumsIndexes.back();
}

bool ParallelChannelFinder::LineFinder::BetweenLastTwo(size_t a_index) const
{
   return ( a_index <= m_extremumsIndexes[m_extremumsIndexes.size()-1] && 
            a_index  >= m_extremumsIndexes[m_extremumsIndexes.size()-2] );
}

bool ParallelChannelFinder::LineFinder::FindLocalExtremum(size_t a_samplesOffset, size_t& a_localExtremumIndex)
{
   size_t lastSampleIdx = m_sourceData.size() - 1 - m_channelPeriod;

   size_t sampleIdx = m_sourceData.size() - 1 - a_samplesOffset;
   double previousValue = m_sourceData[sampleIdx];
   double currentValue = 0;
   --sampleIdx;

   for(; sampleIdx > lastSampleIdx; --sampleIdx)
   {
      currentValue = m_sourceData[sampleIdx];

      if( ExtremumCheckRequired(currentValue, previousValue) )
      {
         // check if previously checked sample is local minimum
         if( true == IsLocalExtremum(sampleIdx + 1) )
         {
            a_localExtremumIndex = sampleIdx + 1;
            return true;
         }
      }
      previousValue = currentValue;
   }

   return false;

}

bool ParallelChannelFinder::LineFinder::IsLocalExtremum(size_t a_sampleIndex)
{
   size_t naigbersCount = 3; // we will check number of samples "on the left" and the same number of samples "on the right"

   size_t mostLeftNeighbourIndex = a_sampleIndex - naigbersCount;
   size_t mostRightNeigbourIndex = a_sampleIndex + naigbersCount;
   if(mostRightNeigbourIndex >= m_sourceData.size())
   {
      mostRightNeigbourIndex = m_sourceData.size() - 1;
   }

   double prevValue = m_sourceData[a_sampleIndex];
   // check "neighbours on the right"
   for(size_t neighbourIndex = a_sampleIndex + 1; neighbourIndex <= mostRightNeigbourIndex; ++neighbourIndex)
   {
      if( ExtremumNeighbourValid(m_sourceData[neighbourIndex], prevValue) )
      {
         prevValue = m_sourceData[neighbourIndex];
      }
      else
      {
         return false;
      }
   }

   prevValue = m_sourceData[a_sampleIndex];
   // check "neighbours on the left"
   for(size_t neighbourIndex = a_sampleIndex -1; neighbourIndex >= mostLeftNeighbourIndex; --neighbourIndex)
   {
      if( ExtremumNeighbourValid(m_sourceData[neighbourIndex], prevValue) )
      {
         prevValue = m_sourceData[neighbourIndex];
      }
      else
      {
         return false;
      }
   }

   // in we get here, it means minimum value had been found
   return true;
}

bool ParallelChannelFinder::LineFinder::IsLineAvailable()
{
   deque<size_t> originalMinimums = m_extremumsIndexes;

   bool   recalculateNewDataSet  = true;
   double deviation2value        = m_context.MaxDev2LineValue();
   size_t currentDataSetSize     = m_extremumsIndexes.size() + 1;

   while(true == recalculateNewDataSet)
   {
      // decrement current data size
      --currentDataSetSize;

      if(2 == currentDataSetSize)
      {
         break;
      }

      // prepare args (minimum indexes) and values (data source value at those indexes) for line calculator
      PrepareArgumentsAndValuesForLineCalculator();

      // calculate potential channel bottom line parameters
      m_calculator.Calculate(m_tempLineArgs, m_tempLineValues);
      // check if bottom line condition is fulfilled
      recalculateNewDataSet = !IsLineConditionFullfilled(deviation2value, true);
   }

   // check if data source sample between first and last minimum fulfill apropriate condition
   if(false == IsDataSourceValuesConditionFullfilled((size_t)m_tempLineArgs[0], (size_t)m_tempLineArgs.back()))
   {
      return false;
   }

   // backtrack (try to add removed minimums backward to have longer bottom line)
   deviation2value *=1.1;  // when backtracking increase deviation 2 value factor
   for(int extremumIndex=(int)m_extremumsIndexes.size()-1; extremumIndex>=0; --extremumIndex)
   {
      if(m_extremumsIndexes[extremumIndex] == s_invalidIndex)
      {
         m_extremumsIndexes[extremumIndex] = originalMinimums[extremumIndex];

         PrepareArgumentsAndValuesForLineCalculator();
         m_calculator.Calculate(m_tempLineArgs, m_tempLineValues);

         // if at least one check below does not pass then remove checked minimum
         if(false == IsLineConditionFullfilled(deviation2value, false) ||
            false == IsDataSourceValuesConditionFullfilled((size_t)m_tempLineArgs[0], (size_t)m_tempLineArgs.back()))
         {
            m_extremumsIndexes[extremumIndex] = s_invalidIndex;
         }
         else
         {
            // mark that minimums amount had been increased
            ++currentDataSetSize;
         }
      }
   }

   if(currentDataSetSize < (size_t)m_context.MinLinePointsCount())
   {
      return false;
   }

   PrepareArgumentsAndValuesForLineCalculator();
   m_calculator.Calculate(m_tempLineArgs, m_tempLineValues);

   return true;
}

void ParallelChannelFinder::LineFinder::PrepareArgumentsAndValuesForLineCalculator()
{
   m_tempLineArgs.clear();
   m_tempLineValues.clear();

   size_t valueIndex = 0;
   // prepare points fro calculating line coefficients
   for(size_t sampleIdx=0; sampleIdx < m_extremumsIndexes.size(); ++sampleIdx)
   {
      if(m_extremumsIndexes[sampleIdx] == s_invalidIndex)
      {
         continue;
      }

      valueIndex = m_extremumsIndexes[sampleIdx];

      m_tempLineArgs.push_back(valueIndex);
      m_tempLineValues.push_back(m_sourceData[valueIndex]);
   }
}

bool ParallelChannelFinder::LineFinder::IsLineConditionFullfilled(double a_maxDev2Value, bool a_removeInvalidMinimumEntry)
{
   // validate minimums
   size_t valueIndex = 0;
   for(size_t sampleIdx=0; sampleIdx < m_extremumsIndexes.size(); ++sampleIdx)
   {
      if(m_extremumsIndexes[sampleIdx] == s_invalidIndex)
      {
         continue;
      }

      // if sample deviation is grater then acceptable sample deviation then sample will be removed and calculation will be performed on
      // new (one sample less) data set
      if(m_calculator.Dev2Value(valueIndex) >= a_maxDev2Value)
      {
         if(true == a_removeInvalidMinimumEntry)
         {
            m_extremumsIndexes[sampleIdx] = s_invalidIndex;
         }
         return false;
      }

      ++valueIndex;
   }

   return true;
}

bool ParallelChannelFinder::LineFinder::IsDataSourceValuesConditionFullfilled(size_t a_dataSourceStartIndex, size_t a_dataSourceEndIndex)
{
   // check if each and every sample between minimums is above (or near) the line 
   for(size_t dataSurceSampleIndex = a_dataSourceStartIndex; dataSurceSampleIndex <= a_dataSourceEndIndex; ++dataSurceSampleIndex)
   {
      double lineValue = (double)dataSurceSampleIndex * m_calculator.A() + m_calculator.B();

      // condition is that source data are above line value (above near of line value)
      if( false == DataSourcValueValid(m_sourceData[dataSurceSampleIndex], lineValue) )
      {
         return false;
      }
   }

   return true;
}





ParallelChannelFinder::BottomLineFinder::BottomLineFinder(const ChannelFinderContext& a_context, shared_ptr<Asset> a_spAsset, 
                                                          const DataSeries& a_sourceData, size_t a_sourceDataIndexOfsset, 
                                                          size_t a_channelPeriod)
    : LineFinder(a_context, a_spAsset, a_sourceData, a_sourceDataIndexOfsset, a_channelPeriod)
{
}

bool ParallelChannelFinder::BottomLineFinder::ExtremumCheckRequired(double a_currentValue, double a_previousValue)
{
   // if current is grater then previous can be minimum
   return a_currentValue > a_previousValue;
}

bool ParallelChannelFinder::BottomLineFinder::ExtremumNeighbourValid(double a_neigbhourValue, double a_lastMinValue)
{
   // neigbour must be grater of equal to minimum value
   return a_neigbhourValue >= a_lastMinValue;
}

bool ParallelChannelFinder::BottomLineFinder::DataSourcValueValid(double a_dataSourceValue, double a_lineValue)
{
   // data value must be obove line value (above value near line value)
   return a_dataSourceValue > (1 - m_context.MaxData2LineValueDistance())*a_lineValue;
}








// ----------- top line finder -------------
ParallelChannelFinder::TopLineFinder::TopLineFinder(const ChannelFinderContext& a_context, shared_ptr<Asset> a_spAsset, 
                                                    const DataSeries& a_sourceData, size_t a_sourceDataIndexOfsset, 
                                                    size_t a_channelPeriod)
   : LineFinder(a_context, a_spAsset, a_sourceData, a_sourceDataIndexOfsset, a_channelPeriod)
{
}

bool ParallelChannelFinder::TopLineFinder::ExtremumCheckRequired(double a_currentValue, double a_previousValue)
{
   // if current is less then previous can be maximum
   return a_currentValue < a_previousValue;
}

bool ParallelChannelFinder::TopLineFinder::ExtremumNeighbourValid(double a_neigbhourValue, double a_lastMinValue)
{
   // neigbour must be less of equal to maximum value
   return a_neigbhourValue <= a_lastMinValue;
}

bool ParallelChannelFinder::TopLineFinder::DataSourcValueValid(double a_dataSourceValue, double a_lineValue)
{
   // data value must be below line value (below value near line value)
   return a_dataSourceValue < (1 + m_context.MaxData2LineValueDistance())*a_lineValue;
}







// -- main class ---
ParallelChannelFinder::ParallelChannelFinder(const ChannelFinderContext& a_context)
: m_context(a_context)
{
   // update period since it can be changed
   m_channelPeriod = m_context.Period();
}
ParallelChannelFinder::~ParallelChannelFinder(void)
{
}


bool ParallelChannelFinder::VerifyAssetConditions()
{
   switch(m_context.AlgType())
   {
   case EChannel:
      return VerifyChannelNearSupportLine() || VerifyChannelNearResistanceLine();

   case ESupportLine:
      return VerifySupportLine();

   case EResistanceLine:
      return VerifyResistanceLine();

   default:
      SA_ASSERT(FALSE);
      return false;
   }
}

bool ParallelChannelFinder::VerifyChannelNearSupportLine()
{
   const int lastMinimumIndexMaxOffset = 2;

   const int smaPeriod = 3;
   shared_ptr<Asset> spDailyAsset = (*m_spAsset)[eDay];

   if(spDailyAsset->Size() <= (smaPeriod + lastMinimumIndexMaxOffset))
   {
      return false;
   }

   if(spDailyAsset->Size() - (size_t)smaPeriod < (size_t)m_channelPeriod)
   {
      m_channelPeriod = spDailyAsset->Size() - (size_t)smaPeriod;
   }

   SMA_pointer smaFromLowestValues(smaPeriod);
   smaFromLowestValues.Create(AssetLowValueGetter(*spDailyAsset));
   BottomLineFinder bottomLineFinder(m_context, spDailyAsset, smaFromLowestValues.Series(0), smaPeriod - 1, m_channelPeriod);


   if(true == bottomLineFinder.Find())
   {
      Kandle& lastKandle = (*spDailyAsset)[spDailyAsset->Size() - 1];

      // if last minimum is NOT near the end, or kandle is decreasing one
      if(bottomLineFinder.LastExtremumIndex() < smaFromLowestValues.Series(0).size() - lastMinimumIndexMaxOffset ||
         false == CheckConfirmationCondition(lastKandle))
      {
         return false;
      }

      SMA_pointer smaFromHighestValues(smaPeriod);
      smaFromHighestValues.Create(AssetHighValueGetter(*spDailyAsset));
      TopLineFinder topLineFinder(m_context, spDailyAsset, smaFromHighestValues.Series(0), smaPeriod - 1, m_channelPeriod);

      if(true == topLineFinder.Find())
      {
         // last minimum index must be between two last maximums
         if( false == bottomLineFinder.BetweenLastTwo(topLineFinder.LastExtremumIndex()) )
         {
            return false;
         }

         shared_ptr<ChartElementsCollection> spElements(new ChartElementsCollection());

         bottomLineFinder.FillGraphicElements(spElements);
         spElements->PushFront( ControlsLibUtils::CreatePointerSeriesElement(smaFromLowestValues.Series(0), *spDailyAsset) );

         topLineFinder.FillGraphicElements(spElements);
         spElements->PushFront( ControlsLibUtils::CreatePointerSeriesElement(smaFromHighestValues.Series(0), *spDailyAsset) );

         spDailyAsset->AttachElementsCollection(spElements);

         return true;
      }
   }

   return false;
}

bool ParallelChannelFinder::VerifyChannelNearResistanceLine()
{
   const int lastMaximumIndexMaxOffset = 2;

   const int smaPeriod = 3;
   shared_ptr<Asset> spDailyAsset = (*m_spAsset)[eDay];

   if(spDailyAsset->Size() <= (smaPeriod + lastMaximumIndexMaxOffset))
   {
      return false;
   }

   if(spDailyAsset->Size() - (size_t)smaPeriod < (size_t)m_channelPeriod)
   {
      m_channelPeriod = spDailyAsset->Size() - (size_t)smaPeriod;
   }

   SMA_pointer smaFromHighestValues(smaPeriod);
   smaFromHighestValues.Create(AssetHighValueGetter(*spDailyAsset));
   TopLineFinder topLineFinder(m_context, spDailyAsset, smaFromHighestValues.Series(0), smaPeriod - 1, m_channelPeriod);


   if(true == topLineFinder.Find())
   {
      Kandle& lastKandle = (*spDailyAsset)[spDailyAsset->Size() - 1];

      
      // if last maximum is NOT near the end, or kandle is decreasing one
      if(topLineFinder.LastExtremumIndex() < smaFromHighestValues.Series(0).size() - lastMaximumIndexMaxOffset ||
         false == CheckConfirmationCondition(lastKandle))
      {
         return false;
      }

      SMA_pointer smaFromLowestValues(smaPeriod);
      smaFromLowestValues.Create(AssetLowValueGetter(*spDailyAsset));
      BottomLineFinder bottomLineFinder(m_context, spDailyAsset, smaFromLowestValues.Series(0), smaPeriod - 1, m_channelPeriod);

      if(true == bottomLineFinder.Find())
      {
         // last minimum index must be between two last maximums
         if( false == topLineFinder.BetweenLastTwo(bottomLineFinder.LastExtremumIndex()) )
         {
            return false;
         }

         shared_ptr<ChartElementsCollection> spElements(new ChartElementsCollection());

         bottomLineFinder.FillGraphicElements(spElements);
         spElements->PushFront( ControlsLibUtils::CreatePointerSeriesElement(smaFromLowestValues.Series(0), *spDailyAsset) );

         topLineFinder.FillGraphicElements(spElements);
         spElements->PushFront( ControlsLibUtils::CreatePointerSeriesElement(smaFromHighestValues.Series(0), *spDailyAsset) );

         spDailyAsset->AttachElementsCollection(spElements);

         return true;
      }
   }

   return false;
}

bool ParallelChannelFinder::VerifySupportLine()
{
   const int lastMinimumIndexMaxOffset = 2;

   const int smaPeriod = 3;
   shared_ptr<Asset> spDailyAsset = (*m_spAsset)[eDay];

   if(spDailyAsset->Size() <= (smaPeriod + lastMinimumIndexMaxOffset))
   {
      return false;
   }

   if(spDailyAsset->Size() - (size_t)smaPeriod < (size_t)m_channelPeriod)
   {
      m_channelPeriod = spDailyAsset->Size() - (size_t)smaPeriod;
   }

   SMA_pointer smaFromLowestValues(smaPeriod);

   smaFromLowestValues.Create(AssetLowValueGetter(*spDailyAsset));

   BottomLineFinder bottomLineFinder(m_context, spDailyAsset, smaFromLowestValues.Series(0), smaPeriod - 1, m_channelPeriod);

   if(true == bottomLineFinder.Find())
   {
      Kandle& lastKandle = (*spDailyAsset)[spDailyAsset->Size() - 1];

      // check if last minimum is near the end, and last kandle is "green"
      if(bottomLineFinder.LastExtremumIndex() >= smaFromLowestValues.Series(0).size() - lastMinimumIndexMaxOffset &&
         CheckConfirmationCondition(lastKandle) )
      {
         shared_ptr<ChartElementsCollection> spElements(new ChartElementsCollection());

         bottomLineFinder.FillGraphicElements(spElements);
         spElements->PushFront( ControlsLibUtils::CreatePointerSeriesElement(smaFromLowestValues.Series(0), *spDailyAsset) );

         spDailyAsset->AttachElementsCollection(spElements);

         return true;
      }
   }

   return false;
}

bool ParallelChannelFinder::VerifyResistanceLine()
{
   const int lastMaximumIndexMaxOffset = 2;

   const int smaPeriod = 3;
   shared_ptr<Asset> spDailyAsset = (*m_spAsset)[eDay];

   if(spDailyAsset->Size() <= (smaPeriod + lastMaximumIndexMaxOffset))
   {
      return false;
   }

   if(spDailyAsset->Size() - (size_t)smaPeriod < (size_t)m_channelPeriod)
   {
      m_channelPeriod = spDailyAsset->Size() - (size_t)smaPeriod;
   }

   SMA_pointer smaFromHighestValues(smaPeriod);

   smaFromHighestValues.Create(AssetHighValueGetter(*spDailyAsset));
   TopLineFinder topLineFinder(m_context, spDailyAsset, smaFromHighestValues.Series(0), smaPeriod - 1, m_channelPeriod);

   if(true == topLineFinder.Find())
   {
      Kandle& lastKandle = (*spDailyAsset)[spDailyAsset->Size() - 1];

      // check if last maximum is near the end, and last kandle is "green"
      if(topLineFinder.LastExtremumIndex() >= smaFromHighestValues.Series(0).size() - lastMaximumIndexMaxOffset &&
         CheckConfirmationCondition(lastKandle))
      {
         shared_ptr<ChartElementsCollection> spElements(new ChartElementsCollection());

         topLineFinder.FillGraphicElements(spElements);
         spElements->PushFront( ControlsLibUtils::CreatePointerSeriesElement(smaFromHighestValues.Series(0), *spDailyAsset) );

         spDailyAsset->AttachElementsCollection(spElements);

         return true;
      }
   }

   return false;
}

bool ParallelChannelFinder::CheckConfirmationCondition(const Kandle& a_kandle) const
{
   bool signalConfirmed = a_kandle.Close() >= a_kandle.Open();

   return m_context.ShowConfirmedSignal() ? signalConfirmed : !signalConfirmed;
}

