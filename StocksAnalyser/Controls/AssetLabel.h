#pragma once
#include "ControlsLib.h"
#include "CommonTypes.h"

class CONTROLS_LIB AssetLabel
{
public:
   AssetLabel(void);
   virtual ~AssetLabel(void);

   void             AssetAggreagation(EAggregationType a_aggregation); 
   EAggregationType AssetAggreagation() const;

   void             SetName(CString a_assetFilePath);
   CString          AssetName() const;

protected:

   CString           m_name;
   EAggregationType  m_aggreagationType;
};
