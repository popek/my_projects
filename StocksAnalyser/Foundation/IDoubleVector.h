#pragma once
#include "FoundationLib.h"


class FOUNDATION_LIB IDoubleVector abstract
{
public:
   virtual ~IDoubleVector(void);

   virtual double operator [](size_t a_elementIndex) const = 0;
   virtual size_t Size() const = 0;
};
