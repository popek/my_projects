#include "StdAfx.h"
#include "NumericStringValidator.h"


NumericStringValidator::NumericStringValidator(void)
: m_state(EStartState)
{
   m_zero = _T('0');
   m_nine = _T('9');
   m_separator = _T('.');
   m_minus = _T('-');
   m_plus = _T('+');
}
NumericStringValidator::~NumericStringValidator(void)
{
}

bool NumericStringValidator::ProperLeftSubstring(const CString& a_text) const
{
   m_state = EStartState;

   for(int charIndex=0; charIndex < a_text.GetLength(); ++charIndex)
   {
      if(false == ValidChar(a_text[charIndex]))
      {
         return false;
      }
   }

   return EStartState != m_state;
}

bool NumericStringValidator::ProperString(const CString& a_text) const
{
   if(false == ProperLeftSubstring(a_text))
   {
      return false;
   }

   return EFinalState == m_state;
}

bool NumericStringValidator::Valid(const TCHAR a_char) const
{
   return CharClass(a_char) != EInvalidChar;
}

bool NumericStringValidator::ValidChar(const TCHAR a_char) const
{
   ECharClass charClass = CharClass(a_char);

   switch(m_state)
   {
   case EStartState:
      if( EDigit == charClass || ESign == charClass )
      {
         m_state = EAfterSignState;
         return true;
      }
      else if(ESeparator == charClass)
      {
         m_state = EAfterSeparatorState;
         return true;
      }

   case EAfterSignState:
      if(ESeparator == charClass)
      {
         m_state = EAfterSeparatorState;
         return true;
      }
      else if(EDigit == charClass)
      {
         return true;
      }

   case EAfterSeparatorState:
      if(EDigit == charClass)
      {
         m_state = EFinalState;
         return true;
      }

   case EFinalState:
      if(EDigit == charClass)
      {
         return true;
      }
   }

   return false;
}

NumericStringValidator::ECharClass NumericStringValidator::CharClass(const TCHAR a_char) const
{
   if(a_char <= m_nine && a_char >= m_zero)
   {
      return EDigit;
   }
   else if(a_char == m_minus || a_char == m_plus)
   {
      return ESign;
   }
   else if(a_char == m_separator)
   {
      return ESeparator;
   }

   return EInvalidChar;
}