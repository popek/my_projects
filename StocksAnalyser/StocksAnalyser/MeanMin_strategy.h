#pragma once
#include "TradeStrategyBase.h"


class SMA_pointer;
class MIN_pointer;
class ChartElementsCollection;
class TransactionDetails;


class MeanMin_strategy : public TradeStrategyBase
{
private:
   enum state
   {
      EIdle, 

      EBoughtAboveMean,
      EBoughtAboveLastMinimum,
      
      ESoldBelowMean,
      ESoldBelowMinimum,
   };

public:
   MeanMin_strategy(shared_ptr<Asset> spAsset);
   virtual ~MeanMin_strategy(void);

   virtual shared_ptr<StrategyResult> RunTest( const KandleDate& a_start, const KandleDate& a_finish);

protected:
   virtual void AddGraphicPresentation(shared_ptr<ChartElementsCollection> elements);
   
private:

   shared_ptr<TransactionDetails> handleIdle(size_t candleIndex);

   shared_ptr<TransactionDetails> handleSoldBelowMean(size_t candleIndex);

   shared_ptr<TransactionDetails> handleSoldBelowMinimum(size_t candleIndex);

   shared_ptr<TransactionDetails> handleBoughtAboveMean(size_t candleIndex);

   shared_ptr<TransactionDetails> handleBoughtAboveLastMinimum(size_t candleIndex);

   shared_ptr<TransactionDetails> StopLossTriggered(size_t candleIndex) const;

private:
   state _state;

   shared_ptr<ChartElementsCollection> _elements;

   double _lastTransactionPrice;
   double _offsetFromMinimumInPercents;

   shared_ptr<SMA_pointer> _sma;
   size_t _smaPeriod;
   
   shared_ptr<MIN_pointer> _minIndicator;
   size_t _minIndicatorPeriod;

   double _currentValue; // asset "snapshot" based on which decision is made
   double _currentStopLossValue; // stop loss value. Value which is changed each sample when asset is in bought state
};


