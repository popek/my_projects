#pragma once
#include "WindowMessages.h"


template<class _BASE_TYPE>
class ChartControlMessagesProxy : public _BASE_TYPE
{
public:

   ChartControlMessagesProxy(void)
   {
   }
   virtual ~ChartControlMessagesProxy(void)
   {
   }

   virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
   {
      if(WM_SHOW_NEW_CHART == message)
      {
         GetParent()->SendMessage(message, wParam, lParam);
         return 0;
      }
      return __super::WindowProc(message, wParam, lParam);
   }
};
