SCRIPT -> LEXPR

; logical expressions
	LEXPR ->  LEXPR_BINOP
			| not LEXPR_BINOP

	LEXPR_BINOP ->   LEXPR_CMPOP
			| LEXPR_BINOP log_op LEXPR_CMPOP

	LEXPR_CMPOP -> EXPR
		| LEXPR_CMPOP cmp_op EXPR

; arithmethic/string expressions
	EXPR ->	  	  EXPR_MUL 
				| EXPR add_opp EXPR_MUL 

	EXPR_MUL ->   EXPR_POW 
				| EXPR_MUL mul_opp EXPR_POW

	EXPR_POW ->   EXPR_ABS 
				| EXPR_POW pow_opp EXPR_ABS

	EXPR_ABS ->   BASE_EXPR
				| add_opp BASE_EXPR
			
	BASE_EXPR -> ( LEXPR )
				| FUN_CALL 
				| CONST_NUMERIC
				| const_string
	
	FUN_CALL -> fun ( ARG_LIST )

	ARG_LIST ->   LEXPR
				| ARG_LIST , LEXPR
	
	CONST_NUMERIC ->  const_int
					| const_double
	
	