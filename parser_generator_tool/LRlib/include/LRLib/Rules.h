#pragma once
#include "Symbol.h"
#include "LessPtr.h"

#include <vector>
#include <map>
#include <set>

namespace lrlib
{

class Rules
{
public:
   Rules(void);
   virtual ~Rules(void);

   //void AddGrammarRule(int a_nResultNonterminalID, vector<int>& a_vecSymbolIDsList);
   void AddGrammarRule(Symbol* a_pNonterminalResult, std::vector<Symbol*>& a_vecSymbols);

   std::multimap<int, std::vector<int>>& RulesByIDs();

   void GenerateRulesByIDsMap();

   //auxiliary methods
   void FindAndAddDescendands(Symbol* a_pSymbol, std::set<Symbol*, LessPtr<Symbol>>& a_rsetDescendands); // used to find root nonterminal
   int  SetNonterminalsIDsAccordingToHierarchy(Symbol* a_pNonterminal, int a_nID = 0);                     // set symbols IDs according to place in grammar tree

protected:
   std::multimap<Symbol*, std::vector<Symbol*>, LessPtr<Symbol>>  m_mmapRules;
   std::multimap<int, std::vector<int>>                          m_mmapRulesByIDs;
};

} // namespace lrlib