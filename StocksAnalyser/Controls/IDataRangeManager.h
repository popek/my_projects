#pragma once
#include "ControlsLib.h"

class CONTROLS_LIB IDataRangeManager
{
public:

   IDataRangeManager(){};
   virtual ~IDataRangeManager(){};

   virtual double GetStartNormalisedPos() const = 0;

   virtual double GetFinishNormalisedPos() const = 0;

   virtual void   ForceSetRange(double a_thumbStart, double a_thumbStop) = 0;
};