#pragma once
#include "ControlsLib.h"


class ChartElement;
class DataSeries;
class Asset;


class CONTROLS_LIB ControlsLibUtils
{
public:

   static shared_ptr<ChartElement> CreateVolumeSeriesElement(const Asset& a_asset); 
   
   static shared_ptr<ChartElement> CreatePointerSeriesElement(const DataSeries& a_dataSeries, const Asset& a_pointerSource);

   static HCURSOR Cursor(LPTSTR a_cursorId);

   static CString FitStringToWidth(double a_value, CDC& a_dc, int a_availableWidth);

   static shared_ptr<DataSeries> CreateEmptySeries(size_t a_offset);

   static shared_ptr<DataSeries> CreateATRSeries(const Asset& a_asset);
  
private:
   static bool TextFit(double a_value, CString& a_out, CString a_format, CDC& a_dc, int a_availableWidth);

   static HCURSOR Sm_cursorWestEast;
   static HCURSOR Sm_cursorNorthSouth;
   static HCURSOR Sm_cursorArrow;
};
