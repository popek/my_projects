#include "StdAfx.h"
#include "BacktestThread.h"
#include "AssetsListBuffer.h"
#include "StrategyParams.h"
#include "AssetContainer.h"
#include "GlobalSettings.h"
#include "CommonTypes.h"
#include "KandleDate.h"
#include "StrategyResult.h"
#include "MeanMax_strategy.h"
#include "MeanMin_strategy.h"
#include "MeanMaxMin_strategy.h"
#include "Strategy_MACD.h"
#include "Misc.h"
#include "PathUtils.h"


BacktestThread::BacktestThread(void)
   : ThreadBase(this)
   , _strategyIndex(0)
   , _databaseDir(GlobalSettings::instance()->DBFolderPath())
   , _backtestObserver(nullptr)
   , _stopProcessing(false)
{
}
BacktestThread::~BacktestThread(void)
{
}

BOOL BacktestThread::ProcessThreadMessage(UINT a_messageId)
{
   // release previous assets set
   AssetsListBuffer::Instance()->Release();

   _stopProcessing = false;

   processAllAssets();

   _backtestObserver->onBactestFinished();

   return TRUE;
}

void BacktestThread::cancelBacktest()
{
   _stopProcessing = true;
}

void BacktestThread::startBacktest( int strategyId, 
                                    CString databaseDir, 
                                    IBacdtestObserver* bactestObserver,
                                    shared_ptr<KandleDate> testStartDate,
                                    shared_ptr<KandleDate> testFinishDate)
{
   _strategyIndex = strategyId;
   _databaseDir = databaseDir;
   _backtestObserver = bactestObserver;
   _startDate = testStartDate;
   _finishDate = testFinishDate;

   // crate thread
   Create();
   // run thread
   Resume();
   // post one and only one message (message no. is not important)
   PostMessage(0);
}

void BacktestThread::processAllAssets()
{
   _params = make_shared<StrategyParams>(0.06, 0.06, 0.09, 40000);

   WIN32_FIND_DATA   fileData;
   HANDLE            hFile;
   vector<CString>   fileNames;

   // get files
   hFile = FindFirstFile(_databaseDir + _T("*.mst"), &fileData);
   if(hFile != INVALID_HANDLE_VALUE)
   {
      do
      {
         fileNames.push_back(fileData.cFileName);
      }
      while(FindNextFile(hFile, &fileData) == TRUE);

      FindClose(hFile);
   }

   _backtestObserver->onAssetsAmountEstablished((int)fileNames.size());

   // process each and every asset
   for(size_t fileIdx=0; fileIdx<fileNames.size(); ++fileIdx)
   {
      if(_stopProcessing)
      {
         break;
      }

      processAsset(PathUtils::concatenateDirAndFile(_databaseDir, fileNames[fileIdx]));
   }
}

void BacktestThread::processAsset(CString filePath)
{
   auto strategy = createStrategy(filePath);
   strategy->AttachParams(_params);
   
   _backtestObserver->onAssetTested(filePath, strategy->RunTest(*_startDate, *_finishDate));
}

shared_ptr<TradeStrategyBase> BacktestThread::createStrategy(const CString& assetFilePath)
{
   auto assetContainer = make_shared<AssetContainer>();
   assetContainer->LoadDaysAssetFromFile(assetFilePath);

   AssetsListBuffer::Instance()->BufferAssetContainer(assetFilePath, assetContainer);

   switch(_strategyIndex)
   {
   case 1:
      return make_shared<MeanMax_strategy>((*assetContainer)[eDay]);
      break;

   case 2:
      return make_shared<MeanMin_strategy>((*assetContainer)[eDay]);
      break;

   case 3:
      return make_shared<MeanMaxMin_strategy>((*assetContainer)[eDay]);
      break;

   default:
      return make_shared<Strategy_MACD>((*assetContainer)[eDay]);
   }
}