#include "StdAfx.h"
#include "Kandle.h"
#include "KandleDate.h"

Kandle::Kandle(CCSVFileReader::TSingleLine & a_CVSKandleRepresentation, size_t a_kandleIndex)
{
   m_spDate   = shared_ptr<KandleDate>( new KandleDate(a_CVSKandleRepresentation[Kandle::eDateIdx], a_kandleIndex) );

   m_dClose   = String2Double(a_CVSKandleRepresentation[Kandle::eCloseIdx]);
   m_dOpen    = String2Double(a_CVSKandleRepresentation[Kandle::eOpenIdx]);
   m_dHigh    = String2Double(a_CVSKandleRepresentation[Kandle::eHighIdx]);
   m_dLow     = String2Double(a_CVSKandleRepresentation[Kandle::eLowIdx]);
   m_nVolume  = String2Int(a_CVSKandleRepresentation[Kandle::eVolumeIdx]);
   m_mediumPrice = (m_dClose + m_dOpen + m_dHigh + m_dLow)/4;
   m_dTheoreticalTurnover = m_nVolume*m_mediumPrice;
}
Kandle::Kandle(shared_ptr<KandleDate> a_spDate)
: m_spDate(a_spDate)
{
}
Kandle::Kandle(const Kandle& a_kandleToCopy)
{
   *this = a_kandleToCopy;
}
Kandle& Kandle::operator = (const Kandle& a_kandleToCopy)
{
   m_spDate   = a_kandleToCopy.m_spDate;

   m_dClose   = a_kandleToCopy.m_dClose;
   m_dOpen    = a_kandleToCopy.m_dOpen;
   m_dHigh    = a_kandleToCopy.m_dHigh;
   m_dLow     = a_kandleToCopy.m_dLow;
   m_nVolume  = a_kandleToCopy.m_nVolume;

   m_dTheoreticalTurnover = a_kandleToCopy.m_dTheoreticalTurnover;

   m_mediumPrice = a_kandleToCopy.m_mediumPrice;

   return *this;
}
Kandle::~Kandle(void)
{
}

double Kandle::String2Double(string& a_strDouble)
{
   return atof(a_strDouble.c_str());
}

int Kandle::String2Int(string& a_strInt)
{
   return atoi(a_strInt.c_str());
}

shared_ptr<KandleDate> Kandle::Date() const
{
   return m_spDate;
}

double Kandle::Open() const
{
   return m_dOpen;
}

double Kandle::Close() const
{
   return m_dClose;
}

double Kandle::High() const
{
   return m_dHigh;
}

double Kandle::Low() const
{
   return m_dLow;
}

size_t Kandle::Volume() const
{
   return m_nVolume;
}

double Kandle::TurnOver() const
{
   return m_dTheoreticalTurnover;
}

double Kandle::MediumPrice() const
{
   return m_mediumPrice;
}

void Kandle::Concatenate(const Kandle& a_kandleToConcatenate)
{
   m_dHigh = max(m_dHigh, a_kandleToConcatenate.High());
   m_dLow  = min(m_dLow, a_kandleToConcatenate.Low());

   m_dClose = a_kandleToConcatenate.Close();

   m_nVolume += a_kandleToConcatenate.Volume();

   m_mediumPrice = (m_dClose + m_dOpen + m_dHigh + m_dLow)/4;
   m_dTheoreticalTurnover = m_nVolume*m_mediumPrice;
}

shared_ptr<Kandle> Kandle::Clone(size_t a_cloneIndex) const
{
   shared_ptr<Kandle> spKandle(new Kandle(*this));

   spKandle->m_spDate = m_spDate->Clone(a_cloneIndex);

   return spKandle;
}

shared_ptr<Kandle> Kandle::CreateNullObject(const KandleDate& a_date)
{
   return shared_ptr<Kandle>(new Kandle(a_date.Clone(0)));
}