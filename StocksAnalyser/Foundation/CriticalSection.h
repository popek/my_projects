#pragma once
#include "FoundationLib.h"

class FOUNDATION_LIB CriticalSection
{
public:
   CriticalSection(void);
   virtual ~CriticalSection(void);

   void Enter();

   void Leave();

protected:

   CRITICAL_SECTION m_cs;
};
