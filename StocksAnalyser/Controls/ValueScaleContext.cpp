#include "StdAfx.h"
#include "ValueScaleContext.h"


ValueScaleContext::ValueScaleContext()
{
   Update(0, 0, 0, 0);
}
ValueScaleContext::ValueScaleContext(double a_maxValue, double a_minValue, Coordinate a_maxValueCoord, Coordinate a_minValueCoord)
{
   Update(a_maxValue, a_minValue, a_maxValueCoord, a_minValueCoord);
}
ValueScaleContext::~ValueScaleContext(void)
{
}

void ValueScaleContext::Update(double a_maxValue, double a_minValue, Coordinate a_maxValueCoord, Coordinate a_minValueCoord)
{
   m_maxValue = a_maxValue;
   m_minValue = a_minValue;
   m_maxValueCoord = a_maxValueCoord;
   m_minValueCoord = a_minValueCoord;

   m_coordinate2value = (double)(m_maxValueCoord - m_minValueCoord)/(double)(m_maxValue - m_minValue);
}

Coordinate ValueScaleContext::ToCoordinate(double a_value) const
{
   return ((a_value-m_minValue)*m_coordinate2value + m_minValueCoord);
}


void ValueScaleContext::CurrentMinMaxValues(double& a_outMaxValue, double& a_outMinValue) const
{
   a_outMinValue = m_minValue;
   a_outMaxValue = m_maxValue;
}