#pragma once
#include "IInstrumentListUpdateObserver.h"
#include "ThreadSafeContainer.h"

class AssetsListDownloader;

class InstrumentListUpdateProgressDlg : public CDialog, public IInstrumentListUpdateObserver
{
   class ProgressDescription
   {
   public:
      ProgressDescription(CString a_description, int a_progressBarPos);

      CString _description;
      int     _numeric;
   };

	DECLARE_DYNAMIC(InstrumentListUpdateProgressDlg)

   // --- IInstrumentListUpdateObserver interface
   virtual void SetRange(int a_assetListSize);

   virtual void SetZippedFilesCount(int a_assetsCount);

   virtual void NextFileUnzippedNotification(CString a_strFileName);

   virtual void OnDataDownloaded(int a_totalDataDownloaded);

   virtual void UnzippingStarted();

   virtual void UnzippingFinished();

   virtual void UpdateCanceled();
   
   virtual  void NotifyError(CString a_strErrorDesc);
   //---------------------------

public:
	InstrumentListUpdateProgressDlg(CWnd* pParent = NULL);
	virtual ~InstrumentListUpdateProgressDlg();

   enum { IDD = IDD_INSTRUMENT_LIST_UPDATE_PROGRESS };

protected:

	virtual void DoDataExchange(CDataExchange* pDX);
   virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
   virtual BOOL OnInitDialog();

   DECLARE_MESSAGE_MAP()
   afx_msg void OnBnClickedCancel();
   afx_msg void OnBnClickedOk();

protected:

   shared_ptr<AssetsListDownloader>                     m_spAssetsListUpdater;

   ThreadSafeContainer<shared_ptr<ProgressDescription>> m_instrumentListUpdateProgressData;

   CProgressCtrl  m_progressBar;
   CStatic        m_progressDescription;

   int            m_zipFileSize;
   int            m_zippedFilesCount;
};
