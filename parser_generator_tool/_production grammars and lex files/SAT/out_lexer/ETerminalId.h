#pragma once

enum ETerminalId
{
	El_par,
	Er_par,
	Ecoma,
	Esemicolon,
	Eadd_op,
	Econst_double,
	Econst_int,
	Eidentifier,
	Emul_op,
	Ecmp_op,
	Eassign,
	Eaccess_op,
	Eeol,
	Eblank,
	Eeof,
	Eerror
};
