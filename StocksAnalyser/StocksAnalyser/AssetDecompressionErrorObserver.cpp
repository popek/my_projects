#include "StdAfx.h"
#include "AssetDecompressionErrorObserver.h"

AssetDecompressionErrorObserver::AssetDecompressionErrorObserver(void)
{
}
AssetDecompressionErrorObserver::~AssetDecompressionErrorObserver(void)
{
}

void AssetDecompressionErrorObserver::OnDecompressError(const void* pSender, std::pair<const Poco::Zip::ZipLocalFileHeader, const std::string>& info)
{
}
