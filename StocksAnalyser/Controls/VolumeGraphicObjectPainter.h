#pragma once
#include "GraphicObjectPainterBase.h"


class GraphicObjectBase;


class VolumeGraphicObjectPainter : public GraphicObjectPainterBase
{
public:
   VolumeGraphicObjectPainter(void);
   virtual ~VolumeGraphicObjectPainter(void);

   virtual void DrawObject(const GraphicObjectBase& a_object, CDC* a_pDC);
};
