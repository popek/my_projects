#pragma once
#include "XMLItem.h"


class FOUNDATION_LIB XMLAttrib : public XMLItem
{
public:
   XMLAttrib(LPCTSTR name, LPCTSTR value);
   virtual ~XMLAttrib(void);

   operator LPCTSTR() const;

private:
   CString _value;
};

