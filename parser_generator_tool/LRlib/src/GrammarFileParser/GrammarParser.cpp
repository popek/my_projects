#include "GrammarFileParser/GrammarParser.h"
#include "GrammarFileParser/SemanticHandler.h"
#include "GrammarFileParser/ShiftReduce.h"
#include "GrammarFileParser/ParserErrorsDefinitions.h"

namespace lrlib
{

GrammarParser::GrammarParser(std::unique_ptr<CompileContext>&& context)
   : context_(std::move(context))
{
}

bool GrammarParser::Init(const char* a_pGrammarDefinitionFile)
{
   return scanner_.Init(a_pGrammarDefinitionFile); // initialisize lex scanner
}

bool GrammarParser::Parse()
{
   LexAtom atom;
   int iTopState;
   bool bAccept = false;

   std::vector<int> _StateStack;
   SemanticHandler _SemActions(*context_);

   _StateStack.push_back(0);// initialize state stack
   scanner_.GetAtom(&atom);
   if (atom.m_ID == e_error)
   {
      context_->errors().AddFormatedError0(PE_LEX_ATOM_ERROR, atom.m_iLine, atom.m_strText);
   }

   while (!(bAccept || context_->errors().DoesErrorExist()))
   {
      iTopState = _StateStack[_StateStack.size() - 1];

      if (TermShiftArray[iTopState][atom.m_ID] > -1) //shift actions appears
      {
         if (ReduceArray[iTopState][atom.m_ID] > -1)
            throw; //shift reduce conflict

         _StateStack.push_back(TermShiftArray[iTopState][atom.m_ID]);
         _SemActions.SymbolStack().Push(SemAtom(atom));
         scanner_.GetAtom(&atom);
         if (atom.m_ID == e_error)
         {
            context_->errors().AddFormatedError0(PE_LEX_ATOM_ERROR, atom.m_iLine, atom.m_strText);
         }
      }
      else if (ReduceArray[iTopState][atom.m_ID] > 0) //if reduction appears
      {
         int iRuleID = ReduceArray[iTopState][atom.m_ID];
         int iLen = RulesLength[iRuleID];
         int iNontermID = Productions[iRuleID];

         _SemActions.DoReduceAction(iRuleID); //do action

         while (iLen--)
            _StateStack.erase(_StateStack.end() - 1);

         _StateStack.push_back(NontermShiftArray[_StateStack[_StateStack.size() - 1]][iNontermID]);
      }
      else if (ReduceArray[iTopState][atom.m_ID] == 0) // if acceptance appears
      {
         _SemActions.DoReduceAction(0); //do accept action
         bAccept = true;
      }
      else // if no reduce, no shift, no acceptance, then error appears
      {
         context_->errors().AddError(PE_GENERIC_ERROR, scanner_.LineNo());
      }
   }

   // if no error, then assign symbols and rules and return true
   return !context_->errors().DoesErrorExist();
}

void GrammarParser::ShowErrors()
{
   context_->errors().ShowErrors();
   scanner_.ShowError();
}

} // namespace lrlib