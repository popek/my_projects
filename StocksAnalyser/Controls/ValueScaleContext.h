#pragma once
#include "controlsLibTypes.h"
#include "ControlsLib.h"


class CONTROLS_LIB ValueScaleContext
{
   NONCOPYABLE_TYPE(ValueScaleContext)

public:
   ValueScaleContext();
   ValueScaleContext(double a_maxValue, double a_minValue, Coordinate a_maxValueCoord, Coordinate a_minValueCoord);
   virtual ~ValueScaleContext(void);

   void Update(double a_maxValue, double a_minValue, Coordinate a_maxValueCoord, Coordinate a_minValueCoord);

   Coordinate ToCoordinate(double a_value) const;

   void CurrentMinMaxValues(double& a_outMaxValue, double& a_outMinValue) const;

protected:

   double m_maxValue;
   double m_minValue;

   Coordinate m_maxValueCoord;
   Coordinate m_minValueCoord;

   //auxiliary data (to make computation faster)
   double m_coordinate2value;
};
