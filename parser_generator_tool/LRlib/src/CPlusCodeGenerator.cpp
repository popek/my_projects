#include "CPlusCodeGenerator.h"
#include "Grammar.h"
#include <foundation/TextFileWriter.h>

#pragma warning( disable : 4018)

namespace lrlib
{

CPlusCodeGenerator::CPlusCodeGenerator(Grammar* a_pGrammarObj)
   : m_pGrammarObj(a_pGrammarObj)
{
   m_bufSize = 1024;
   m_szBuff = new char[m_bufSize];
}

CPlusCodeGenerator::~CPlusCodeGenerator(void)
{
   delete[] m_szBuff;
}


void CPlusCodeGenerator::ShowErrors()
{
   for (size_t nErrorIdx = 0; nErrorIdx < m_deqErrors.size(); ++nErrorIdx)
   {
      printf("%d CPlusCodeGenerator %s\n", nErrorIdx, m_deqErrors[nErrorIdx].c_str());
   }
}


//--------------------------------------------------------------------------------
// GenerateAll - generates all files needed in the project
//--------------------------------------------------------------------------------
void CPlusCodeGenerator::GenerateAll(std::string a_strFilePath)
{
   //GenerateParserClass(a_strFilePath);
   //GenerateSemStackClass(a_strFilePath);
   //GenerateSemAtomClass(a_strFilePath);
   GenerateSemActionsClass(a_strFilePath);
   //GenerateLexerClass(a_strFilePath);
   //GenerateLexAtomClass(a_strFilePath);
   GenerateCommonDefinitionsFile(a_strFilePath);
   GenerateTransitionGraphFile(a_strFilePath);

   GenerateAuxiliaryFileWithStates(a_strFilePath);
}


void CPlusCodeGenerator::GenerateParserClass(std::string a_strFilePath)
{
   GenerateParserClass_header(a_strFilePath);
   GenerateParserClass_definition(a_strFilePath);
}


void CPlusCodeGenerator::GenerateParserClass_header(std::string a_strFilePath)
{
   a_strFilePath += "\\LRParser.h";
   TextFileWriter writter(a_strFilePath);

   writter.WriteText(
      "#ifndef __LR_Parser__\n"
      "#define __LR_Parser__\n"
      "#include \"Lex.h\"\n\n"
      "class LRParser\n"
      "{\n"
      "  Lex m_lexer;\n"
      "public:\n"
      "  LRParser();\n"
      "  virtual ~LRParser();\n"
      "  void Compile();\n"
      "  void ErrorRecovery();\n"
      "};\n\n"
      "#endif //__LR_Parser__");
}


void CPlusCodeGenerator::GenerateParserClass_definition(std::string a_strFilePath)
{
   a_strFilePath += "\\LRParser.cpp";
   TextFileWriter writter(a_strFilePath);

   writter.WriteText(
      "#include \"LRParser.h\"\n"
      "#include \"SemanticHandler.h\"\n"
      "#include \"ShiftReduce.h\"\n\n"
      "LRParser::LRParser()\n"
      "{\n"
      "}\n\n"
      "LRParser::~LRParser()\n"
      "{\n"
      "}\n\n"
      "void LRParser::Compile()\n"
      "{\n"
      "  LexAtom atom;\n"
      "  int iTopState;\n"
      "  bool bAccept = false,\n"
      "       bError  = false;\n"
      "\n"
      "  vector<int> _StateStack;\n"
      "  SemActions _SemActions;\n"
      "\n"
      "  _StateStack.push_back(0);// initialize state stack\n"
      "  m_LexScaner.GetAtom(&atom);\n"
      "  if(atom.m_ID == e_error)\n"
      "  {\n"
      "    bError = true;\n"
      "    ErrorRecovery();\n"
      "  }\n"
      "\n"
      "  while( !(bAccept || bError) )\n"
      "  {\n"
      "    iTopState = _StateStack[_StateStack.size()-1];\n"
      "\n"
      "    if(TermShiftArray[iTopState][atom.m_ID] > -1) //shift actions appears\n"
      "    {\n"
      "      if(ReduceArray[iTopState][atom.m_ID] > -1)\n"
      "        throw; //shift reduce conflict\n"
      "\n"
      "      _StateStack.push_back(TermShiftArray[iTopState][atom.m_ID]);\n"
      "      _SemActions.SymbolStack().Push(SemAtom(atom));\n"
      "      m_LexScaner.GetAtom(&atom);\n"
      "      if(atom.m_ID == e_error)\n"
      "      {\n"
      "        bError = true;\n"
      "        ErrorRecovery();\n"
      "      }\n"
      "    }\n"
      "    else if(ReduceArray[iTopState][atom.m_ID] > 0) //if reduction appears\n"
      "    {\n"
      "      int iRuleID = ReduceArray[iTopState][atom.m_ID];\n"
      "      int iLen = RulesLength[iRuleID];\n"
      "      int iNontermID = Productions[iRuleID];\n"
      "\n"
      "      _SemActions.DoReduceAction(iRuleID); //do action\n"
      "\n"
      "      while(iLen--)\n"
      "        _StateStack.erase(_StateStack.end()-1);\n"
      "\n"
      "      _StateStack.push_back(NontermShiftArray[ _StateStack[_StateStack.size()-1] ][iNontermID]);\n"
      "    }\n"
      "    else if(ReduceArray[iTopState][atom.m_ID] == 0) // if acceptance appears\n"
      "    {\n"
      "      _SemActions.DoReduceAction(0); //do accept action\n"
      "      bAccept = true;\n"
      "    }\n"
      "    else // if no reduce, no shift, no acceptance, then error appears\n"
      "    {\n"
      "      bError = true;\n"
      "      ErrorRecovery(); // print out compilation error message\n"
      "    }\n"
      "  }\n"
      "}\n\n"
      "void LRParser::ErrorRecovery()\n"
      "{\n"
      "}");
}


void CPlusCodeGenerator::GenerateSemStackClass(std::string a_strFilePath)
{
   GenerateSemStackClass_header(a_strFilePath);
   GenerateSemStackClass_definition(a_strFilePath);
}

void CPlusCodeGenerator::GenerateSemStackClass_header(std::string a_strFilePath)
{
   a_strFilePath += "\\SemStack.h";
   FILE* pfile = NULL;
   fopen_s(&pfile, a_strFilePath.c_str(), "w");

   if (NULL == pfile)
   {
      m_deqErrors.push_back("could not write to semantic stack class header file");
   }
   else
   {
      fprintf(pfile, "%s",
         "#ifndef __LR_SemStack_Header__\n"
         "#define __LR_SemStack_Header__\n"
         "#include \"Lex.h\"\n\n"
         "class SemStack\n"
         "{\n"
         "public:\n"
         "  SemStack(void);\n"
         "  virtual ~SemStack(void);\n\n"
         "  void Push(SemAtom semAtom);\n"
         "  void Pop();\n"
         "  void Top(SemAtom* pSemAtom);\n"
         "  vector<SemAtom> m_Stack;\n"
         "};\n\n"
         "#endif //__LR_SemStack_Header__"
         );

      fclose(pfile);
   }
}

void CPlusCodeGenerator::GenerateSemStackClass_definition(std::string a_strFilePath)
{
   a_strFilePath += "\\SemStack.cpp";
   FILE* pfile = NULL;
   fopen_s(&pfile, a_strFilePath.c_str(), "w");

   if (NULL == pfile)
   {
      m_deqErrors.push_back("could no write to semantic stack class definition file");
   }
   else
   {
      fprintf(pfile, "%s",
         "#include \"SemStack.h\"\n\n"
         "//----------------------------------\n"
         "//------------ SemStack -----------\n"
         "//----------------------------------\n"
         "SemStack::SemStack(void)\n"
         "{\n"
         "}\n\n"
         "SemStack::~SemStack(void)\n"
         "{\n"
         "}\n\n"
         "void SemStack::Push(SemAtom semAtom)\n"
         "{\n"
         "  m_Stack.push_back(semAtom);\n"
         "}\n\n"
         "void SemStack::Pop()\n"
         "{\n"
         "  if(!m_Stack.empty())\n"
         "    m_Stack.erase(m_Stack.end()-1);\n"
         "}\n\n"
         "void SemStack::Top(SemAtom* pSemAtom)\n"
         "{\n"
         "  if(!m_Stack.empty())\n"
         "    *pSemAtom = m_Stack[m_Stack.size()-1];\n"
         "}"
         );

      fclose(pfile);
   }
}




void CPlusCodeGenerator::GenerateSemAtomClass(std::string a_strFilePath)
{
   GenerateSemAtomClass_header(a_strFilePath);
   GenerateSemAtomClass_definition(a_strFilePath);
}


void CPlusCodeGenerator::GenerateSemAtomClass_header(std::string a_strFilePath)
{
   a_strFilePath += "\\SemAtom.h";
   FILE* pfile = NULL;
   fopen_s(&pfile, a_strFilePath.c_str(), "w");

   if (NULL == pfile)
   {
      m_deqErrors.push_back("could not write to semantic atom class header file");
   }
   else
   {
      fprintf(pfile, "%s",
         "#ifndef __LR_SemAtom_Header__\n"
         "#define __LR_SemAtom_Header__\n"
         "#include \"Lex.h\"\n\n"
         "class SemAtom\n"
         "{\n"
         "public:\n"
         "  SemAtom(void);\n"
         "  SemAtom(LexAtom lexAtom);\n"
         "  virtual ~SemAtom(void);\n\n"
         "  void operator = (const SemAtom& sa);\n"
         "  LexAtom m_LexAtom;\n"
         "};\n\n"
         "#endif //__LR_SemAtom_Header__"
         );
      fclose(pfile);
   }
}


void CPlusCodeGenerator::GenerateSemAtomClass_definition(std::string a_strFilePath)
{
   a_strFilePath += "\\SemAtom.cpp";
   FILE* pfile = NULL;
   fopen_s(&pfile, a_strFilePath.c_str(), "w");

   if (NULL == pfile)
   {
      m_deqErrors.push_back("could not write to semantic atom class definition file");
   }
   else
   {
      fprintf(pfile, "%s",
         "#include \"SemStack.h\"\n\n"
         "//----------------------------------\n"
         "//------------ SemAtom ------------\n"
         "//----------------------------------\n"
         "SemAtom::SemAtom(void)\n"
         "{\n"
         "}\n\n"
         "SemAtom::SemAtom(LexAtom lexAtom)\n"
         "{\n"
         "  m_LexAtom = lexAtom;\n"
         "}\n\n"
         "SemAtom::~SemAtom(void)\n"
         "{\n"
         "}\n\n"
         "void SemAtom::operator = (const SemAtom& sa)\n"
         "{\n"
         "  m_LexAtom = sa.m_LexAtom;\n"
         "}"
         );

      fclose(pfile);
   }
}


void CPlusCodeGenerator::GenerateSemActionsClass(std::string a_strFilePath)
{
   GenerateSemActionsClass_header(a_strFilePath);
   GenerateSemActionsClass_definition(a_strFilePath);

   GeneretaSemActionsEnum_header(a_strFilePath);

   GeneretaSemActionsNamesArray_header(a_strFilePath);
}


void CPlusCodeGenerator::GenerateSemActionsClass_header(std::string a_strFilePath)
{
   TextFileWriter fileWritter(a_strFilePath + "\\SemanticHandler.h");

   fileWritter.WriteText(
      "#pragma once\n"
      "#include \"SemStack.h\"\n\n"
      "class SemActions\n"
      "{\n"
      "public:\n"
      "  SemActions(void);\n"
      "  virtual ~SemActions(void);\n"
      "\n"
      "\tvoid DoReduceAction(int a_ruleId);\n"
      "\n"
      "  virtual void Rule_acceptance();\n"
      );

   int nRuleIdx;
   for (nRuleIdx = 1; nRuleIdx < m_pGrammarObj->m_mmapRules.size(); nRuleIdx++)
      fileWritter.WriteText(GenerateRuleMethod(nRuleIdx, true, false).c_str());

   fileWritter.WriteText(
      "\n"
      "protected:\n"
      "\tCSemStack m_semStack;\n};"
      );
}


void CPlusCodeGenerator::GenerateSemActionsClass_definition(std::string a_strFilePath)
{
   TextFileWriter fileWritter(a_strFilePath + "\\SemActions.cpp");

   fileWritter.WriteText(
      "#include \"SemanticHandler.h\"\n"
      "#include \"CommonDef.h\"\n\n"
      "SemActions::SemActions(void)\n{\n}\n"
      "SemActions::~SemActions(void)\n{\n}\n\n"
      );

   fileWritter.WriteText(SemActions__DoAction_method().c_str());

   fileWritter.WriteText(
      "//------------------ rule actions --------------------\n"
      "void SemActions::Rule_acceptance()\n"
      "{\n}\n\n"
      );

   int nRuleIdx;
   for (nRuleIdx = 1; nRuleIdx < m_pGrammarObj->m_mmapRules.size(); nRuleIdx++)
      fileWritter.WriteText(GenerateRuleMethod(nRuleIdx, false, false).c_str());

}

void CPlusCodeGenerator::GeneretaSemActionsEnum_header(std::string a_folderPath)
{
   TextFileWriter fileWritter(a_folderPath + "\\ESemanticRuleId.h");

   fileWritter.WriteText(
      "#pragma once\n"
      "\n"
      "enum ESemanticRuleId\n"
      "{\n"
      );

   for (int nRuleIdx = 1; nRuleIdx < m_pGrammarObj->m_mmapRules.size(); nRuleIdx++)
      fileWritter.WriteText("\tESR_%s,\n", GenerateRuleName(nRuleIdx).c_str());

   fileWritter.WriteText(
      "};\n"
      );
}

void CPlusCodeGenerator::GeneretaSemActionsNamesArray_header(std::string a_folderPath)
{
   TextFileWriter fileWritter(a_folderPath + "\\SemActionsHelper.h");

   fileWritter.WriteText(
      "#pragma once\n"
      "\n"
      "static LPCTSTR G_arrSemActionNames[] =\n"
      "{\n"
      );

   for (int nRuleIdx = 1; nRuleIdx < m_pGrammarObj->m_mmapRules.size(); nRuleIdx++)
      fileWritter.WriteText("\t_T(\"%s\"),\n", GenerateRuleName(nRuleIdx).c_str());

   fileWritter.WriteText(
      "};\n"
      );
}

std::string CPlusCodeGenerator::GenerateRuleName(int a_ruleId)
{
   std::string   strName = "";
   std::string strAux;

   t_mmapInt2VecInt::iterator itRule = m_pGrammarObj->m_mmapRules.begin();
   while (a_ruleId--) itRule++;

   sprintf_s(m_szBuff, m_bufSize, "%s__to", m_pGrammarObj->m_mapAuxSymbID2Sym[itRule->first]->name().c_str());
   strName = m_szBuff;

   for (size_t i = 0; i < (itRule->second).size(); i++)
   {
      strAux = m_pGrammarObj->m_mapAuxSymbID2Sym[(itRule->second)[i]]->name();
      if (strAux.size() == 1)
         strAux = TerminalAuxName(strAux[0]);
      sprintf_s(m_szBuff, m_bufSize, "__%s", strAux.c_str());
      strName += m_szBuff;
   }

   return strName;
}

void CPlusCodeGenerator::GenerateLexerClass(std::string a_strFilePath)
{
   GenerateLexerClass_header(a_strFilePath);
   GenerateLexerClass_definition(a_strFilePath);
}


void CPlusCodeGenerator::GenerateLexerClass_header(std::string a_strFilePath)
{
   a_strFilePath += "\\Lex.h";
   FILE* pfile = NULL;
   fopen_s(&pfile, a_strFilePath.c_str(), "w");

   if (NULL == pfile)
   {
      m_deqErrors.push_back("could not write into lexer header file");
   }
   else
   {
      fprintf(pfile, "%s",
         "#ifndef __Lex_Header__\n"
         "#define __Lex_Header__\n\n"
         "class LexAtom;\n\n"
         "class Scanner\n"
         "{\n"
         "public:\n"
         "  Scanner(void);\n"
         "  virtual ~Scanner(void);\n"
         "  void GetAtom(LexAtom* pLexAtom);\n"
         "};\n\n"
         "#endif //__Lex_Header__"
         );

      fclose(pfile);
   }
}


void CPlusCodeGenerator::GenerateLexerClass_definition(std::string a_strFilePath)
{
   a_strFilePath += "\\Lex.cpp";
   FILE* pfile = NULL;
   fopen_s(&pfile, a_strFilePath.c_str(), "w");

   if (NULL == pfile)
   {
      m_deqErrors.push_back("could not write into lexer definition file");
   }
   else
   {
      fprintf(pfile, "%s",
         "#include \"Lex.h\";\n"
         "#include \"LexAtom.h\";\n\n"
         "Scanner::Scanner()\n"
         "{\n"
         "}\n\n"
         "Scanner::~Scanner()\n"
         "{\n"
         "}\n\n"
         "void Scanner::GetAtom(LexAtom* pLexAtom)\n"
         "{\n"
         "}\n\n"
         );
      fclose(pfile);
   }
}



void CPlusCodeGenerator::GenerateLexAtomClass(std::string a_strFilePath)
{
   GenerateLexAtomClass_header(a_strFilePath);
   GenerateLexAtomClass_definition(a_strFilePath);
}


void CPlusCodeGenerator::GenerateLexAtomClass_header(std::string a_strFilePath)
{
   a_strFilePath += "\\LexAtom.h";
   FILE* pfile = NULL;
   fopen_s(&pfile, a_strFilePath.c_str(), "w");

   if (NULL == pfile)
   {
      m_deqErrors.push_back("could not write into lex atom header file");
   }
   else
   {
      fprintf(pfile, "%s",
         "#ifndef __LexAtom_Header__\n"
         "#define __LexAtom_Header__\n\n"
         );

      fprintf(pfile, "%s", GenerateTerminalsEnum().c_str());

      fprintf(pfile, "%s",
         "class LexAtom\n"
         "{\n"
         "public:\n"
         "  LexAtom(void);\n"
         "  virtual ~LexAtom(void);\n\n"
         "  int     m_iLine;\n"
         "  eTermID m_ID;\n"
         "  std::string  m_strText;\n"
         "  double  m_dValue;\n"
         "  int     m_iValue;\n"
         "};\n\n"
         "#endif //__LexAtom_Header__"
         );

      fclose(pfile);
   }
}


void CPlusCodeGenerator::GenerateLexAtomClass_definition(std::string a_strFilePath)
{
   a_strFilePath += "\\LexAtom.cpp";
   FILE* pfile = NULL;
   fopen_s(&pfile, a_strFilePath.c_str(), "w");

   if (NULL == pfile)
   {
      m_deqErrors.push_back("could not write into lex atom definition file");
   }
   else
   {
      fprintf(pfile, "%s",
         "#include \"LexAtom.h\";\n\n"
         "//----------------------------------\n"
         "//------------ LexAtom ------------\n"
         "//----------------------------------\n"
         "LexAtom::LexAtom(void)\n"
         "{\n"
         "}\n\n"
         "LexAtom::~LexAtom(void)\n"
         "{\n"
         "}"
         );
      fclose(pfile);
   }
}


std::string CPlusCodeGenerator::GenerateRuleMethod(int a_nRuleIdx, bool a_header, bool a_generateDebugPrint)
{
   std::string   strName = "";
   std::string   strRuleName = GenerateRuleName(a_nRuleIdx);
   int i;

   t_mmapInt2VecInt::iterator itRule = m_pGrammarObj->m_mmapRules.begin();
   while (a_nRuleIdx--)
   {
      ++itRule;
   }

   sprintf_s(m_szBuff, m_bufSize, "%svoid %sRule_%s", (a_header ? "  virtual " : ""), (a_header ? "" : "SemActions::"), strRuleName.c_str());

   strName = m_szBuff;

   if (a_header)
      strName += "();\n";
   else
   {
      strName += "()\n{\n";

      for (i = 0; i < (itRule->second).size(); i++)
         strName += "\tm_semStack.Pop();\n";

      if (a_generateDebugPrint)
      {
         strName += "#ifdef _DEBUG\n\tprintf(\"";
         strName += "Rule_";
         strName += strRuleName;
         strName += "\\n\");\n#endif\n";
      }

      strName += "}\n\n";
   }

   return strName;
}


std::string CPlusCodeGenerator::TerminalAuxName(char cOneCharName)
{
   std::string strAux = "";
   strAux += cOneCharName;

   switch (cOneCharName)
   {
   case ',':
      strAux = "comma";
      break;
   case '.':
      strAux = "dot";
      break;
   case ':':
      strAux = "colon";
      break;
   case '(':
      strAux = "lpar";
      break;
   case ')':
      strAux = "rpar";
      break;
   case '[':
      strAux = "lbr";
      break;
   case ']':
      strAux = "rbr";
      break;
   case '{':
      strAux = "lbracebr";
      break;
   case '}':
      strAux = "rbracebr";
      break;
   case '=':
      strAux = "eq";
      break;

   case '+':
      strAux = "add";
      break;
   case '-':
      strAux = "minus";
      break;
   case '*':
      strAux = "mul";
      break;
   case '/':
      strAux = "div";
      break;
   case '^':
      strAux = "pow";
      break;
   }

   return strAux;
}


std::string CPlusCodeGenerator::SemActions__DoAction_method()
{
   std::string strName = "";
   std::string strRet;
   std::string strAux;
   int i;

   strRet = "void SemActions::DoReduceAction(int a_ruleId)\n"
      "{\n"
      "  typedef void (SemActions::*RuleFunction)();\n\n"
      "  static RuleFunction Rules[RULES_COUNT]=\n"
      "  {\n"
      "    &SemActions::Rule_acceptance,\n";

   t_mmapInt2VecInt::iterator it;
   it = m_pGrammarObj->m_mmapRules.begin();
   for (++it; it != m_pGrammarObj->m_mmapRules.end(); it++)
   {
      sprintf_s(m_szBuff, m_bufSize, "    &SemActions::Rule_%s__to", m_pGrammarObj->m_mapAuxSymbID2Sym[it->first]->name().c_str());
      strName = m_szBuff;
      for (i = 0; i < (it->second).size(); i++)
      {
         strAux = m_pGrammarObj->m_mapAuxSymbID2Sym[(it->second)[i]]->name();
         if (strAux.size() == 1)
            strAux = TerminalAuxName(strAux[0]);

         sprintf_s(m_szBuff, m_bufSize, "__%s", strAux.c_str());
         strName += m_szBuff;
      }
      strName += ",\n";
      strRet += strName;
   }

   strRet += "  };\n\n"
      "  (this->*(Rules[a_ruleId]))();\n"
      "}\n\n";

   return strRet;
}


std::string CPlusCodeGenerator::GenerateTerminalsEnum()
{
   int j;
   std::string strRet = "enum eTermID\n{\n";

   for (j = 0; j < m_pGrammarObj->m_mapAuxSymbID2Sym.size() - 2; j++)
   {
      if (m_pGrammarObj->m_mapAuxSymbID2Sym[j]->term())
      {
         if (m_pGrammarObj->m_mapAuxSymbID2Sym[j]->name().size() > 1)
            sprintf_s(m_szBuff, m_bufSize, "  e_%s ,\n", m_pGrammarObj->m_mapAuxSymbID2Sym[j]->name().c_str());
         else
            sprintf_s(m_szBuff, m_bufSize, "  e_%s ,\n", TerminalAuxName(m_pGrammarObj->m_mapAuxSymbID2Sym[j]->name()[0]).c_str());

         strRet += m_szBuff;
      }
   }
   strRet += "  e_eof ,\n  e_error\n";
   strRet += "};\n\n";

   return strRet;
}


void CPlusCodeGenerator::GenerateCommonDefinitionsFile(std::string a_strFilePath)
{
   FILE* pfile = NULL;
   a_strFilePath += "\\CommonDef.h";
   fopen_s(&pfile, a_strFilePath.c_str(), "w");
   if (NULL == pfile)
   {
      m_deqErrors.push_back("could not write into common definitions file");
   }
   else
   {
      unsigned int uiStatesAmount = m_pGrammarObj->m_vecLR0States.size(), j,
         uiSymbolsAmount = m_pGrammarObj->m_setGrammarSymbols.size() - 1; // without extended nonterminal

      //get number of terminals and nonterminals
      int iTermCount = 1;// because of "�" terminal
      int iNontermCount = 0;
      for (j = 0; j < uiSymbolsAmount - 1; j++)
      {
         if (m_pGrammarObj->m_mapAuxSymbID2Sym[j]->term())
            iTermCount++;
         else
            iNontermCount++;
      }

      fprintf(pfile, "%s", "#ifndef __COMMON_DEF_FILE__\n#define __COMMON_DEF_FILE__\n\n");
      fprintf(pfile, "#define RULES_COUNT   %d\n#define STATES_COUNT  %d\n", m_pGrammarObj->m_mmapRules.size(), uiStatesAmount);
      fprintf(pfile, "#define TERM_COUNT    %d\n#define NONTERM_COUNT %d\n\n", iTermCount, iNontermCount);
      fprintf(pfile, "%s", "#endif //__COMMON_DEF_FILE__");
      fclose(pfile);
   }
}


void CPlusCodeGenerator::GenerateTransitionGraphFile(std::string a_strFilePath)
{
   FILE* pfile = NULL;
   a_strFilePath += "\\ShiftReduce.h";
   fopen_s(&pfile, a_strFilePath.c_str(), "w");
   if (NULL == pfile)
   {
      m_deqErrors.push_back("could not write into common definitions file");
   }
   else
   {
      unsigned int uiStatesAmount = m_pGrammarObj->m_vecLR0States.size(),
         uiSymbolsAmount = m_pGrammarObj->m_setGrammarSymbols.size() - 1; // without extended nonterminal

      fprintf(pfile, "/* File generated by SLR(1) parser generator */\n\n");    //print out header information
      TGF_Conflicts(pfile);                                                     //print out conflict information      
      fprintf(pfile, "%s", "#include \"CommonDef.h\"\n");
      TGF_Rules(pfile);                                                         //print out grammar rules
      TGF_Symbols(pfile, uiSymbolsAmount, true);                                //print out terminals
      TGF_Symbols(pfile, uiSymbolsAmount, false);                               //print out nonterminals
      TGF_RuleLenght(pfile);                                                    //printf out table with rules lengths
      TGF_ProductionNontermID(pfile, uiSymbolsAmount);                          //printf out table with rules production Nonterminal id's
      TGF_ShiftArray(pfile, uiSymbolsAmount, uiStatesAmount, true);             //print out the transition table for terminals   
      TGF_ShiftArray(pfile, uiSymbolsAmount, uiStatesAmount, false);            //print out the transition table for nonterminals
      TGF_ReduceArray(pfile, uiSymbolsAmount, uiStatesAmount);                    //print out the reduce table 

      fclose(pfile);
   }
}


void CPlusCodeGenerator::TGF_Conflicts(FILE* a_pTransitionGraphFile)
{
   unsigned int uiStatesAmount = m_pGrammarObj->m_vecStateActions.size(), i;
   std::string strConflict;

   fprintf(a_pTransitionGraphFile, "/*---------------------------------------------------------------------------\n");
   for (i = 0; i < uiStatesAmount; i++)
   {
      strConflict = m_pGrammarObj->m_vecStateActions[i]->GetConflictInfo();
      if (strConflict.size())
         fprintf(a_pTransitionGraphFile, "Conflicts in state %d:\n%s", i, strConflict.c_str());
   }
   fprintf(a_pTransitionGraphFile, "---------------------------------------------------------------------------*/\n\n");
}


void CPlusCodeGenerator::TGF_Rules(FILE* a_pTransitionGraphFile)
{
   t_mmapInt2VecInt::iterator aux = m_pGrammarObj->m_mmapRules.begin();
   aux++;
   fprintf(a_pTransitionGraphFile, "#ifdef _DEBUG\n\tstatic char* __GrammarRules[RULES_COUNT] = \n\t{\n\t\t/*R0*/ \"[acceptance]\",\n");

   for (unsigned int i = 1; i < m_pGrammarObj->m_mmapRules.size(); i++, aux++)
   {
      fprintf(a_pTransitionGraphFile, "\t\t/*R%d*/ \"%s ->", i, (m_pGrammarObj->m_mapAuxSymbID2Sym[aux->first])->name().c_str());
      for (unsigned int i = 0; i < (aux->second).size(); i++)
         fprintf(a_pTransitionGraphFile, " %s", (aux->second)[i] == m_pGrammarObj->m_iDotID ?
            "." :
            m_pGrammarObj->m_mapAuxSymbID2Sym[(aux->second)[i]]->name().c_str());

      fprintf(a_pTransitionGraphFile, "\",\n");
   }
   fprintf(a_pTransitionGraphFile, "\t};\n#endif //grammar rules\n\n");
}


void CPlusCodeGenerator::TGF_Symbols(FILE* a_pTransitionGraphFile, unsigned int uiSymbolsAmount, bool bPrintForTerminals)
{
   int j, iauxCnt = 0;

   fprintf(a_pTransitionGraphFile, "/*\nGrammar %s:\n", bPrintForTerminals ? "terminals" : "nonterminals");
   for (j = 0; j < uiSymbolsAmount - 1; j++)
   {
      if (m_pGrammarObj->m_mapAuxSymbID2Sym[j]->term() == bPrintForTerminals)
         fprintf(a_pTransitionGraphFile, "%d\t%s\n", iauxCnt++, m_pGrammarObj->m_mapAuxSymbID2Sym[j]->name().c_str());
   }
   if (bPrintForTerminals)
      fprintf(a_pTransitionGraphFile, "%d\t%s\n*/\n\n", iauxCnt, m_pGrammarObj->m_mapAuxSymbID2Sym[m_pGrammarObj->m_iEofID]->name().c_str());
   else
      fprintf(a_pTransitionGraphFile, "*/\n\n");
}


void CPlusCodeGenerator::TGF_RuleLenght(FILE* a_pTransitionGraphFile)
{
   t_mmapInt2VecInt::iterator aux = m_pGrammarObj->m_mmapRules.begin();
   aux++;
   fprintf(a_pTransitionGraphFile, "static int RulesLength[RULES_COUNT] = \n{\n%4d,", 2l);

   for (unsigned int i = 1; i < m_pGrammarObj->m_mmapRules.size(); i++, aux++)
   {
      fprintf(a_pTransitionGraphFile, "%4d,", (aux->second).size());
      if (!(i % 15))
         fprintf(a_pTransitionGraphFile, "\n");
   }
   fprintf(a_pTransitionGraphFile, "\n};//RulesLenght\n\n");
}


void CPlusCodeGenerator::TGF_ProductionNontermID(FILE* a_pTransitionGraphFile, unsigned int uiSymbolsAmount)
{
   t_mmapInt2VecInt::iterator aux = m_pGrammarObj->m_mmapRules.begin();
   aux++;

   std::map<int, int> mapNontermID2OrderNo;
   int j, iAuxCnt = 0;

   for (j = 0; j < uiSymbolsAmount - 1; j++)
      if (!(m_pGrammarObj->m_mapAuxSymbID2Sym[j]->term()))
         mapNontermID2OrderNo[m_pGrammarObj->m_mapAuxSymbID2Sym[j]->id()] = iAuxCnt++;

   fprintf(a_pTransitionGraphFile, "static int Productions[RULES_COUNT] = \n{\n%4d,", -1);

   for (unsigned int i = 1; i < m_pGrammarObj->m_mmapRules.size(); i++, aux++)
   {
      fprintf(a_pTransitionGraphFile, "%4d,", mapNontermID2OrderNo[aux->first]);
      if (!(i % 15))
         fprintf(a_pTransitionGraphFile, "\n");
   }
   fprintf(a_pTransitionGraphFile, "\n}; //Productions\n\n");
}


void CPlusCodeGenerator::TGF_ShiftArray(FILE* a_pTransitionGraphFile, unsigned int uiSymbolsAmount, unsigned int uiStatesAmount, bool bPrintForTerminals)
{
   int i, j, iauxCnt;

   //print out the transition table for terminals
   fprintf(a_pTransitionGraphFile, "static int %s[STATES_COUNT][%s] = {\n", bPrintForTerminals ? "TermShiftArray" : "NontermShiftArray",
      bPrintForTerminals ? "TERM_COUNT" : "NONTERM_COUNT");
   iauxCnt = 0;

   fprintf(a_pTransitionGraphFile, "//\t%");

   for (j = 0; j < uiSymbolsAmount - 1; j++)
      if (m_pGrammarObj->m_mapAuxSymbID2Sym[j]->term() == bPrintForTerminals)
         fprintf(a_pTransitionGraphFile, "\t[%3d]", iauxCnt++);

   if (bPrintForTerminals)
      fprintf(a_pTransitionGraphFile, "\t[%3d]\n", iauxCnt);
   else
      fprintf(a_pTransitionGraphFile, "\n");


   for (i = 0; i < uiStatesAmount; i++)
   {
      fprintf(a_pTransitionGraphFile, "/*%3d*/", i);
      for (j = 0; j < uiSymbolsAmount - 1; j++)
         if (m_pGrammarObj->m_mapAuxSymbID2Sym[j]->term() == bPrintForTerminals)
            fprintf(a_pTransitionGraphFile, "\t%3d,", m_pGrammarObj->m_vecStateActions[i]->GetShiftStateID(j));

      if (bPrintForTerminals)
         fprintf(a_pTransitionGraphFile, "\t%3d,\n", m_pGrammarObj->m_vecStateActions[i]->GetShiftStateID(uiSymbolsAmount - 1));
      else
         fprintf(a_pTransitionGraphFile, "\n");
   }
   fprintf(a_pTransitionGraphFile, "};\n\n");
}


void CPlusCodeGenerator::TGF_ReduceArray(FILE* a_pTransitionGraphFile, unsigned int uiSymbolsAmount, unsigned int uiStatesAmount)
{
   int i, j, iauxCnt;

   //print out the transition table for terminals
   fprintf(a_pTransitionGraphFile, "static int ReduceArray[STATES_COUNT][TERM_COUNT] = {\n");
   fprintf(a_pTransitionGraphFile, "//\t");
   iauxCnt = 0;
   for (j = 0; j < uiSymbolsAmount - 1; j++)
      if (m_pGrammarObj->m_mapAuxSymbID2Sym[j]->term())
         fprintf(a_pTransitionGraphFile, "\t[%3d]", iauxCnt++);

   fprintf(a_pTransitionGraphFile, "\t[%3d]\n", iauxCnt);
   for (i = 0; i < uiStatesAmount; i++)
   {
      fprintf(a_pTransitionGraphFile, "/*%3d*/", i);
      for (j = 0; j < uiSymbolsAmount - 1; j++)
         if (m_pGrammarObj->m_mapAuxSymbID2Sym[j]->term())
            fprintf(a_pTransitionGraphFile, "\t%3d,", m_pGrammarObj->m_vecStateActions[i]->GetReductionID(j));

      fprintf(a_pTransitionGraphFile, "\t%3d,\n", m_pGrammarObj->m_vecStateActions[i]->GetReductionID(uiSymbolsAmount - 1));
   }
   fprintf(a_pTransitionGraphFile, "};\n\n");
}

void CPlusCodeGenerator::GenerateAuxiliaryFileWithStates(std::string a_strFilePath)
{
   FILE* pfile = NULL;
   a_strFilePath += "\\_STATES_.txt";
   fopen_s(&pfile, a_strFilePath.c_str(), "w");
   if (NULL == pfile)
   {
      m_deqErrors.push_back("could not into auxiliary states file");
   }
   else
   {
      for (unsigned int i = 0; i < m_pGrammarObj->m_vecLR0States.size(); i++)
      {
         fprintf(pfile, "S%d : ", i);
         WriteStateInfo(m_pGrammarObj->m_vecLR0States[i], pfile);
      }
      fclose(pfile);
   }
}

void CPlusCodeGenerator::WriteStateInfo(std::multimap<int, std::vector<int>>& mmapState, FILE* file)
{
   int iCnt = 0;
   for (auto it = mmapState.begin(); it != mmapState.end(); it++, ++iCnt)
   {
      fprintf(file, "(%s ->", (m_pGrammarObj->m_mapAuxSymbID2Sym[it->first])->name().c_str());
      for (unsigned int i = 0; i < (it->second).size(); i++)
         fprintf(file, " %s", (it->second)[i] == m_pGrammarObj->m_iDotID ?
            "." :
            m_pGrammarObj->m_mapAuxSymbID2Sym[(it->second)[i]]->name().c_str());
      fprintf(file, ")  ");

      if (iCnt % 4)
         fprintf(file, "\n\t");
   }
   fprintf(file, "\n");
}

} // namespace lrlib