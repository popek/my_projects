#pragma once


class PropertyDialogBase : public CDialog
{
	DECLARE_DYNAMIC(PropertyDialogBase)

protected:
   PropertyDialogBase(UINT a_dialogId);
	virtual ~PropertyDialogBase();

protected:

	DECLARE_MESSAGE_MAP()
   virtual void OnOK();
   virtual void OnCancel();
};
