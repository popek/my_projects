//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by StocksAnalyser.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_DIALOGBAR                   103
#define IDR_MAINFRAME                   128
#define IDR_StocksAnalyserTYPE          129
#define IDD_SEARCH_GMMA_SIGNALS_DIALOG  133
#define IDD_BEGINING_OF_TREND           134
#define IDD_CONSOLIDATION_PARAMETERS    135
#define IDD_INSTRUMENT_LIST_UPDATE_PROGRESS 136
#define IDD_CHANNEL_PROPERTIES          137
#define IDD_DOUBLE_BOTTOM_CONTEXT       138
#define IDD_TRADE_STRATEGY              139
#define IDD_MARKET_SCANNER              141
#define IDC_RADIO1                      1000
#define IDC_EDIT1                       1001
#define IDC_SUPPORT_LINE                1001
#define IDC_CHECK_ALL_SIMPLE_WAY        1001
#define IDC_LIST1                       1002
#define IDC_RESISTANCE_LINE             1002
#define IDC_CHECK_ALL_WITH_REGARDS_TO_ALL_PERIODS 1002
#define IDC_PROGRESS                    1003
#define IDC_CHOOSE_ASSETS_PATH          1004
#define IDC_ASSETS_PATH                 1005
#define IDC_SEARCH_BUY                  1006
#define IDC_ASSETS_LIST                 1007
#define ID_STOP_PROCESSING              1008
#define IDC_PROGRESS_DESC               1009
#define IDC_GMMA_ALGORITHM_TYPE_GROUP   1010
#define IDC_TRADE_STRATEGIES_LIST       1010
#define IDC_BEGINING_OF_TREND           1012
#define IDC_NEAR_BUY_SIGNAL             1014
#define IDC_PARALLEL_CHANNEL            1015
#define IDC_TURNOVER_GROUP              1016
#define IDC_TURNOVER_KANDLES_AMOUNT     1017
#define IDC_TURNOVER_MIN_VALUE          1018
#define IDC_SHORT_ABOVE_LONG            1019
#define IDC_DOUBLE_BOTTOM               1019
#define IDC_SHORT_BELOW_LONG            1020
#define IDC_TURNOVER_MAX_VALUE          1020
#define IDC_BREAK_THROUGH               1021
#define IDC_ADDITIONAL_PARAMS_PLACEHOLDER 1023
#define IDC_MIN_SHORT_ABOVE_LONG        1023
#define IDC_INSTRUMENT_LIST_UPDATE_PROGRESS 1024
#define IDC_INSTRUMENTS_LIST_UPDATE_DESCRIPTION 1025
#define IDC_CHANNEL                     1026
#define IDC_EDIT2                       1027
#define IDC_EDIT_MIN_POINTS_ON_LINE     1028
#define IDC_EDIT_MAX_DEVIATION_TO_LINE_VALUE 1029
#define IDC_EDIT_MAX_DATA_TO_LINE_DISTANCE 1030
#define IDC_                            1031
#define IDC_CONFIRMED_SIGNAL            1031
#define IDC_WEEKLY_CONFIRMATION         1032
#define IDC_EDIT_SEARCH_AREA            1033
#define IDC_CHECK_WEEK_SIGNAL           1033
#define IDC_CHECK_ONLY_BUY_PERIOD       1034
#define IDC_TRADE_STRATEGY_GROUP        1035
#define IDC_EDIT_NEIGHBOURS_COUNT       1036
#define IDC_TEST_STRATEGY               1036
#define IDC_EDIT_DEPTH                  1037
#define IDC_EDIT_LEVELS_DIFF            1038
#define IDC_TEST_START_DATE             1040
#define IDC_TEST_END_DATE               1041
#define IDC_STATIC_ASSET_RESULT_DESCRIPTION 1042
#define IDC_CONFIRM_END_DATE            1043
#define IDC_START_SCAN                  1043
#define IDC_STOP_SCAN                   1044
#define IDC_PROCESSED_ASSET_DESCRIPTION 1045
#define ID_DAILY                        32772
#define ID_WEEKLY                       32774
#define ID_SHOW_TOOLS                   32775
#define ID_SHOW_TOOLS_WINDOW            32776
#define ID_SHOW_STOCKS_TO_BUY           32779
#define ID_ASSETSLIST_UPDATE            32781
#define ID_AKTUALIZAJAIGMMA_URUCHOMTESTERSTRATEGII 32784
#define ID_SHOW_STRATEGY_TESTER         32785
#define ID_AKTUALIZAJAIGMMA_URUCHOMSKANERRYNKU 32786
#define ID_SHOW_MARKET_SCANNER          32787

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        142
#define _APS_NEXT_COMMAND_VALUE         32788
#define _APS_NEXT_CONTROL_VALUE         1046
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
