#pragma once

class IMarketScannerObserver
{
public:
   virtual ~IMarketScannerObserver(void);

   virtual void NotifyTotalAssetsToScanCount(size_t a_assetsToScanCount) = 0;

   virtual void NextAssetScanned(CString a_scannedAssetName, bool a_addToWatchList) = 0;

   virtual void MarketScanFinished() = 0;
};

