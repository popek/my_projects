#include "StdAfx.h"
#include "KandleDate.h"


KandleDate::KandleDate(size_t a_year, size_t a_month, size_t a_day)
: m_nYear(a_year)
, m_nMonth(a_month)
, m_nDay(a_day)
{
}
KandleDate::KandleDate(string& a_strDate, size_t a_index)
{
   FromString(a_strDate);
   m_index = a_index;
}
KandleDate::KandleDate(const KandleDate& a_dateToCopy)
{
   *this = a_dateToCopy;
}
KandleDate& KandleDate::operator = (const KandleDate& a_dateToCopy)
{
   m_nYear  = a_dateToCopy.m_nYear;
   m_nMonth = a_dateToCopy.m_nMonth;
   m_nDay   = a_dateToCopy.m_nDay;

   m_index  = a_dateToCopy.m_index;

   return *this;
}

KandleDate::~KandleDate()
{
}

size_t KandleDate::hash() const
{
   return m_nYear*10000 + m_nMonth*100 + m_nDay;
}

void KandleDate::FromString(string& a_strDate)
{
   long nNumber = atoi(a_strDate.c_str()); // <YYYYMMDD>
   if(nNumber)
   {
      m_nDay   = nNumber%100;
      nNumber  = nNumber/100;
      m_nMonth = nNumber%100;
      m_nYear  = nNumber/100;
   }
}

CString KandleDate::ToString() const
{
   CString strDate;
   strDate.Format(_T("%2.2d.%2.2d.%2.2d"), m_nYear%100, m_nMonth, m_nDay, m_index);
   //strDate.Format(_T("%d"), m_index);

   return strDate;
}

KandleDate::EWeekDay KandleDate::ToWeekDay() const
{  
   /*
      http://pl.wikipedia.org/wiki/Kalendarz_wieczny   
      dzie� tygodnia = ([23m/9] + d + 4 + y + [z/4] - [z/100] + [z/400] - c) mod 7
      gdzie
      [ ] oznacza cz�� ca�kowit� liczby
      mod - funkcja modulo (reszta z dzielenia)
      m - numer miesi�ca (month) (od stycznia = 1 do grudnia = 12)
      d - numer dnia (day) miesi�ca
      y - rok (year)
      z - rok z poprawk�: z = y - 1 je�eli m < 3; z = y, je�eli m >= 3
      c - korekta (correction): c = 0, je�eli m < 3; c = 2, je�eli m >= 3
      dni tygodnia - 0, 1, 2, 3, 4, 5, 6
      gdzie
      0 - niedziela, 1 - poniedzia�ek, 2 - wtorek, 3 - �roda, 4 - czwartek, 5 - pi�tek, 6 - sobota
   */

   int Z = m_nMonth<3 ? m_nYear - 1 : m_nYear;
   int C = m_nMonth<3 ? 0 : 2;

   return (EWeekDay)( ( (23*m_nMonth/9) + m_nDay + 4 + m_nYear + Z/4 - Z/100 + Z/400 - C ) % 7 );
}

int KandleDate::DaysOffset(const KandleDate& a_date) const
{
   int offset = 0;

   if(a_date.m_nYear >= m_nYear)
   {
      for(size_t year=m_nYear; year<a_date.m_nYear - 1; ++year)
      {
         offset += LeapYear(year) ? 366 : 365;
      }

      offset += a_date.ToDayOfYear();

      offset -= ToDayOfYear();
   }
   else
   {
      offset = -a_date.DaysOffset(*this);
   }

   return offset;
}

size_t KandleDate::Index() const
{
   return m_index;
}

int KandleDate::Month() const
{
   return m_nMonth;
}

bool KandleDate::operator < (const KandleDate& a_toCompare) const
{
   return hash() < a_toCompare.hash();
}

shared_ptr<KandleDate> KandleDate::Clone(size_t a_indexForClone) const
{
   shared_ptr<KandleDate> spClone(new KandleDate(*this));
   spClone->m_index = a_indexForClone;

   return spClone;
}

int KandleDate::ToDayOfYear() const
{
   static int daysInMonth[]         = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
   static int daysInMonthLeapYear[] = {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

   static int dayNumber[]           = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};
   static int dayNumberInLeapYear[] = {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366};

   if(LeapYear(m_nYear))
   {
      return dayNumberInLeapYear[m_nMonth] - daysInMonthLeapYear[m_nMonth] + m_nDay;
   }
   else
   {
      return dayNumber[m_nMonth] - daysInMonth[m_nMonth] + m_nDay;
   }
}

bool KandleDate::LeapYear(int a_year)
{
   return (a_year % 4) == 0 && ((a_year % 100) != 0 || (a_year % 400) == 0);
}
// int KandleDate::DaysOfMonth()
// {
//    static int daysOfMonthTable[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
// 
//    if (m_nMonth == 2 && LeapYear())
//       return 29;
//    else
//       return daysOfMonthTable[m_nMonth];
// }