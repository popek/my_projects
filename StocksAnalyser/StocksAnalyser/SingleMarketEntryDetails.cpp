#include "StdAfx.h"
#include "SingleMarketEntryDetails.h"
#include "TransactionDetails.h"


SingleMarketEntryDetails::SingleMarketEntryDetails(shared_ptr<TransactionDetails> a_buy, 
                                                   shared_ptr<TransactionDetails> a_sell)
: _buy(a_buy)
, _sell(a_sell)
{
}
SingleMarketEntryDetails::~SingleMarketEntryDetails(void)
{
}

double SingleMarketEntryDetails::Income(double a_bankroll, double a_brokerFee) const
{
   double sharesCount = (double)(int)(a_bankroll / _buy->price());

   double incomeBrutto = (sharesCount*_sell->price())*(1-a_brokerFee);
   double incomeCost   = (sharesCount*_buy->price())*(1+a_brokerFee);

   return incomeBrutto - incomeCost;
}