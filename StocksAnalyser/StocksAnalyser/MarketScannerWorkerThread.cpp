#include "StdAfx.h"
#include "MarketScannerWorkerThread.h"
#include "TradeStrategyBase.h"
#include "AssetContainer.h"
#include "IMarketScannerObserver.h"
#include "Asset.h"
#include <atlpath.h>


MarketScannerWorkerThread::MarketScannerWorkerThread(void)
   : m_stop(false)
   , m_strategyId(StrategiesUtils::EInvalidStrategyId)
{
}
MarketScannerWorkerThread::~MarketScannerWorkerThread(void)
{
}

BOOL MarketScannerWorkerThread::ProcessThreadMessage(UINT a_messageId)
{
   ProcessAllAssets(m_assetsFilesDirectory);
   return TRUE;
}

void MarketScannerWorkerThread::ProcessAllAssets(CString a_assetsFilesFolder)
{
   WIN32_FIND_DATA   fileData;
   HANDLE            hFile;
   vector<CString>   files;

   // get files
   hFile = FindFirstFile(a_assetsFilesFolder + _T("*.mst"), &fileData);
   if(hFile != INVALID_HANDLE_VALUE)
   {
      do
      {
         files.push_back(fileData.cFileName);
      }
      while(FindNextFile(hFile, &fileData) == TRUE);

      FindClose(hFile);
   }

   m_pScannerObserver->NotifyTotalAssetsToScanCount(files.size());

   // process each and every asset
   for(size_t fileIdx=0; fileIdx<files.size(); ++fileIdx)
   {
      if(m_stop)
      {
         break;
      }

      CPath path;
      path.Combine(a_assetsFilesFolder, files[fileIdx]);

      if(path.FileExists())
      {
         ProcessAsset(path);
      }
   }

   m_pScannerObserver->MarketScanFinished();
}

void MarketScannerWorkerThread::ProcessAsset(CString a_fileFullPath)
{
   // read asset
   shared_ptr<AssetContainer> spAssetContainer(new AssetContainer());
   spAssetContainer->LoadDaysAssetFromFile(a_fileFullPath);

   // if turnover conditions are not met then do not process asset
   if(!AssetTurnoverOK((*spAssetContainer)[eDay]))
   {
      m_pScannerObserver->NextAssetScanned(a_fileFullPath, false);
      return;
   }

   auto spStrategy = StrategiesUtils::CreateStrategy(m_strategyId, (*spAssetContainer)[eDay]);
   
   m_pScannerObserver->NextAssetScanned(a_fileFullPath, 
                        spStrategy.get() && spStrategy->TakePosition(TradeStrategyBase::ELong));
}

bool MarketScannerWorkerThread::AssetTurnoverOK(shared_ptr<Asset> a_spAsset) const
{
   const int turnoverKandleAmount = 10;
   const int minTurnoverTotal = 50000;
   const int maxTurnoverTotal = 200000;

   double assetTotalTurnover = 0;
   if(a_spAsset->Size() >= (size_t)turnoverKandleAmount)
   {
      size_t counter = 0;

      for(size_t kandleOffset=1; kandleOffset<=(size_t)turnoverKandleAmount; ++kandleOffset)
      {
         assetTotalTurnover += (*a_spAsset)[a_spAsset->Size()-kandleOffset].TurnOver();
      }
   }

   return assetTotalTurnover >= minTurnoverTotal && assetTotalTurnover <= maxTurnoverTotal;
}

void MarketScannerWorkerThread::RunMarketScan(IMarketScannerObserver* a_pObserver, 
                                              StrategiesUtils::strategyId a_strategyId,  
                                              CString a_assetsFilesDir)
{
   m_pScannerObserver = a_pObserver;
   m_assetsFilesDirectory = a_assetsFilesDir;
   m_strategyId = a_strategyId;
   m_stop = false;

   // crate thread
   Create();
   // run thread
   Resume();

   // post one and only one message (message no. is not important)
   PostMessage(0);
}

void MarketScannerWorkerThread::StopMarketScan()
{
   m_stop = true;
}