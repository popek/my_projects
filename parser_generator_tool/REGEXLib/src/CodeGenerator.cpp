#include "CodeGenerator.h"
#include <foundation\TextFileWriter.h>

namespace regexlib
{

CodeGenerator::CodeGenerator(t_Char2SymbolID& Char2SymID, t_DFAdiagram& DFA, t_DFAAcceptingState2RegExNo& Accept, t_RegExDefs& reName2reDef, unsigned int uiDFAStatesAmount) :
   _Char2SymID(Char2SymID),
   _DFA(DFA),
   _Accept(Accept),
   _reName2reDef(reName2reDef),
   _uiDFAStatesAmount(uiDFAStatesAmount)
{
   //initialize auxiliary container of symbol ids
   t_Char2SymbolID::iterator it = _Char2SymID.begin();
   while (it != _Char2SymID.end())
   {
      _setSymIDs.insert(it->second);
      it++;
   }
}

CodeGenerator::~CodeGenerator(void)
{
}

bool CodeGenerator::GenerateCode(const char* path)
{
   TextFileWriter fileWriter(std::string(path) + "\\LexicalAnalyzer.cpp");

   fileWriter.WriteText("#include \"stdafx.h\"\n#include \"LexicalAnalyzer.h\"\n\n");
   WriteDFAtable(fileWriter);
   WriteChar2ColIDtable(fileWriter);
   WriteAcceptStatesTable(fileWriter);
   WriteGetNextLexAtomMethod(fileWriter);

   WriteLexHeaderFile(path);
   WriteTerminalIdsHeader(path);
   WriteLexerTypesHeader(path);

   return true;
}

void CodeGenerator::WriteDFAtable(TextFileWriter& a_fileWritter)
{
   t_DFAdiagram::iterator DFAit = _DFA.begin();
   t_DFAtransition::iterator tranIt;
   unsigned int iAuxCnt = 0;
   std::set<short>::iterator SymIDsIt;

   a_fileWritter.WriteText("static int LexDFA[%d][%d] =\n{\n", _uiDFAStatesAmount, _setSymIDs.size());
   //fprintf header of state machine
   a_fileWritter.WriteText("//");
   for (SymIDsIt = _setSymIDs.begin(); SymIDsIt != _setSymIDs.end(); SymIDsIt++)
      a_fileWritter.WriteText("\t%d", *SymIDsIt);
   a_fileWritter.WriteText("\n");

   for (iAuxCnt = 0; iAuxCnt < _uiDFAStatesAmount; iAuxCnt++)
   {
      DFAit = _DFA.find(iAuxCnt);
      if (DFAit != _DFA.end()) // if there is at least one transition
      {
         t_DFAtransition& transitions = DFAit->second;
         SymIDsIt = _setSymIDs.begin();
         for (SymIDsIt = _setSymIDs.begin(); SymIDsIt != _setSymIDs.end(); SymIDsIt++)
         {
            if ((tranIt = transitions.find(*SymIDsIt)) == transitions.end())
               a_fileWritter.WriteText("\t-1,");
            else
               a_fileWritter.WriteText("\t%d,", tranIt->second);
         }
      }
      else // if there is no transition (probably state is acceptable)
      {
         unsigned int uiAuxSize = _setSymIDs.size();
         while (uiAuxSize--)
            a_fileWritter.WriteText("\t-1,");
      }
      a_fileWritter.WriteText("\n");

   }
   a_fileWritter.WriteText("};\n\n");
}

void CodeGenerator::WriteChar2ColIDtable(TextFileWriter& a_fileWritter)
{
   a_fileWritter.WriteText("static short Char2ColID[256] =\n{\n\t", *(_setSymIDs.begin()) < 0 ? *(_setSymIDs.begin()) - 1 : -1);
   t_Char2SymbolID::iterator it = _Char2SymID.begin();
   unsigned int iAuxCnt;
   for (iAuxCnt = 0; iAuxCnt < 256; iAuxCnt++)
   {
      if (it != _Char2SymID.end() &&
         it->first == iAuxCnt)
      {
         a_fileWritter.WriteText("%d,\t", symbolID2ColID(it->second));
         it++;
      }
      else
         a_fileWritter.WriteText("-1,\t");

      if (iAuxCnt > 0 && iAuxCnt % 8 == 0)
         a_fileWritter.WriteText("\n\t");
   }
   a_fileWritter.WriteText("\n};\n\n");
}

void CodeGenerator::WriteAcceptStatesTable(TextFileWriter& a_fileWritter)
{
   a_fileWritter.WriteText("static int AcceptedStates[%d] =\n{\n\t", _uiDFAStatesAmount);
   unsigned int i;
   t_DFAAcceptingState2RegExNo::iterator AcceptIt;
   t_DFAdiagram::iterator DFAit;
   for (i = 0; i < _uiDFAStatesAmount; i++)
   {
      if ((AcceptIt = _Accept.find(i)) != _Accept.end())
         a_fileWritter.WriteText("%s,\t", regexID2regexEnum(AcceptIt->second).c_str());
      else
         a_fileWritter.WriteText("-1,\t");

      if (i > 0 && i % 8 == 0)
         a_fileWritter.WriteText("\n\t");
   }

   a_fileWritter.WriteText("\n};\n\n");
}

void CodeGenerator::WriteGetNextLexAtomMethod(TextFileWriter& a_fileWritter)
{
   a_fileWritter.WriteText(

      "ETerminalId LexicalAnalyzer::GetAtom(LexAtom* a_pLexAtom)\n"
      "{\n"
      "   int     state     = 0;\n"
      "   char_t  character = 0;\n"
      "   int     charsCnt  = 0;\n"
      "   stack<int> steps;\n"
      "\n"
      "   // check if end of file had been reached during last GetAtom call\n"
      "   if(_EOF)\n"
      "   {\n"
      "      a_pLexAtom->_text = \"\";\n"
      "      a_pLexAtom->_id = Eeof;\n"
      "      return Eeof;\n"
      "   }\n"
      "\n"
      "   while(state>-1)\n"
      "   {\n"
      "      steps.push(state);\n"
      "      character=readNextChar();\n"
      "\n"
      "      // save next token character\n"
      "      _token[charsCnt] = character;\n"
      "      ++charsCnt;\n"
      "\n"
      "      // move to another state\n"
      "      if(Char2ColID[character]>-1)\n"
      "      {\n"
      "         state = LexDFA[state][Char2ColID[character]];\n"
      "      }\n"
      "      else\n"
      "      {\n"
      "         // if EOF reached (during last char read) then break (else return error token)\n"
      "         if(_EOF)\n"
      "         {\n"
      "            break;\n"
      "         }\n"
      "         else\n"
      "         {\n"
      "            return Eerror;\n"
      "         }\n"
      "      }\n"
      "   }\n"
      "\n"
      "   //remember last atom text (in case atom will be invalid)\n"
      "   a_pLexAtom->_text = _token;\n"
      "\n"
      "   while(steps.size())\n"
      "   {\n"
      "      // \"give back\" last red character\n"
      "      writeBackChar();\n"
      "      --charsCnt;\n"
      "      _token[charsCnt] = _T('\\0');\n"
      "\n"
      "      state = steps.top();\n"
      "      steps.pop();\n"
      "\n"
      "      // if \"remembered\" state is accepting state then return it\n"
      "      if(AcceptedStates[state]>-1)\n"
      "      {\n"
      "         a_pLexAtom->_text = _token;\n"
      "         a_pLexAtom->_id = (ETerminalId)AcceptedStates[state];\n"
      "         return a_pLexAtom->_id;\n"
      "      }\n"
      "\n"
      "   }\n"
      "\n"
      "   return Eerror;\n"
      "}\n\n"
      );
}

short CodeGenerator::symbolID2ColID(short symID)
{
   short uiColID = 0;
   auto it = _setSymIDs.begin();
   while ((it != _setSymIDs.end()) && (symID != *it))
   {
      uiColID++;
      it++;
   }
   if (it == _setSymIDs.end()) uiColID = -1;

   return uiColID;
}

std::string CodeGenerator::regexID2regexEnum(unsigned int uiRegexNo)
{
   return regexName2regexEnum(_reName2reDef[uiRegexNo].first);
}

std::string CodeGenerator::regexName2regexEnum(std::string strRegExName)
{
   return ("E" + strRegExName);
}

void CodeGenerator::WriteLexHeaderFile(const char* a_folderPath)
{
   TextFileWriter fileWritter(std::string(a_folderPath) + "\\LexicalAnalyzer.h");

   fileWritter.WriteText
      (
         "#pragma once\n"
         "#include \"ETerminalId.h\"\n"
         "#include \"typeDefs.h\"\n"
         "\n\n"
         "// lex atom class\n"
         "class LexAtom\n"
         "{\n"
         "public:\n"
         "  int         _line;\n"
         "  ETerminalId _id;\n"
         "  string_t    _text;\n"
         "  double      _dValue;\n"
         "  int         _iValue;\n"
         "};\n"
         "\n"
         "\n"
         "// lexical analyzer class\n"
         "class LexicalAnalyzer\n{\n"
         "public:\n\n"
         "\t//read next token\n"
         "\tETerminalId GetAtom(LexAtom* a_pLexAtom);\n\n"
         "protected:\n"
         "\t//read next character from input stream\n"
         "\tvirtual char_t readNextChar() = 0;\n\n"
         "\t//writes back last character\n"
         "\tvirtual void  writeBackChar() = 0;\n\n"
         "\tchar_t _token[1024];         // auxiliary container for token string\n"
         "\tbool  _EOF;                 // flag set during reading character. Set when end of file have beed reached\n\n"
         "};\n"
         );
}

void CodeGenerator::WriteTerminalIdsHeader(const char* a_folderPath)
{
   TextFileWriter fileWritter(std::string(a_folderPath) + "\\ETerminalId.h");

   fileWritter.WriteText("#pragma once\n\n"
      "enum ETerminalId\n{\n");

   for (unsigned int ui = 0; ui < _reName2reDef.size(); ui++)
   {
      fileWritter.WriteText("\t%s,\n", regexName2regexEnum(_reName2reDef[ui].first).c_str());
   }

   fileWritter.WriteText("\tEeof,\n"
      "\tEerror\n};\n");
}

void CodeGenerator::WriteLexerTypesHeader(const char* a_folderPath)
{
   TextFileWriter fileWritter(std::string(a_folderPath) + "\\typeDefs.h");

   fileWritter.WriteText(
      "#pragma once\n\n\n"
      "typedef CString  string_t;\n"
      "typedef TCHAR    char_t;\n");
}

} // namespace regexlib

