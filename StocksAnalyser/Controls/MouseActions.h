/**
 * @file
 * @author  Marcin Wrobel <marwrobel@gmail.com>
 *
 * @section DESCRIPTION
 * class managing chart mouse actions
 */
 
#pragma once
#include "IMouseInteraction.h"

class MouseActions : public IMouseInteraction
{
public:
   MouseActions(void);
   virtual ~MouseActions(void);

   void         AddAction(shared_ptr<IMouseInteraction> a_action);

   virtual void OnLButtonDown(UINT a_nFlags, const CPoint& a_point);

   virtual void OnLButtonUp(UINT a_nFlags, const CPoint& a_point);

   virtual void OnMouseMove(UINT a_nFlags, const CPoint& a_point);

   virtual void OnSize(int a_chartWidth);

   virtual void Draw(CDC& a_dc);

   virtual bool DrawingRequired() const;

protected:

   vector<shared_ptr<IMouseInteraction>> m_vActions;
};
