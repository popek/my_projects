#pragma once

template<class _T>
class sharedPtrLess
{
public:

   bool operator()(const shared_ptr<_T>& _spLeft, const shared_ptr<_T>& _spRight) const
   {
      return (*_spLeft < *_spRight);
   }
};
