#include "stdafx.h"
#include "StocksAnalyser.h"
#include "TradeStrategiesTesterDlg.h"
#include "Strategy_MACD.h"
#include "MeanMax_strategy.h"
#include "MeanMin_strategy.h"
#include "MeanMaxMin_strategy.h"
#include "StrategyResult.h"
#include "AssetsListBuffer.h"
#include "AssetContainer.h"
#include "Asset.h"
#include "Misc.h"
#include "ClientRect.h"
#include "Misc.h"
#include "PathUtils.h"
#include <Shlwapi.h>
#include <atlpath.h>


class SubincomeObserver : public StrategyResult::ISubIncomeObserver
{
public:
   SubincomeObserver(double initialiBankroll)
      : _initialiBankroll(initialiBankroll)
      , _internalIndex(0)
   {
      _description = _T("");
   }
   virtual void Notify(double a_bankrollAfterTrade)
   {
      CString subIncomeDesc;
      subIncomeDesc.Format(_T("[%d]%2.2f(%d%c)\n"), 
                           ++_internalIndex, 
                           a_bankrollAfterTrade,
                           (int)(100*(a_bankrollAfterTrade/_initialiBankroll - 1)),
                           _T('%'));
      _description.Append(subIncomeDesc);
   }

   operator LPCTSTR()
   {
      return _description;
   }

private:
   CString _description;
   double  _initialiBankroll;
   size_t  _internalIndex;
};

const UINT g_updateUiMsg = WM_USER + 1;
const UINT g_setInstrumentsCountMsg = WM_USER + 2;
const UINT g_nextInstrumentProcessedMsg = WM_USER + 3;
const UINT g_addAssetMeetingRequirements = WM_USER + 4;


const UINT g_processingFinished = WM_USER + 5;

IMPLEMENT_DYNAMIC(TradeStrategiesTesterDlg, CDialog)

TradeStrategiesTesterDlg::TradeStrategiesTesterDlg()
	: CDialog(TradeStrategiesTesterDlg::IDD, NULL)
   , _assetCounter(0)
   , _assetToTestAmount(0)
{
}
TradeStrategiesTesterDlg::~TradeStrategiesTesterDlg()
{
}

void TradeStrategiesTesterDlg::onAssetsAmountEstablished(int assetsToBacktestAmount)
{
   _assetToTestAmount = assetsToBacktestAmount;
   _assetCounter = 0;

   shared_ptr<ui_message> spMessage(new ui_message(g_setInstrumentsCountMsg, _T("")));
   spMessage->_value = _assetCounter;

   _updateUIMessagesQueue.AddTail(spMessage);
   PostMessage(g_updateUiMsg);
}

void TradeStrategiesTesterDlg::onAssetTested(const CString& assetFilePath, shared_ptr<StrategyResult> result)
{
   // update description text
   CString desc;
   desc.Format(_T("%s (%d/%d) "), PathUtils::extractFileName(assetFilePath), ++_assetCounter, _assetToTestAmount);
   _updateUIMessagesQueue.AddTail(make_shared<ui_message>(g_nextInstrumentProcessedMsg, desc));
   PostMessage(g_updateUiMsg);

   if(!result)
   {
      return;
   }

   CString fullDescription;
   double initialBankroll = 10000;

   // run back-test period
   CString description;
   SubincomeObserver additionalInfo(initialBankroll);

   double income = result->Income(initialBankroll, &additionalInfo);
   description.Format(_T("bankroll: %2.2f\nincome:%2.2f (%d%%)\n"), initialBankroll, income, (int)(100*income/initialBankroll));
   description.Append((LPCTSTR)additionalInfo);
   description.Append(_T("\n"));

   fullDescription.Append(description);

   auto spMessage(make_shared<ui_message>(g_addAssetMeetingRequirements, assetFilePath));
   spMessage->_additionalText = fullDescription;
   _updateUIMessagesQueue.AddTail(spMessage);
   PostMessage(g_updateUiMsg);
}

void TradeStrategiesTesterDlg::onBactestFinished()
{
   PostMessage(g_processingFinished);
}

bool TradeStrategiesTesterDlg::AssetTurnoverOK(shared_ptr<Asset> a_spAsset) const
{
   const int turnoverKandleAmount = 10;
   const int minTurnoverTotal = turnoverKandleAmount * 50000;

   double assetTotalTurnover = 0;
   if(a_spAsset->Size() >= (size_t)turnoverKandleAmount)
   {
      size_t counter = 0;

      for(size_t kandleOffset=1; kandleOffset<=(size_t)turnoverKandleAmount; ++kandleOffset)
      {
         assetTotalTurnover += (*a_spAsset)[a_spAsset->Size()-kandleOffset].TurnOver();
      }
   }

   return assetTotalTurnover >= minTurnoverTotal;
}

void TradeStrategiesTesterDlg::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_TEST_START_DATE, _fromDate);
   DDX_Control(pDX, IDC_TEST_END_DATE, _toDate);
   DDX_Control(pDX, IDC_ASSETS_PATH, _assetsDirectory);
   DDX_Control(pDX, IDC_TRADE_STRATEGIES_LIST, _strategiesList);
   DDX_Control(pDX, IDC_ASSETS_LIST, _resultsList);
   DDX_Control(pDX, IDC_PROGRESS, _progress);
   DDX_Control(pDX, IDC_CONFIRM_END_DATE, _confirmStrategyEndDate);
}

bool TradeStrategiesTesterDlg::ReadyToProcessAssets()
{
   if(PathUtils::doesDirectoryExist(_assetsDirectory))
   {
      AfxMessageBox(_T("niepoprawny katalog z plikami instrumentów"), MB_OK);
      return false;
   }

   return true;
}

void TradeStrategiesTesterDlg::BeforeProcessing()
{
   GetDlgItem(IDC_TEST_STRATEGY)->EnableWindow(FALSE);
   GetDlgItem(ID_STOP_PROCESSING)->EnableWindow(TRUE);

   _assetsListMeetingRequirements.clear();
   _resultsList.DeleteAllItems();

   _assetsListMeetingRequirementsResult.clear();

   _progress.SetPos(0);
}

BEGIN_MESSAGE_MAP(TradeStrategiesTesterDlg, CDialog)
   ON_BN_CLICKED(IDC_CHOOSE_ASSETS_PATH, &TradeStrategiesTesterDlg::OnBnClickedChooseAssetsPath)
   ON_BN_CLICKED(IDC_TEST_STRATEGY, &TradeStrategiesTesterDlg::OnBnClickedTestStrategy)
   ON_BN_CLICKED(ID_STOP_PROCESSING, &TradeStrategiesTesterDlg::OnBnClickedStopProcessing)
   ON_NOTIFY(LVN_ITEMCHANGED, IDC_ASSETS_LIST, &TradeStrategiesTesterDlg::OnLvnItemchangedAssetsList)
   ON_NOTIFY(NM_DBLCLK, IDC_ASSETS_LIST, &TradeStrategiesTesterDlg::OnNMDblclkAssetsList)
END_MESSAGE_MAP()


// TradeStrategiesTesterDlg message handlers

void TradeStrategiesTesterDlg::OnBnClickedChooseAssetsPath()
{
   _assetsDirectory.SetWindowText(Misc::ChooseAssetsFilesDirectory());
}

void TradeStrategiesTesterDlg::OnBnClickedTestStrategy()
{
   UpdateData(TRUE);

   if(ReadyToProcessAssets())
   {
      BeforeProcessing();

      CString assetsDirectory;
      _assetsDirectory.GetWindowText(assetsDirectory);

      _thread.startBacktest( _strategiesList.GetSelectionMark(), 
                             assetsDirectory,
                             this, 
                             Misc::ExtractKandleDate(_fromDate), 
                             Misc::ExtractKandleDate(_toDate) );
   }
}

void TradeStrategiesTesterDlg::OnBnClickedStopProcessing()
{
   _thread.cancelBacktest();
}

BOOL TradeStrategiesTesterDlg::OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
   switch(message)
   {
   case g_updateUiMsg:
      ProcessUiMessages();
      return TRUE;

   case g_processingFinished:
      GetDlgItem(IDC_TEST_STRATEGY)->EnableWindow(TRUE);
      GetDlgItem(ID_STOP_PROCESSING)->EnableWindow(FALSE);
       
      return TRUE;
   }

   return __super::OnWndMsg(message, wParam, lParam, pResult);
}


void TradeStrategiesTesterDlg::ProcessUiMessages()
{
   while(_updateUIMessagesQueue.Size())
   {
      shared_ptr<ui_message> spMessage = _updateUIMessagesQueue.GetFront();

      switch(spMessage->_id)
      {
      case g_nextInstrumentProcessedMsg:
         GetDlgItem(IDC_PROGRESS_DESC)->SetWindowText(spMessage->_text);
         _progress.OffsetPos(1);
         break;

      case g_setInstrumentsCountMsg:
         _progress.SetRange32(0, spMessage->_value);
         break;

      case g_addAssetMeetingRequirements:
         _assetsListMeetingRequirements.push_back(spMessage->_text);
         _assetsListMeetingRequirementsResult.push_back(spMessage->_additionalText);

         _resultsList.InsertItem(_resultsList.GetItemCount(), PathUtils::extractFileName(spMessage->_text));
         break;
      }

      _updateUIMessagesQueue.ReleaseFront();
   }
}

BOOL TradeStrategiesTesterDlg::OnInitDialog()
{
   __super::OnInitDialog();

   GetDlgItem(IDC_TEST_STRATEGY)->EnableWindow(TRUE);
   GetDlgItem(ID_STOP_PROCESSING)->EnableWindow(FALSE);

   ClientRect rcListBox(_resultsList);
   _resultsList.InsertColumn(0, _T(""));
   _resultsList.SetColumnWidth(0, rcListBox.Width()-10);

   ClientRect rcListBox2(_strategiesList);
   _strategiesList.InsertColumn(0, _T(""));
   _strategiesList.SetColumnWidth(0, rcListBox2.Width()-10);

   _strategiesList.InsertItem(_strategiesList.GetItemCount(), _T("MACD"));
   _strategiesList.InsertItem(_strategiesList.GetItemCount(), _T("Mean-max"));
   _strategiesList.InsertItem(_strategiesList.GetItemCount(), _T("Mean-min"));
   _strategiesList.InsertItem(_strategiesList.GetItemCount(), _T("Mean-max-min"));

   _strategiesList.SetSelectionMark(0);

   COleDateTime yearBefore = COleDateTime::GetCurrentTime();
   yearBefore -= COleDateTimeSpan(366, 0, 0, 0);

   COleDateTime twoYearsBefore = yearBefore;
   twoYearsBefore -= COleDateTimeSpan(366, 0, 0, 0);

   _fromDate.SetTime(twoYearsBefore);
   _toDate.SetTime(yearBefore);
   _confirmStrategyEndDate.SetTime(COleDateTime::GetCurrentTime());

   UpdateData(FALSE);

   return TRUE;
}

void TradeStrategiesTesterDlg::OnLvnItemchangedAssetsList(NMHDR *pNMHDR, LRESULT *pResult)
{
   LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
   
   GetDlgItem(IDC_STATIC_ASSET_RESULT_DESCRIPTION)->SetWindowText(_assetsListMeetingRequirementsResult[ pNMLV->iItem ]);

   *pResult = 0;
}

void TradeStrategiesTesterDlg::OnNMDblclkAssetsList(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  if(pNMItemActivate->iItem > -1)
  {
     G_stockAnalyserApp.OpenDocumentFile( _assetsListMeetingRequirements[pNMItemActivate->iItem] );
  }

  *pResult = 0;
}
