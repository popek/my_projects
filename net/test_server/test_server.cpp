// test_server.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#pragma comment(lib, "Ws2_32.lib")

#include <sdkddkver.h>

#include <WinSock2.h>
#include <Windows.h>

#include <iostream>
#include <thread>

//#define SCK_VERSION2 0x0202

using namespace std;

void exitProgram(int code)
{
   system("PAUSE");
   exit(code);
}

int _tmain(int argc, _TCHAR* argv[])
{
   int res = 0;

   WSAData winSockData;
   WORD dllVersion = MAKEWORD(2, 1);

   if(WSAStartup(dllVersion, &winSockData))
   {
      cout << "failed to init socket lib";
      exitProgram(1);
   }

   SOCKADDR_IN address;
   int addressLen = sizeof(SOCKADDR_IN);
   address.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
   address.sin_family = AF_INET;
   address.sin_port = htons(444);

   SOCKET sock_listen;
   sock_listen = socket(AF_INET, SOCK_STREAM, NULL);
   if(INVALID_SOCKET == sock_listen)
   {
      cout << "fail when creating listen socket. Error code: " << WSAGetLastError() << endl;
      WSACleanup();
      exitProgram(1);
   }

   res = ::bind(sock_listen, (SOCKADDR*)&address, sizeof(address));
   if(SOCKET_ERROR == res)
   {
      cout << "failed to bind listend socket. Error: " << WSAGetLastError() << endl;
      WSACleanup();
      exitProgram(1);
   }

   res = listen(sock_listen, SOMAXCONN);
   if(SOCKET_ERROR == res)
   {
      cout << "failed to start listen. Error: " << WSAGetLastError() << endl;
      WSACleanup();
      exitProgram(1);
   }

   long receivedRequestsCount = 1;
   char request[512];
   char response[512];


   for(;;)
   {
      cout << "Server: waiting for incomming connection...";

      SOCKET sock_connection;
      sock_connection = socket(AF_INET, SOCK_STREAM, NULL);
      if(INVALID_SOCKET == sock_connection)
      {
         cout << "error when trying to create server socket. Error: " << WSAGetLastError() << endl;
         WSACleanup();
         exitProgram(1);
      }

      sock_connection = accept(sock_listen, (SOCKADDR*)&address, &addressLen);
      if(INVALID_SOCKET == sock_connection)
      {
         cout << "error while accepting connection. Error: " << WSAGetLastError() << endl;
         WSACleanup();
         exitProgram(1);
      }

      cout << "connection was found!" << endl;
      
      std::thread t([&]() {
         res = recv(sock_connection, request, sizeof(request), NULL);
         if(SOCKET_ERROR == res)
         {
            cout << "error while receving data on channel. Error: " << WSAGetLastError() << endl;
            closesocket(sock_connection);
            return;
         }
         request[res] = '\0';

         while(res > 0)
         {
            cout << "request is: " << request << endl;
            sprintf_s(response, "server responsd to request no %d", receivedRequestsCount++);
            cout << "response is: " << response << endl;

            res = send(sock_connection, response, strlen(response), NULL);
            if(SOCKET_ERROR == res)
            {
               cout << "error while sending data on channel. Error: " << WSAGetLastError() << endl;
               closesocket(sock_connection);
               return;
            }

            res = recv(sock_connection, request, sizeof(request), NULL);
            if(SOCKET_ERROR == res)
            {
               cout << "error while receving data on channel. Error: " << WSAGetLastError() << endl;
               closesocket(sock_connection);
               return;
            }
            request[res] = '\0';
         }

         closesocket(sock_connection);
         cout << "channel closed." << endl;

      });
      t.join();         
   }
}

