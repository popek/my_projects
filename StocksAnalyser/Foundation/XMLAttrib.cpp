#include "StdAfx.h"
#include "XMLAttrib.h"


XMLAttrib::XMLAttrib(LPCTSTR name, LPCTSTR value)
   : XMLItem(name)
   , _value(value)
{
}
XMLAttrib::~XMLAttrib(void)
{
}

XMLAttrib::operator LPCTSTR() const
{
   return (LPCTSTR)_value;
}
