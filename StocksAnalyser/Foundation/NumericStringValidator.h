#pragma once
#include "FoundationLib.h"
#include "StringValidator.h"


class FOUNDATION_LIB NumericStringValidator : public StringValidator
{
   enum ECharClass
   {
      ESign,
      EDigit,
      ESeparator,
      
      EInvalidChar
   };

   enum State
   {
      EStartState,
      EAfterSignState,
      EAfterSeparatorState,
      EFinalState
   };

public:
   NumericStringValidator(void);
   virtual ~NumericStringValidator(void);

   virtual bool ProperLeftSubstring(const CString& a_text) const;

   virtual bool ProperString(const CString& a_text) const;

   virtual bool Valid(const TCHAR a_char) const;

protected:

   bool ValidChar(const TCHAR a_char) const;

   ECharClass CharClass(const TCHAR a_char) const;

protected:

   TCHAR   m_zero;
   TCHAR   m_nine;
   TCHAR   m_separator;
   TCHAR   m_minus;
   TCHAR   m_plus;

   mutable State   m_state;
};
