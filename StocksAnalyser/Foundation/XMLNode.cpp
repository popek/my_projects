#include "StdAfx.h"
#include "XMLNode.h"
#include "XMLAttrib.h"
#include <algorithm>


XMLNode::XMLNode(LPCTSTR name)
   : XMLItem(name)
{
}
XMLNode::~XMLNode(void)
{
}

void XMLNode::addParam(LPCTSTR paramaName, LPCTSTR paramValue)
{
   _attribs.push_back(make_shared<XMLAttrib>(paramaName, paramValue));
}

void XMLNode::addNode(shared_ptr<XMLNode> node)
{
   _nodes.push_back(node);
}

shared_ptr<XMLNode> XMLNode::addNode(LPCTSTR nodeName, LPCTSTR nodeText)
{
   auto node = make_shared<XMLNode>(nodeName);
   node->setText(nodeText);

   addNode(node);

   return node;
}

void XMLNode::setText(LPCTSTR nodeText)
{
   _text = nodeText;
}

const CString& XMLNode::getText() const
{
   return _text;
}

void XMLNode::traverseNode(IXMLDataObserver* observer) const
{
   observer->onEnterNewNode(getName(), _text);
   
   std::for_each(_attribs.begin(), _attribs.end(),
      [&observer](const shared_ptr<XMLAttrib>& attrib)
   {
      observer->onAttribute(attrib->getName(), (LPCTSTR)(*attrib));
   });

   std::for_each(_nodes.begin(), _nodes.end(),
   [&observer](const shared_ptr<XMLNode>& node)
   {
      node->traverseNode(observer);
   });

   observer->onLeaveNode();
}