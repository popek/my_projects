#pragma once
#include "ControlsLib.h"


class StringValidator;


class CONTROLS_LIB RegularTextEdit : public CEdit
{
	DECLARE_DYNAMIC(RegularTextEdit)

public:
	RegularTextEdit();
	virtual ~RegularTextEdit();

   void SetValidator(shared_ptr<StringValidator> a_spValidator);

protected:

   bool SpecialChar(const TCHAR a_char) const;

   CString GetClipboardText() const;

   CString TextAfterTextInsert(CString a_textToAdd);

   CString TextAfterCharInsert(TCHAR a_char) const;

   virtual BOOL PreTranslateMessage(MSG *a_pMSG);

	DECLARE_MESSAGE_MAP()
   afx_msg LRESULT OnPaste(WPARAM wParam, LPARAM lParam);

protected:
   shared_ptr<StringValidator> m_spStringValidator;

   static set<TCHAR> Sm_specialChars;
public:
   afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
};


