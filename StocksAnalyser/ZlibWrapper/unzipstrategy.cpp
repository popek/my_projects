#include "StdAfx.h"
#include "unzipstrategy.h"
#include "iowin32.h"


#define WRITEBUFFERSIZE (8192)


namespace zlibwrapper
{
   unzipstrategy::unzipstrategy(void)
      : _pOutFilePath(nullptr)
      , _pFileNameStart(nullptr)
      , _pFilename_inzip(nullptr)
      , _maxFileName(_MAX_FNAME)
      , _buffer(nullptr)
   {
   }

   unzipstrategy::~unzipstrategy(void)
   {
      delete [] _pOutFilePath;
      delete [] _pFilename_inzip;

      free(_buffer);
   }

   int unzipstrategy::extract(const char *zipfilename, const char* outFolderPath)
   {
      int folderPathLen = strlen(outFolderPath);
      _pOutFilePath = new char[_MAX_PATH];
      strcpy_s(_pOutFilePath, _MAX_PATH, outFolderPath);

      if(_pOutFilePath[folderPathLen-1] != '\\' || _pOutFilePath[folderPathLen-1] != '/')
      {
         ++folderPathLen;
         _pOutFilePath[folderPathLen-1] = '\\';
      }

      _pFileNameStart = _pOutFilePath + folderPathLen;
      _maxFileName -= folderPathLen;
      _pFilename_inzip = new char[_maxFileName];

      _buffer = (void*)malloc(WRITEBUFFERSIZE);
      if(nullptr == _buffer)
      {
         return UNZ_INTERNALERROR;
      }
      
      unzFile uf = unzOpen64(zipfilename);
      
      int result = uf ? extract(uf, nullptr, outFolderPath) : UNZ_INTERNALERROR;

      unzClose(uf);

      return result;
   }

   
   int unzipstrategy::extract(unzFile uf, const char* password, const char* outFolderPath)
   {
      uLong i;
      unz_global_info64 gi;
      int err;
      FILE* fout = nullptr;

      err = unzGetGlobalInfo64(uf,&gi);
      if (err!=UNZ_OK)
      {
         // notify error
      }
         

      for (i=0;i<gi.number_entry;i++)
      {
         if (extract_currentfile(uf, password, outFolderPath) != UNZ_OK)
            break;

         if ((i+1)<gi.number_entry)
         {
            err = unzGoToNextFile(uf);
            if (err!=UNZ_OK)
            {
               // notify error
               break;
            }
         }
      }
      return UNZ_OK;
   }

   int unzipstrategy::extract_currentfile(unzFile uf, const char* password, const char* outFolderPath)
   {
      int err=UNZ_OK;
      FILE *fout=NULL;
      
      unz_file_info64 file_info;
      uLong ratio=0;
      err = unzGetCurrentFileInfo64(uf,&file_info,_pFilename_inzip,_maxFileName,NULL,0,NULL,0);

      if (err!=UNZ_OK)
      {
         return err;
      }

      strcpy_s(_pFileNameStart, _maxFileName, _pFilename_inzip);
      
      err = unzOpenCurrentFilePassword(uf,password);
      if (err!=UNZ_OK)
      {
         // notify error ("error %d with zipfile in unzOpenCurrentFilePassword\n",err);
      }

      if(err==UNZ_OK)
      {
         fout=fopen64(_pOutFilePath,"wb");
      }

      if (fout!=NULL)
      {
         //printf(" extracting: %s\n",write_filename);

         do
         {
            err = unzReadCurrentFile(uf,_buffer,WRITEBUFFERSIZE);
            if (err<0)
            {
               //notify error ("error %d with zipfile in unzReadCurrentFile\n",err);
               break;
            }
            if (err>0)
               if (fwrite(_buffer,err,1,fout)!=1)
               {
                  // notify error ("error in writing extracted file\n");
                  err=UNZ_ERRNO;
                  break;
               }
         }
         while (err>0);
         if (fout)
            fclose(fout);

         if (err==0)
            change_file_date(_pOutFilePath,file_info.dosDate,
            file_info.tmu_date);
      }

      if (err==UNZ_OK)
      {
         err = unzCloseCurrentFile (uf);
         if (err!=UNZ_OK)
         {
            // notify error ("error %d with zipfile in unzCloseCurrentFile\n",err);
         }
      }
      else
         unzCloseCurrentFile(uf); /* don't lose the error */

      return err;
   }

   void unzipstrategy::change_file_date(const char *filename, uLong dosdate, tm_unz tmu_date)
   {
      HANDLE hFile;
      FILETIME ftm,ftLocal,ftCreate,ftLastAcc,ftLastWrite;

      hFile = CreateFileA(filename,GENERIC_READ | GENERIC_WRITE,
         0,NULL,OPEN_EXISTING,0,NULL);
      GetFileTime(hFile,&ftCreate,&ftLastAcc,&ftLastWrite);
      DosDateTimeToFileTime((WORD)(dosdate>>16),(WORD)dosdate,&ftLocal);
      LocalFileTimeToFileTime(&ftLocal,&ftm);
      SetFileTime(hFile,&ftm,&ftLastAcc,&ftm);
      CloseHandle(hFile);
   }

}