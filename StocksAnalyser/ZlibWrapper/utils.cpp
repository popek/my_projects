#include "StdAfx.h"
#include "utils.h"
#include "unzipstrategy.h"
#include "unzip.h"


namespace zlibwrapper
{
   bool utils::decompress(const char* a_compressedFilePath, const char* a_otputFolderPath)
   {
      unzipstrategy unzipStrategy;

      return UNZ_OK == unzipStrategy.extract(a_compressedFilePath, a_otputFolderPath);
   }
}
