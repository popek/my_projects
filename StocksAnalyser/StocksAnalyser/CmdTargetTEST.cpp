#include "stdafx.h"
#include "StocksAnalyser.h"
#include "CmdTargetTEST.h"
#include "UpdateData_test.h"

IMPLEMENT_DYNAMIC(CmdTargetTEST, CCmdTarget)
CmdTargetTEST::CmdTargetTEST()
{
}
CmdTargetTEST::~CmdTargetTEST()
{
}


BEGIN_MESSAGE_MAP(CmdTargetTEST, CCmdTarget)
//    ON_COMMAND(ID_TEST_GETDATA, &CmdTargetTEST::OnTestGetdata)
//    ON_UPDATE_COMMAND_UI(ID_TEST_GETDATA, &CmdTargetTEST::OnUpdateTestGetdata)
END_MESSAGE_MAP()


void CmdTargetTEST::OnTestGetdata()
{
   UpdateData_test::GetData();
}

void CmdTargetTEST::OnUpdateTestGetdata(CCmdUI *pCmdUI)
{
   pCmdUI->Enable(TRUE);
}
