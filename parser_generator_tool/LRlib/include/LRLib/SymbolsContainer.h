#pragma once
#include "Symbol.h"
#include "LessString.h"
#include "auxiliary_types.h"

namespace lrlib
{

class SymbolsContainer
{
public:
   SymbolsContainer() = default;
   ~SymbolsContainer() = default;

   void     Insert(Symbol& a_oSymbol);

   size_t   Size();

   Symbol* FindSymbol(std::string a_strSymbolName);
   Symbol* FindSymbol(int a_nSymbolID);

   void     GenerateTerminalsIDs(int a_nFirstID);

   t_setSymbol::iterator Begin();
   t_setSymbol::iterator End();

   // auxiliary methods 
   t_setSymbol&          symbols();

protected:
   // all members below will be references to containers in Grammar
   t_setSymbol           m_setSymbols;
   std::map<std::string, Symbol*, LessString> m_mapName2Symbol;
   std::map<int, Symbol*>    m_mapID2Symbol;
};

} // namespace lrlib