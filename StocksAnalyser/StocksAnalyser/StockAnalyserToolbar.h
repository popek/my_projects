#pragma once


class StockAnalyserToolbar : public CDialog
{
	DECLARE_DYNAMIC(StockAnalyserToolbar)

public:
	StockAnalyserToolbar(CWnd* pParent = NULL);
	virtual ~StockAnalyserToolbar();

	enum { IDD = IDD_DIALOGBAR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP()
};
