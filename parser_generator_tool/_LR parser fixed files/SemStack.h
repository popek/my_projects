#pragma once


class SemAtom;


class SemStack
{
public:
  SemStack(void);
  virtual ~SemStack(void);

  void Push(shared_ptr<SemAtom> a_spSemAtom);
  void Pop();

  shared_ptr<SemAtom> Top();
  
private:

  deque<shared_ptr<SemAtom>> m_stack;
};