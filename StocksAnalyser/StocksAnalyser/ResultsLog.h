/**
 * @file
 * @author  Marcin Wrobel <marwrobel@gmail.com>
 *
 * @section DESCRIPTION
 * gmma assets actions logger
 */
#pragma once
#include "sharedPtrLess.h"

class Asset;
class TextFile;

class ResultsLog
{
public:

   // action parameters class
   class ActionData
   {
      typedef map<CString, double> TParamsMap;

   public:
      ActionData();
      ~ActionData();

      void WriteToFile(TextFile& a_openedFile, int indent);

      void SetParam(CString a_paramName, double a_paramValue);

      bool operator < (const ActionData& a_parametersToCompare) const;

   protected:
      TParamsMap           m_mamName2Value;
   };

   // single action result
   class SingleResult
   {
   public:
      SingleResult();
      ~SingleResult();

      void WriteToFile(TextFile& a_openedFile, int indent, int a_resultIdx);

      void UpdateParamsMap(CString a_paramName, double a_paramValue);

      void AddAsset(shared_ptr<Asset> a_spAsset);

      bool operator < (const SingleResult& a_resultToCompare) const;

   protected:
      COleDateTime                     m_timestamp;
      ActionData                       m_actionData;
      vector<shared_ptr<Asset>> m_vAssets;
   };

private:

   typedef set<shared_ptr<SingleResult>, sharedPtrLess<SingleResult>> TResultsSet;
   typedef map<int, TResultsSet>                                             TResultsMap;


public:
   ResultsLog(void);
   virtual ~ResultsLog(void);

   void AddActionResults(int a_actionType, shared_ptr<SingleResult> a_spSingleResult);

   BOOL Save();

protected:

   CString  ActionId2ActionName(int a_actionType) const;
   
   CString  GenerateLogFileName() const;

   CString  GenerateSectionHeader(int a_actionType) const;

protected:
   TResultsMap    m_mapActionResults;
};
