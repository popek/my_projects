#include "StdAfx.h"
#include "ChartElement.h"
#include "TimeScaleContext.h"
#include "ValueScaleContext.h"
#include "ChartGraphicElementFactory.h"
#include "GraphicObjectBase.h"
#include "GrxStyle.h"



ChartElement::ChartElement(EChartElementId a_elementId)
: m_elementId(a_elementId)
{
   _elementStyle = ChartGraphicElementFactory::Instance()->CreateDefaultGrxStyle(a_elementId);
}
ChartElement::ChartElement(EChartElementId a_elementId, size_t a_totalPointsCount)
: m_elementId(a_elementId)
{
   _elementStyle = ChartGraphicElementFactory::Instance()->CreateDefaultGrxStyle(a_elementId);
   m_points.reserve(a_totalPointsCount);
}

ChartElement::~ChartElement(void)
{
}

shared_ptr<GraphicObjectBase> ChartElement::CreateGraphicObject(const TimeScaleContext& a_timeScaleContext, const ValueScaleContext& a_valueScaleContext) const
{
   return CreateGraphicObject(a_timeScaleContext, a_valueScaleContext, 0, m_points.size()-1);
}

void ChartElement::AddPoint(const ChartPoint& a_chartPoint)
{
   m_points.push_back(a_chartPoint);
}

void ChartElement::AddPoint(shared_ptr<KandleDate> a_spDate, double a_value)
{
   m_points.push_back( ChartPoint(a_spDate, a_value) );
}

shared_ptr<GraphicObjectBase> ChartElement::CreateGraphicObject(const TimeScaleContext& a_timeScaleContext, const ValueScaleContext& a_valueScaleContext, 
                                       size_t a_startPointIndex, size_t a_endPointIndex) const
{
   if(NULL == m_spGraphicObject.get())
   {
      m_spGraphicObject = ChartGraphicElementFactory::Instance()->CreateGraphicObject(m_elementId, _elementStyle);
      m_spGraphicObject->Reserve(m_points.size());
   }

   const ValueScaleContext& valueScaleContextToBeUsed = _spOwnValueScaleContex ? *_spOwnValueScaleContex : a_valueScaleContext;

   for(size_t pointIndex=a_startPointIndex; pointIndex<=a_endPointIndex; ++pointIndex)
   {
      m_spGraphicObject->UpdatePoint( pointIndex, 
                                      a_timeScaleContext.ToCoordinate( m_points[pointIndex].Timestamp()), 
                                      valueScaleContextToBeUsed.ToCoordinate( m_points[pointIndex].Value()) );
   }

   return m_spGraphicObject;
}

void ChartElement::Style(shared_ptr<GrxStyle> a_style)
{
   _elementStyle = a_style;
}

shared_ptr<GrxStyle> ChartElement::Style() const
{
   return _elementStyle;
}

void ChartElement::OwnValueScaleContext(shared_ptr<ValueScaleContext> a_spOwneValueScaleContext)
{
   _spOwnValueScaleContex = a_spOwneValueScaleContext;
}

shared_ptr<ValueScaleContext> ChartElement::OwnValueScaleContext()
{
   return _spOwnValueScaleContex;
}
