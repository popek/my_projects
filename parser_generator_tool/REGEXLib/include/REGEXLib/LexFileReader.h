#pragma once
#include "CommonDefs.h"

namespace regexlib
{

class LexFileReader
{
public:
   LexFileReader(eRegExModel modelType);
   virtual ~LexFileReader(void);
   bool Read(const char* pFilePath);

private:
   //interface for reading symbols definition
   void ReadSymbolDefinition(char *pSymDefString);
   void GenerateSymbols(char* sSymbolSubDef, unsigned int uiAuxLen, std::set<unsigned char>& setSymbols);

   //interface for reading regular expressions definition
   void ReadRegExDefinition(char *pRegExDefString);
   void ParseRegExDefinition(std::string strName, std::string strRegEx);
   void GenerateRegExSubstrings(std::string strSimpleRegEx, std::vector<std::string>& vecRegExSubstrings);

   void AddPredefinedSymbols();

protected:
   std::map<std::string, std::set<unsigned char>> m_ComplexSymbolsDefs;
   t_RegExDefs m_RegExDefs;
   t_Char2SymbolID m_mapChar2SymbolID;

   t_SymbolName2SymbolID m_mapComplexSymbolName2SymbolID;

   eRegExModel m_ModelType;
private:
   unsigned int _uiAuxCnt;
};

} // namespace regexlib