#pragma once
#include "RegExClasses.h"
#include "ILex.h"
#include <stack>
#include "CommonDefs.h"

namespace regexlib
{

class RegExManager : public ILex
{

public:
   RegExManager();
   virtual ~RegExManager(void);

   void	DebPrint_RegExTree();
   unsigned int ParseToken(std::string strToken);

protected:
   void				GenerateNFAdiagram();
   void				GenerateDFAdiagram();
   t_DFASubSet eclosure(t_DFASubSet dfass);
   void				eclosure(unsigned int NFAstate, t_DFASubSet& Out, std::stack<unsigned int>& AuxStack);
   t_DFASubSet move(t_DFASubSet from, short SymbolID);
   void				move(unsigned int NFAstate, short SymbolID, t_DFASubSet& Out);

   unsigned int AcceptingSubset(t_DFASubSet& dfass);
   void DebPrint_RegEx(RegEx* pre, unsigned int uiIndent);

   ORRegEx m_RegExRoot;
   SNFADiagram m_NFA;
   t_DFAdiagram m_DFAdiagram;
   /*
   one DFA accepting states can recognize more then one regular expression.
   Rule:
   if DFA accepting state recognize more then one regex then the first one written in
   lex file definition is recognized
   */
   std::map<unsigned int, unsigned int> m_DFAacceptStates;
   unsigned int m_uiDFAStatesAmount;
private:
   bool m_bNFAgenerated;
};

} // namespace regexlib