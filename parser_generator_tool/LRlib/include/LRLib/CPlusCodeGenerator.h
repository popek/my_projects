#pragma once

#include <vector>
#include <map>
#include <deque>

namespace lrlib
{

class Grammar;

class CPlusCodeGenerator
{
public:
   CPlusCodeGenerator(Grammar* a_pGrammarObj);
   virtual ~CPlusCodeGenerator(void);

   void ShowErrors();

   //--------------------------------------------------------------------------------
   // GenerateAll - generates all files needed in the project
   //--------------------------------------------------------------------------------
   void GenerateAll(std::string a_strFilePath);

   void GenerateParserClass(std::string a_strFilePath);

   void GenerateSemStackClass(std::string a_strFilePath);

   void GenerateSemAtomClass(std::string a_strFilePath);

   void GenerateSemActionsClass(std::string a_strFilePath);

   void GenerateLexerClass(std::string a_strFilePath);

   void GenerateLexAtomClass(std::string a_strFilePath);


   void GenerateCommonDefinitionsFile(std::string a_strFilePath);

   void GenerateTransitionGraphFile(std::string a_strFilePath);

protected:

   void GenerateParserClass_header(std::string a_strFilePath);
   void GenerateParserClass_definition(std::string a_strFilePath);


   void GenerateSemStackClass_header(std::string a_strFilePath);
   void GenerateSemStackClass_definition(std::string a_strFilePath);


   void GenerateSemAtomClass_header(std::string a_strFilePath);
   void GenerateSemAtomClass_definition(std::string a_strFilePath);


   void GenerateSemActionsClass_header(std::string a_strFilePath);
   void GenerateSemActionsClass_definition(std::string a_strFilePath);

   void GeneretaSemActionsEnum_header(std::string a_folderPath);
   void GeneretaSemActionsNamesArray_header(std::string a_folderPath);

   std::string GenerateRuleName(int a_ruleId);

   void GenerateLexerClass_header(std::string a_strFilePath);
   void GenerateLexerClass_definition(std::string a_strFilePath);


   void GenerateLexAtomClass_header(std::string a_strFilePath);
   void GenerateLexAtomClass_definition(std::string a_strFilePath);

   std::string GenerateRuleMethod(int a_nRuleIdx, bool a_header, bool a_generateDebugPrint);
   std::string TerminalAuxName(char cOneCharName);
   std::string SemActions__DoAction_method();
   std::string GenerateTerminalsEnum();

   void TGF_Conflicts(FILE* a_pTransitionGraphFile);
   void TGF_Rules(FILE* a_pTransitionGraphFile);
   void TGF_Symbols(FILE* a_pTransitionGraphFile, unsigned int uiSymbolsAmount, bool bPrintForTerminals);
   void TGF_RuleLenght(FILE* a_pTransitionGraphFile);
   void TGF_ProductionNontermID(FILE* a_pTransitionGraphFile, unsigned int uiSymbolsAmount);
   void TGF_ShiftArray(FILE* a_pTransitionGraphFile, unsigned int uiSymbolsAmount, unsigned int uiStatesAmount, bool bPrintForTerminals);
   void TGF_ReduceArray(FILE* a_pTransitionGraphFile, unsigned int uiSymbolsAmount, unsigned int uiStatesAmount);

   void GenerateAuxiliaryFileWithStates(std::string a_strFilePath);

   void WriteStateInfo(std::multimap<int, std::vector<int>>& mmapState, FILE* file);

protected:

   std::deque<std::string> m_deqErrors;

   Grammar*     m_pGrammarObj;

   char*         m_szBuff;
   int           m_bufSize;
};

} // namespace lrlib