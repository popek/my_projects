#include "StdAfx.h"
#include "XercescXMLObjectsFactory.h"
#include "XercescXMLSerializer.h"


XercescXMLObjectsFactory::XercescXMLObjectsFactory(void)
{
}
XercescXMLObjectsFactory::~XercescXMLObjectsFactory(void)
{
}


shared_ptr<IXMLSerialiser> XercescXMLObjectsFactory::createSerialiser() const
{
   return make_shared<XercescXMLSerializer>();
}
