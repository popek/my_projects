#pragma once
#include "FoundationLib.h"
#include "IXMLObjectsFactory.h"


class FOUNDATION_LIB FoundationUtils
{
public:
   static shared_ptr<IXMLObjectsFactory> xmlFactory();
};

