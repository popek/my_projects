#include "StdAfx.h"
#include "MACD_pointer.h"
#include "EMA_pointer.h"
#include "STLVectorWrapper.h"
#include "DataSeries.h"
#include "ControlsLibUtils.h"


MACD_pointer::MACD_pointer(void)
{
   Init(12, 26, 9);
}
MACD_pointer::~MACD_pointer(void)
{
}
void MACD_pointer::Init(size_t a_shortEmaPeriod, size_t a_longEmaPeriod, size_t a_singalPeriod)
{
   _shortEMA = shared_ptr<EMA_pointer>(new EMA_pointer(a_shortEmaPeriod));
   _longEMA = shared_ptr<EMA_pointer>(new EMA_pointer(a_longEmaPeriod));
   _signal = shared_ptr<EMA_pointer>(new EMA_pointer(a_singalPeriod));

   _spMacdSeries = ControlsLibUtils::CreateEmptySeries(a_longEmaPeriod-1);
}

void MACD_pointer::Create(const DataSeries& a_inputData)
{
   // first try to create long ema
   _longEMA->Create(a_inputData);
   if(0 == _longEMA->Series(0).size())
   {
      return;
   }

   // then try to create short ema
   _shortEMA->Create(a_inputData);
   if(0 == _shortEMA->Series(0).size())
   {
      return;
   }

   shared_ptr<vector<double>> spMacdData(new vector<double>());
   vector<double>& macd = *spMacdData;

   macd.reserve(_longEMA->Series(0).size());

   size_t offset = _shortEMA->Series(0).size() - _longEMA->Series(0).size();
   for(size_t valueIndex = 0; valueIndex < _longEMA->Series(0).size(); ++valueIndex)
   {
      macd.push_back( _shortEMA->Series(0)[valueIndex + offset] - _longEMA->Series(0)[valueIndex]);
   }

   _spMacdSeries = shared_ptr<DataSeries>(new VectorBasedSeries(&_longEMA->Series(0), spMacdData, 0));

   _signal->Create(*_spMacdSeries);
}

const DataSeries& MACD_pointer::Series(size_t a_seriesId) const
{
   switch(a_seriesId)
   {
   case ELongEMA:
      return _longEMA->Series(0);

   case EShortEMA:
      return _shortEMA->Series(0);

   case EMACD:
      return *_spMacdSeries;

   case ESignal:
      return _signal->Series(0);

   default:
      ASSERT(FALSE);
      return *_spMacdSeries;
   }
}

size_t MACD_pointer::Period(MACD_pointer::ESeriesId a_seriesId) const
{
   switch(a_seriesId)
   {
   case ELongEMA:
      return _longEMA->period();

   case EShortEMA:
      return _shortEMA->period();

   case ESignal:
      return _signal->period();

   default:
      ASSERT(FALSE);
      return 0;
   }
}