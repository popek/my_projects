#pragma once
#include "FoundationLib.h"
#include "CommonDefines.h"


class FOUNDATION_LIB XercescUtils
{
   NONCOPYABLE_TYPE(XercescUtils);

public:

   static void initialize();

   static void release();
};

