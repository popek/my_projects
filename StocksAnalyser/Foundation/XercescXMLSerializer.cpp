#include "StdAfx.h"
#include "XercescXMLSerializer.h"
#include "XercescUtils.h"
#include <xercesc/util/XMLException.hpp>
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOMImplementationRegistry.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMText.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/parsers//SAXParser.hpp>

#include <iostream>

#include "XercescXMLDataObserver.h"
#include "XercescSAXHandler.h"
#include "XMLNode.h"


XercescXMLSerializer::XercescXMLSerializer(void)
{
}
XercescXMLSerializer::~XercescXMLSerializer(void)
{
}

bool XercescXMLSerializer::serialise(LPCTSTR outputFilePath, const XMLNode& node)
{
   bool result = false;
   try
   {
      XercescUtils::initialize();
   }
   catch(const xercesc::XMLException&)
   {
      return result;
   }

   XercescXMLDataObserver observer;
   node.traverseNode(&observer);

   xercesc::DOMImplementation *impl = xercesc::DOMImplementationRegistry::getDOMImplementation(_T("LS"));
   xercesc::DOMLSSerializer* theSerializer = static_cast<xercesc::DOMImplementationLS*>(impl)->createLSSerializer();

   // optionally you can set some features on this serializer
   if (theSerializer->getDomConfig()->canSetParameter(xercesc::XMLUni::fgDOMWRTDiscardDefaultContent, true))
      theSerializer->getDomConfig()->setParameter(xercesc::XMLUni::fgDOMWRTDiscardDefaultContent, true);

   if (theSerializer->getDomConfig()->canSetParameter(xercesc::XMLUni::fgDOMWRTFormatPrettyPrint, true))
      theSerializer->getDomConfig()->setParameter(xercesc::XMLUni::fgDOMWRTFormatPrettyPrint, true);

   // StdOutFormatTarget prints the resultant XML stream
   // to stdout once it receives any thing from the serializer.
   xercesc::XMLFormatTarget *myFormTarget = new xercesc::LocalFileFormatTarget(outputFilePath);
   xercesc::DOMLSOutput* theOutput = ((xercesc::DOMImplementationLS*)impl)->createLSOutput();
   theOutput->setByteStream(myFormTarget);

   try 
   {
      // do the serialization through DOMLSSerializer::write();
      theSerializer->write(observer.document()->getDocumentElement(), theOutput);
      result = true;
   }
   catch (const xercesc::XMLException&) 
   {
   }
   catch (const xercesc::DOMException&) 
   {
   }
   catch (...) 
   {
   }

   theOutput->release();
   theSerializer->release();
   delete myFormTarget;

   return result;
}

shared_ptr<XMLNode> XercescXMLSerializer::unserialise(LPCTSTR inputFilePath)
{
   try
   {
      XercescUtils::initialize();
   }
   catch(const xercesc::XMLException&)
   {
      return nullptr;
   }

   xercesc::SAXParser* parser = new xercesc::SAXParser();
   XercescSAXHandler handler;
   parser->setDocumentHandler(&handler);
   parser->setErrorHandler(&handler);
   parser->setValidationScheme(xercesc::SAXParser::Val_Auto);
   parser->setDoNamespaces(false);
   parser->setDoSchema(false);
   parser->setHandleMultipleImports(true);
   parser->setValidationSchemaFullChecking(false);

   parser->parse(inputFilePath);

   delete parser;

   return handler.detachNode();
}