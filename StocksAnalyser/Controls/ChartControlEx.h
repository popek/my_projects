#pragma once
class ChartControl;
class ScrollBarExt;
class Asset;

class ChartControlEx : public CWnd
{
	DECLARE_DYNAMIC(ChartControlEx)

public:
	ChartControlEx(bool a_bUseScroll);
	virtual ~ChartControlEx();

   void SetAsset(shared_ptr<Asset> a_spAsset);

protected:

   virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

   DECLARE_MESSAGE_MAP()
   afx_msg int    OnCreate(LPCREATESTRUCT lpCreateStruct);
   afx_msg void   OnSize(UINT nType, int cx, int cy);

protected:

   shared_ptr<ChartControl>  m_spChart;
   shared_ptr<ScrollBarExt>  m_spScroll;

   bool m_bUseScrollbar;
   int  m_nScrollbarHeight;
};


