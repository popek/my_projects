#include "stdafx.h"
#include "StocksAnalyser.h"
#include "StocksAnalyserDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CStocksAnalyserDoc, ChartDocument)

BEGIN_MESSAGE_MAP(CStocksAnalyserDoc, ChartDocument)
END_MESSAGE_MAP()

CStocksAnalyserDoc::CStocksAnalyserDoc()
{
}
CStocksAnalyserDoc::~CStocksAnalyserDoc()
{
}
// CStocksAnalyserDoc diagnostics

#ifdef _DEBUG
void CStocksAnalyserDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CStocksAnalyserDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG