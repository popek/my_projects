#pragma once
#include "ControlsLib.h"


class CONTROLS_LIB WindowUtils
{
public:

   static BOOL RegisterWindowClass(LPCTSTR a_className, HINSTANCE a_moduleInstance, UINT a_style);

   static void SetWindowPos(CWnd* a_pWindowToMove, const CWnd* a_pWndInsertAfter, const CRect& a_rect, UINT a_flags);
};
