#include "StdAfx.h"
#include "MAX_pointer.h"
#include "ControlsLibUtils.h"
#include "DataSeries.h"
#include "MathUtils.h"


MAX_pointer::MAX_pointer(size_t period)
   : _period(period)
{
   _spDataSeries = ControlsLibUtils::CreateEmptySeries(_period - 1);
}
MAX_pointer::~MAX_pointer(void)
{
}

void MAX_pointer::Create(const DataSeries& inputData)
{
   // check if samples amount neede to prepare mean value is grater then 0
   if(!_period)
      return;

   // make calculation only in case asset samples amount is grater then samples amount needed
   if(inputData.size() < _period)
      return;

   shared_ptr<vector<double>> spData(new vector<double>(inputData.size() - _period + 1, 0.0));
   vector<double>& maxValues = *spData;

   size_t maxValueIndex = 0;
   for(size_t valueIndex = _period - 1; valueIndex < inputData.size(); ++valueIndex)
   {
      size_t startIndex = valueIndex - _period + 1;
      maxValueIndex = startIndex;
      for(size_t subValueIndex = 0; subValueIndex < _period; ++subValueIndex)
      {
         if(inputData[startIndex + subValueIndex] > inputData[maxValueIndex])
         {
            maxValueIndex = startIndex + subValueIndex;
         }
      }

      maxValues[startIndex] = inputData[maxValueIndex];
   }

   _spDataSeries = shared_ptr<DataSeries>(new VectorBasedSeries(&inputData, spData, _period - 1));
}

const DataSeries& MAX_pointer::Series(size_t a_seriesId) const
{
   return *_spDataSeries;
}

