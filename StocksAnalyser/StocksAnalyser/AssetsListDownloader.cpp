#include "StdAfx.h"
#include "AssetsListDownloader.h"
#include "Misc.h"
#include "Poco/Net/HTTPStreamFactory.h"
#include "Poco/Net/FTPStreamFactory.h"
#include "Poco/URIStreamOpener.h"
#include "Poco/StreamCopier.h"
#include "Poco/URI.h"
#include "Poco/Exception.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Zip/Decompress.h"
#include "Poco/Delegate.h"
#include <fstream>
#include <sstream>
#include "AssetDecompressionErrorObserver.h"
#include "StocksAnalyser.h"
#include "Poco/Buffer.h"
#include "UIStrings.h"
#include "IInstrumentListUpdateObserver.h"
#include "ZlibWrapper\utils.h"
#include "GlobalSettings.h"
#include "PathUtils.h"


using namespace Poco;
using namespace Poco::Net;


const UINT G_downloadAssetList = 1;
const CString G_zipFileName = _T("assetsList.zip");

const UINT    G_KB          = 1024;
//"http://bossa.pl/pub/metastock/mstock/mstall.zip"

AssetsListDownloader::AssetsListDownloader(IInstrumentListUpdateObserver* a_pUpdateObserver)
: m_strHostURI("bossa.pl")
, m_strURIPath("/pub/metastock/mstock/mstall.zip")
, m_strFTPFullURI("ftp://ftp.bossa.pl/metastock/mstock/mstall.zip")
, m_workingThread(this)
, m_pUpdateObserver(a_pUpdateObserver)
, m_bCancelUpdate(false)
{
   _zipFilePath = PathUtils::concatenateDirAndFile(GlobalSettings::instance()->DBFolderPath(), G_zipFileName);
   _downloadFolder = GlobalSettings::instance()->DBFolderPath();
}

void AssetsListDownloader::Init()
{
   HTTPStreamFactory::registerFactory();
   FTPStreamFactory::registerFactory();
}

AssetsListDownloader::~AssetsListDownloader(void)
{
   m_workingThread.Stop();
}

BOOL AssetsListDownloader::ProcessThreadMessage(UINT a_messageId)
{
   if(G_downloadAssetList == a_messageId)
   {
      HandleUpdateAssetsList();
      return TRUE;
   }
   else
   {
      ASSERT(FALSE);
   }

   return FALSE;
}

void AssetsListDownloader::UpdateAssetsList()
{
   m_workingThread.Create();
   m_workingThread.Resume();

   m_workingThread.PostMessage(G_downloadAssetList);
}

void AssetsListDownloader::CancelUpdateProcess()
{
   m_bCancelUpdate = true;
}

bool AssetsListDownloader::HandleUpdateAssetsList()
{
   bool bListUpdated = false;
   bool bUnzippingSuccess = false;

   Misc::ClearFolderContent(_downloadFolder);
   bListUpdated = DownloadAssetsZIPFileUsingHTTPSession();

   if(bListUpdated)
   {
      if(m_bCancelUpdate)
      {
         m_pUpdateObserver->UpdateCanceled();
      }
      else
      {
         m_pUpdateObserver->UnzippingStarted();

         bUnzippingSuccess = zlibwrapper::utils::decompress( Misc::ToAnsi(_zipFilePath).c_str(), 
                                                             Misc::ToAnsi(_downloadFolder).c_str() );

         m_pUpdateObserver->UnzippingFinished();
      }
   }
   else
   {
      m_pUpdateObserver->NotifyError(m_strError);
   }

   if(true == bUnzippingSuccess)
   {
      Misc::RemoveFile(_zipFilePath);
   }

   return bListUpdated;
}

bool AssetsListDownloader::DownloadAssetsZIPFileUsingHTTPSession()
{
   bool bSuccess = true;

   try
   {
      // prepare request
      HTTPRequest request(HTTPRequest::HTTP_GET, m_strURIPath, HTTPMessage::HTTP_1_1);
      request.setHost(m_strHostURI, 80);
      request.setKeepAlive(true);

      std::ofstream zipFile(Misc::ToAnsi(_zipFilePath).c_str(), std::ios_base::out | std::ios_base::binary);
      ASSERT(zipFile.is_open());

      // start HTTP session 
      HTTPClientSession s(m_strHostURI, 80);
      s.sendRequest(request);

      //receive response (header is red so far)
      HTTPResponse response;
      std::istream& rs = s.receiveResponse(response);

      m_pUpdateObserver->SetRange((int)response.getContentLength()/G_KB);
      
      CopyStream(rs, zipFile);
      zipFile.flush();
   }
   catch(Poco::Exception& exc)
   {
      m_strError = Misc::ToTSTR( exc.displayText() );
      bSuccess   = false;
   }
   catch(...)
   {
      m_strError  = STR_INSTRUMENT_LIST_UPDATE_UNKNOWN_ERROR;
      bSuccess = false;
   }

   return bSuccess;
}

bool AssetsListDownloader::DownloadAssetsZIPFileUsingDefaultHTTPStreamOpener()
{
   string resourceURI = "http://" + m_strHostURI + m_strURIPath;

   std::ofstream zipFile(Misc::ToAnsi(_zipFilePath).c_str(), 
                         std::ios_base::out | std::ios_base::binary);

   ASSERT(zipFile.is_open());

   if(!zipFile.is_open())
   {
      return false;
   }

   try
   {
      URI uri(resourceURI);

      shared_ptr<std::istream> pStr(URIStreamOpener::defaultOpener().open(uri));
      StreamCopier::copyStream(*pStr.get(), zipFile);
   }
   catch (Exception&)
   {
      ASSERT(FALSE);
      return false;
   }

   return true;
}

void AssetsListDownloader::DownloadAssetsZIPFileUsingDefaultFTPStreamOpener()
{
   std::ofstream zipFile(Misc::ToAnsi(_zipFilePath).c_str(), 
                         std::ios_base::out | std::ios_base::binary);

   ASSERT(zipFile.is_open());

   try
   {
      URI uri(m_strFTPFullURI);

      shared_ptr<std::istream> pStr(URIStreamOpener::defaultOpener().open(uri));
      StreamCopier::copyStream(*pStr.get(), zipFile);
   }
   catch (Exception&)
   {
      ASSERT(FALSE);
   }
}

bool AssetsListDownloader::UNZIPFromNet()
{
   URI uri("http://" + m_strHostURI + m_strURIPath);
   
   HTTPClientSession session(uri.getHost(), uri.getPort());
   HTTPRequest req(HTTPRequest::HTTP_GET, uri.getPath(), HTTPMessage::HTTP_1_1);
   session.sendRequest(req);
   HTTPResponse res;
   std::istream& rs = session.receiveResponse(res);
   Zip::Decompress dec(rs, Poco::Path());

   AssetDecompressionErrorObserver errorObserver;

   // if an error happens invoke the ZipTest::onDecompressError method
   dec.EError += Poco::Delegate<
                           AssetDecompressionErrorObserver, std::pair<const Poco::Zip::ZipLocalFileHeader, const std::string> 
                               >(&errorObserver, &AssetDecompressionErrorObserver::OnDecompressError);
   
   try
   {
      dec.decompressAllFiles();
   }
   catch (Poco::Exception& exc)
   {
      MessageBox(NULL, Misc::ToTSTR(exc.displayText()), _T("error durng decompressing"), MB_OK);
      return false;
   }
   

   dec.EError -= Poco::Delegate<
                        AssetDecompressionErrorObserver, std::pair<const Poco::Zip::ZipLocalFileHeader, const std::string> 
                               >(&errorObserver, &AssetDecompressionErrorObserver::OnDecompressError);

   return true;
}

bool AssetsListDownloader::Unzip()
{
   std::ifstream inp(Misc::ToAnsi(_zipFilePath).c_str(), std::ios::binary);

   CString strAssetsDirectory = Misc::ClearFolderContent(_downloadFolder);

   if(FALSE == strAssetsDirectory.IsEmpty())
   {
      Zip::Decompress dec(inp, Misc::ToAnsi(strAssetsDirectory) );

      AssetDecompressionErrorObserver errorObserver;

      // if an error happens invoke the AssetDecompressionErrorObserver::OnDecompressError method
      dec.EError += Poco::Delegate<
                                    AssetDecompressionErrorObserver, std::pair<const Poco::Zip::ZipLocalFileHeader, const std::string> 
                                  >(&errorObserver, &AssetDecompressionErrorObserver::OnDecompressError);

      try
      {
         dec.decompressAllFiles();
      }
      catch (Poco::Exception& exc)
      {
         MessageBox(NULL, Misc::ToTSTR(exc.displayText()), _T("error durng decompressing"), MB_OK);
         return false;
      }

      dec.EError -= Poco::Delegate<
                                    AssetDecompressionErrorObserver, std::pair<const Poco::Zip::ZipLocalFileHeader, const std::string> 
                                  >(&errorObserver, &AssetDecompressionErrorObserver::OnDecompressError);
      
   }

   return true;
}

void AssetsListDownloader::CopyStream(std::istream& istr, std::ostream& ostr)
{
   const std::size_t bufferSize = 80192;

   Buffer<char> buffer(bufferSize);
   std::streamsize len = 0;
   istr.read(buffer.begin(), bufferSize);
   std::streamsize n = istr.gcount();
   
   while (n > 0)
   {
      if(m_bCancelUpdate)
      {
         return;
      }

      len += n;
      ostr.write(buffer.begin(), n);

      m_pUpdateObserver->OnDataDownloaded(len/G_KB);

      if (istr && ostr)
      {
         istr.read(buffer.begin(), bufferSize);
         n = istr.gcount();
      }
      else n = 0;
   }
}