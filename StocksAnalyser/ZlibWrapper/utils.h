#pragma once
#include "ZlibWrapperLib.h"

namespace zlibwrapper
{
   class ZLIBWRAPPER_LIB utils
   {
   public:
      static bool decompress(const char* a_compressedFilePath, const char* a_otputFolderPath);
   };
}


