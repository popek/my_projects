#pragma once


class ValueScaleContextEx;
class ValueScaleContext;

class ValueScaleObject
{

   NONCOPYABLE_TYPE(ValueScaleObject)

public:
   enum EResult
   {
      EMinMaxChanged,
      ENothing
   };

public:
   ValueScaleObject(CWnd* a_pChartControl);
   virtual ~ValueScaleObject(void);

   void UpdateArea(const CRect& a_newValueScaleObjectArea);

   BOOL IsPointOverObjectArea(const CPoint& a_point) const;

   EResult OnLButtonDown(UINT a_flags, const CPoint& a_point);

   EResult OnLButtonUp(UINT a_flags, const CPoint& a_point);

   EResult OnMouseMove(UINT a_flags, const CPoint& a_point);

   EResult OnLButtonDblClk(UINT nFlags, CPoint point);


   void   SetMostReasonableMinMax(double a_minimum, double a_maximum);

   double CurrentMinimum() const;

   double CurrentMaximum() const;

   shared_ptr<ValueScaleContext> Context() const;

protected:

   shared_ptr<ValueScaleContextEx> m_spContext;

   CRect m_rcValueScaleArea;

   CWnd* m_pChartControl;

   int   m_dragStartPos;

   bool  m_changingMaxValue;
};
