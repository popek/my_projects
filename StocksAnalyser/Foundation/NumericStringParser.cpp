#include "StdAfx.h"
#include "NumericStringParser.h"
#include <limits>


NumericStringParser::NumericStringParser(void)
: m_state(EStartState)
{
   m_zero = _T('0');
   m_nine = _T('9');
   m_separator = _T('.');
   m_minus = _T('-');
   m_plus = _T('+');
}
NumericStringParser::~NumericStringParser(void)
{
}

bool NumericStringParser::Parse(CString a_text, double& a_value)
{
   m_state = EStartState;
   m_negativeValue = false;
   m_value = 0;
   m_auxValueDivider = 1;
   m_strValue = a_text.MakeLower();
   m_nextTokenIndex = 0;

   // check if infinity is set
   if(m_strValue == _T("inf"))
   {
      a_value = numeric_limits<double>::infinity();
      return true;
   }
   
   while(m_state != EFinalState && m_state != EInvalidState && m_nextTokenIndex < m_strValue.GetLength())
   {
      Token token = NextToken();

      switch(m_state)
      {
      case EStartState:
         {
            switch(token)
            {
            case ESign:
               m_state = EAfterSignState;
               break;

            case EDigit:
               m_value = m_value*10 + m_lastDigit;
               m_state = EAfterSignState;
               break;

            case ESeparator:
               m_state = EAfterSeparatorState;
               break;

            default:
               m_state = EInvalidState;
            }
         }
         break;

      case EAfterSignState:
         {
            switch(token)
            {
            case EDigit:
               m_value = m_value*10 + m_lastDigit;
               break;

            case ESeparator:
               m_state = EAfterSeparatorState;
              break;

            case EMultiplier:
               m_value *= m_multiplier;
               m_state = EFinalState;
               break;

            default:
               m_state = EInvalidState;
            }
         }
         break;

      case EAfterSeparatorState:
         {
            switch(token)
            {
            case EDigit:
               m_auxValueDivider *= 10;
               m_value = m_value + m_lastDigit/m_auxValueDivider;
               break;

            case EMultiplier:
               m_value *= m_multiplier;
               m_state = EFinalState;
               break;

            default:
               m_state = EInvalidState;
            }
         }
      }
   };

   if(m_nextTokenIndex < m_strValue.GetLength())
   {
      return false;
   }

   if(m_negativeValue)
   {
      m_value *= -1;
   }

   a_value = m_value;

   return IsInAcceptingState();
}

NumericStringParser::Token NumericStringParser::NextToken()
{
   TCHAR nextChar = m_strValue[m_nextTokenIndex++];

   if(nextChar <= m_nine && nextChar >= m_zero)
   {
      m_lastDigit = double(nextChar - m_zero);
      return EDigit;
   }
   else if(nextChar == m_minus || nextChar == m_plus)
   {
      m_negativeValue = nextChar == m_minus ? true : false;
      return ESign;
   }
   else if(nextChar == m_separator)
   {
      return ESeparator;
   }
   else if(nextChar == _T('m') || nextChar == _T('M'))
   {
      m_multiplier = 1000000;
      return EMultiplier;
   }
   else if(nextChar == _T('k') || nextChar == _T('K'))
   {
      m_multiplier = 1000;
      return EMultiplier;
   }

   return EInvalidChar;
}

bool NumericStringParser::IsInAcceptingState()
{
   return m_state == EFinalState || 
          m_state == EAfterSignState ||
          m_state == EAfterSeparatorState;
}