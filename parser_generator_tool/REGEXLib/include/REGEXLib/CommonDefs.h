#pragma once
#include <vector>
#include <set>
#include <map>
#include <string>

namespace regexlib
{

typedef std::pair<std::string, std::vector<std::string>>			t_RegExName2RegExSub;
typedef std::vector<t_RegExName2RegExSub>				t_RegExDefs;
typedef std::map<std::string, short>									t_SymbolName2SymbolID;
typedef std::map<std::string, short>									t_SymbolName2SymbolID;
typedef std::map<short, std::string>									t_SymbolID2SymbolName;

typedef std::multimap<short, unsigned int>				t_NFAtransitions;
typedef std::map<unsigned int, t_NFAtransitions> t_NFAdiagram;
typedef std::pair<short, unsigned int>						t_Token2State;

typedef std::set<unsigned int>										t_DFASubSet;
typedef std::vector<t_DFASubSet>									t_DFAstates;

typedef std::map<short, unsigned int>						t_DFAtransition;
typedef std::map<unsigned int, t_DFAtransition>	t_DFAdiagram;

typedef std::map<unsigned char, short>						t_Char2SymbolID;
typedef std::map<unsigned int, unsigned int>			t_DFAAcceptingState2RegExNo;

enum eRegExModel { eREM_SimpleRegEx, eREM_ComplexRegEx };

} // namespace regexlib