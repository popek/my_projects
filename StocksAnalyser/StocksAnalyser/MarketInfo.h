#pragma once


class XMLNode;


class MarketInfo
{
public:
   enum group
   {
      MIG_WIG20,
      MIG_WIG40,
      MIG_WIG80,
      MIG_WIG,

      MIG_unknown
   };

   class AssetInfo
   {
      AssetInfo(const CString& name)
         : _name(name)
      {
      }

      const CString& getName() const
      {
         return _name;
      }

   private:
      CString _name;
   };

   NONCOPYABLE_TYPE(MarketInfo);

private:
   MarketInfo(void);

public:
   virtual ~MarketInfo(void);

   static MarketInfo* instance();
   static void release();


private:
   shared_ptr<XMLNode> _marketInfoNode;

   static MarketInfo* s_instance;
};

