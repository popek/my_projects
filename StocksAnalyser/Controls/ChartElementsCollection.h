#pragma once
#include "ControlsLib.h"


class ChartElement;


class CONTROLS_LIB ChartElementsCollection
{
   NONCOPYABLE_TYPE(ChartElementsCollection)

public:
   ChartElementsCollection(void);
   virtual ~ChartElementsCollection(void);

   void PushBack(shared_ptr<ChartElement> a_spElement);
   
   void PushFront(shared_ptr<ChartElement> a_spElement);

   shared_ptr<ChartElement> GetElement(size_t a_elementIndex);

   size_t ElementsCount() const;

public:
   deque<shared_ptr<ChartElement>> m_elements;
};
