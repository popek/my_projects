#include "StdAfx.h"
#include "ValueScalePainter.h"
#include "CommonDefines.h"
#include "ControlsLibUtils.h"


const int G_nMinPixelsDiffBetweenValues = 50;
const int G_lineLen                     = 5;
const int G_lineToTextOffset            = 3;

ValueScalePainter::ValueScalePainter(void)
: m_nPaintAreaHeight(0)
, m_nPaintAreaWidth(0)
, m_dMinValue(0)
, m_dMaxValue(0)
, m_dValueModule(0)
, m_dValue2Pixel(0)
, m_Pen(PS_SOLID, 1, RGB(0, 0, 0))
{
   LOGFONT logFont;
   ZeroMemory(&logFont, sizeof(LOGFONT));
   GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &logFont);
   m_font.CreateFontIndirect(&logFont);
}
ValueScalePainter::~ValueScalePainter(void)
{
}

int ValueScalePainter::Width() const
{
   return m_nPaintAreaWidth;
}

void ValueScalePainter::Width(int a_nNewPaintAreadWidth)
{
   m_nPaintAreaWidth = a_nNewPaintAreadWidth;
}

int ValueScalePainter::Height() const
{
   return m_nPaintAreaHeight;
}

void ValueScalePainter::Height(int a_nNewPaintAreadHeight)
{
   m_nPaintAreaHeight = a_nNewPaintAreadHeight;

   m_dValue2Pixel = (m_dValueModule)/m_nPaintAreaHeight;
}

int ValueScalePainter::CalculateMinWidth()
{
   CDC* pScreenDC = CDC::FromHandle(GetDC(NULL));

   const int nLineWidth        = m_nPaintAreaWidth < 5 ? 0 : 5;
   const int nLineToTextOffset = (m_nPaintAreaWidth - nLineWidth) < 5 ? 0 : 5; 

   pScreenDC->SelectObject((CFont*)&m_font);

   m_rcValue.SetRect(0, 0, 0, 0);
   pScreenDC->DrawText(_T("00000.00"), m_rcValue, DT_CALCRECT);

   return m_rcValue.Width() + G_lineToTextOffset + G_lineLen;
}

void ValueScalePainter::UpdateMinMax(double a_dNewMin, double a_dNewMax)
{
   m_dMinValue = a_dNewMin;
   m_dMaxValue = a_dNewMax;

   m_dValueModule = m_dMaxValue - m_dMinValue;

   m_dValue2Pixel = (m_dValueModule)/m_nPaintAreaHeight;
}

void ValueScalePainter::Draw(CDC& a_rDC, int a_nXOffset)
{
   if(0 == m_nPaintAreaHeight || 0 == m_nPaintAreaWidth)
      return;

   CPen*    pOldPen        = a_rDC.SelectObject(&m_Pen);
   CFont*   pOldFont       = a_rDC.SelectObject(&m_font);

   int      nLineYCoord    = 0;
   int      leftPos        = a_nXOffset + G_lineLen + G_lineToTextOffset;
   int      halfRectHeight = m_rcValue.Height()/2;
   
   m_rcValue.MoveToXY(leftPos, 0);

   while(nLineYCoord < m_nPaintAreaHeight)
   {
      a_rDC.DrawText(ControlsLibUtils::FitStringToWidth((m_dMaxValue - m_dValue2Pixel*nLineYCoord), a_rDC, m_rcValue.Width()), 
                     m_rcValue, DT_VCENTER|DT_RIGHT|DT_END_ELLIPSIS);
      
      a_rDC.MoveTo(a_nXOffset            , nLineYCoord);
      a_rDC.LineTo(a_nXOffset + G_lineLen, nLineYCoord);

      nLineYCoord += G_nMinPixelsDiffBetweenValues;
      m_rcValue.MoveToXY(leftPos, nLineYCoord - halfRectHeight);
   }

   a_rDC.SelectObject(pOldPen);
   a_rDC.SelectObject(pOldFont);
}