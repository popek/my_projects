#include "stdafx.h"
#include "StocksAnalyser.h"
#include "SearchGMMAsignalsDlg.h"
#include "GMMA_BeginingOfTrend.h"
#include "GMMA_NearBuySignal.h"
#include "EMA_pointer.h"
#include "Asset.h"
#include "AssetContainer.h"
#include "ClientRect.h"
#include "NumericStringParser.h"
#include <Shlwapi.h>
#include <atlpath.h>
#include "WindowRect.h"
#include "ScopeLock.h"
#include "ParallelChannelFinder.h"
#include "DoubleBottomFinder.h"
#include "AssetsListBuffer.h"
#include "Misc.h"
#include "PathUtils.h"


IMPLEMENT_DYNAMIC(SearchGMMAsignalsDlg, CDialog)

SearchGMMAsignalsDlg::SearchGMMAsignalsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SearchGMMAsignalsDlg::IDD, pParent)
   , m_UIRefreshHandler(this)
   , m_thread(this)
   , m_stopProcessing(true)
   , m_algorithmType(0)
   , m_strTurnoverKandlesAmount(_T("10"))
   , m_strMinTurnover(_T("50k"))
   , m_strMaxTurnover(_T("inf"))
{
   ResetTurnOver();
}
SearchGMMAsignalsDlg::~SearchGMMAsignalsDlg()
{
}

BOOL SearchGMMAsignalsDlg::ProcessThreadMessage(UINT a_messageId)
{
   // release previous assets set
   AssetsListBuffer::Instance()->Release();

   ProcessAllAssets();

   m_UIRefreshHandler.PostAfterProcessing();

   return TRUE;
}

void SearchGMMAsignalsDlg::UpdateProgress(CString a_strLabel, int a_progressPos)
{
   GetDlgItem(IDC_PROGRESS_DESC)->SetWindowText(a_strLabel);
   m_progress.SetPos(a_progressPos);

   UpdateData(FALSE);
}

void SearchGMMAsignalsDlg::SetProgressRange(int a_range)
{
   m_progress.SetRange32(0, a_range);

   UpdateData(FALSE);
}

void SearchGMMAsignalsDlg::AddNewAssetToTheList(CString a_assetFilePath)
{
   m_assetsListItemsData.push_back(a_assetFilePath);
   m_assetsList.InsertItem(m_assetsList.GetItemCount(), PathUtils::extractFileName(a_assetFilePath));

   UpdateData(FALSE);
}

void SearchGMMAsignalsDlg::AfterProcessing()
{
   // enable/disable controls
   GetDlgItem(IDC_CHOOSE_ASSETS_PATH)->EnableWindow(TRUE);
   m_assetsFilesPath.EnableWindow(TRUE);

   GetDlgItem(IDC_SEARCH_BUY)->EnableWindow(TRUE);
   GetDlgItem(ID_STOP_PROCESSING)->EnableWindow(FALSE);
}

void SearchGMMAsignalsDlg::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_ASSETS_LIST, m_assetsList);
   DDX_Control(pDX, IDC_PROGRESS, m_progress);
   DDX_Control(pDX, IDC_ASSETS_PATH, m_assetsFilesPath);
   DDX_Radio(pDX, IDC_BEGINING_OF_TREND, m_algorithmType);
   DDX_Text(pDX, IDC_TURNOVER_KANDLES_AMOUNT, m_strTurnoverKandlesAmount);
   DDX_Text(pDX, IDC_TURNOVER_MIN_VALUE, m_strMinTurnover);
   DDX_Text(pDX, IDC_TURNOVER_MAX_VALUE, m_strMaxTurnover);

   if(pDX->m_bSaveAndValidate)
   {
      m_assetsFilesPath.GetWindowText(m_strAssetFilePath);

      NumericStringParser valueParser;

      if(false == valueParser.Parse(m_strMinTurnover, m_minTurnoverTotal) ||
         false == valueParser.Parse(m_strMaxTurnover, m_maxTurnoverTotal) ||
         false == valueParser.Parse(m_strTurnoverKandlesAmount, m_turnoverKandleAmount))
      {
         ResetTurnOver();
      }
      else
      {
         m_minTurnoverTotal *= m_turnoverKandleAmount;
         m_maxTurnoverTotal *= m_turnoverKandleAmount;
      }
   }
}

BOOL SearchGMMAsignalsDlg::OnInitDialog()
{
   __super::OnInitDialog();

   m_algorithmTypeList.push_back(IDC_BEGINING_OF_TREND);
   m_algorithmTypeList.push_back(IDC_NEAR_BUY_SIGNAL);
   m_algorithmTypeList.push_back(IDC_PARALLEL_CHANNEL);
   m_algorithmTypeList.push_back(IDC_DOUBLE_BOTTOM);

   ClientRect rcListBox(m_assetsList);

   m_assetsList.InsertColumn(0, _T(""));
   m_assetsList.SetColumnWidth(0, rcListBox.Width()-10);

   // prepare rectangle
   WindowRect placeholder(GetDlgItem(IDC_ADDITIONAL_PARAMS_PLACEHOLDER));
   ScreenToClient(&placeholder);

   // set position of additional dialogs
   m_beginingOfTrendParams.Create(this);
   m_beginingOfTrendParams.MoveWindow(&placeholder, FALSE);

   m_parallelChannelParams.Create(ChannelAlgorithParametersDlg::IDD, this);
   m_parallelChannelParams.MoveWindow(&placeholder, FALSE);

   m_doubleBottomContextDlg.Create(DoubleBottomContextDlg::IDD, this);
   m_doubleBottomContextDlg.MoveWindow(&placeholder, FALSE);

   UpdateData(FALSE);

   AfterProcessing();

   OnAlgorithgTypeChanged();

   return TRUE;  
}

BOOL SearchGMMAsignalsDlg::OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
   if(m_UIRefreshHandler.TryToHandle(message))
   {
      return TRUE;
   }

   return __super::OnWndMsg(message, wParam, lParam, pResult);
}

void SearchGMMAsignalsDlg::AbortProcessing()
{
   m_stopProcessing = true;
   m_thread.Stop();
}

void SearchGMMAsignalsDlg::InitialiseNextResult()
{
   m_spTemporarySingleResult = shared_ptr<ResultsLog::SingleResult>(new ResultsLog::SingleResult());

   switch(m_algorithmTypeList[m_algorithmType])
   {
   case IDC_BEGINING_OF_TREND:
      {
         shared_ptr<GMMA_BeginingOfTrend::Context> spContext = m_beginingOfTrendParams.Context(false);
         m_spTemporarySingleResult->UpdateParamsMap(_T("typ"), (double)spContext->AlgorithType());
         m_spTemporarySingleResult->UpdateParamsMap(_T("maksymalny okres trwania sygna�u"), (double)spContext->ShortMeansAboveLongMeansPeriod(false));
         m_spTemporarySingleResult->UpdateParamsMap(_T("okres przebicia"), (double)spContext->BreakingPeriod());
         m_spTemporarySingleResult->UpdateParamsMap(_T("okres przed przebiciem"), (double)spContext->ShortMeansBelowLongMeansPeriod());

      }
      break;

   case IDC_PARALLEL_CHANNEL:
      m_spTemporarySingleResult->UpdateParamsMap(_T("typ"), (double)m_parallelChannelParams.Context().AlgType());
      m_spTemporarySingleResult->UpdateParamsMap(_T("okres"), (double)m_parallelChannelParams.Context().Period());
      m_spTemporarySingleResult->UpdateParamsMap(_T("maks. dewiacja/warto�� linii"), (double)m_parallelChannelParams.Context().MaxDev2LineValue());
      m_spTemporarySingleResult->UpdateParamsMap(_T("maks. dystans dane/linia"), (double)m_parallelChannelParams.Context().MaxData2LineValueDistance());
      break;

   case IDC_DOUBLE_BOTTOM:
      m_spTemporarySingleResult->UpdateParamsMap(_T("okres"), (double)m_doubleBottomContextDlg.Context()->_searchArea);
      m_spTemporarySingleResult->UpdateParamsMap(_T("ilo�� s�siad�w"), (double)m_doubleBottomContextDlg.Context()->_bottomNeigboursMinCount);
      m_spTemporarySingleResult->UpdateParamsMap(_T("g��boko�� [%]"), m_doubleBottomContextDlg.Context()->_bottomDepth);
      m_spTemporarySingleResult->UpdateParamsMap(_T("r�nica poziom�w [%]"), m_doubleBottomContextDlg.Context()->_bottomsValueDiff);
      break;
   }

   m_spTemporarySingleResult->UpdateParamsMap(_T("ilo�� �wieczek z min. obrotem"), m_turnoverKandleAmount);
   m_spTemporarySingleResult->UpdateParamsMap(_T("min. �redni obr�t na �wieczce"), m_minTurnoverTotal/m_turnoverKandleAmount);
   m_spTemporarySingleResult->UpdateParamsMap(_T("max. �redni obr�t na �wieczce"), m_maxTurnoverTotal/m_turnoverKandleAmount);

   m_results.AddActionResults(m_algorithmTypeList[m_algorithmType], m_spTemporarySingleResult);
}

void SearchGMMAsignalsDlg::OnAlgorithgTypeChanged()
{
   UpdateData(TRUE);

   switch(m_algorithmTypeList[m_algorithmType])
   {
   case IDC_BEGINING_OF_TREND:
      m_beginingOfTrendParams.ShowWindow(SW_SHOW);
      m_parallelChannelParams.ShowWindow(SW_HIDE);
      m_doubleBottomContextDlg.ShowWindow(SW_HIDE);
      break;

   case IDC_PARALLEL_CHANNEL:
      m_beginingOfTrendParams.ShowWindow(SW_HIDE);
      m_parallelChannelParams.ShowWindow(SW_SHOW);
      m_doubleBottomContextDlg.ShowWindow(SW_HIDE);
      break;

   case IDC_DOUBLE_BOTTOM:
      m_beginingOfTrendParams.ShowWindow(SW_HIDE);
      m_parallelChannelParams.ShowWindow(SW_HIDE);
      m_doubleBottomContextDlg.ShowWindow(SW_SHOW);
      break;

   default:
      m_beginingOfTrendParams.ShowWindow(SW_HIDE);
      m_parallelChannelParams.ShowWindow(SW_HIDE);
      m_doubleBottomContextDlg.ShowWindow(SW_HIDE);
   }
}

BEGIN_MESSAGE_MAP(SearchGMMAsignalsDlg, CDialog)
   ON_BN_CLICKED(IDC_CHOOSE_ASSETS_PATH, &SearchGMMAsignalsDlg::OnBnClickedChooseAssetsPath)
   ON_BN_CLICKED(IDC_SEARCH_BUY, &SearchGMMAsignalsDlg::OnBnClickedSearchBuy)
   ON_BN_CLICKED(ID_STOP_PROCESSING, &SearchGMMAsignalsDlg::OnBnClickedStopProcessing)
   ON_NOTIFY(NM_DBLCLK, IDC_ASSETS_LIST, &SearchGMMAsignalsDlg::OnNMDblclkAssetsList)
   ON_BN_CLICKED(IDC_BEGINING_OF_TREND, &SearchGMMAsignalsDlg::OnBnClickedBeginingOfTrend)
   ON_BN_CLICKED(IDC_NEAR_BUY_SIGNAL, &SearchGMMAsignalsDlg::OnBnClickedNearBuySignal)
   ON_BN_CLICKED(IDC_PARALLEL_CHANNEL, &SearchGMMAsignalsDlg::OnBnClickedParallelChannel)
   ON_BN_CLICKED(IDC_DOUBLE_BOTTOM, &SearchGMMAsignalsDlg::OnBnClickedDoubleBottom)
   ON_WM_DESTROY()
END_MESSAGE_MAP()


// SearchGMMAsignalsDlg message handlers
void SearchGMMAsignalsDlg::OnBnClickedChooseAssetsPath()
{
   CFileDialog fileDialog(TRUE, _T("mst"), _T("*.mst"), OFN_FILEMUSTEXIST|OFN_HIDEREADONLY, _T("Pliki (*.mst)|*.mst|(*.*)|*.*||"), this);

   if(IDOK == fileDialog.DoModal())
   {
      CPath path(fileDialog.GetPathName());
      path.RemoveFileSpec();

      path.AddBackslash();

      m_assetsFilesPath.SetWindowText(path);
   }
}

void SearchGMMAsignalsDlg::OnBnClickedSearchBuy()
{
   UpdateData(TRUE);

   if(ReadyToProcessAssets())
   {
      BeforeProcessing();

      // initiate flag
      m_stopProcessing = false;

      // crate thread
      m_thread.Create();
      // run thread
      m_thread.Resume();

      // post one and only one message (message no. is not important)
      m_thread.PostMessage(0);
   }
}

void SearchGMMAsignalsDlg::OnBnClickedStopProcessing()
{
   AbortProcessing();
}

void SearchGMMAsignalsDlg::ProcessAllAssets()
{
   CString           strFilePath;
   WIN32_FIND_DATA   oFileData;
   HANDLE            hFile;
   vector<CString>   vFilesNames;

   // get files
   hFile = FindFirstFile(m_strAssetFilePath + _T("*.mst"), &oFileData);
   if(hFile != INVALID_HANDLE_VALUE)
   {
      do
      {
         vFilesNames.push_back(oFileData.cFileName);
      }
      while(FindNextFile(hFile, &oFileData) == TRUE);

      FindClose(hFile);
   }

   m_UIRefreshHandler.PostSetProgressRange(vFilesNames.size());

   // process each and every asset
   for(size_t fileIdx=0; fileIdx<vFilesNames.size(); ++fileIdx)
   {
      if(m_stopProcessing)
      {
         break;
      }

      CPath path;
      path.Combine(m_strAssetFilePath, vFilesNames[fileIdx]);

      if(path.FileExists())
      {
         ProcessAsset(path);
      }

      // update description text
      CString desc;
      desc.Format(_T("%s (%d/%d) "), vFilesNames[fileIdx], fileIdx+1, vFilesNames.size());
      m_UIRefreshHandler.PostUpdateProgress(desc, fileIdx+1);
   }
}

void SearchGMMAsignalsDlg::ProcessAsset(CString a_fileFullPath)
{
   // read asset
   shared_ptr<AssetContainer> spAssetContainer(new AssetContainer());
   spAssetContainer->LoadDaysAssetFromFile(a_fileFullPath);

   // if turnover conditions are not met then do not process asset
   if(!AssetTurnoverOK((*spAssetContainer)[eDay]))
   {
      return;
   }

   // check GMMA buy signal
   shared_ptr<AssetFinderBase> spAssetFinder;

   switch(m_algorithmTypeList[m_algorithmType])
   {
   case IDC_BEGINING_OF_TREND:
      spAssetFinder = shared_ptr<GMMA_BeginingOfTrend>(new GMMA_BeginingOfTrend(spAssetContainer, m_beginingOfTrendParams.Context(false)));
      break;

   case IDC_NEAR_BUY_SIGNAL:
      spAssetFinder = shared_ptr<GMMA_NearBuySignal>(new GMMA_NearBuySignal(spAssetContainer));
      break;

   case IDC_PARALLEL_CHANNEL:
      spAssetFinder = shared_ptr<ParallelChannelFinder>(new ParallelChannelFinder(m_parallelChannelParams.Context()));
      spAssetFinder->SetAsset(spAssetContainer);
      break;

   case IDC_DOUBLE_BOTTOM:
      spAssetFinder = shared_ptr<DoubleBottomFinder>(new DoubleBottomFinder(m_doubleBottomContextDlg.Context()));
      spAssetFinder->SetAsset(spAssetContainer);
      break;

   default:
      ASSERT(FALSE);
   }
   
   if( spAssetFinder.get() && spAssetFinder->VerifyAssetConditions() )
   {
      AssetsListBuffer::Instance()->BufferAssetContainer(a_fileFullPath, spAssetContainer);

      m_UIRefreshHandler.PostAddNewAsset(a_fileFullPath);
      m_spTemporarySingleResult->AddAsset((*spAssetContainer)[eDay]);
   }
}

bool SearchGMMAsignalsDlg::AssetTurnoverOK(shared_ptr<Asset> a_spAsset) const
{
   double assetTotalTurnover = 0;
   if(a_spAsset->Size() >= (size_t)m_turnoverKandleAmount)
   {
      size_t counter = 0;

      for(size_t kandleOffset=1; kandleOffset<=(size_t)m_turnoverKandleAmount; ++kandleOffset)
      {
         assetTotalTurnover += (*a_spAsset)[a_spAsset->Size()-kandleOffset].TurnOver();
      }
   }

   return assetTotalTurnover >= m_minTurnoverTotal && assetTotalTurnover <= m_maxTurnoverTotal;
}

void SearchGMMAsignalsDlg::BeforeProcessing()
{
   // enable/disable controls
   GetDlgItem(IDC_CHOOSE_ASSETS_PATH)->EnableWindow(FALSE);
   m_assetsFilesPath.EnableWindow(FALSE);

   GetDlgItem(IDC_SEARCH_BUY)->EnableWindow(FALSE);
   GetDlgItem(ID_STOP_PROCESSING)->EnableWindow(TRUE);

   m_assetsList.DeleteAllItems();
   m_assetsListItemsData.clear();

   m_progress.SetPos(0);
   GetDlgItem(IDC_PROGRESS_DESC)->SetWindowText(_T(""));

   InitialiseNextResult();
}

void SearchGMMAsignalsDlg::ResetTurnOver()
{
   m_turnoverKandleAmount = -1.0;
   m_minTurnoverTotal     = -1.0;
   m_maxTurnoverTotal     = -1.0;
}

bool SearchGMMAsignalsDlg::TurnoverDefinedProperly() const
{
   return m_turnoverKandleAmount >= 0 && m_minTurnoverTotal >= 0 && m_maxTurnoverTotal >= 0;
}

bool SearchGMMAsignalsDlg::AssetsFilesPathDefinedProperly() const
{
   CString strAssetsFilesPath;
   m_assetsFilesPath.GetWindowText(strAssetsFilesPath);

   CPath path(strAssetsFilesPath);

   if(FALSE == path.IsDirectory() || FALSE == path.FileExists())
   {
      return false;
   }

   return true;
}

bool SearchGMMAsignalsDlg::ReadyToProcessAssets()
{
   if(!TurnoverDefinedProperly())
   {
      AfxMessageBox(_T("Obr�t zdefiniowany nie poprawnie"), MB_OK);
      return false;
   }
   
   if(!AssetsFilesPathDefinedProperly())
   {
      AfxMessageBox(_T("niepoprawny katalog z plikami instrument�w"), MB_OK);
      return false;
   }

   switch(m_algorithmTypeList[m_algorithmType])
   {
   case IDC_BEGINING_OF_TREND:
      // refresh dialog data
      m_beginingOfTrendParams.UpdateData();
      return true;

   case IDC_PARALLEL_CHANNEL:
      m_parallelChannelParams.UpdateData();
      return m_parallelChannelParams.Context().IsValid();

   case IDC_DOUBLE_BOTTOM:
      m_doubleBottomContextDlg.UpdateData(TRUE);
      return true;
   }

   return true;
}

void SearchGMMAsignalsDlg::OnNMDblclkAssetsList(NMHDR *pNMHDR, LRESULT *pResult)
{
   LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

   if(pNMItemActivate->iItem > -1)
   {
      G_stockAnalyserApp.OpenDocumentFile( m_assetsListItemsData[pNMItemActivate->iItem] );
   }

   *pResult = 0;
}

void SearchGMMAsignalsDlg::OnBnClickedBeginingOfTrend()
{
   OnAlgorithgTypeChanged();
}

void SearchGMMAsignalsDlg::OnBnClickedNearBuySignal()
{
   OnAlgorithgTypeChanged();
}

void SearchGMMAsignalsDlg::OnBnClickedParallelChannel()
{
   OnAlgorithgTypeChanged();
}

void SearchGMMAsignalsDlg::OnBnClickedDoubleBottom()
{
   OnAlgorithgTypeChanged();
}

void SearchGMMAsignalsDlg::OnDestroy()
{
   AbortProcessing();

   AssetsListBuffer::Instance()->Release();

   __super::OnDestroy();
}
