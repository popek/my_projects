#pragma once
#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols
#include <GdiPlus.h>
#include "ToolbarContainer.h"
#include "SearchGMMAsignalsDlg.h"
#include "TradeStrategiesTesterDlg.h"
#include "MarketScannerDlg.h"
#include "IBeforeShowChartEvent.h"


class Asset;


class CStocksAnalyserApp : public CWinApp, public IBeforeShowChartEvent
{
public:
	CStocksAnalyserApp();


// Overrides
public:
	virtual BOOL InitInstance();
   
   virtual int ExitInstance();
   
   virtual BOOL PreTranslateMessage(MSG* pMsg);

   ToolbarContainer& ToolsWindow();
   
   void              ShowGMMADlg();

   BOOL              GMMADlgVisible() const;

   void              ShowTradeStrategyTesterDlg();

   BOOL              TradeStrategyTesterDlgVisible() const;

   void ShowMarketScannerDlg();

   BOOL MarketScannerDlgVisible() const;


   void              UpdateCSVFiles();

   static UINT       AssetsListUpdateSuccessMsg();

   static UINT       AssetsListUpdateErrorMsg();

   virtual void      OnBeforeShowingAsset(Asset& a_assetToShow) const;

   // Implementation
	DECLARE_MESSAGE_MAP()
   afx_msg void OnAppAbout();


protected:
   Gdiplus::GdiplusStartupInput _gdiplusStartupInput;
   ULONG_PTR           _gdiplusToken;

   SearchGMMAsignalsDlg m_GMMAsignalDlg;

   TradeStrategiesTesterDlg m_strategyTesterDlg;

   MarketScannerDlg m_marketScannerDlg;
};

extern CStocksAnalyserApp G_stockAnalyserApp;