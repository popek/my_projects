#include "StdAfx.h"
#include "AssetsListBuffer.h"
#include "AssetContainer.h"


AssetsListBuffer* AssetsListBuffer::Sm_pInstance = NULL;


AssetsListBuffer::AssetsListBuffer(void)
{
}
AssetsListBuffer::~AssetsListBuffer(void)
{
}

AssetsListBuffer* AssetsListBuffer::Instance()
{
   if(NULL == Sm_pInstance)
   {
      Sm_pInstance = new AssetsListBuffer();
   }

   return Sm_pInstance;
}

void AssetsListBuffer::Release()
{
   m_mapCSVFilePath2Asset.clear();
}

void AssetsListBuffer::BufferAssetContainer(CString a_strAssetCSVFilePath, shared_ptr<AssetContainer> a_spAssetContainer)
{
   m_mapCSVFilePath2Asset[a_strAssetCSVFilePath] = a_spAssetContainer;
}

shared_ptr<AssetContainer> AssetsListBuffer::GetAssetContainer(CString a_strAssetCSVFilePath)
{
   
   TInternalAssetsMap::iterator itCSVFilePath2Asset = m_mapCSVFilePath2Asset.find(a_strAssetCSVFilePath);

   if(itCSVFilePath2Asset != m_mapCSVFilePath2Asset.end())
   {
      return itCSVFilePath2Asset->second;
   }
   else
   {
      AssetContainer* pEmpty = NULL;
      return shared_ptr<AssetContainer>(pEmpty);
   }
}