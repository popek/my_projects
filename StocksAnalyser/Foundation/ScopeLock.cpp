#include "StdAfx.h"
#include "ScopeLock.h"

ScopeLock::ScopeLock(CriticalSection& a_criticalSection)
: m_criticalSection(a_criticalSection)
{
   m_criticalSection.Enter();
}

ScopeLock::~ScopeLock(void)
{
   m_criticalSection.Leave();
}
