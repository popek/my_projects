#pragma once
#include "FoundationLib.h"
#include "CriticalSection.h"

class IThreadMessageHandler;

class FOUNDATION_LIB ThreadBase
{
public:
   enum
   {
      EDiscardQueuedMessages = 1,         // if set, only last message from queue is processed. Rest is removed from queue
      EUseFifo               = 2          // second bit in flag is set then quue is FIFO (else it's FILO)
   };

public:
   ThreadBase(IThreadMessageHandler* a_pMessageHandler);
   virtual ~ThreadBase(void);

   void Create();

   void Stop();

   void Suspend();

   void Resume();

   void SetFlags(UINT a_newFlags);

   UINT GetFlags() const;

   void PostMessage(UINT a_nMessageID);

   void PostIfNotInQueue(UINT a_messageId);

protected:

   bool IsQueued(UINT a_messageId);

   void Run();

   void Reset();

private:

   bool GetNextMessageId(UINT& a_rMessageId);

protected:

   IThreadMessageHandler* m_pHandler;

   
private:

   static unsigned __stdcall ThreadProc(void* a_pThread);

   HANDLE m_hMessageEvent;

   deque<UINT>        m_messagesQueue;
   CriticalSection   m_csLock;

   bool              m_bThreadRunning;
   bool              m_bShouldStop;
   HANDLE            m_hThread;

   UINT              m_flags;
};
