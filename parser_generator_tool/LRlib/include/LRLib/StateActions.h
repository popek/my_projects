#pragma once
#include <vector>

namespace lrlib
{

class Grammar;

/*------------------------------------------------------
class name: StateActions
-------------------------------------------------------*/
/**
* class representing some state actions
*
*//*--------------------------------------------------*/
class StateActions
{
   // vector that keeps goto (shift) state ids
   std::vector<int> m_vecGotoStateIDs;

   //vector that keeps reduction rules id's
   //(usually there will be one reduction, but in case reduce/reduce conflict, vector of reduction ids is needed instead of reduction id) 
   std::vector<std::vector<int>> m_vecReductions;

   //vector that keeps action ids (shift or reduce)
   std::vector<int> m_vecActionIDs;
   int m_iSymbolsAmount;
   Grammar* m_pGrammar;

public:
   StateActions(int iSymbolsAmount, Grammar* pG);
   virtual ~StateActions(void);

   void AddShiftAction(int iSymbolID, int iGotoStateID);
   void AddReduceAction(int iSymbolID, std::vector<int> vecReductionsIDs);
   int  GetSymbolIDIdx(int iSymbolID);

   std::string GetActionDebugInfo(int idx);
   int GetShiftStateID(int idx);
   int GetReductionID(int idx);
   std::string GetConflictInfo();
};

} // namespace lrlib