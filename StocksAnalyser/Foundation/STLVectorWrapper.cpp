#include "StdAfx.h"
#include "STLVectorWrapper.h"


STLVectorWrapper::STLVectorWrapper(vector<double>& a_vector)
: _vector(a_vector)
{
}
STLVectorWrapper::~STLVectorWrapper(void)
{
}

double STLVectorWrapper::operator [](size_t a_elementIndex) const
{
return _vector[a_elementIndex];
}

size_t STLVectorWrapper::Size() const
{
   return _vector.size();
}
