#include "StdAfx.h"
#include "LineCoefficientCalculator.h"
#include "STLVectorWrapper.h"
#include "MathUtils.h"


//
LineCoefficientCalculator::LineCoefficientCalculator(void)
{
}
LineCoefficientCalculator::~LineCoefficientCalculator(void)
{
}


void LineCoefficientCalculator::Calculate(vector<double> a_arguments, vector<double> & a_values)
{
   STLVectorWrapper vectorArguments(a_arguments);
   STLVectorWrapper vectorValues(a_values);

   Calculate(vectorArguments, vectorValues);
}

double LineCoefficientCalculator::ValueDeviation(size_t a_valueIndex) const
{
   return _valueDeviations[a_valueIndex];
}

double LineCoefficientCalculator::Value(size_t a_valueIndex) const
{
   return _values[a_valueIndex];
}

double LineCoefficientCalculator::Dev2Value(size_t a_valueIndex) const
{
   return _dev2value[a_valueIndex];
}

void LineCoefficientCalculator::Calculate(const IDoubleVector& a_arguments, const IDoubleVector& a_values)
{
   SA_ASSERT(a_arguments.Size() == a_values.Size());

   Reset();

   _S = (double)a_arguments.Size();

   for(size_t valueIndex=0; valueIndex<a_arguments.Size(); ++valueIndex)
   {
      double argument = a_arguments[valueIndex];
      double seriesValue = a_values[valueIndex];

      _Sx += argument;
      _Sy += seriesValue;

      _Sxy += argument*seriesValue;
      _Sxx += argument*argument;
      _Syy += seriesValue*seriesValue;
   }

   double delta = _S*_Sxx - _Sx*_Sx;

   _a = (_S*_Sxy - _Sx*_Sy)/delta;
   _b = (_Sxx*_Sy - _Sx*_Sxy)/delta;

   // calculate deviations
   _meanDeviation = 0;
   _valueDeviations.reserve(a_arguments.Size());
   _valueDeviations.clear();

   for(size_t valueIndex=0; valueIndex<a_arguments.Size(); ++valueIndex)
   {
      _values.push_back(_a*a_arguments[valueIndex] + _b);
      _valueDeviations.push_back( MathUtils::Abs(a_values[valueIndex] - _values.back()) );
      _dev2value.push_back(_valueDeviations.back()/_values.back());
      _meanDeviation += _valueDeviations.back();
   }
   _meanDeviation /= a_arguments.Size();


}

double LineCoefficientCalculator::A() const
{
   return _a;
}

double LineCoefficientCalculator::B() const
{
   return _b;
}


void LineCoefficientCalculator::Reset()
{
   _S   = 0;
   _Sx  = 0;
   _Sy  = 0;
   _Sxy = 0;
   _Sxx = 0;
   _Syy = 0;

   _meanDeviation = 0;
   _valueDeviations.clear();
   _values.clear();
   _dev2value.clear();
}