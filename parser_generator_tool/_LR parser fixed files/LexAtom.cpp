#include "StdAfx.h"
#include "LexAtom.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


LexAtom::LexAtom(void)
{
}
LexAtom::~LexAtom(void)
{
}

int LexAtom::line() const
{
   return _line;
}
ETerminalId LexAtom::id() const
{
   return _id;
}
string_t LexAtom::text() const
{
   return _text;
}
double LexAtom::doubleValue() const
{
   return _dValue;
}
int LexAtom::intValue() const
{
   return _iValue;
}


void LexAtom::line(int a_line)
{
   _line = a_line;
}
void LexAtom::id(ETerminalId a_id)
{
   _id = a_id;
}
void LexAtom::text(const string_t& a_text)
{
   _text = a_text;
}
void LexAtom::doubleValue(double a_value)
{
   _dValue = a_value;
}
void LexAtom::intValue(int a_value)
{
   _iValue = a_value;
}
