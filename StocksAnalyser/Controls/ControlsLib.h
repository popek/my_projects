#pragma once

#ifdef EXPORT_CONTROLS_LIB
#  define CONTROLS_LIB      __declspec(dllexport)
#else
#  define CONTROLS_LIB      __declspec(dllimport)
#endif