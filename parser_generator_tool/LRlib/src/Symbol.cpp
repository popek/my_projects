#include "Symbol.h"

namespace lrlib
{

Symbol::Symbol(void)
{
   Init("", INVALID_SYMBOL_ID, false, -1);
}


Symbol::Symbol(std::string strName, int iID, bool bTerm, int a_nLineNo)
{
   Init(strName, iID, bTerm, a_nLineNo);
}
Symbol::~Symbol(void)
{
}

std::string Symbol::name() const
{
   return m_strName;
}

bool Symbol::term() const
{
   return m_bTerm;
}

int Symbol::id() const
{
   return m_iID;
}

void Symbol::id(int a_nID)
{
   m_iID = a_nID;
}

void Symbol::used(bool flag)
{
   m_bUsed = flag;
}

bool Symbol::used()
{
   return m_bUsed;
}

void Symbol::defined(bool flag)
{
   m_bDefined = flag;
}

bool Symbol::defined()
{
   return m_bDefined;
}

int Symbol::lineNo()
{
   return m_nLineNo;
}

void Symbol::lineNo(int a_nLineNo)
{
   m_nLineNo = a_nLineNo;
}

void Symbol::rootSymbol(bool a_bValue)
{
   m_bGrammarRootSymbol = a_bValue;
}

bool Symbol::rootSymbol()
{
   return m_bGrammarRootSymbol;
}

void Symbol::Init(std::string a_strName, int a_iID, bool a_bTerm, int a_nLineNo)
{
   m_strName = a_strName;
   m_iID = a_iID;
   m_bTerm = a_bTerm;
   m_nLineNo = a_nLineNo;

   m_bGrammarRootSymbol = false;
   m_bUsed = false;
   m_bDefined = false;
}

bool Symbol::operator < (const Symbol& symbol) const
{
   return (strcmp(m_strName.c_str(), symbol.name().c_str()) < 0);
}

bool Symbol::operator == (const Symbol& symbol) const
{
   return (strcmp(m_strName.c_str(), symbol.name().c_str()) == 0);
}

bool Symbol::operator != (const Symbol& symbol) const
{
   return !(this->operator ==(symbol));
}

} // namespace lrlib