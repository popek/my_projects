#pragma once
#include "ChartView.h"

class CStocksAnalyserView : public ChartView
{
protected: // create from serialization only
	CStocksAnalyserView();
	DECLARE_DYNCREATE(CStocksAnalyserView)

public:
	CStocksAnalyserDoc* GetDocument() const;

public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
   virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);

public:
	virtual ~CStocksAnalyserView();


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in StocksAnalyserView.cpp
inline CStocksAnalyserDoc* CStocksAnalyserView::GetDocument() const
   { return reinterpret_cast<CStocksAnalyserDoc*>(m_pDocument); }
#endif

