#pragma once
#include "FoundationLib.h"

class FOUNDATION_LIB TextFile
{
public:

   TextFile(void);
   virtual ~TextFile(void);

   BOOL  OpenForWritting(CString a_strFilePath);

   void  Write(CString a_strText);

   void  Close();

protected:
   HANDLE   m_hFile;
};
