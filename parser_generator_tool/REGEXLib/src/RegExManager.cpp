#include "RegExManager.h"
#include "Exceptions.h"

namespace regexlib
{

RegExManager::RegExManager() :
   m_RegExRoot(ORRegEx(true)),
   m_bNFAgenerated(false)
{
}

RegExManager::~RegExManager(void)
{
}

void RegExManager::GenerateNFAdiagram()
{
   m_NFA.clear();
   m_RegExRoot.SetNFA(&m_NFA);
   m_RegExRoot.GenerateNFA(0);
   m_bNFAgenerated = true;
}

void RegExManager::GenerateDFAdiagram()
{
   if (m_bNFAgenerated)
   {
      typedef std::map<t_DFASubSet, unsigned int> t_MarkedDFASubSets;
      m_DFAdiagram.clear();
      unsigned int i, uiStNo = 0, uiAuxDFAState, uiRecognizedRegExIdx;
      bool bAuxAlreadyFound = false;
      t_MarkedDFASubSets::iterator itMarked;
      t_DFAstates DFA;
      t_MarkedDFASubSets DFAmarked;
      t_DFASubSet DFAss;
      t_Char2SymbolID& Char2SymbolID = GetChar2SymbolIDMap();
      t_Char2SymbolID::iterator itSym;

      DFAss.insert(0);
      DFAss = eclosure(DFAss);

      DFA.push_back(DFAss);
      DFAmarked[DFAss] = uiStNo;
      uiStNo++;

      for (i = 0; i < DFA.size(); i++)
      {
         bAuxAlreadyFound = false;

         for (itSym = Char2SymbolID.begin(); itSym != Char2SymbolID.end(); itSym++)
         {
            DFAss = eclosure(move(DFA[i], itSym->second));
            if (DFAss.size())//if there is a pass to another state/sub set
            {
               itMarked = DFAmarked.find(DFAss);
               if (itMarked == DFAmarked.end())//if state is not marked
               {
                  DFA.push_back(DFAss);
                  DFAmarked[DFAss] = uiStNo;
                  if ((uiRecognizedRegExIdx = AcceptingSubset(DFAss)) != -1)// DFA accepting state also recognize regex idx which is recognized by it
                     m_DFAacceptStates[uiStNo] = uiRecognizedRegExIdx;

                  if (!bAuxAlreadyFound)
                  {
                     uiAuxDFAState = DFAmarked[DFA[i]];
                     bAuxAlreadyFound = true;
                  }
                  m_DFAdiagram[uiAuxDFAState][itSym->second] = uiStNo;
                  uiStNo++;
               }
               else //state is already marked
               {
                  if (!bAuxAlreadyFound)
                  {
                     uiAuxDFAState = DFAmarked[DFA[i]];
                     bAuxAlreadyFound = true;
                  }
                  m_DFAdiagram[uiAuxDFAState][itSym->second] = itMarked->second;
               }
            }
         }
      }

      m_uiDFAStatesAmount = DFA.size();
   }
   else
   {
      GenerateNFAdiagram();
      GenerateDFAdiagram();
   }
}

t_DFASubSet RegExManager::eclosure(t_DFASubSet from)
{
   t_DFASubSet to;
   std::stack<unsigned int> AuxStack;
   t_DFASubSet::iterator it = from.begin();
   unsigned int uiTmpNFAstate;

   while (it != from.end())
   {
      AuxStack.push(*it);
      to.insert(*it);
      it++;
   }

   while (AuxStack.size())
   {
      uiTmpNFAstate = AuxStack.top();
      AuxStack.pop();
      eclosure(uiTmpNFAstate, to, AuxStack);
   }

   return to;
}

void RegExManager::eclosure(unsigned int NFAstate, t_DFASubSet& Out, std::stack<unsigned int>& AuxStack)
{
   t_NFAdiagram::iterator itNFA = m_NFA._EmptyTranDiag.find(NFAstate);
   if (itNFA != m_NFA._EmptyTranDiag.end())
   {
      unsigned int uiSize = Out.size();
      t_NFAtransitions& ET = itNFA->second;
      t_NFAtransitions::iterator it = ET.begin();
      while (it != ET.end())
      {
         Out.insert(it->second);// insert NFA state into DFA sub set
         if (Out.size() > uiSize) // if new NFA state added
         {
            AuxStack.push(it->second);
            uiSize++;
         }
         it++;
      }
   }
}

t_DFASubSet RegExManager::move(t_DFASubSet from, short SymbolID)
{
   t_DFASubSet to;
   t_DFASubSet::iterator it = from.begin();
   while (it != from.end())
   {
      move(*it, SymbolID, to);
      it++;
   }

   return to;
}

void RegExManager::move(unsigned int NFAstate, short SymbolID, t_DFASubSet& Out)
{
   t_NFAdiagram::iterator itNFA = m_NFA._SymbolTranDiag.find(NFAstate);
   if (itNFA != m_NFA._SymbolTranDiag.end())
   {
      t_NFAtransitions& ST = itNFA->second;
      t_NFAtransitions::iterator it = ST.find(SymbolID);
      while (it != ST.end() && it->first == SymbolID)
      {
         Out.insert(it->second);// insert NFA state into DFA sub set
         it++;
      }
   }
}

/*
*	name: AcceptingSubset
*
* description: used to recognized if DFA subset is accepting subset
*
* return: -1, if subset is not DFA accepting subset
*					idx of first regex which NFA accepting state is in DFA subset, is subset is DFA accepting subset
*/
unsigned int RegExManager::AcceptingSubset(t_DFASubSet& dfass)
{
   for (unsigned int i = 0; i < m_NFA._uiNFAacceptStates.size(); i++)
      if (dfass.find(m_NFA._uiNFAacceptStates[i]) != dfass.end())// if DFA subset has at leas one NFA accepting state, then DFA subset is accepting subset
         return i;
   return -1;
}

unsigned int RegExManager::ParseToken(std::string strToken)
{
   unsigned int uiCnt = 0, uiSize = strToken.size(), uiCurrDFAstate = 0;

   char cSym;
   while (uiCnt < uiSize)
   {
      cSym = strToken[uiCnt];

      try
      {
         t_DFAtransition& AllTrans = m_DFAdiagram[uiCurrDFAstate];
         t_DFAtransition::iterator SpecificTran = AllTrans.find(Char2SymbolID(cSym));
         if (SpecificTran != AllTrans.end())
            uiCurrDFAstate = m_DFAdiagram[uiCurrDFAstate][Char2SymbolID(cSym)];
         else
            return -1;
      }
      catch (CharNotFoundExc&)
      {
         return false;
      }

      uiCnt++;
   }

   auto it = m_DFAacceptStates.find(uiCurrDFAstate);
   if (it != m_DFAacceptStates.end())
      return it->second;//index of recognized regex

   return -1; // if strToken was not recognized by analyzer
}



void RegExManager::DebPrint_RegExTree()
{
   DebPrint_RegEx(&m_RegExRoot, 0);
}

void RegExManager::DebPrint_RegEx(RegEx* pre, unsigned int uiIndent)
{
   for (unsigned int i = 0; i < uiIndent; i++)
      printf("\t");

   switch (pre->Type())
   {
      case eRE_Simple:
      {
         printf("Simple %s\n", static_cast<SimpleRegEx*>(pre)->name().c_str());
         break;
      }
      case eRE_MUL:
      {
         printf("Multi :\n");
         DebPrint_RegEx(static_cast<MULRegEx*>(pre)->GetBaseRegEx(), uiIndent + 1);
         break;
      }
      case eRE_OR:
      {
         ORRegEx* porre = static_cast<ORRegEx*>(pre);
         printf("OR:\n");
         for (unsigned int i = 0; i < porre->size(); i++)
            DebPrint_RegEx((*porre)[i], uiIndent + 1);
         break;
      }
      case eRE_Compound:
      {
         CompoundRegEx* pcre = static_cast<CompoundRegEx*>(pre);
         printf("Compound:\n");

         for (unsigned int i = 0; i < pcre->size(); i++)
            DebPrint_RegEx((*pcre)[i], uiIndent + 1);
      }
   }
}

} // namespace regexlib