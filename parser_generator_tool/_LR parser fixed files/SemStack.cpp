#include "stdafx.h"
#include "SemStack.h"
#include "SemAtom.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


SemStack::SemStack(void)
{
}

SemStack::~SemStack(void)
{
}

void SemStack::Push(shared_ptr<SemAtom> a_spSemAtom)
{
  m_stack.push_back(a_spSemAtom);
}

void SemStack::Pop()
{
  if(!m_stack.empty())
  {
    m_stack.pop_back();
  }
}

shared_ptr<SemAtom> SemStack::Top()
{
  if(!m_stack.empty())
  {
    return m_stack.back();
  }

  return nullptr;
}