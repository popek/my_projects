#pragma once
#include "PropertyDialogBase.h"
#include "DoubleBottomFinder.h"


class DoubleBottomContextDlg : public PropertyDialogBase
{
	DECLARE_DYNAMIC(DoubleBottomContextDlg)

public:
	DoubleBottomContextDlg();
	virtual ~DoubleBottomContextDlg();

	enum { IDD = IDD_DOUBLE_BOTTOM_CONTEXT };

   shared_ptr<DoubleBottomFinder::Context> Context();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP()

protected:

   shared_ptr<DoubleBottomFinder::Context> m_spContext;

   CString m_period;
   CString m_neigboursCount;
   CString m_bottomDepth;
   CString m_bottomsDifference;
};
