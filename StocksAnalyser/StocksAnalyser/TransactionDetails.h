#pragma once


class KandleDate;


class TransactionDetails
{
public:
   TransactionDetails(shared_ptr<KandleDate> a_spDate, double a_price);
   virtual ~TransactionDetails(void);

   double price() const { return m_price; };
   void price(double value) { m_price = value; }


protected:
   shared_ptr<KandleDate> _spDate;
   double m_price;
};
