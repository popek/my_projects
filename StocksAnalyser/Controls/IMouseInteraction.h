#pragma once

class IMouseInteraction
{
public:
   virtual ~IMouseInteraction(void){};

   virtual void OnLButtonDown(UINT a_nFlags, const CPoint& a_point)    = 0;

   virtual void OnLButtonUp(UINT a_nFlags, const CPoint& a_point)      = 0;

   virtual void OnMouseMove(UINT a_nFlags, const CPoint& a_point)      = 0;

   virtual void OnSize(int a_chartWidth)              = 0;

   virtual void Draw(CDC& a_dc)                       = 0;

   virtual bool DrawingRequired() const               = 0;
};
