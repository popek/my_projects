


/*
//	LEX_DEF -> lex_keyword { LEX_SYMBOLS_LIST_EX LEX_TOKEN_LIST_EX } ;
	
	LEX_SYMBOLS_LIST_EX -> { LEX_SYMBOLS_LIST }; 
						//|  {} ;
	
	LEX_SYMBOLS_LIST -> LEX_SYMBOL_DEF
					|   LEX_SYMBOLS_LIST LEX_SYMBOL_DEF ;
	
	
	
	
	//LEX_SYMBOLS_LIST_EX -> {} ;
	LEX_SYMBOL_DEF   -> identifier eval_to LEX_SYMBOL_DEF_PART
					 |  LEX_SYMBOL_DEF , LEX_SYMBOL_DEF_PART ;
	
	LEX_SYMBOL_DEF_PART -> LEX_SYMBOLS_RANGE 
					| identifier 
					| " numeric_value " ;
	
	LEX_SYMBOLS_RANGE -> identifier - identifier ;
	
	LEX_TOKEN_LIST_EX -> { LEX_TOKEN_LIST }
						| { } ;
	
	LEX_TOKEN_LIST -> LEX_TOKEN_DEF_EX
					| LEX_TOKEN_LIST LEX_TOKEN_DEF_EX ;
					
	LEX_TOKEN_DEF_EX -> identifier eval_to LEX_TOKEN_DEF srednik ;
	
	LEX_TOKEN_DEF -> LEX_TOKEN_DEF_OR
				  |  LEX_TOKEN_DEF LEX_TOKEN_DEF_OR ;
	
	LEX_TOKEN_DEF_OR  -> LEX_TOKEN_DEF_AND
	                   | LEX_TOKEN_DEF_AND or token ;
					   
					   
					   
					   
					   
					   
					   
					   
					   
					   
					   
					   
LEX_DEF -> lex_keyword { LEX_SYMBOLS_LIST_EX LEX_TOKEN_LIST_EX } ;

		
					   
	LEX_TOKEN_DEF_AND ->  LEX_TOKEN_DEF_MUL
						| LEX_TOKEN_DEF_MUL token;

	LEX_TOKEN_DEF_MUL ->  LEX_TOKEN_DEF_BASE
						| LEX_TOKEN_DEF_BASE *;

	LEX_TOKEN_DEF_BASE -> identifier
						| ( LEX_TOKEN_DEF );
						
*/
	NONTERMINAL -> identifier;

	
	
	PRODUCTION -> PRODUCTION_ASSIGN PRODUCTION_EXPR semicolon;
	
	SYMBOL  -> identifier
			| single_char_token;

	PRODUCTION_ASSIGN -> NONTERMINAL eval_to;
	
	PRODUCTION_EXPR -> SYMBOL_LIST
					| PRODUCTION_EXPR or SYMBOL_LIST;

*/						
	SYMBOL_LIST -> SYMBOL
				| SYMBOL_LIST SYMBOL;
	
	PRODUCTION_LIST -> PRODUCTION
			|  PRODUCTION_LIST PRODUCTION;       
