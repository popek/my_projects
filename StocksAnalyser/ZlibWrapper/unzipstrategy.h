#pragma once
#include "unzip.h"


namespace zlibwrapper
{
   class unzipstrategy
   {
   public:
      unzipstrategy(void);
      virtual ~unzipstrategy(void);

      int extract(const char *zipfilename, const char* outFolderPath);

   private:
      int extract(unzFile uf, const char* password, const char* outFolderPath);
      int extract_currentfile(unzFile uf, const char* password, const char* outFolderPath);

      void change_file_date(const char *filename, uLong dosdate, tm_unz tmu_date);

   private:
      int _maxFileName;
      char* _pOutFilePath;
      
      char* _pFileNameStart;
      char* _pFilename_inzip;

      void* _buffer;
   };
}


