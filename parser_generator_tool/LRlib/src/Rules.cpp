#include "Rules.h"
#include <utility>

namespace lrlib
{

Rules::Rules(void)
{
}

Rules::~Rules(void)
{
}

void Rules::AddGrammarRule(Symbol* a_pNonterminalResult, std::vector<Symbol*>& a_vecSymbols)
{
   m_mmapRules.insert(std::make_pair(a_pNonterminalResult, a_vecSymbols));
}

void Rules::GenerateRulesByIDsMap()
{
   auto itRule = m_mmapRules.begin();
   std::vector<int>  vecSymbolsIDs;
   m_mmapRulesByIDs.clear();

   while (itRule != m_mmapRules.end())
   {
      for (size_t nSymIdx = 0; nSymIdx < (itRule->second).size(); ++nSymIdx)
      {
         vecSymbolsIDs.push_back((itRule->second)[nSymIdx]->id());
      }
      m_mmapRulesByIDs.insert(make_pair(itRule->first->id(), vecSymbolsIDs));

      vecSymbolsIDs.clear();

      ++itRule;
   }
}

std::multimap<int, std::vector<int>>& Rules::RulesByIDs()
{
   return m_mmapRulesByIDs;
}

void Rules::FindAndAddDescendands(Symbol* a_pSymbol, std::set<Symbol*, LessPtr<Symbol>>& a_rsetDescendands)
{
   auto itSymbolProduction = m_mmapRules.find(a_pSymbol);

   while ((itSymbolProduction != m_mmapRules.end()) && (*(itSymbolProduction->first) == *(a_pSymbol)))
   {
      auto& rvecChilds = itSymbolProduction->second;

      for (size_t nChildIdx = 0; nChildIdx < rvecChilds.size(); ++nChildIdx)                   // go throough all descendands (rule child symbols)
      {
         if (a_rsetDescendands.find(rvecChilds[nChildIdx]) == a_rsetDescendands.end())     // check descendands only for childs which were not marked as descendands yet
         {
            a_rsetDescendands.insert(rvecChilds[nChildIdx]);

            FindAndAddDescendands(rvecChilds[nChildIdx], a_rsetDescendands);
         }
      }
      ++itSymbolProduction;
   }
}

int Rules::SetNonterminalsIDsAccordingToHierarchy(Symbol* a_pNonterminal, int a_nID /*=0*/)
{
   auto itSymbolProduction = m_mmapRules.find(a_pNonterminal);

   if (m_mmapRules.end() == itSymbolProduction)
   {
      return a_nID;
   }

   std::vector<Symbol*> vJustNonterminals;
   size_t nSymbolIdx = 0;

   if (INVALID_SYMBOL_ID == a_pNonterminal->id())      // if symbol has no ID yet...
   {
      a_pNonterminal->id(a_nID);                      // ... assign one to it
      ++a_nID;
   }

   while ((*itSymbolProduction->first) == (*a_pNonterminal))                                                             //loop through each production of nonterminal
   {
      auto& rvProductionList = itSymbolProduction->second;

      for (nSymbolIdx = 0; nSymbolIdx < rvProductionList.size(); ++nSymbolIdx)                                               // set IDs of each nonterminal in production list
      {
         if (false == rvProductionList[nSymbolIdx]->term() && INVALID_SYMBOL_ID == rvProductionList[nSymbolIdx]->id())  // if sub-symbol is nonterminal and it's ID is not defined yet
         {
            rvProductionList[nSymbolIdx]->id(a_nID);
            ++a_nID;

            vJustNonterminals.push_back(rvProductionList[nSymbolIdx]);
         }
      }

      ++itSymbolProduction;                                                                                              // go to the next product

      if (itSymbolProduction == m_mmapRules.end())
      {
         break;
      }
   }

   // loop all nonterminals which are on production list
   for (nSymbolIdx = 0; nSymbolIdx < vJustNonterminals.size(); ++nSymbolIdx)
   {
      a_nID = SetNonterminalsIDsAccordingToHierarchy(vJustNonterminals[nSymbolIdx], a_nID);
   }

   return a_nID;
}

} // namespace lrlib