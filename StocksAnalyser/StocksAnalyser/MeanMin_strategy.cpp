#include "StdAfx.h"
#include "MeanMin_strategy.h"
#include "Asset.h"
#include "AssetAuxiliaryWrappers.h"
#include "StrategyParams.h"
#include "SMA_pointer.h"
#include "MIN_pointer.h"
#include "ChartElementsCollection.h"
#include "TransactionDetails.h"
#include "StrategyResult.h"
#include "ControlsLibUtils.h"
#include "ColorUtils.h"
#include "LineStyle.h"
#include "ChartElement.h"
#include "MathUtils.h"


MeanMin_strategy::MeanMin_strategy(shared_ptr<Asset> spAsset)
   : TradeStrategyBase(spAsset)
   , _state(EIdle)
   , _minIndicatorPeriod(5)
   , _offsetFromMinimumInPercents(0.02)
   , _smaPeriod(100)
   , _currentValue(-1.0)
   , _currentStopLossValue(-1.0)
{
}
MeanMin_strategy::~MeanMin_strategy(void)
{
}

shared_ptr<StrategyResult> MeanMin_strategy::RunTest( const KandleDate& a_start, const KandleDate& a_finish)
{
   _elements = _spAsset->ElementsCollection() ? 
      _spAsset->ElementsCollection() : 
      make_shared<ChartElementsCollection>();

   auto result = make_shared<StrategyResult>();
   shared_ptr<TransactionDetails> transaction;
   vector<shared_ptr<TransactionDetails>> allTransactions;

   size_t sampleIndex = _spAsset->FindNearestKandle(a_start);
   size_t endIndex = _spAsset->FindNearestKandle(a_finish);

   if(sampleIndex < _minIndicatorPeriod)
   {
      sampleIndex = _minIndicatorPeriod;
   }
   if(sampleIndex < _smaPeriod)
   {
      sampleIndex = _smaPeriod;
   }
   if(sampleIndex >= endIndex)
   {
      return nullptr;
   }

   _sma = make_shared<SMA_pointer>(_smaPeriod);
   _sma->Create(AssetCloseValueGetter(*_spAsset));

   _minIndicator = make_shared<MIN_pointer>(_minIndicatorPeriod);
   _minIndicator->Create(AssetLowValueGetter(*_spAsset));

   for(; sampleIndex<endIndex; ++sampleIndex)
   {
      // we are making decisiton on the end of session
      _currentValue = (*_spAsset)[sampleIndex].Close(); 

      // stop loss is calculated based on previous sample
      size_t prevIndex = sampleIndex > 0 ? sampleIndex - 1 : 0;
      _currentStopLossValue = MathUtils::Max(_minIndicator->Series(0).GetDataBasedOnRootSeriesIndex(prevIndex > 0 ? prevIndex - 1 : 0),
                                             _sma->Series(0).GetDataBasedOnRootSeriesIndex(prevIndex));
      
      transaction = StopLossTriggered(sampleIndex);
      if(transaction)
      {
         allTransactions.push_back(transaction);
         _state = EIdle;
      }

      switch(_state)
      {
      case EIdle:
         transaction = handleIdle(sampleIndex);
         break;

      case EBoughtAboveMean:
         transaction = handleBoughtAboveMean(sampleIndex);
         break;

      case EBoughtAboveLastMinimum:
         transaction = handleBoughtAboveLastMinimum(sampleIndex);
         break;

      case ESoldBelowMean:
         transaction = handleSoldBelowMean(sampleIndex);
         break;

      case ESoldBelowMinimum:
         transaction = handleSoldBelowMinimum(sampleIndex);
         break;
      }

      if(transaction)
      {
         allTransactions.push_back(transaction);
      }
   }

   if(allTransactions.size())
   {
      if(allTransactions.size() % 2 > 0)
      {
         allTransactions.push_back( make_shared<TransactionDetails>(_spAsset->GetKandle(endIndex - 1).Date(), _spAsset->GetKandle(endIndex - 1).Close()) );
      }

      for(size_t transactionIndex = 0; transactionIndex < allTransactions.size(); transactionIndex += 2)
      {
         result->RegisterEntry(allTransactions[transactionIndex], allTransactions[transactionIndex+1]);
      }

      AddGraphicPresentation(_elements);
      _spAsset->AttachElementsCollection(_elements);
   }

   return result;
}

shared_ptr<TransactionDetails> MeanMin_strategy::handleIdle(size_t candleIndex)
{
   const Kandle& candle = (*_spAsset)[candleIndex];
   double smaValue = _sma->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex);

   if(_currentValue < smaValue)
   {
      _state = ESoldBelowMean;
      return nullptr;
   }

   if(_currentValue < _minIndicator->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex > 0 ? candleIndex - 1 : 0))
   {
      _state = ESoldBelowMinimum;
      return nullptr;
   }

   if(_currentValue > _minIndicator->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex > 0 ? candleIndex - 1 : 0) * (1 + _offsetFromMinimumInPercents))
   {
      _state = EBoughtAboveLastMinimum;
   }
   else
   {
      _state = EBoughtAboveMean;
   }

   _elements->PushBack(CreateSignalChartElement(true, _currentValue, candle.Date()));
   return make_shared<TransactionDetails>(candle.Date(), _currentValue);
}

shared_ptr<TransactionDetails> MeanMin_strategy::handleSoldBelowMean(size_t candleIndex)
{
   const Kandle& candle = (*_spAsset)[candleIndex];
   double smaValue = _sma->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex);

   if(_currentValue < smaValue)
   {
      return nullptr;
   }

   _state = EBoughtAboveMean;
   _elements->PushBack(CreateSignalChartElement(true, _currentValue, candle.Date()));
   return make_shared<TransactionDetails>(candle.Date(), _currentValue);
}


shared_ptr<TransactionDetails> MeanMin_strategy::handleSoldBelowMinimum(size_t candleIndex)
{
   bool isSold = false;
   const Kandle& candle = (*_spAsset)[candleIndex];
   double buyValue = _minIndicator->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex > 0 ? candleIndex - 1 : 0) * (1 + _offsetFromMinimumInPercents);

   if(_currentValue > buyValue)
   {
      _state = EBoughtAboveLastMinimum;
      _elements->PushBack(CreateSignalChartElement(true, _currentValue, candle.Date()));
      return make_shared<TransactionDetails>(candle.Date(), _currentValue);
   }
   
   if(_currentValue < _sma->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex)) // if candle get below sma, then state needs to be changed
   {
      _state = ESoldBelowMean;
   }

   return nullptr;
}


shared_ptr<TransactionDetails> MeanMin_strategy::handleBoughtAboveMean(size_t candleIndex)
{
   bool isSold = false;
   const Kandle& candle = (*_spAsset)[candleIndex];

   if(_currentValue < _sma->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex))        // sell if asset value is below mean
   {
      _state = ESoldBelowMean;
      isSold = true;
   }
   else
   {
      double sellValue = _minIndicator->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex > 0 ? candleIndex - 1 : 0);
      if(_currentValue < sellValue)      // ... or if asset value gets below minimal acceptable price (this is added to keep current winnings, without waiting form value to get below mean)
      {
         _state = ESoldBelowMinimum;
         isSold = true;
      }
   }

   if(isSold)
   {
      _elements->PushBack(CreateSignalChartElement(false, _currentStopLossValue, candle.Date()));
      return make_shared<TransactionDetails>(candle.Date(), _currentStopLossValue);
   }

   return nullptr;
}

shared_ptr<TransactionDetails> MeanMin_strategy::handleBoughtAboveLastMinimum(size_t candleIndex)
{
   return handleBoughtAboveMean(candleIndex);
}

void MeanMin_strategy::AddGraphicPresentation(shared_ptr<ChartElementsCollection> elements)
{
   elements->PushBack(ControlsLibUtils::CreatePointerSeriesElement(_sma->Series(0), *_spAsset));
   elements->GetElement(elements->ElementsCount()-1)->Style()->SetNumericParam(LineStyle::EColor, 
      ColorUtils::MixColors(ColorUtils::EBlue, ColorUtils::EWhite, 0.4));

   elements->PushBack(ControlsLibUtils::CreatePointerSeriesElement(_minIndicator->Series(0), *_spAsset));
   elements->GetElement(elements->ElementsCount()-1)->Style()->SetNumericParam(LineStyle::EColor, 
      ColorUtils::MixColors(ColorUtils::EBlue, ColorUtils::EWhite, 0.65));
}

shared_ptr<TransactionDetails> MeanMin_strategy::StopLossTriggered(size_t candleIndex) const
{
   if( EBoughtAboveMean == _state || EBoughtAboveLastMinimum == _state)
   {
     if( (*_spAsset)[candleIndex].Low() < _currentValue )
     {
        return make_shared<TransactionDetails>((*_spAsset)[candleIndex].Date(), _currentStopLossValue);
     }
   }

   return nullptr;
}