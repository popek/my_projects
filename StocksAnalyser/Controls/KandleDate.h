#pragma once
#include "ControlsLib.h"

class CONTROLS_LIB KandleDate
{
public:

    enum EWeekDay {eSunday = 0, eMonday, eTuestady, eWednesday, eThursday, eFriday, eSaturday };
//    
//    enum EMonths
//    {
//       eJanuary= 1,
//       eFebruary,
//       eMarch,
//       eApril,
//       eMay,
//       eJune,
//       eJuly,
//       eAugust,
//       eSeptember,
//       eOctober,
//       eNovember,
//       eDecember
//    };

   KandleDate(size_t a_year, size_t a_month, size_t a_day);
   KandleDate(string& a_strDate, size_t a_index);
   virtual ~KandleDate();
   
   CString           ToString() const;

   EWeekDay          ToWeekDay() const;

   int               DaysOffset(const KandleDate& a_date) const;

   size_t            Index() const;

   int               Month() const;

   bool operator < (const KandleDate& a_toCompare) const;

   shared_ptr<KandleDate> Clone(size_t a_indexForClone) const;


protected:
   KandleDate(const KandleDate& a_dateToCopy);
   KandleDate& operator = (const KandleDate& a_dateToCopy);

   size_t            hash() const;

   void              FromString(string& a_strDate);

   int               ToDayOfYear() const;

   static bool       LeapYear(int a_year);


protected:
   size_t m_nYear;
   size_t m_nMonth;
   size_t m_nDay;

   size_t m_index;
};
