#include "internal/GrammarGenerationObserver.h"
#include <iostream>

using namespace std::chrono;

namespace lrlib
{
namespace helper
{

GrammarGenerationTimings::GrammarGenerationTimings()
   : gotoCnt_(0)
   , stateCnt_(0)
{
}

void GrammarGenerationTimings::onGrammarGenerationStarted()
{
   startTime_ = system_clock::now();
}
void GrammarGenerationTimings::onGrammarGenerationFinished()
{
   auto wholeTime = duration_cast<milliseconds>(system_clock::now() - startTime_);

   std::cout << "[parser generated in " << wholeTime.count() << " ms]" << std::endl;

   auto gotosTime = (long long)gotosAccumulator_ / 1000;
   std::cout
      << "[parser gotos counted in " << gotosTime << " ms [" << gotoCnt_ << "])]" 
      << std::endl;
}

void GrammarGenerationTimings::onStateGenerationStarted()
{
   stateStartTime_ = system_clock::now();
}
void GrammarGenerationTimings::onStateGenerationFinished()
{
   ++stateCnt_;
   auto time = duration_cast<milliseconds>(system_clock::now() - stateStartTime_);

   std::cout 
      << "[state " << stateCnt_ << " generated in " << time.count() << " ms]" 
      << std::endl;
}

void GrammarGenerationTimings::onGotoGenerationStarted()
{
   ++gotoCnt_;
   gotoStartTime_ = system_clock::now();
}
void GrammarGenerationTimings::onGotoGenerationFinished()
{
   gotosAccumulator_ += duration_cast<microseconds>(system_clock::now() - gotoStartTime_).count();
}

}
}