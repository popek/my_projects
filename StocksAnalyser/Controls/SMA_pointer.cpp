#include "StdAfx.h"
#include "SMA_pointer.h"
#include "CommonDefines.h"
#include "IDoubleVector.h"
#include "DataSeries.h"
#include "ControlsLibUtils.h"


SMA_pointer::SMA_pointer(int a_nMeanValueCalculationSamplesAmount)
: PointerAvarageBase(a_nMeanValueCalculationSamplesAmount)
{
}

SMA_pointer::~SMA_pointer(void)
{
}

void SMA_pointer::Create(const DataSeries& a_inputData)
{
   // check if samples amount neede to prepare mean value is grater then 0
   if(m_nMeanValueCalculationSamplesAmount <= 0)
      return;
   // make calculation only in case asset samples amount is grater then samples amount needed to prepare mean value
   if((int)a_inputData.size() < m_nMeanValueCalculationSamplesAmount)
      return;

   size_t elementIndex   = 0;
   double dSamplesSum  = 0;

   shared_ptr<vector<double>> spData(new vector<double>());
   vector<double>& meanValues = *spData;

   meanValues.reserve(a_inputData.size() - m_nMeanValueCalculationSamplesAmount + 1);

   // prepare first sum
   for(elementIndex=0; elementIndex<(size_t)m_nMeanValueCalculationSamplesAmount; ++elementIndex)
   {
      dSamplesSum += a_inputData[elementIndex];
   }

   meanValues.push_back(dSamplesSum/m_nMeanValueCalculationSamplesAmount);

   for(elementIndex=m_nMeanValueCalculationSamplesAmount; elementIndex<a_inputData.size(); ++elementIndex)
   {
      // substract last sum last sample value
      dSamplesSum -= a_inputData[elementIndex - m_nMeanValueCalculationSamplesAmount];
      // add new sample value
      dSamplesSum += a_inputData[elementIndex];

      meanValues.push_back(dSamplesSum/m_nMeanValueCalculationSamplesAmount);
   }

   m_spDataSeries = shared_ptr<DataSeries>(new VectorBasedSeries(&a_inputData, spData, m_nMeanValueCalculationSamplesAmount - 1));
}