#pragma once
#include "Grammar.h"

namespace lrlib
{

class Utils
{
public:
   static Symbol& SymbolRef(t_setSymbol::value_type& value);

   static Symbol& SymbolRef(t_setSymbol::iterator& a_iterator);
   static Symbol* SymbolPtr(t_setSymbol::iterator& a_iterator);
};

} // namespace lrlib