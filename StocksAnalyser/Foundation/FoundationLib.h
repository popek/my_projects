#pragma once

#ifdef EXPORT_FOUNDATION_LIB
#  define FOUNDATION_LIB      __declspec(dllexport)
#else
#  define FOUNDATION_LIB      __declspec(dllimport)
#endif