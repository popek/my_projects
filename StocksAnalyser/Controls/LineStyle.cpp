#include "StdAfx.h"
#include "LineStyle.h"
#include "ColorUtils.h"


LineStyle::LineStyle(void)
{
   _style = PS_SOLID;
   _width = 1;
   _lineColor = ColorUtils::Color(ColorUtils::EBlack);
   _pointColor = ColorUtils::Color(ColorUtils::EBlue);
}
LineStyle::~LineStyle(void)
{
}

LONG_PTR LineStyle::GetNumericParam(paramId_t a_paramId) const
{
   if(EStyle == a_paramId)
   {
      return (LONG_PTR)_style;
   }
   else if(EWidth == a_paramId)
   {
      return (LONG_PTR)_width;
   }
   else if(EColor == a_paramId)
   {
      return (LONG_PTR)_lineColor;
   }
   else if(EPointColor == a_paramId)
   {
      return (LONG_PTR)_pointColor;
   }
   else
   {
      ASSERT(FALSE);
      return 0;
   }
}

void LineStyle::SetNumericParam(paramId_t a_paramId, LONG_PTR a_value)
{
   if(EStyle == a_paramId)
   {
      _style = (int)a_value;
   }
   else if(EWidth == a_paramId)
   {
      _width = (int) a_value;
   }
   else if(EColor == a_paramId)
   {
      _lineColor = (COLORREF)a_value;
   }
   else if(EPointColor == a_paramId)
   {
      _pointColor = (COLORREF)a_value;
   }
   else
   {
      ASSERT(FALSE);
   }
}