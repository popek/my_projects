#include "stdafx.h"
#include "StocksAnalyser.h"
#include "StockAnalyserToolbar.h"

IMPLEMENT_DYNAMIC(StockAnalyserToolbar, CDialog)

StockAnalyserToolbar::StockAnalyserToolbar(CWnd* pParent /*=NULL*/)
	: CDialog(StockAnalyserToolbar::IDD, pParent)
{

}

StockAnalyserToolbar::~StockAnalyserToolbar()
{
}

void StockAnalyserToolbar::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(StockAnalyserToolbar, CDialog)
END_MESSAGE_MAP()

