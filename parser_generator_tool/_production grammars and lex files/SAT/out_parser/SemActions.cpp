#include "SemanticHandler.h"
#include "CommonDef.h"

SemActions::SemActions(void)
{
}
SemActions::~SemActions(void)
{
}

void SemActions::DoReduceAction(int a_ruleId)
{
  typedef void (SemActions::*RuleFunction)();

  static RuleFunction Rules[RULES_COUNT]=
  {
    &SemActions::Rule_acceptance,
    &SemActions::Rule_SAT_FORMULA__to__EXPR_LIST,
    &SemActions::Rule_SAT_FORMULA__to__EXPR_LIST__MULTIPLE_SEMICOLON,
    &SemActions::Rule_EXPR_LIST__to__EXPR,
    &SemActions::Rule_EXPR_LIST__to__EXPR_LIST__MULTIPLE_SEMICOLON__EXPR,
    &SemActions::Rule_MULTIPLE_SEMICOLON__to__semicolon,
    &SemActions::Rule_MULTIPLE_SEMICOLON__to__MULTIPLE_SEMICOLON__semicolon,
    &SemActions::Rule_EXPR__to__EXPR_NAME__assign__CMP_EXPR,
    &SemActions::Rule_EXPR__to__CMP_EXPR,
    &SemActions::Rule_EXPR_NAME__to__EXPR_NAME_PART,
    &SemActions::Rule_EXPR_NAME__to__EXPR_NAME__EXPR_NAME_PART,
    &SemActions::Rule_CMP_EXPR__to__AEXPR_ADD,
    &SemActions::Rule_CMP_EXPR__to__CMP_EXPR__cmp_op__AEXPR_ADD,
    &SemActions::Rule_EXPR_NAME_PART__to__identifier,
    &SemActions::Rule_EXPR_NAME_PART__to__const_int,
    &SemActions::Rule_EXPR_NAME_PART__to__const_double,
    &SemActions::Rule_AEXPR_ADD__to__AEXPR_MUL,
    &SemActions::Rule_AEXPR_ADD__to__AEXPR_ADD__add_op__AEXPR_MUL,
    &SemActions::Rule_AEXPR_MUL__to__SIGNED_EXPR,
    &SemActions::Rule_AEXPR_MUL__to__AEXPR_MUL__mul_op__SIGNED_EXPR,
    &SemActions::Rule_SIGNED_EXPR__to__BASE_EXPR,
    &SemActions::Rule_SIGNED_EXPR__to__SIGNED__BASE_EXPR,
    &SemActions::Rule_BASE_EXPR__to__CONST_NUMERIC,
    &SemActions::Rule_BASE_EXPR__to__lpar__EXPR__rpar,
    &SemActions::Rule_BASE_EXPR__to__FUNCTION,
    &SemActions::Rule_SIGNED__to__add_op,
    &SemActions::Rule_SIGNED__to__SIGNED__add_op,
    &SemActions::Rule_CONST_NUMERIC__to__const_int,
    &SemActions::Rule_CONST_NUMERIC__to__const_double,
    &SemActions::Rule_CONST_NUMERIC__to__identifier,
    &SemActions::Rule_FUNCTION__to__FUNCTION_CALL,
    &SemActions::Rule_FUNCTION__to__SCRIPT_ACCESS__FUNCTION_CALL,
    &SemActions::Rule_FUNCTION_CALL__to__FUNCTION_NAME__lpar__ARG_LIST__rpar,
    &SemActions::Rule_FUNCTION_CALL__to__FUNCTION_NAME__lpar__rpar,
    &SemActions::Rule_SCRIPT_ACCESS__to__identifier__access_op,
    &SemActions::Rule_FUNCTION_NAME__to__identifier,
    &SemActions::Rule_ARG_LIST__to__ARG,
    &SemActions::Rule_ARG_LIST__to__NEXT_ARG_LIST__ARG,
    &SemActions::Rule_ARG__to__EXPR,
    &SemActions::Rule_ARG__to__text,
    &SemActions::Rule_NEXT_ARG_LIST__to__NEXT_ARG,
    &SemActions::Rule_NEXT_ARG_LIST__to__NEXT_ARG_LIST__NEXT_ARG,
    &SemActions::Rule_NEXT_ARG__to__ARG__comma,
  };

  (this->*(Rules[a_ruleId]))();
}

//------------------ rule actions --------------------
void SemActions::Rule_acceptance()
{
}

void SemActions::Rule_SAT_FORMULA__to__EXPR_LIST()
{
	m_semStack.Pop();
}

void SemActions::Rule_SAT_FORMULA__to__EXPR_LIST__MULTIPLE_SEMICOLON()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_EXPR_LIST__to__EXPR()
{
	m_semStack.Pop();
}

void SemActions::Rule_EXPR_LIST__to__EXPR_LIST__MULTIPLE_SEMICOLON__EXPR()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_MULTIPLE_SEMICOLON__to__semicolon()
{
	m_semStack.Pop();
}

void SemActions::Rule_MULTIPLE_SEMICOLON__to__MULTIPLE_SEMICOLON__semicolon()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_EXPR__to__EXPR_NAME__assign__CMP_EXPR()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_EXPR__to__CMP_EXPR()
{
	m_semStack.Pop();
}

void SemActions::Rule_EXPR_NAME__to__EXPR_NAME_PART()
{
	m_semStack.Pop();
}

void SemActions::Rule_EXPR_NAME__to__EXPR_NAME__EXPR_NAME_PART()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_CMP_EXPR__to__AEXPR_ADD()
{
	m_semStack.Pop();
}

void SemActions::Rule_CMP_EXPR__to__CMP_EXPR__cmp_op__AEXPR_ADD()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_EXPR_NAME_PART__to__identifier()
{
	m_semStack.Pop();
}

void SemActions::Rule_EXPR_NAME_PART__to__const_int()
{
	m_semStack.Pop();
}

void SemActions::Rule_EXPR_NAME_PART__to__const_double()
{
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_ADD__to__AEXPR_MUL()
{
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_ADD__to__AEXPR_ADD__add_op__AEXPR_MUL()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_MUL__to__SIGNED_EXPR()
{
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_MUL__to__AEXPR_MUL__mul_op__SIGNED_EXPR()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_SIGNED_EXPR__to__BASE_EXPR()
{
	m_semStack.Pop();
}

void SemActions::Rule_SIGNED_EXPR__to__SIGNED__BASE_EXPR()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_BASE_EXPR__to__CONST_NUMERIC()
{
	m_semStack.Pop();
}

void SemActions::Rule_BASE_EXPR__to__lpar__EXPR__rpar()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_BASE_EXPR__to__FUNCTION()
{
	m_semStack.Pop();
}

void SemActions::Rule_SIGNED__to__add_op()
{
	m_semStack.Pop();
}

void SemActions::Rule_SIGNED__to__SIGNED__add_op()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_CONST_NUMERIC__to__const_int()
{
	m_semStack.Pop();
}

void SemActions::Rule_CONST_NUMERIC__to__const_double()
{
	m_semStack.Pop();
}

void SemActions::Rule_CONST_NUMERIC__to__identifier()
{
	m_semStack.Pop();
}

void SemActions::Rule_FUNCTION__to__FUNCTION_CALL()
{
	m_semStack.Pop();
}

void SemActions::Rule_FUNCTION__to__SCRIPT_ACCESS__FUNCTION_CALL()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FUNCTION_CALL__to__FUNCTION_NAME__lpar__ARG_LIST__rpar()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FUNCTION_CALL__to__FUNCTION_NAME__lpar__rpar()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_SCRIPT_ACCESS__to__identifier__access_op()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FUNCTION_NAME__to__identifier()
{
	m_semStack.Pop();
}

void SemActions::Rule_ARG_LIST__to__ARG()
{
	m_semStack.Pop();
}

void SemActions::Rule_ARG_LIST__to__NEXT_ARG_LIST__ARG()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_ARG__to__EXPR()
{
	m_semStack.Pop();
}

void SemActions::Rule_ARG__to__text()
{
	m_semStack.Pop();
}

void SemActions::Rule_NEXT_ARG_LIST__to__NEXT_ARG()
{
	m_semStack.Pop();
}

void SemActions::Rule_NEXT_ARG_LIST__to__NEXT_ARG_LIST__NEXT_ARG()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_NEXT_ARG__to__ARG__comma()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

