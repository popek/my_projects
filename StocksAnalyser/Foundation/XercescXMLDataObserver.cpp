#include "StdAfx.h"
#include "XercescXMLDataObserver.h"

#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/util/XMLException.hpp>
#include <xercesc/dom/DOMImplementationRegistry.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
//#include <xercesc/dom/DOMElement.hpp>
//#include <xercesc/dom/DOMText.hpp>
//#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>


XercescXMLDataObserver::XercescXMLDataObserver(void)
   : _xercescDoc(nullptr)
{
//    DOMImplementation* impl =  DOMImplementationRegistry::getDOMImplementation(_T("Core"));
// 
//    _xercescDoc = impl->createDocument(0, _T(""), 0);
// 
//    DOMElement* rootElem = _xercescDoc->getDocumentElement();
// 
//    DOMElement*  prodElem = doc->createElement(X("product"));
//    rootElem->appendChild(prodElem);
// 
//    DOMText*    prodDataVal = doc->createTextNode(X("Xerces-C"));
//    prodElem->appendChild(prodDataVal);
// 
//    DOMElement*  catElem = doc->createElement(X("category"));
//    rootElem->appendChild(catElem);
// 
//    catElem->setAttribute(X("idea"), X("great"));
// 
//    DOMText*    catDataVal = doc->createTextNode(X("XML Parsing Tools"));
//    catElem->appendChild(catDataVal);
// 
//    DOMElement*  devByElem = doc->createElement(X("developedBy"));
//    rootElem->appendChild(devByElem);
// 
//    DOMText*    devByDataVal = doc->createTextNode(X("Apache Software Foundation"));
//    devByElem->appendChild(devByDataVal);
// 
//    //
//    // Now count the number of elements in the above DOM tree.
//    //
// 
//    const XMLSize_t elementCount = doc->getElementsByTagName(X("*"))->getLength();
//    XERCES_STD_QUALIFIER cout << "The tree just created contains: " << elementCount
//       << " elements." << XERCES_STD_QUALIFIER endl;
// 
//    doc->release();
}
XercescXMLDataObserver::~XercescXMLDataObserver(void)
{
   _xercescDoc->release();
}

void XercescXMLDataObserver::onEnterNewNode(LPCTSTR nodeName, LPCTSTR nodeText)
{
   xercesc::DOMElement* newNode = nullptr;

   if(!_xercescDoc)
   {
      xercesc::DOMImplementation* impl =  xercesc::DOMImplementationRegistry::getDOMImplementation(_T("Core"));
      _xercescDoc = impl->createDocument(0, nodeName, 0);
      newNode = _xercescDoc->getDocumentElement();
   }
   else
   {
      newNode = _xercescDoc->createElement(nodeName);
      _nodes.top()->appendChild(newNode);
   }

   _nodes.push(newNode);
}

void XercescXMLDataObserver::onLeaveNode()
{
   _nodes.pop();
}

void XercescXMLDataObserver::onAttribute(LPCTSTR attrName, LPCTSTR attrText)
{
   _nodes.top()->setAttribute(attrName, attrText);
}

const xercesc::DOMDocument* XercescXMLDataObserver::document() const
{
   return _xercescDoc;
}
