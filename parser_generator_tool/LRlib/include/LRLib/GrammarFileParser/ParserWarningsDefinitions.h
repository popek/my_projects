#pragma once

#define  PW_NONTERMINAL_ALREADY_DEFINED "nonterminal '%s' already defined in line %d. Consider extending nonterminal definition using '|' operator"