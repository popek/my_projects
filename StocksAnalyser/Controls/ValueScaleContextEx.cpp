#include "StdAfx.h"
#include "ValueScaleContextEx.h"


ValueScaleContextEx::ValueScaleContextEx(void)
: m_mostReasonableMinimum(0)
, m_mostReasonableMaximum(0)
{
}
ValueScaleContextEx::~ValueScaleContextEx(void)
{
}

void ValueScaleContextEx::Buffer()
{
   m_minValueBuffer = m_minValue;
   m_maxValueBuffer = m_maxValue;
   m_coordinate2valueBuffer = m_coordinate2value;
}

void ValueScaleContextEx::RecalculateMaxValue(Coordinate a_previousPos, Coordinate a_currentPos)
{
   // small "hack" to avoid dividing by 0
   if(a_currentPos == m_minValueCoord)
   {
      a_currentPos += 0.01;
   }

   // calculate value in previous pos
   double value = m_minValueBuffer + (a_previousPos - m_minValueCoord)/m_coordinate2valueBuffer;
   // calculate max value
   m_maxValue = (value - m_minValueBuffer)/(a_currentPos - m_minValueCoord) * (m_maxValueCoord - a_currentPos) + value;
   // call update
   Update(m_maxValue, m_minValue, m_maxValueCoord, m_minValueCoord);
}


void ValueScaleContextEx::RecalculateMinValue(Coordinate a_previousPos, Coordinate a_currentPos)
{
   // small "hack" to avoid dividing by 0
   if(a_currentPos == m_maxValueCoord)
   {
      a_currentPos += 0.01;
   }

   // calculate value in previous pos
   double value = m_minValueBuffer + (a_previousPos - m_minValueCoord)/m_coordinate2valueBuffer;
   // calculate min value
   m_minValue = value - (m_maxValueBuffer - value)/(m_maxValueCoord - a_currentPos) * (a_currentPos - m_minValueCoord);
   // call update
   Update(m_maxValue, m_minValue, m_maxValueCoord, m_minValueCoord);
}

void ValueScaleContextEx::UpdateFromMostReasanobleValues()
{
   Update(m_mostReasonableMaximum, m_mostReasonableMinimum, m_maxValueCoord, m_minValueCoord);
}

void ValueScaleContextEx::BufferMostReasonableMinMax(double a_minimum, double a_maximum)
{
   m_mostReasonableMinimum = a_minimum;
   m_mostReasonableMaximum = a_maximum;
}

double ValueScaleContextEx::Minimum() const
{
   return m_minValue;
}

double ValueScaleContextEx::Maximum() const
{
   return m_maxValue;
}
