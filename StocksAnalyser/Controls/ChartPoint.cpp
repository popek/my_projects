#include "StdAfx.h"
#include "ChartPoint.h"


ChartPoint::ChartPoint(shared_ptr<KandleDate> a_spTimestamp, Coordinate a_value)
: m_spTimestamp(a_spTimestamp)
, m_value(a_value)
{
}
ChartPoint::~ChartPoint(void)
{
}

shared_ptr<KandleDate> ChartPoint::Timestamp() const
{
   return m_spTimestamp;
}

double ChartPoint::Value() const
{
   return m_value;
}

