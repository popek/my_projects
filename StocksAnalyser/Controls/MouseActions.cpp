#include "StdAfx.h"
#include "MouseActions.h"
#include "IMouseInteraction.h"

MouseActions::MouseActions(void)
{
}
MouseActions::~MouseActions(void)
{
}

void MouseActions::AddAction(shared_ptr<IMouseInteraction> a_action)
{
   m_vActions.push_back(a_action);
}

void MouseActions::OnLButtonDown(UINT a_nFlags, const CPoint& a_point)
{
   for(size_t actionIdx=0; actionIdx<m_vActions.size(); ++actionIdx)
   {
      m_vActions[actionIdx]->OnLButtonDown(a_nFlags, a_point);
   }
}

void MouseActions::OnLButtonUp(UINT a_nFlags, const CPoint& a_point)
{
   for(size_t actionIdx=0; actionIdx<m_vActions.size(); ++actionIdx)
   {
      m_vActions[actionIdx]->OnLButtonUp(a_nFlags, a_point);
   }
}

void MouseActions::OnMouseMove(UINT a_nFlags, const CPoint& a_point)
{
   for(size_t actionIdx=0; actionIdx<m_vActions.size(); ++actionIdx)
   {
      m_vActions[actionIdx]->OnMouseMove(a_nFlags, a_point);
   }
}

void MouseActions::OnSize(int a_chartWidth)
{
   for(size_t actionIdx=0; actionIdx<m_vActions.size(); ++actionIdx)
   {
      m_vActions[actionIdx]->OnSize(a_chartWidth);
   }
}

void MouseActions::Draw(CDC& a_dc)
{
   for(size_t actionIdx=0; actionIdx<m_vActions.size(); ++actionIdx)
   {
      m_vActions[actionIdx]->Draw(a_dc);
   }
}

bool MouseActions::DrawingRequired() const
{
   bool bRequired = false;

   for(size_t actionIdx=0; actionIdx<m_vActions.size(); ++actionIdx)
   {
      bRequired |= m_vActions[actionIdx]->DrawingRequired();
   }

   return bRequired;
}