#pragma once
//
class MemoryDC
{
public:
   MemoryDC(CDC* a_pSourceDC, CWnd* a_pWindowObj);
   
   virtual ~MemoryDC(void);

   bool     Initialised();

   CDC&     BufferedDC();

protected:

   CDC*     m_pSourceDC;

   CDC      m_BufferedDC;

   CBitmap  m_Bitmap;

   CRect    m_rcBufferedArea;

   bool     m_bInitialised;
};
