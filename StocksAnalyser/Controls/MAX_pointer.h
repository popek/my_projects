#pragma once
#include "IPointer.h"


class CONTROLS_LIB MAX_pointer : public IPointer
{
public:
   MAX_pointer(size_t period);
   virtual ~MAX_pointer(void);

   virtual void   Create(const DataSeries& inputData);

   virtual const DataSeries& Series(size_t a_seriesId) const;

private:
   shared_ptr<DataSeries> _spDataSeries;
   size_t _period;
};

