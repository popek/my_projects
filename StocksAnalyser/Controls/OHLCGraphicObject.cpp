#include "StdAfx.h"
#include "OHLCGraphicObject.h"


const COLORREF G_clrDecreaseColor = RGB(220, 23, 1);
const COLORREF G_clrIncreaseColor = RGB(64, 124, 35);


OHLCGraphicObject::OHLCGraphicObject(void)
: m_decresePen(PS_SOLID, 1, G_clrDecreaseColor)
, m_increasePen(PS_SOLID, 1, G_clrIncreaseColor)
{
}
OHLCGraphicObject::~OHLCGraphicObject(void)
{
}

void OHLCGraphicObject::Draw(CDC* a_pDC)
{
   COLORREF crKandleColor = 0;

   size_t pointsCount = m_objectPoints.size();

   if(0 == pointsCount)
   {
      return;
   }

   Coordinate kandleWidht = (1 == pointsCount ? 100 : m_objectPoints[1]._x - m_objectPoints[0]._x);
   Coordinate leftPos = m_objectPoints[0]._x - kandleWidht/2;

   int kandleWidhtRounded = (int)kandleWidht;
   // deflate kandle body
   if(kandleWidhtRounded >= 2 && kandleWidhtRounded <= 4)
   {
      --kandleWidhtRounded;
   }
   else if(kandleWidht > 4)
   {
      kandleWidhtRounded -=2;
   }
   if(0 == kandleWidhtRounded%2)
   {
      --kandleWidhtRounded;
   }

   for(size_t pointIndex=0; pointIndex<pointsCount; ++pointIndex)
   {
      int leftPosRounded = (int)leftPos;

      CRect rcKandle(leftPosRounded, (int)m_openValueCoordinates[pointIndex], 
                     leftPosRounded + kandleWidhtRounded, (int)m_objectPoints[pointIndex]._y);
      rcKandle.NormalizeRect();

      // in case kandle body is invisible, make it visible
      if(0 == rcKandle.Height())
      {
         ++rcKandle.bottom;
      }

      // if open coordinate is below close coordinate then price has increased
      bool priceIncreased = m_openValueCoordinates[pointIndex] > m_objectPoints[pointIndex]._y;

      CPen* pOldPen = a_pDC->SelectObject(priceIncreased ? &m_increasePen : &m_decresePen);

      int shadowsX = (rcKandle.left + rcKandle.right)/2;
      // draw shadows line
      a_pDC->MoveTo(CPoint(shadowsX, (int)m_highValueCoordinates[pointIndex]));
      a_pDC->LineTo(CPoint(shadowsX, (int)m_lowValueCoordinates[pointIndex]));

      // draw kandle body
      a_pDC->FillSolidRect(&rcKandle, priceIncreased ? G_clrIncreaseColor : G_clrDecreaseColor);

      // select back old pen
      a_pDC->SelectObject(pOldPen);

      leftPos += kandleWidht;
   }
}

void OHLCGraphicObject::Reserve(size_t a_pointsCount)
{
   __super::Reserve(a_pointsCount);

   m_openValueCoordinates.reserve(a_pointsCount);
   m_openValueCoordinates.assign(a_pointsCount, 0);

   m_lowValueCoordinates.reserve(a_pointsCount);
   m_lowValueCoordinates.assign(a_pointsCount, 0);

   m_highValueCoordinates.reserve(a_pointsCount);
   m_highValueCoordinates.assign(a_pointsCount, 0);
}

void OHLCGraphicObject::UpdateOHLCoordinates(size_t a_pointIndex, Coordinate a_openCoord, Coordinate a_highCoord, Coordinate a_lowCoord)
{
   m_openValueCoordinates[a_pointIndex] = a_openCoord;
   m_highValueCoordinates[a_pointIndex] = a_highCoord;
   m_lowValueCoordinates[a_pointIndex]  = a_lowCoord;
}
