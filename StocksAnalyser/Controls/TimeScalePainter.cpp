#include "StdAfx.h"
#include "TimeScalePainter.h"
#include "TimeScaleContext.h"
#include "KandleDate.h"


TimeScalePainter::TimeScalePainter(void)
{
   m_distanceBeetwenDates = 100;
   m_minLineLength        = 5;
   m_minDiffBetweenLines  = 10;

   m_timeScaleColor       = RGB(0, 0, 0);
   m_Pen.CreatePen(PS_SOLID, 1, m_timeScaleColor);
   m_boldPen.CreatePen(PS_SOLID, 2, m_timeScaleColor);

   LOGFONT logFont;
   ZeroMemory(&logFont, sizeof(LOGFONT));
   GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &logFont);

   logFont.lfWeight = FW_NORMAL;
   logFont.lfHeight = (LONG)(0.8*(double)logFont.lfHeight);

   m_font.CreateFontIndirect(&logFont);
   
   CRect rcDate;
   CDC*  pScreenDC = CDC::FromHandle(GetDC(NULL));
   pScreenDC->DrawText(_T("2222.22.22"), rcDate, DT_CALCRECT);
   m_nDateRectWidth = rcDate.Width();
}
TimeScalePainter::~TimeScalePainter(void)
{
}

int TimeScalePainter::Height()
{
   return m_nHeight;
}

void TimeScalePainter::Height(int a_nNewTimeScaleHeight)
{
   m_nHeight = a_nNewTimeScaleHeight;

   if( m_nHeight > 6*m_minLineLength)
   {
      m_nLineLength = m_nHeight/6;
   }
   else
   {
      m_nLineLength = m_minLineLength;
   }
}

void TimeScalePainter::VertOffset(int a_nNewLeftBottomCornerYOffset)
{
   m_nLeftTopCornerYOffset = a_nNewLeftBottomCornerYOffset - m_nHeight;
}

void TimeScalePainter::Draw(CDC& a_rDC, const TimeScaleContext& a_timescaleContext)
{
   double      dDiff            = (double)m_minDiffBetweenLines/3;
   int         lastDateLineXpos = 0;
   vector<int> vLinesXPos;

   int nLineDensity = CalcLineDensity(a_timescaleContext);
   int nDateDensity = CalcDateDensity(a_timescaleContext);

   int oldBkMode = a_rDC.SetBkMode(TRANSPARENT);

   for(size_t dateIndex=a_timescaleContext.FirstVisibleTimestampIndex(); dateIndex<a_timescaleContext.LastVisibleTimestampIndex(); ++dateIndex)
   {
      if(0 == dateIndex%nDateDensity)
      {
         lastDateLineXpos = (int)a_timescaleContext.GetCoordinate(dateIndex);

         DrawDate(a_rDC, a_timescaleContext.GetTimestamp(dateIndex), a_timescaleContext.GetCoordinate(dateIndex));
         DrawLine(a_rDC, (int)a_timescaleContext.GetCoordinate(dateIndex), true);

         int lastLineXpos = vLinesXPos.size() ? vLinesXPos.back() : 0;

         if((double)(lastDateLineXpos - lastLineXpos) < dDiff)
         {
            if(vLinesXPos.size())
            {
               vLinesXPos.erase(vLinesXPos.end()-1);
            }
         }
      }
      else if(0 == dateIndex%nLineDensity)
      {
         int lineXPos = (int)a_timescaleContext.GetCoordinate(dateIndex);

         if((double)(lineXPos - lastDateLineXpos) > dDiff)
         {
            vLinesXPos.push_back( lineXPos );
         }
      }
   }

   for(size_t lineIdx=0; lineIdx<vLinesXPos.size(); ++lineIdx)
   {
      DrawLine(a_rDC, vLinesXPos[lineIdx], false);
   }

   a_rDC.SetBkMode(oldBkMode);
}

int TimeScalePainter::CalcLineDensity(const TimeScaleContext& a_timescaleContext)
{
   int nDensity = 1; // number shows which items will have lines drawn (e.g.: if nDensity is 4, items 0, 4, 8, 12 and so on will have tmestamps drawn)
   size_t visibleSampleCount = a_timescaleContext.LastVisibleTimestampIndex() - a_timescaleContext.FirstVisibleTimestampIndex() + 1;

   if(visibleSampleCount>1)
   {
      if((int)a_timescaleContext.NeighbourDistance() < m_minDiffBetweenLines)
      {
         nDensity = (int)((double)m_minDiffBetweenLines/a_timescaleContext.NeighbourDistance() + 1); 
      }
   }

   return nDensity;
}

int TimeScalePainter::CalcDateDensity(const TimeScaleContext& a_timescaleContext)
{
   int  nDateDensity = 1; // number shows which items will have timestamp drawn (e.g.: if nDensity is 4, items 0, 4, 8, 12 and so on will have tmestamps drawn)
   size_t visibleSampleCount = a_timescaleContext.LastVisibleTimestampIndex() - a_timescaleContext.FirstVisibleTimestampIndex() + 1;

   if(visibleSampleCount>1)
   {
      if((int)a_timescaleContext.NeighbourDistance() < m_distanceBeetwenDates)
      {
         nDateDensity = (int)((double)m_distanceBeetwenDates/a_timescaleContext.NeighbourDistance() + 1); 
      }
   }

   return nDateDensity;   
}

void TimeScalePainter::DrawDate(CDC& a_rDC, shared_ptr<KandleDate> a_spTimestamp, Coordinate a_timestampCoordinate)
{
   CRect rcDate(0, m_nLineLength*2, m_nDateRectWidth, m_nHeight);
   
   int nLeftDateCorner = (int)a_timestampCoordinate - m_nDateRectWidth/2;

   rcDate.OffsetRect( nLeftDateCorner, m_nLeftTopCornerYOffset);

   CFont* pOldFont = a_rDC.SelectObject(&m_font);
   
   //a_rDC.FillSolidRect(rcDate, RGB(40, 100, 90));
   a_rDC.DrawText(a_spTimestamp->ToString(), &rcDate, DT_SINGLELINE|DT_VCENTER|DT_CENTER|DT_END_ELLIPSIS);

   a_rDC.SelectObject(pOldFont);
}

void TimeScalePainter::DrawLine(CDC& a_rDC, int a_nXpos, bool a_bold)
{
   CPen* pOldPen = a_rDC.SelectObject(a_bold ? &m_boldPen : &m_Pen);

   a_rDC.MoveTo(a_nXpos, m_nLeftTopCornerYOffset);
   a_rDC.LineTo(a_nXpos, m_nLeftTopCornerYOffset + (a_bold ? m_nLineLength*2 : m_nLineLength));

   a_rDC.SelectObject(pOldPen);
}