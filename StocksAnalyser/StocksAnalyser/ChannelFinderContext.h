#pragma once

class ChannelFinderContext
{
public:
   ChannelFinderContext(void);
   virtual ~ChannelFinderContext(void);

   void Update(int a_period,
               UINT a_algorithmType,
               int a_minimumLinePointsAmount,
               double a_maxDeviation2LineValue,
               double a_maxDataSource2LineValueDistance,
               bool a_showConfirmedSignal);

   int Period() const;

   UINT AlgType() const;

   int MinLinePointsCount() const;

   double MaxDev2LineValue() const;

   double MaxData2LineValueDistance() const;

   bool ShowConfirmedSignal() const;

   bool IsValid() const;

protected:

   int   m_period;                           // period in which we are looking for a channel (lines)
   UINT  m_algorithmType;                    // algorithm type (channel or lines)

   int   m_minimumLinePointsAmount;          // minimum amount of point on the line

   double m_maxDeviation2LineValue;          // maximum value for (dataSourceValue - lineValue)/lineValue

   double m_maxDataSource2LineValueDistance; // for support line data source values are above (1 - value) * LineValue

   bool   m_showConfirmedSignal; // indicates if confirmed signals will be shown (algoritm can find confirmed or potential signals)
};
