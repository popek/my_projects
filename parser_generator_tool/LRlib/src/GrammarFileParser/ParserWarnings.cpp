#include "GrammarFileParser/ParserWarnings.h"

namespace lrlib
{

ParserWarnings::CWarning::CWarning(std::string a_strWarningDescription, int a_nLineNo)
{
   m_strWarningDescription = a_strWarningDescription;
   m_nLineNo = a_nLineNo;
}

ParserWarnings::ParserWarnings(void)
{
}

ParserWarnings::~ParserWarnings(void)
{
}

void ParserWarnings::AddFormatedWarning0(std::string a_strWarningDescription, int a_nLineNo, std::string a_strMiddleString, int a_nMiddleInt)
{
   char sWarningDescription[1024];
   sprintf_s(sWarningDescription, 1024, a_strWarningDescription.c_str(), a_strMiddleString.c_str(), a_nMiddleInt);
   m_deqWarnings.push_back(CWarning(sWarningDescription, a_nLineNo));
}

void ParserWarnings::Clear()
{
   m_deqWarnings.clear();
}

} // namespace lrlib