#pragma once
#include "FoundationLib.h"
#include "IXMLObjectsFactory.h"


class FOUNDATION_LIB XercescXMLObjectsFactory : public IXMLObjectsFactory
{
public:
   XercescXMLObjectsFactory(void);
   virtual ~XercescXMLObjectsFactory(void);

   virtual shared_ptr<IXMLSerialiser> createSerialiser() const;
};