#ifndef __LexClasses_Header__
#define __LexClasses_Header__
#include "LexAtom.h"

namespace lrlib
{

class Scanner
{
public:
   Scanner(void);
   //Scanner(const char* pszFilePath);
   virtual ~Scanner(void);
   void          GetAtom(LexAtom* pLexAtom);
   bool          Init(const char* pszFilePath);

   unsigned int  LineNo();
   void          ShowError();

protected:
   eTermID		  getAtom(LexAtom* pLexAtom);
   void          recognizeSingleChar(LexAtom* a_pSingleCharToken);
   void          handleCommentsState(LexAtom* a_pLexToken);

   unsigned char readNextChar();
   void			  writeBackChar();

   bool          inCommentBlock();

   void          Cleanup();

   char* _pszErrDesc;
   char* _InBuffer[2];

   unsigned int _uiCurrPos;
   unsigned int _uiBytesRed[2];
   unsigned int _uiCurrBuff;
   bool _bEOF;
   bool _bInSingleLineComments;
   bool _bInMultiLineComments;
   FILE* _InputFile;

   bool _bAuxReadingFileAllowed;

   unsigned int _uiAuxLineNo;
   char _szAuxAtomName[1024];
};

} //namespace lrlib

#endif //__LexClasses_Header__