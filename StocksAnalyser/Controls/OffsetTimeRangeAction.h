#pragma once
#include "IMouseInteraction.h"

class ChartControl;

class OffsetTimeRangeAction : public IMouseInteraction
{
public:
   OffsetTimeRangeAction(ChartControl* a_pOwner);
   virtual ~OffsetTimeRangeAction(void);

   virtual void OnLButtonDown(UINT a_nFlags, const CPoint& a_point);

   virtual void OnLButtonUp(UINT a_nFlags, const CPoint& a_point);

   virtual void OnMouseMove(UINT a_nFlags, const CPoint& a_point);

   virtual void OnSize(int a_chartWidth);

   virtual void Draw(CDC& a_dc);

   virtual bool DrawingRequired() const;

protected:

   void         Reset();

   void         UpdateRange(const int& a_mouseXpos);
   
protected:

   ChartControl* m_pOwner;
   CWnd*         m_pOldCapture;

   int           m_chartWidth;

   int           m_startPos;

   bool          m_bActionStarted;
};
