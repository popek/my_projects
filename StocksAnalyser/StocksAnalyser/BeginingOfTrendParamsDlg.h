#pragma once
#include "PropertyDialogBase.h"
#include "GMMA_BeginingOfTrend.h"
#include "resource.h"


class BeginingOfTrendParamsDlg : public PropertyDialogBase
{
	DECLARE_DYNAMIC(BeginingOfTrendParamsDlg)

public:
	BeginingOfTrendParamsDlg();
   virtual ~BeginingOfTrendParamsDlg();

   enum { IDD = IDD_BEGINING_OF_TREND };

   BOOL    Create(CWnd* a_parent);

   const shared_ptr<GMMA_BeginingOfTrend::Context> Context(bool a_update);

protected:

   GMMA_BeginingOfTrend::Context::EAlgorithType CurrentAlgorithType() const;

	virtual void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP()
   afx_msg void OnBnClickedCheckOnlyBuyPeriod();
   afx_msg void OnBnClickedCheckAllSimpleWay();
   afx_msg void OnBnClickedCheckAllWithRegardsToAllPeriods();

protected:

   shared_ptr<GMMA_BeginingOfTrend::Context> m_spContext;

   CString m_shortAboveLongMinimalPeriod;
   CString m_shortAboveLongPeriodRange;
   CString m_breakingThorughPeriod;
   CString m_shortBelowLongPeriod;
   BOOL    m_weeklyConfirmation;
   BOOL    m_algorithTypeIndex;
   BOOL    m_checkWeekSignal;
};
