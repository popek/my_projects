#include "StdAfx.h"
#include "MinMaxGetter.h"
#include "Asset.h"


MinMaxGetter::MinMaxGetter(MinMaxGetter::EContext a_context)
: m_context(a_context)
{
}
MinMaxGetter::~MinMaxGetter(void)
{
}

void MinMaxGetter::GetMinMax(const Asset& a_asset, const size_t& a_firstSampleIndex, const size_t& a_lastSampleIndex,
                             double& a_min, double& a_max)
{
   switch(m_context)
   {
   case EGetPriceMinMax:
      a_asset.GetMinMax(a_firstSampleIndex, a_lastSampleIndex, a_min, a_max);
      break;

   case EGetVolumenMinMax:
      a_asset.GetVolumeMinMax(a_firstSampleIndex, a_lastSampleIndex, a_min, a_max);
      a_min = 0;
      break;
   }
}

void MinMaxGetter::GetMinMax(const Asset& a_asset, double& a_min, double& a_max)
{
   switch(m_context)
   {
   case EGetPriceMinMax:
      a_asset.GetMinMax(a_min, a_max);
      break;

   case EGetVolumenMinMax:
      a_asset.GetVolumeMinMax(a_min, a_max);
      a_min = 0;
      break;
   }
}

