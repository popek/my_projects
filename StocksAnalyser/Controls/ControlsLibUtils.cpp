#include "StdAfx.h"
#include "ControlsLibUtils.h"
#include "DataSeriesChartElement.h"
#include "Asset.h"
#include "DataSeries.h"
#include "MathUtils.h"


HCURSOR ControlsLibUtils::Sm_cursorWestEast = NULL;
HCURSOR ControlsLibUtils::Sm_cursorNorthSouth = NULL;
HCURSOR ControlsLibUtils::Sm_cursorArrow = NULL;


shared_ptr<ChartElement> ControlsLibUtils::CreateVolumeSeriesElement(const Asset& a_asset)
{
   shared_ptr<DataSeriesChartElement> spVolumeElement(new DataSeriesChartElement(EVolume, a_asset.Size(), 0));
   for(size_t kandleIndex=0; kandleIndex<a_asset.Size(); ++kandleIndex)
   {
      spVolumeElement->AddPoint( a_asset[kandleIndex].Date(), a_asset[kandleIndex].Volume());
   }
   return spVolumeElement;
}

shared_ptr<ChartElement> ControlsLibUtils::CreatePointerSeriesElement(const DataSeries& a_dataSeries, const Asset& a_pointerSource)
{
   size_t offset = a_dataSeries.offset(false);

   shared_ptr<DataSeriesChartElement> spPointerElement(new DataSeriesChartElement(ELine, a_dataSeries.size(), offset));
   for(size_t valueIndex=0; valueIndex<a_dataSeries.size(); ++valueIndex)
   {
      spPointerElement->AddPoint( a_pointerSource[valueIndex + offset].Date(), a_dataSeries[valueIndex] );
   }
   return spPointerElement;
}

HCURSOR ControlsLibUtils::Cursor(LPTSTR a_cursorId)
{
   if(IDC_SIZEWE == a_cursorId)
   {
      if(NULL == Sm_cursorWestEast)
      {
         Sm_cursorWestEast = LoadCursor(NULL, MAKEINTRESOURCE(IDC_SIZEWE));
      }
      return Sm_cursorWestEast;
   }

   if(IDC_SIZENS == a_cursorId)
   {
      if(NULL == Sm_cursorNorthSouth)
      {
         Sm_cursorNorthSouth = LoadCursor(NULL, MAKEINTRESOURCE(IDC_SIZENS));
      }
      return Sm_cursorNorthSouth;
   }

   if(NULL == Sm_cursorArrow)
   {
      Sm_cursorArrow = LoadCursor(NULL, MAKEINTRESOURCE(IDC_ARROW));
   }
   return Sm_cursorArrow;
}

CString ControlsLibUtils::FitStringToWidth(double a_value, CDC& a_dc, int a_availableWidth)
{
   CRect rcText;
   CString strValue;

   if(TextFit(a_value, strValue, _T("%2.2f"), a_dc, a_availableWidth))
   {
      return strValue;
   }

   a_value /= 1000;
   if(TextFit(a_value, strValue, _T("%2.2fk"), a_dc, a_availableWidth))
   {
      return strValue;
   }
   if(TextFit(a_value, strValue, _T("%2.1fk"), a_dc, a_availableWidth))
   {
      return strValue;
   }
   if(TextFit(a_value, strValue, _T("%0.0fk"), a_dc, a_availableWidth))
   {
      return strValue;
   }

   a_value /= 1000;
   if(TextFit(a_value, strValue, _T("%2.2fm"), a_dc, a_availableWidth))
   {
      return strValue;
   }
   if(TextFit(a_value, strValue, _T("%2.1fm"), a_dc, a_availableWidth))
   {
      return strValue;
   }
   
   TextFit(a_value, strValue, _T("%0.0fm"), a_dc, a_availableWidth);

   return strValue;
}

bool ControlsLibUtils::TextFit(double a_value, CString& a_out, CString a_format, CDC& a_dc, int a_availableWidth)
{
   CRect rcText;
   a_out.Format(a_format, a_value);
   a_dc.DrawText(a_out, rcText, DT_CALCRECT);
   return rcText.Width() <= a_availableWidth;
}

shared_ptr<DataSeries> ControlsLibUtils::CreateEmptySeries(size_t a_offset)
{
   return shared_ptr<DataSeries>(new VectorBasedSeries(nullptr, 
                                                       shared_ptr<vector<double>>(new vector<double>()), 
                                                       a_offset));
}

shared_ptr<DataSeries> ControlsLibUtils::CreateATRSeries(const Asset& a_asset)
{
   auto spATR = shared_ptr<vector<double>>(new vector<double>());
   spATR->reserve(a_asset.Size());
   
   spATR->push_back( MathUtils::Abs(a_asset[0].High() - a_asset[0].Low()) );

   for(size_t kandleIndex=1; kandleIndex<a_asset.Size(); ++kandleIndex)
   {
      spATR->push_back( MathUtils::Max( 
                           MathUtils::Max( MathUtils::Abs(a_asset[kandleIndex].High() - a_asset[kandleIndex].Low()),
                                           MathUtils::Abs(a_asset[kandleIndex-1].Close() - a_asset[kandleIndex].Low()) ),
                           MathUtils::Abs(a_asset[kandleIndex-1].Close() - a_asset[kandleIndex].High()) )
                      );
   }

   return make_shared<VectorBasedSeries>(nullptr, spATR, 0);
}