#include "stdafx.h"
#include "ChartView.h"
#include "ChartControlSplitted.h"
#include "WindowMessages.h"
#include "AssetContainer.h"
#include "ChartDocument.h"
#include "WindowUtils.h"


IMPLEMENT_DYNCREATE(ChartView, CView)

ChartView::ChartView()
{
}
ChartView::~ChartView()
{
}
void ChartView::OnDraw(CDC* pDC)
{
}

LRESULT ChartView::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
   if(WM_SHOW_NEW_CHART == message)
   {
      ((ChartDocument*)m_pDocument)->CreateAnotherViewForDocument((EAggregationType)wParam);
      return 0;
   }

   return CView::WindowProc(message, wParam, lParam);
}

void ChartView::SetAsset(shared_ptr<Asset> a_spAsset)
{
   m_spChartWnd->SetAsset(a_spAsset);
}

BEGIN_MESSAGE_MAP(ChartView, CView)
   ON_WM_CREATE()
   ON_WM_SIZE()
   ON_WM_SETFOCUS()
END_MESSAGE_MAP()

int ChartView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
   if (CView::OnCreate(lpCreateStruct) == -1)
   {
      return -1;
   }

   ModifyStyle(0, WS_CLIPCHILDREN, 0);

   CRect rcClient;
   GetClientRect(&rcClient);

   m_spChartWnd = shared_ptr<ChartControlMessagesProxy<ChartControlSplitted>>( new ChartControlMessagesProxy<ChartControlSplitted>() );

   if( FALSE == m_spChartWnd->Create(NULL, NULL, WS_CHILD|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE, rcClient, this, 0))
   {
      return -1;
   }

   return 0;
}

void ChartView::OnSize(UINT nType, int cx, int cy)
{
   CView::OnSize(nType, cx, cy);

   CRect rcClient;
   GetClientRect(&rcClient);

   WindowUtils::SetWindowPos(m_spChartWnd.get(), NULL, rcClient, SWP_NOZORDER|SWP_SHOWWINDOW);
}

void ChartView::OnSetFocus(CWnd* pOldWnd)
{
   CView::OnSetFocus(pOldWnd);

   // set focus to chart window so it can recieve wm_mousewheel
   if( m_spChartWnd->GetSafeHwnd() )
   {
      m_spChartWnd->SetFocus();
   }
}

#ifdef _DEBUG
void ChartView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void ChartView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif