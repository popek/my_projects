#include "GrammarFileParser/SemStack.h"

namespace lrlib
{

//----------------------------------
//------------ SemStack -----------
//----------------------------------
SemStack::SemStack(void)
{
}

SemStack::~SemStack(void)
{
}

void SemStack::Push(SemAtom& semAtom)
{
   m_Stack.push_back(semAtom);
}

void SemStack::Pop()
{
   if (!m_Stack.empty())
      m_Stack.erase(m_Stack.end() - 1);
}

void SemStack::Top(SemAtom* pSemAtom)
{
   if (!m_Stack.empty())
      *pSemAtom = m_Stack[m_Stack.size() - 1];
}

} // namespace lrlib