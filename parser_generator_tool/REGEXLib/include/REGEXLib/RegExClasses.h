#pragma once;
#include "CommonDefs.h"

namespace regexlib
{

enum eRegExType
{
   eRE_Simple,
   eRE_OR,
   eRE_Compound,
   eRE_MUL
};

struct SNFADiagram
{
   t_NFAdiagram _SymbolTranDiag;
   t_NFAdiagram _EmptyTranDiag;
   std::vector<unsigned int> _uiNFAacceptStates;
   void clear();
};

class RegEx
{
public:
   RegEx(eRegExType t) :m_type(t) {}
   eRegExType Type() { return m_type; }
   virtual unsigned int GenerateNFA(unsigned int uiNextStartStateNo) = 0;
   virtual void SetNFA(SNFADiagram* pnfa);
protected:
   eRegExType m_type;
   SNFADiagram *m_pNFA;
};

class SimpleRegEx : public RegEx
{
public:
   SimpleRegEx(std::string s, short SymbolID) :RegEx(eRE_Simple), m_sSymbolName(s), m_iSymbolID(SymbolID) {}
   SimpleRegEx(unsigned char c, short SymbolID);
   std::string& name() { return m_sSymbolName; }
   unsigned int GenerateNFA(unsigned int uiNextStartStateNo);
   void SetNFA(SNFADiagram* pnfa);
private:
   std::string m_sSymbolName;
   short m_iSymbolID;
};

class ORRegEx : public RegEx
{
public:
   ORRegEx(bool bMainRegEx = false) :RegEx(eRE_OR), m_bMainRegEx(bMainRegEx) {};
   RegEx* operator [](unsigned int uiIdx) { return m_vRE[uiIdx]; }
   unsigned int size() { return m_vRE.size(); }
   void		AddOR(RegEx* pre) { m_vRE.push_back(pre); }
   RegEx* GetLastOR() { return (m_vRE.size() ? m_vRE[m_vRE.size() - 1] : NULL); }
   void		SetLastOR(RegEx* pre) { if (m_vRE.size()) m_vRE[m_vRE.size() - 1] = pre; }

   unsigned int GenerateNFA(unsigned int uiNextStartStateNo);
   void SetNFA(SNFADiagram* pnfa);
private:
   std::vector<RegEx*> m_vRE;
   bool m_bMainRegEx;
};

class CompoundRegEx : public RegEx
{
public:
   CompoundRegEx() :RegEx(eRE_Compound) {};
   std::vector<RegEx*>& GetRegExContainer() { return m_vRE; }
   RegEx* operator [](unsigned int uiIdx) { return m_vRE[uiIdx]; }
   unsigned int size() { return m_vRE.size(); }

   unsigned int GenerateNFA(unsigned int uiNextStartStateNo);
   void SetNFA(SNFADiagram* pnfa);
private:
   std::vector<RegEx*> m_vRE;
};

class MULRegEx : public RegEx
{
public:
   MULRegEx() :RegEx(eRE_MUL) {};
   void SetBaseRegEx(RegEx* pre) { m_BaseRegEx = pre; }

   RegEx* GetBaseRegEx() { return m_BaseRegEx; }

   unsigned int GenerateNFA(unsigned int uiNextStartStateNo);
   void SetNFA(SNFADiagram* pnfa);
private:
   RegEx* m_BaseRegEx;
};

} // namespace regexlib