#include "StdAfx.h"
#include "Misc.h"
#include "Asset.h"
#include "EMA_pointer.h"
#include "SMA_pointer.h"
#include "AssetAuxiliaryWrappers.h"
#include "ChartElementsCollection.h"
#include "ControlsLibUtils.h"
#include "LineStyle.h"
#include "ColorUtils.h"
#include "ChartElement.h"
#include <Shlwapi.h>
#include <atlpath.h>


static const int g_gmmaAvaragesPeriods[] = {3, 5, 8, 10, 12, 15, 30, 35, 40, 45, 50, 60};
static const int g_gmmaAvaragesCount = ARRAYSIZE(g_gmmaAvaragesPeriods);


CString Misc::GetBinFolder()
{
   CString strStockAnalyserBinDir;
   TCHAR* pszModuleFileName = new TCHAR[_MAX_PATH];

   if(GetModuleFileName(NULL, pszModuleFileName, _MAX_PATH))
   {
      CPath path(pszModuleFileName);
      path.RemoveFileSpec();
      path.AddBackslash();

      strStockAnalyserBinDir = (CString)path;
   }

   delete [] pszModuleFileName;

   return strStockAnalyserBinDir;
}

CString Misc::ClearFolderContent(CString a_strFolderPath)
{
   BOOL result = CreateDirectory( a_strFolderPath, NULL);
   
   if(0 == result)
   {
      if(ERROR_ALREADY_EXISTS == GetLastError())
      {
         CString strFrom = a_strFolderPath;
         strFrom.Append(_T("\\*.*"));
         strFrom.AppendChar(_T('\0'));

         SHFILEOPSTRUCT op;
         memset(&op,0,sizeof(op));
         op.wFunc = FO_DELETE;
         op.pFrom = strFrom;
         op.fFlags = FOF_NO_UI|FOF_FILESONLY;

         if(SHFileOperation(&op))
         {
            return _T("");
         }
      }
   }

   return a_strFolderPath;
}

void Misc::RemoveFile(CString a_strFilePath)
{
   a_strFilePath.AppendChar(_T('\0'));

   SHFILEOPSTRUCT op;
   memset(&op,0,sizeof(op));
   op.wFunc = FO_DELETE;
   op.pFrom = a_strFilePath;
   op.fFlags = FOF_NO_UI|FOF_FILESONLY;

   VERIFY(0 == SHFileOperation(&op));
}

std::string Misc::ToAnsi(CString a_string)
{
   USES_CONVERSION;
   return T2A(a_string.GetBuffer());
}

CString Misc::ToTSTR(std::string a_string)
{
   USES_CONVERSION;
   return A2T(a_string.c_str());
}

void Misc::PreparePointers(const Asset& a_asset, vector<shared_ptr<IPointer>>& a_vPointers, size_t& a_shortPointerCnt)
{
   for(size_t pointerIdx=0; pointerIdx<g_gmmaAvaragesCount; ++pointerIdx)
   {
      a_vPointers.push_back(shared_ptr<IPointer>(new EMA_pointer(g_gmmaAvaragesPeriods[pointerIdx])));
      a_vPointers.back()->Create( AssetCloseValueGetter(a_asset) );
   }

   a_shortPointerCnt = 6;
}

void Misc::TryToAttachStantardCharElementsSet(Asset& a_assetToWhichAttach)
{
   if(NULL == a_assetToWhichAttach.ElementsCollection().get())
   {
      a_assetToWhichAttach.AttachElementsCollection(shared_ptr<ChartElementsCollection>(new ChartElementsCollection()));
   }

   // if there are some elements attached to asset then return without doing anything
   if(a_assetToWhichAttach.ElementsCollection()->ElementsCount() > 0)
   {
      return;
   }

   COLORREF firstEmaColor = ColorUtils::MixColors(ColorUtils::EBlack, ColorUtils::EWhite, 0.5);
   
   for(size_t pointerIdx=0; pointerIdx<g_gmmaAvaragesCount; ++pointerIdx)
   {
      EMA_pointer ema(g_gmmaAvaragesPeriods[pointerIdx]);
      
      // create ema pointer
      ema.Create(AssetCloseValueGetter(a_assetToWhichAttach));

      shared_ptr<ChartElement> spEmaGrx = ControlsLibUtils::CreatePointerSeriesElement(ema.Series(0), a_assetToWhichAttach);
      spEmaGrx->Style()->SetNumericParam( LineStyle::EColor, ColorUtils::MixColors(firstEmaColor, 
                                                                                   ColorUtils::Color(ColorUtils::EWhite), 
                                                                                   1 - pointerIdx*0.07) );
      // attach pointer to asset from which ema had been created
      a_assetToWhichAttach.ElementsCollection()->PushFront(spEmaGrx);
   }

   // add also 200 sma
   SMA_pointer sma(200);
   sma.Create(AssetCloseValueGetter(a_assetToWhichAttach));
   a_assetToWhichAttach.ElementsCollection()->PushFront( ControlsLibUtils::CreatePointerSeriesElement(sma.Series(0), a_assetToWhichAttach) );
}

int Misc::ToInt(CString a_strValue)
{
   return _tstoi(a_strValue);
}

double Misc::ToDouble(CString a_strValue)
{
   TCHAR* dummy;
   return _tcstod(a_strValue, &dummy);
}

CString Misc::ToString(double a_value)
{
   CString strValue;
   strValue.Format(_T("%2.5f"), a_value);

   CutUnnecessaryDecimalPlaces(strValue);

   return strValue;
}

CString Misc::ToString(int a_value)
{
   CString strValue;
   strValue.Format(_T("%d"), a_value);

   return strValue;
}

void Misc::CutUnnecessaryDecimalPlaces(CString& a_strDoubleValue)
{
   int charIndex = a_strDoubleValue.GetLength() - 1;
   int dotIndex = a_strDoubleValue.Find(_T('.'));


   while(charIndex > dotIndex)
   {
      if(a_strDoubleValue[charIndex] == _T('0'))  
      {
         a_strDoubleValue.SetAt(charIndex, _T('\0'));
      }
      else
      {
         break;
      }

      --charIndex;
   }

   if(charIndex == dotIndex)
   {
      a_strDoubleValue.SetAt(dotIndex, _T('\0'));
   }
}

shared_ptr<KandleDate> Misc::FromOleDate(const COleDateTime& a_date)
{
   return shared_ptr<KandleDate>(new KandleDate(a_date.GetYear(), a_date.GetMonth(), a_date.GetDay()));
}

shared_ptr<KandleDate> Misc::ExtractKandleDate(CDateTimeCtrl& a_dateControl)
{
   COleDateTime date;
   a_dateControl.GetTime(date);

   return FromOleDate(date); 
}

CString Misc::ChooseAssetsFilesDirectory()
{
   CFileDialog fileDialog(TRUE, _T("mst"), _T("*.mst"), OFN_FILEMUSTEXIST|OFN_HIDEREADONLY, _T("Pliki (*.mst)|*.mst|(*.*)|*.*||"), nullptr);

   if(IDOK == fileDialog.DoModal())
   {
      CPath path(fileDialog.GetPathName());
      path.RemoveFileSpec();

      path.AddBackslash();

      return path;
   }

   return _T("");
}
