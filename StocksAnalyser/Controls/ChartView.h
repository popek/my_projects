#pragma once
#include "ControlsLib.h"
#include "ChartControlMessagesProxy.h"

class ChartControlSplitted;
class Asset;

class CONTROLS_LIB ChartView : public CView
{
	DECLARE_DYNCREATE(ChartView)

   void SetAsset(shared_ptr<Asset> a_spAsset);

protected:
	ChartView();           // protected constructor used by dynamic creation
	virtual ~ChartView();
   virtual void OnDraw(CDC* pDC);

   virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

   DECLARE_MESSAGE_MAP()
   afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
   afx_msg void OnSize(UINT nType, int cx, int cy);
   afx_msg void OnSetFocus(CWnd* pOldWnd);

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


protected:

   shared_ptr<ChartControlMessagesProxy<ChartControlSplitted>> m_spChartWnd;
};


