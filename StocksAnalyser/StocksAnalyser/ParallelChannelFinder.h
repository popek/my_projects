#pragma once
#include "AssetFinderBase.h"
#include "LineCoefficientCalculator.h"

class IDoubleVector;
class Asset;
class ChartElementsCollection;
class ChannelFinderContext;
class Kandle;
class DataSeries;


class ParallelChannelFinder : public AssetFinderBase
{
public:

   enum
   {
      EChannel,
      ESupportLine,
      EResistanceLine,

      EInvalidType
   };

private:
   // ------------
   // 
   class LineFinder
   {
   public:
      LineFinder(const ChannelFinderContext& a_context,
                 shared_ptr<Asset>  a_spAsset,
                 const DataSeries& a_sourceData,
                 size_t             a_sourceDataIndexOfsset,
                 size_t             a_channelPeriod);

      void FillGraphicElements(shared_ptr<ChartElementsCollection> a_spElementsCollection);

      bool Find();

      size_t LastExtremumIndex() const;

      bool BetweenLastTwo(size_t a_index) const;
   
   private:

      virtual bool ExtremumCheckRequired(double a_currentValue, double a_previousValue) = 0;

      virtual bool ExtremumNeighbourValid(double a_neigbhourValue, double a_lastMinValue) = 0;

      virtual bool DataSourcValueValid(double a_dataSourceValue, double a_lineValue) = 0;

      bool FindLocalExtremum(size_t a_samplesOffset, size_t& a_localMinimumIndex);

      bool IsLocalExtremum(size_t a_sampleIndex);

      bool IsLineAvailable();

      void PrepareArgumentsAndValuesForLineCalculator();

      bool IsLineConditionFullfilled(double a_maxDev2Value, bool a_removeInvalidMinimumEntry);

      bool IsDataSourceValuesConditionFullfilled(size_t a_dataSourceStartIndex, size_t a_dataSourceEndIndex);

   protected:
      deque<size_t>              m_extremumsIndexes;
      vector<double>             m_tempLineArgs;
      vector<double>             m_tempLineValues;
      
      const DataSeries&          m_sourceData;
      size_t                     m_sourceDataIndexOfsset;

      size_t                     m_firstFoundExtremumIndex;
      size_t                     m_lastFoundExtremumIndex;


      const ChannelFinderContext& m_context;
      shared_ptr<Asset>          m_spAsset;
      LineCoefficientCalculator  m_calculator;
      size_t                     m_channelPeriod;
   };



   //
   class BottomLineFinder : public LineFinder
   {
   public:
      BottomLineFinder(const ChannelFinderContext& a_context, shared_ptr<Asset> a_spAsset, 
                       const DataSeries& a_sourceData, size_t a_sourceDataIndexOfsset, 
                       size_t a_channelPeriod);

   private:

      virtual bool ExtremumCheckRequired(double a_currentValue, double a_previousValue);

      virtual bool ExtremumNeighbourValid(double a_neigbhourValue, double a_lastMinValue);

      virtual bool DataSourcValueValid(double a_dataSourceValue, double a_lineValue);
   };

   //
   class TopLineFinder : public LineFinder
   {
   public:
      TopLineFinder(const ChannelFinderContext& a_context, shared_ptr<Asset> a_spAsset, 
                    const DataSeries& a_sourceData,size_t a_sourceDataIndexOfsset, 
                    size_t a_channelPeriod);

      virtual bool ExtremumCheckRequired(double a_currentValue, double a_previousValue);

      virtual bool ExtremumNeighbourValid(double a_neigbhourValue, double a_lastMinValue);

      virtual bool DataSourcValueValid(double a_dataSourceValue, double a_lineValue);
   };
   // -----------------

public:
   ParallelChannelFinder(const ChannelFinderContext& a_context);
   virtual ~ParallelChannelFinder(void);

   // check if asset meets concrete finder conditions
   virtual bool VerifyAssetConditions();

protected:

   bool VerifyChannelNearSupportLine();

   bool VerifyChannelNearResistanceLine();

   bool VerifySupportLine();
   
   bool VerifyResistanceLine();

   bool CheckConfirmationCondition(const Kandle& a_kandle) const;

protected:
   
   const ChannelFinderContext& m_context;
   int   m_channelPeriod;
};
