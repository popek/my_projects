#include "StdAfx.h"
#include "MathUtils.h"


double MathUtils::Abs(const double& a_value)
{
   return a_value > 0 ? a_value : -a_value;
}

double MathUtils::Max(const double& a_value1, const double& a_value2)
{
   return a_value1 > a_value2 ? a_value1 : a_value2;
}

double MathUtils::Min(const double& a_value1, const double& a_value2)
{
   return a_value1 < a_value2 ? a_value1 : a_value2;
}