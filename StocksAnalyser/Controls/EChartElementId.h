#pragma once

enum EChartElementId
{
   EPoint,
   ELine,
   ELineWithPoints,
   EVolume,
   EOHLC,

   EUnknownElement
};