#pragma once
#include "KandleDate.h"


class Asset;
class StrategyResult;
class ChartElementsCollection;
class ChartElement;
class StrategyParams;


class TradeStrategyBase
{
private:
   enum state
   {
      EBought,
      ESold,
      ELossReached
   };

public:
   enum position
   {
      ELong,
      EShort
   };

public:
   TradeStrategyBase(shared_ptr<Asset> a_spAsset);
   virtual ~TradeStrategyBase(void);

   virtual shared_ptr<StrategyResult> RunTest( const KandleDate& a_start, const KandleDate& a_finish);

   bool TakePosition(position a_positionType);

   void AttachParams(shared_ptr<StrategyParams> a_spParams);

protected:
   virtual bool ShouldBuy(size_t a_sampleIndex, double& a_buyPrice)
   {
      ASSERT(FALSE);
      return false;
   }

   virtual bool ShouldSell(size_t a_sampleIndex, double& a_sellPrice)
   {
      ASSERT(FALSE);
      return false;
   }

   virtual void AddGraphicPresentation(shared_ptr<ChartElementsCollection> a_elements)
   {
      ASSERT(FALSE);
   }

   shared_ptr<ChartElement> CreateSignalChartElement(bool a_buySignal, double a_price, shared_ptr<KandleDate> a_spDate);

protected:
   shared_ptr<Asset> _spAsset;

   double            _maxLost;      // maximum lost during single market entry (0.05 means that we allow to loss maximum 5%)

   bool              _tradeNextDayAfterSignal;

   shared_ptr<StrategyParams> _spParams;

private:
   state             _currentState;
};
