#pragma once
#include "FoundationLib.h"


class IXMLSerialiser;


class FOUNDATION_LIB IXMLObjectsFactory
{
public:
   virtual ~IXMLObjectsFactory(void);

   virtual shared_ptr<IXMLSerialiser> createSerialiser() const = 0;
};

