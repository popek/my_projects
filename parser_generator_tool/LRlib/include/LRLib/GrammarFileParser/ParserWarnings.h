#pragma once

#include <string>
#include <deque>

namespace lrlib
{

class ParserWarnings
{
   class CWarning
   {
   public:
      CWarning(std::string, int);

      std::string m_strWarningDescription;
      int m_nLineNo;
   };

public:
   ParserWarnings(void);
   virtual ~ParserWarnings(void);

   void AddFormatedWarning0(std::string, int, std::string, int);

   void Clear();

protected:
   std::deque<CWarning>   m_deqWarnings;
};

} // namespace lrlib