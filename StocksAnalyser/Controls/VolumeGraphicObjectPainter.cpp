#include "StdAfx.h"
#include "VolumeGraphicObjectPainter.h"
#include "GraphicObjectBase.h"


VolumeGraphicObjectPainter::VolumeGraphicObjectPainter(void)
{
}
VolumeGraphicObjectPainter::~VolumeGraphicObjectPainter(void)
{
}

void VolumeGraphicObjectPainter::DrawObject(const GraphicObjectBase& a_object, CDC* a_pDC)
{
   size_t pointsCount = a_object.PointsCount();

   if(0 == pointsCount)
   {
      return;
   }

   Coordinate barWidth = (1 == pointsCount ? 100 : a_object.GetAt(1)._x - a_object.GetAt(0)._x);
   Coordinate leftPos = a_object.GetAt(0)._x - barWidth/2;
   Coordinate rightPos = 0;

   for(size_t pointIndex=0; pointIndex<pointsCount; ++pointIndex)
   {
      rightPos = leftPos + barWidth;

      CRect rcBar((int)leftPos, (int)a_object.GetAt(pointIndex)._y, (int)rightPos, 1000);
      rcBar.NormalizeRect();

      // in case bar body is invisible, make it visible
      if(0 == rcBar.Height())
      {
         --rcBar.top;
      }
      // deflate bar body
      if(barWidth >= 2 && barWidth<= 4)
      {
         rcBar.DeflateRect(1, 0, 0, 0);
      }
      else if(barWidth > 4)
      {
         rcBar.DeflateRect(2, 0, 0, 0);
      }

      // draw bar body
      a_pDC->FillSolidRect(&rcBar, RGB(0, 0, 0));
      leftPos = rightPos;
   }
}

