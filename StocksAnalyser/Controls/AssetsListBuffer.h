/**
 * @file
 * @author  Marcin Wrobel <marwrobel@gmail.com>
 *
 * @section DESCRIPTION
 */
 
#pragma once
#include "ControlsLib.h"

class AssetContainer;

class CONTROLS_LIB AssetsListBuffer
{
   typedef map<CString, shared_ptr<AssetContainer>> TInternalAssetsMap;

private:
   AssetsListBuffer(void);
   virtual ~AssetsListBuffer(void);

public:
   static AssetsListBuffer* Instance();

   void Release();

   void BufferAssetContainer(CString a_strAssetCSVFilePath, shared_ptr<AssetContainer> a_spAssetContainer);

   shared_ptr<AssetContainer> GetAssetContainer(CString a_strAssetCSVFilePath);

protected:

   TInternalAssetsMap m_mapCSVFilePath2Asset;

   static AssetsListBuffer* Sm_pInstance;
};
