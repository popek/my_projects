#include "StdAfx.h"
#include "MeanMax_strategy.h"
#include "Asset.h"
#include "AssetAuxiliaryWrappers.h"
#include "StrategyParams.h"
#include "SMA_pointer.h"
#include "MAX_pointer.h"
#include "ChartElementsCollection.h"
#include "TransactionDetails.h"
#include "StrategyResult.h"
#include "ControlsLibUtils.h"
#include "ColorUtils.h"
#include "LineStyle.h"
#include "ChartElement.h"


MeanMax_strategy::MeanMax_strategy(shared_ptr<Asset> spAsset)
   : TradeStrategyBase(spAsset)
   , _state(EIdle)
   , _maxIndicatorPeriod(5)
   , _maxDecreseFromMaxValue(0.02)
   , _smaPeriod(100)
{
}
MeanMax_strategy::~MeanMax_strategy(void)
{
}

shared_ptr<StrategyResult> MeanMax_strategy::RunTest( const KandleDate& a_start, const KandleDate& a_finish)
{
   _elements = _spAsset->ElementsCollection() ? 
      _spAsset->ElementsCollection() : 
      make_shared<ChartElementsCollection>();

   auto result = make_shared<StrategyResult>();
   shared_ptr<TransactionDetails> transaction;
   vector<shared_ptr<TransactionDetails>> allTransactions;

   size_t sampleIndex = _spAsset->FindNearestKandle(a_start);
   size_t endIndex = _spAsset->FindNearestKandle(a_finish);

   if(sampleIndex < _maxIndicatorPeriod)
   {
      sampleIndex = _maxIndicatorPeriod;
   }
   if(sampleIndex < _smaPeriod)
   {
      sampleIndex = _smaPeriod;
   }
   if(sampleIndex >= endIndex)
   {
      return nullptr;
   }

   _sma = make_shared<SMA_pointer>(_smaPeriod);
   _sma->Create(AssetCloseValueGetter(*_spAsset));

   _maxIndicator = make_shared<MAX_pointer>(_maxIndicatorPeriod);
   _maxIndicator->Create(AssetHighValueGetter(*_spAsset));


   for(; sampleIndex<endIndex; ++sampleIndex)
   {
//       if(false == _spParams->isParamFulfilled(StrategyParams::EMinTurnover, (*_spAsset)[sampleIndex]))
//       {
//          return nullptr;
//       }

      switch(_state)
      {
      case EIdle:
         transaction = handleIdle(sampleIndex);
         break;

      case EBoughtAboveMean:
         transaction = handleBoughtAboveMean(sampleIndex);
         break;

      case EBoughtAboveLastMaximum:
         transaction = handleBoughtAboveLastMaximum(sampleIndex);
         break;

      case ESoldBelowMean:
         transaction = handleSoldBelowMean(sampleIndex);
         break;

      case ESoldOnCutoffValue:
         transaction = handleSoldOnCutoffValue(sampleIndex);
         break;
      }

      if(transaction)
      {
         allTransactions.push_back(transaction);
      }
   }

   if(allTransactions.size())
   {
      if(allTransactions.size() % 2 > 0)
      {
         allTransactions.push_back( make_shared<TransactionDetails>(_spAsset->GetKandle(endIndex - 1).Date(), _spAsset->GetKandle(endIndex - 1).Close()) );
      }

      for(size_t transactionIndex = 0; transactionIndex < allTransactions.size(); transactionIndex += 2)
      {
         result->RegisterEntry(allTransactions[transactionIndex], allTransactions[transactionIndex+1]);
      }

      AddGraphicPresentation(_elements);
      _spAsset->AttachElementsCollection(_elements);
   }

   return result;
}

shared_ptr<TransactionDetails> MeanMax_strategy::handleIdle(size_t candleIndex)
{
   const Kandle& candle = (*_spAsset)[candleIndex];
   double smaValue = _sma->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex);

   if(candle.High() < smaValue)
   {
      _state = ESoldBelowMean;
   }
   else
   {
      if(candle.High() < _maxIndicator->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex))
      {
         _lastMaxBasedOnWhichCutofSellWasGenerated = _maxIndicator->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex);
         _state = ESoldOnCutoffValue;
      }
      else
      {
         _state = EBoughtAboveMean;
         _lastTransactionPrice = candle.Low() > smaValue ? candle.Low() : smaValue;

         _elements->PushBack(CreateSignalChartElement(true, _lastTransactionPrice, candle.Date()));
         return make_shared<TransactionDetails>(candle.Date(), _lastTransactionPrice);
      }
   }

   return nullptr;
}

shared_ptr<TransactionDetails> MeanMax_strategy::handleSoldBelowMean(size_t candleIndex)
{
   const Kandle& candle = (*_spAsset)[candleIndex];
   double smaValue = _sma->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex);
   double highestValue = candle.High();

   if(highestValue > smaValue)
   {
      _lastTransactionPrice = candle.Low() > smaValue ? candle.Low() : smaValue;

      _state = EBoughtAboveMean;
      _elements->PushBack(CreateSignalChartElement(true, _lastTransactionPrice, candle.Date()));
      return make_shared<TransactionDetails>(candle.Date(), _lastTransactionPrice);
   }

   return nullptr;
}


shared_ptr<TransactionDetails> MeanMax_strategy::handleSoldOnCutoffValue(size_t candleIndex)
{
   bool isSold = false;
   const Kandle& candle = (*_spAsset)[candleIndex];
   
   if(candle.High() > _lastMaxBasedOnWhichCutofSellWasGenerated)
   {
      _lastTransactionPrice = candle.Low() > _lastMaxBasedOnWhichCutofSellWasGenerated ? candle.Low() : _lastMaxBasedOnWhichCutofSellWasGenerated;
      _state = EBoughtAboveLastMaximum;
      _elements->PushBack(CreateSignalChartElement(true, _lastTransactionPrice, candle.Date()));
      return make_shared<TransactionDetails>(candle.Date(), _lastTransactionPrice);
   }
   else if(candle.High() < _sma->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex)) // if candle get below sma, then state needs to be changed
   {
      _state = ESoldBelowMean;
   }

   return nullptr;
}


shared_ptr<TransactionDetails> MeanMax_strategy::handleBoughtAboveMean(size_t candleIndex)
{
   bool isSold = false;
   const Kandle& candle = (*_spAsset)[candleIndex];
   double smaValue = _sma->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex);

   double lowestValue = candle.Low();

   if(lowestValue < smaValue)        // sell if asset value is below mean
   {
      _lastTransactionPrice = smaValue;
      _state = ESoldBelowMean;
      isSold = true;
   }
   else
   {
      double cutoffValue = _maxIndicator->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex > 0 ? candleIndex - 1 : 0) * (1-_maxDecreseFromMaxValue);
      if(lowestValue < cutoffValue)      // ... or if asset value gets below minimal acceptable price (this is added to keep current winnings, without waiting form value to get below mean)
      {
         _lastMaxBasedOnWhichCutofSellWasGenerated =  _maxIndicator->Series(0).GetDataBasedOnRootSeriesIndex(candleIndex);
         _lastTransactionPrice = candle.High() < cutoffValue ? candle.High() : cutoffValue;
         _state = ESoldOnCutoffValue;
         isSold = true;
      }
   }

   if(isSold)
   {
      _elements->PushBack(CreateSignalChartElement(false, _lastTransactionPrice, candle.Date()));
      return make_shared<TransactionDetails>(candle.Date(), _lastTransactionPrice);
   }

   return nullptr;
}

shared_ptr<TransactionDetails> MeanMax_strategy::handleBoughtAboveLastMaximum(size_t candleIndex)
{
   return handleBoughtAboveMean(candleIndex);
}

void MeanMax_strategy::AddGraphicPresentation(shared_ptr<ChartElementsCollection> elements)
{
   elements->PushBack(ControlsLibUtils::CreatePointerSeriesElement(_sma->Series(0), *_spAsset));
   elements->GetElement(elements->ElementsCount()-1)->Style()->SetNumericParam(LineStyle::EColor, 
      ColorUtils::MixColors(ColorUtils::EBlue, ColorUtils::EWhite, 0.4));

   elements->PushBack(ControlsLibUtils::CreatePointerSeriesElement(_maxIndicator->Series(0), *_spAsset));
   elements->GetElement(elements->ElementsCount()-1)->Style()->SetNumericParam(LineStyle::EColor, 
      ColorUtils::MixColors(ColorUtils::EBlue, ColorUtils::EWhite, 0.65));
}