#pragma once
#include "SymbolUniqueKey.h"
#include "LessString.h"

#include <vector>
#include <set>
#include <map>
#include <memory>

namespace lrlib
{

class Symbol;
class StateActions;

typedef std::multimap<int, std::vector<int>>	         t_mmapInt2VecInt;
typedef std::map<int, std::set<int>>				      t_mapInt2SetInt;
typedef std::set<t_mmapInt2VecInt>				         t_setOfMMap;

typedef std::map<std::string, Symbol*, LessString>   t_mapStr2Sym;
typedef std::map<int, Symbol*>					         t_mapInt2Sym;
typedef std::map<SymbolUniqueKey, Symbol>		      t_setSymbol;
typedef std::vector<std::unique_ptr<StateActions>> t_transitionGraph;

} // namespace lrlib