%%
LETTER -> a-z,A-Z,
DIGIT  -> 0-9,
OR	   -> |,
SLASH  -> /,
STAR   -> *,
WS     -> "\t", "\s",
CR	   -> "\r",
LF	   -> "\n",
SINGLECHARTOKENS -> ~!#$%^&+=[]{};'\:".@<?(),

%%
identifier 		  -> (LETTER|_)(LETTER|DIGIT|_)*
single_char_token -> SINGLECHARTOKENS | STAR | SLASH | - | , | >
eval_to  	      -> - >
or				  -> OR
blank 	 	      -> WS WS*
eol		 		  -> (CR LF)|LF
single_line_comment_start -> SLASH SLASH
multi_line_comment_start  -> SLASH STAR
multi_line_comment_finish -> STAR SLASH
%%
