#include "StdAfx.h"
#include "GMMA_NearBuySignal.h"
#include "Asset.h"
#include "AssetContainer.h"
#include "Misc.h"


GMMA_NearBuySignal::GMMA_NearBuySignal(shared_ptr<AssetContainer> a_spAssetContainer)
   : GMMA_Signal(a_spAssetContainer)
{
   m_shortAndLongMeansNearEachOtherLongestPeriod = 9;
   m_shortMeansGettingToLongMeansPeriod = 10;
   m_dLongMeanMinimumValueOffset        = 0.005;
}
GMMA_NearBuySignal::~GMMA_NearBuySignal(void)
{
}

bool GMMA_NearBuySignal::VerifyAssetConditions()
{
   m_spTemporaryAsset = (*m_spAsset)[eDay];
   Misc::PreparePointers(*m_spTemporaryAsset, m_vTemporaryPointers, m_shortPeriodsPointersAmount);

   // first check if week signal exists
   if(true == WeekSignalExists())
   {
      return true;
   }

   // then check if strong signal exists (this would mean we are after week signal)
   return StrongSignalExists();
}

bool GMMA_NearBuySignal::WeekSignalExists()
{
   // if pointers does not have enough samples then return week signal does not exist
   if( false == CheckPointersSamplesAmount(m_shortMeansGettingToLongMeansPeriod))
   {
      return false;
   }

   // if short means are NOT below long means then week signal does not exist
   if(false == ShortMeansBelowLongMeans(2, m_shortMeansGettingToLongMeansPeriod))
   {
      return false;
   }

   // last kandle value must be near minimal long term mean
   return (*m_spTemporaryAsset)[m_spTemporaryAsset->Size()-1].Close() > GetLongTermsMeansMinimum(1)*(1-m_dLongMeanMinimumValueOffset);
}

bool GMMA_NearBuySignal::StrongSignalExists()
{
   // if pointers does not have enough samples then return week signal does not exist
   if( false == CheckPointersSamplesAmount(m_shortAndLongMeansNearEachOtherLongestPeriod))
   {
      return false;
   }

   // check if maximum short mean is between long means boundries
   size_t indexOffset = 1;
   size_t shortMeansBetweenLongMeansSamplesCount = 0;
   for(; indexOffset < m_shortAndLongMeansNearEachOtherLongestPeriod; ++indexOffset, ++shortMeansBetweenLongMeansSamplesCount)
   {
      double shortMeansMax = GetShortTermsMeansMaximum(indexOffset);

      if( shortMeansMax < GetLongTermsMeansMinimum(indexOffset)  || 
          shortMeansMax > GetLongTermsMeansMaximum(indexOffset))
      {
         break;
      }
   }
   // if there is no samples where short max mean is between long means boundries, 
   // or samples count extends allowed number then return false
   if(0 == shortMeansBetweenLongMeansSamplesCount ||
      shortMeansBetweenLongMeansSamplesCount >= m_shortAndLongMeansNearEachOtherLongestPeriod)
   {
      return false;
   }

   // check samples amount again (this time check if also additional period exists)
   if( false == CheckPointersSamplesAmount(shortMeansBetweenLongMeansSamplesCount + m_shortMeansGettingToLongMeansPeriod))
   {
      return false;
   }

   // if short means are below long means then strong signal exists
   return ShortMeansBelowLongMeans(indexOffset, indexOffset + m_shortMeansGettingToLongMeansPeriod);
}

bool GMMA_NearBuySignal::ShortMeansBelowLongMeans(const size_t a_offsetStart, const size_t a_offsetEnd) const
{
   for(size_t offset = a_offsetStart; offset < a_offsetEnd; ++offset)
   {
      if( GetShortTermsMeansMaximum(offset) > GetLongTermsMeansMinimum(offset) )
      {
         return false;
      }
   }

   return true;
}
