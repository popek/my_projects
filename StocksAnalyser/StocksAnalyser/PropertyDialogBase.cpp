#include "stdafx.h"
#include "StocksAnalyser.h"
#include "PropertyDialogBase.h"


IMPLEMENT_DYNAMIC(PropertyDialogBase, CDialog)

PropertyDialogBase::PropertyDialogBase(UINT a_dialogId)
	: CDialog(a_dialogId, NULL)
{
}
PropertyDialogBase::~PropertyDialogBase()
{
}


BEGIN_MESSAGE_MAP(PropertyDialogBase, CDialog)
END_MESSAGE_MAP()


// PropertyDialogBase message handlers
void PropertyDialogBase::OnOK()
{
}

void PropertyDialogBase::OnCancel()
{
}
