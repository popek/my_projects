#pragma once

#include "SemStack.h"
#include "..\SymbolsContainer.h"
#include "..\Rules.h"
#include "..\Symbol.h"

namespace lrlib
{

class GrammarParser;
class CompileContext;

class SemanticHandler
{
   friend GrammarParser;

   enum ESymbolType { eST_Invalid, eST_nonterminal, eST_terminal };
   SemStack semStack_;

public:
   SemanticHandler(CompileContext& context);
   ~SemanticHandler() = default;

   void         DoReduceAction(int iRuleID);
   SemStack&   SymbolStack();

   void Rule_acceptance();
   void Rule_PRODUCTION_LIST__to__PRODUCTION();
   void Rule_PRODUCTION_LIST__to__PRODUCTION_LIST__PRODUCTION();
   void Rule_PRODUCTION__to__PRODUCTION_ASSIGN__PRODUCTION_EXPR__semicolon();
   void Rule_PRODUCTION_ASSIGN__to__NONTERMINAL__eval_to();
   void Rule_PRODUCTION_EXPR__to__SYMBOL_LIST();
   void Rule_PRODUCTION_EXPR__to__PRODUCTION_EXPR__or__SYMBOL_LIST();
   void Rule_NONTERMINAL__to__identifier();
   void Rule_SYMBOL_LIST__to__SYMBOL();
   void Rule_SYMBOL_LIST__to__SYMBOL_LIST__SYMBOL();
   void Rule_SYMBOL__to__identifier();
   void Rule_SYMBOL__to__single_char_token();

protected:
   ESymbolType CheckIdentifier(std::string a_strIdentifier);
   void        HandleNewRule();

   Symbol*    FindRootNonterminal();        // auxiliary method to find root nonterminal
   void        SetSymbolsIDs(Symbol* a_pRootNonterminal);

protected:
   Symbol currentNonterminal_;
   std::vector<Symbol*> currentSymbolList_;

   CompileContext& context_;
};

} //namespace lrlib