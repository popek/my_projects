#include "StdAfx.h"
#include "ThreadBase.h"
#include "ScopeLock.h"
#include "IThreadMessageHandler.h"
#include <process.h>

ThreadBase::ThreadBase(IThreadMessageHandler* a_pMessageHandler)
{
   m_flags = 0;
   m_flags &= ~EUseFifo;    // use FILO queue
   
   m_pHandler = a_pMessageHandler;

   Reset();

   m_hMessageEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
}
ThreadBase::~ThreadBase(void)
{
   Stop();

   CloseHandle(m_hMessageEvent);
}

void ThreadBase::Create()
{
   LOCK_SCOPE(m_csLock);

   if(!m_hThread)
   {
      m_hThread = (HANDLE)_beginthreadex(NULL, 0, ThreadBase::ThreadProc, this, CREATE_SUSPENDED, NULL);
   }
}

void ThreadBase::Stop()
{
   {
      LOCK_SCOPE(m_csLock);

      if(NULL == m_hThread)
      {
         return;
      }

      m_bShouldStop = true;
      if(false == m_bThreadRunning)
      {
         Resume();
      }
   }

   SetEvent(m_hMessageEvent);

   // wait from 
   if( WAIT_OBJECT_0 != ::WaitForSingleObject(m_hThread, 10000) )
   {
      ASSERT(FALSE);
   }

   Reset();
}

void ThreadBase::Suspend()
{
   {
      LOCK_SCOPE(m_csLock);
      m_bThreadRunning = false;
   }

   SuspendThread(m_hThread);
}

void ThreadBase::Resume()
{
   {
      LOCK_SCOPE(m_csLock);
      m_bThreadRunning = true;
   }

   ResumeThread(m_hThread);
}
void ThreadBase::SetFlags(UINT a_newFlags)
{
   m_flags = a_newFlags;
}

UINT ThreadBase::GetFlags() const
{
   return m_flags;
}

void ThreadBase::PostMessage(UINT a_nMessageID)
{
   {
      LOCK_SCOPE(m_csLock);
      // don't post if thread is not running or if is in the middle of stopping process
      if(false == m_bThreadRunning || true == m_bShouldStop)
      {
         return;
      }

      m_messagesQueue.push_back(a_nMessageID);
      ASSERT(m_messagesQueue.size() < 100);
   }

   SetEvent(m_hMessageEvent);
}

void ThreadBase::PostIfNotInQueue(UINT a_messageId)
{
   if(false == IsQueued(a_messageId))
   {
      PostMessage(a_messageId);
   }
}

bool ThreadBase::IsQueued(UINT a_messageId)
{
   bool messageAlreadyWaitingInQueue = false;

   {
      LOCK_SCOPE(m_csLock);

      for(size_t messageIdx=0; messageIdx<m_messagesQueue.size(); ++messageIdx)
      {
         if(m_messagesQueue[messageIdx] == a_messageId)
         {
            messageAlreadyWaitingInQueue = true;
            break;
         }
      }
   }

   return messageAlreadyWaitingInQueue;
}

void ThreadBase::Run()
{
   UINT messageId = 0;

   while( WAIT_OBJECT_0 == ::WaitForSingleObject(m_hMessageEvent, INFINITE) )
   {
      if(m_bShouldStop)
      {
         break;
      }

      while(GetNextMessageId(messageId))
      {
         m_pHandler->ProcessThreadMessage( messageId );
      }
   }
}

void ThreadBase::Reset()
{
   m_hThread = NULL;
   m_bThreadRunning = false;
   m_bShouldStop = false;

   m_messagesQueue.clear();
}

bool ThreadBase::GetNextMessageId(UINT& a_rMessageId)
{
   LOCK_SCOPE(m_csLock);
   
   if(0 == m_messagesQueue.size())
   {
      return false;
   }

   // get message id
   if(m_flags&EUseFifo)
   {
      a_rMessageId = m_messagesQueue.back();
   }
   else
   {
      a_rMessageId = m_messagesQueue.front();
   }

   // remove appropraite message(s) from message queue
   if(m_flags&EDiscardQueuedMessages)
   {
      m_messagesQueue.clear();
   }
   else
   {
      if(m_flags&EUseFifo)
      {
         m_messagesQueue.erase(m_messagesQueue.end()-1);
      }
      else
      {
         m_messagesQueue.erase(m_messagesQueue.begin());
      }
   }

   return true;
}

unsigned ThreadBase::ThreadProc(void* a_pThreadInstance)
{
   ( (ThreadBase*)(a_pThreadInstance) )->Run();

   return 0;
}