#pragma once
#include "controlsLibTypes.h"


class TimeScaleContext;
class KandleDate;

class TimeScalePainter
{
public:
   TimeScalePainter(void);
   virtual ~TimeScalePainter(void);

   int  Height();

   void Height(int a_nNewTimeScaleHeight);

   void VertOffset(int a_nNewLeftBottomCornerYOffset);

   virtual void Draw(CDC& a_rDC, const TimeScaleContext& a_timescaleContext);

protected:
   
   int   CalcLineDensity(const TimeScaleContext& a_timescaleContext);
   
   int   CalcDateDensity(const TimeScaleContext& a_timescaleContext);

   void  DrawDate(CDC& a_rDC, shared_ptr<KandleDate> a_spTimestamp, Coordinate a_timestampCoordinate);

   void  DrawLine(CDC& a_rDC, int a_nXpos, bool a_bold);

protected:

   int         m_nHeight;
   int         m_nLeftTopCornerYOffset;

   CPen        m_Pen;
   CPen        m_boldPen;

   int         m_nLineLength;

   CFont       m_font;
   int         m_nDateRectWidth;
   
   COLORREF    m_timeScaleColor;
   int         m_minDiffBetweenLines;
   int         m_distanceBeetwenDates;
   int         m_minLineLength;
};
