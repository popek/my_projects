#pragma once
#include "ControlsLib.h"

class CONTROLS_LIB ClientRect : public CRect
{
public:
   ClientRect(CWnd* a_pWindow);
   ClientRect(CWnd& a_Window);
   virtual ~ClientRect(void);
};
