#pragma once
#include <string>
#include "Exceptions.h"
#include "CommonDefs.h"

namespace regexlib
{

class ILex
{
public:
   virtual short SymbolName2SymbolID(std::string strSymName) = 0;
   virtual short	Char2SymbolID(char cChar) = 0;

protected:
   virtual t_Char2SymbolID&   GetChar2SymbolIDMap() = 0;
};

} // namespace regexlib