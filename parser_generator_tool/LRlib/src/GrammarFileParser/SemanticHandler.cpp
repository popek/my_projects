#include "GrammarFileParser/SemanticHandler.h"
#include "GrammarFileParser/CommonDef.h"
#include "GrammarFileParser/GrammarParser.h"
#include "GrammarFileParser/ParserErrorsDefinitions.h"
#include "GrammarFileParser/ParserWarningsDefinitions.h"
#include "GrammarFileParser/CompileContext.h"
#include "Utils.h"
#include "LessPtr.h"

namespace lrlib
{

SemanticHandler::SemanticHandler(CompileContext& context)
   : context_(context)
{
}

SemStack& SemanticHandler::SymbolStack()
{
   return semStack_;
};

void SemanticHandler::DoReduceAction(int iRuleID)
{
   typedef void (SemanticHandler::*RuleFunction)();

   static RuleFunction Rules[RULES_COUNT] =
   {
     &SemanticHandler::Rule_acceptance,
     &SemanticHandler::Rule_PRODUCTION_LIST__to__PRODUCTION,
     &SemanticHandler::Rule_PRODUCTION_LIST__to__PRODUCTION_LIST__PRODUCTION,
     &SemanticHandler::Rule_PRODUCTION__to__PRODUCTION_ASSIGN__PRODUCTION_EXPR__semicolon,
     &SemanticHandler::Rule_PRODUCTION_ASSIGN__to__NONTERMINAL__eval_to,
     &SemanticHandler::Rule_PRODUCTION_EXPR__to__SYMBOL_LIST,
     &SemanticHandler::Rule_PRODUCTION_EXPR__to__PRODUCTION_EXPR__or__SYMBOL_LIST,
     &SemanticHandler::Rule_NONTERMINAL__to__identifier,
     &SemanticHandler::Rule_SYMBOL_LIST__to__SYMBOL,
     &SemanticHandler::Rule_SYMBOL_LIST__to__SYMBOL_LIST__SYMBOL,
     &SemanticHandler::Rule_SYMBOL__to__identifier,
     &SemanticHandler::Rule_SYMBOL__to__single_char_token,
   };

   (this->*(Rules[iRuleID]))();
}

//------------------ rule actions --------------------
void SemanticHandler::Rule_acceptance()
{
   auto itSymbol = context_.symbols().Begin();
   auto itEnd = context_.symbols().End();

   Symbol* pRootNonterminal = FindRootNonterminal();

   if (NULL != pRootNonterminal)
   {
      pRootNonterminal->rootSymbol(true);

      SetSymbolsIDs(pRootNonterminal);

      context_.rules().GenerateRulesByIDsMap();
   }
   else
   {
      context_.errors().AddError(PE_ROOT_TERMINAL_NOT_FOUND, -1);
   }

   while (itSymbol != itEnd)
   {
      Symbol* pSymbol = Utils::SymbolPtr(itSymbol);

      if (false == pSymbol->term())
      {
         if (false == pSymbol->defined())
         {
            context_.errors().AddFormatedError0(PE_NONTERMINAL_NOT_DEFINED, pSymbol->lineNo(), pSymbol->name());
         }

         if (false == pSymbol->used() && false == pSymbol->rootSymbol())
         {
            context_.errors().AddFormatedError0(PE_NONTERMINAL_NOT_USED, pSymbol->lineNo(), pSymbol->name());
         }
      }

      ++itSymbol;
   }

#ifdef _DEBUG
   printf("###RULE_ACCEPTANCE###\n");
#endif
}

void SemanticHandler::Rule_PRODUCTION_LIST__to__PRODUCTION()
{
#ifdef _DEBUG
   printf("Rule_PRODUCTION_LIST__to__PRODUCTION\n");
#endif
}

void SemanticHandler::Rule_PRODUCTION_LIST__to__PRODUCTION_LIST__PRODUCTION()
{
   semStack_.Pop();
#ifdef _DEBUG
   printf("Rule_PRODUCTION_LIST__to__PRODUCTION_LIST__PRODUCTION\n");
#endif
}

void SemanticHandler::Rule_PRODUCTION__to__PRODUCTION_ASSIGN__PRODUCTION_EXPR__semicolon()
{
   semStack_.Pop();
#ifdef _DEBUG
   printf("Rule_PRODUCTION__to__PRODUCTION_ASSIGN__PRODUCTION_EXPR__semicolon\n");
#endif
}

void SemanticHandler::Rule_PRODUCTION_ASSIGN__to__NONTERMINAL__eval_to()
{
   semStack_.Pop();
#ifdef _DEBUG
   printf("Rule_PRODUCTION_ASSIGN__to__NONTERMINAL__eval_to\n");
#endif
}

void SemanticHandler::Rule_PRODUCTION_EXPR__to__SYMBOL_LIST()
{
   HandleNewRule();
#ifdef _DEBUG
   printf("Rule_PRODUCTION_EXPR__to__SYMBOL_LIST\n");
#endif
}

void SemanticHandler::Rule_PRODUCTION_EXPR__to__PRODUCTION_EXPR__or__SYMBOL_LIST()
{
   semStack_.Pop();

   HandleNewRule();
#ifdef _DEBUG
   printf("Rule_PRODUCTION_EXPR__to__PRODUCTION_EXPR__or__SYMBOL_LIST\n");
#endif
}

void SemanticHandler::Rule_NONTERMINAL__to__identifier()
{
   SemAtom oSymbol;
   Symbol* pSymbol = NULL;

   semStack_.Top(&oSymbol);

   switch (CheckIdentifier(oSymbol.m_LexAtom.m_strText))
   {
   case eST_terminal:
      context_.errors().AddFormatedError0(PE_SHOULD_BE_NONTERMINAL, oSymbol.m_LexAtom.m_iLine, oSymbol.m_LexAtom.m_strText);  // report error
      break;

   case eST_nonterminal:
      pSymbol = context_.symbols().FindSymbol(oSymbol.m_LexAtom.m_strText);

      if (NULL == pSymbol)                                                              // if nonterminal did not appear yet...
      {
         currentNonterminal_ = Symbol(oSymbol.m_LexAtom.m_strText, INVALID_SYMBOL_ID, false, oSymbol.m_LexAtom.m_iLine);         // remember currently defined nonterminal
         currentNonterminal_.defined(true);

         context_.symbols().Insert(currentNonterminal_);
      }
      else
      {
         if (true == pSymbol->defined())                                                                            // if symbol definition already appeared ...   
         {
            context_.warnings().AddFormatedWarning0(PW_NONTERMINAL_ALREADY_DEFINED, oSymbol.m_LexAtom.m_iLine, oSymbol.m_LexAtom.m_strText, pSymbol->lineNo());
         }

         pSymbol->defined(true);
         pSymbol->lineNo(oSymbol.m_LexAtom.m_iLine);     // update line number in which symbol is defined

         currentNonterminal_ = *pSymbol;
      }
      break;

   case eST_Invalid:
      context_.errors().AddFormatedError0(PE_INVALID_SYMBOL_DEFINITION, oSymbol.m_LexAtom.m_iLine, oSymbol.m_LexAtom.m_strText);  // report error
      break;
   }

#ifdef _DEBUG
   printf("Rule_NONTERMINAL__to__identifier\n");
#endif
}

void SemanticHandler::Rule_SYMBOL_LIST__to__SYMBOL()
{
   SemAtom oSymbol;

   semStack_.Top(&oSymbol);

   currentSymbolList_.push_back(context_.symbols().FindSymbol(oSymbol.m_LexAtom.m_strText));

#ifdef _DEBUG
   printf("Rule_SYMBOL_LIST__to__SYMBOL\n");
#endif
}

void SemanticHandler::Rule_SYMBOL_LIST__to__SYMBOL_LIST__SYMBOL()
{
   SemAtom oSymbol;

   semStack_.Top(&oSymbol);
   semStack_.Pop();

   currentSymbolList_.push_back(context_.symbols().FindSymbol(oSymbol.m_LexAtom.m_strText));
#ifdef _DEBUG
   printf("Rule_SYMBOL_LIST__to__SYMBOL_LIST__SYMBOL\n");
#endif

}

void SemanticHandler::Rule_SYMBOL__to__identifier()
{
   SemAtom oSymbol;
   Symbol* pSymbol = NULL;
   semStack_.Top(&oSymbol);

   switch (CheckIdentifier(oSymbol.m_LexAtom.m_strText))
   {
   case eST_terminal:
      if (NULL == context_.symbols().FindSymbol(oSymbol.m_LexAtom.m_strText))    // in terminal did not appear yet...
      {
         context_.symbols().Insert(Symbol(oSymbol.m_LexAtom.m_strText, INVALID_SYMBOL_ID, true, -1));
      }
      break;

   case eST_nonterminal:
      pSymbol = context_.symbols().FindSymbol(oSymbol.m_LexAtom.m_strText);
      if (NULL == pSymbol)                                               // if nonterminal did not appeared yet ... 
      {
         Symbol oAuxSymbol(oSymbol.m_LexAtom.m_strText, INVALID_SYMBOL_ID, false, oSymbol.m_LexAtom.m_iLine);

         oAuxSymbol.used(true);

         context_.symbols().Insert(oAuxSymbol);
      }
      else                                                              // if nonterminal appeared ...
      {
         if (currentNonterminal_.name() != pSymbol->name())
         {
            pSymbol->used(true);                                           // mark nonterminal as used only if it is not part of itself production (e.g. SYM -> SYM term, then SYM is shouldn't be marked as used)
         }
      }
      break;

   case eST_Invalid:
      context_.errors().AddFormatedError0(PE_INVALID_SYMBOL_DEFINITION, oSymbol.m_LexAtom.m_iLine, oSymbol.m_LexAtom.m_strText);  // report error
      break;
   }

#ifdef _DEBUG
   printf("Rule_SYMBOL__to__identifier\n");
#endif
}

void SemanticHandler::Rule_SYMBOL__to__single_char_token()
{
   SemAtom oSymbol;
   semStack_.Top(&oSymbol);

   if (NULL == context_.symbols().FindSymbol(oSymbol.m_LexAtom.m_strText))
   {
      context_.symbols().Insert(Symbol(oSymbol.m_LexAtom.m_strText, INVALID_SYMBOL_ID, true, -1));
   }

#ifdef _DEBUG
   printf("Rule_SYMBOL__to__single_char_token\n");
#endif
}

SemanticHandler::ESymbolType SemanticHandler::CheckIdentifier(std::string a_strIdentifier)
{
   size_t nCharIdx = 0;
   bool   bTerminal = false;

   while (a_strIdentifier[nCharIdx] == '_')                           // skip all undersocre on the begining of string
   {
      ++nCharIdx;
   }

   bTerminal = (a_strIdentifier[nCharIdx] >= 'a' && a_strIdentifier[nCharIdx] <= 'z') ? true : false;;
   ++nCharIdx;

   for (; nCharIdx < a_strIdentifier.size(); ++nCharIdx)
   {
      if (a_strIdentifier[nCharIdx] >= '0' && a_strIdentifier[nCharIdx] <= '9' || a_strIdentifier[nCharIdx] == '_')   //skip digits and underscore
      {
         continue;
      }

      if (bTerminal)                                                             //if small letter was at a begining ...
      {
         if (a_strIdentifier[nCharIdx] >= 'A' && a_strIdentifier[nCharIdx] <= 'Z')  //and capital letter appears ...
         {
            return eST_Invalid;                                                 // then this is invalid symbol definition
         }
      }
      else                                                                      //if capital letter was at a begining ...          
      {
         if (a_strIdentifier[nCharIdx] >= 'a' && a_strIdentifier[nCharIdx] <= 'z')  //and small letter appears ...
         {
            return eST_Invalid;                                                 // then this is invalid symbol definition
         }
      }
   }

   return bTerminal ? eST_terminal : eST_nonterminal;
}

void SemanticHandler::HandleNewRule()
{
   context_.rules().AddGrammarRule(context_.symbols().FindSymbol(currentNonterminal_.name()), currentSymbolList_);   	//... create new rule

   currentSymbolList_.clear(); // clear auxiliary container (to be ready for new symbol list)
}

Symbol* SemanticHandler::FindRootNonterminal()
{
   Symbol*                         pRootNonterminal = NULL;
   t_setSymbol::iterator            itSymbol = context_.symbols().Begin();
   std::set<Symbol*, LessPtr<Symbol>>  setDescendands;

   while (itSymbol != context_.symbols().End())                                        // loop thrugh all symbols
   {
      Symbol* pSymbol = Utils::SymbolPtr(itSymbol);

      setDescendands.clear();

      if (false == pSymbol->term())                                                    // check only nonterminals
      {
         context_.rules().FindAndAddDescendands(pSymbol, setDescendands);             // gather nonterminal descendands
         size_t descendantsCount = setDescendands.size() - (setDescendands.find(pSymbol) != setDescendands.end() ? 1 : 0);

         if (descendantsCount >= context_.symbols().Size() - 1)                     // if each and every symbol is nonterminal descendant, then nonterminal is root nonterminal
         {
            pRootNonterminal = pSymbol;
            break;
         }
      }

      ++itSymbol;
   }

   return pRootNonterminal;
}

void SemanticHandler::SetSymbolsIDs(Symbol* a_pRootNonterminal)
{
   int nNextSymbolID = context_.rules().SetNonterminalsIDsAccordingToHierarchy(a_pRootNonterminal);
   context_.symbols().GenerateTerminalsIDs(nNextSymbolID);
}

} // namespace lrlib