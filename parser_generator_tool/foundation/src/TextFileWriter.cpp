#include "TextFileWriter.h"
#include <stdarg.h>


TextFileWriter::TextFileWriter(std::string a_textFilePath)
   : _file(nullptr)
{
   fopen_s(&_file, a_textFilePath.c_str(), "w");
}
TextFileWriter::~TextFileWriter(void)
{
   if(_file != nullptr)
   {
      fclose(_file);
   }
}

void TextFileWriter::WriteText(const char* a_format, ...)
{
   va_list args;
   va_start (args, a_format);

   vfprintf (_file, a_format, args);

   va_end (args);
}