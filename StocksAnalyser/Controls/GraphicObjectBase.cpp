#include "StdAfx.h"
#include "GraphicObjectBase.h"
#include "GraphicObjectPainterBase.h"

GraphicObjectBase::GraphicObjectBase()
{
}
GraphicObjectBase::GraphicObjectBase(shared_ptr<GraphicObjectPainterBase> a_spPainter)
: m_spPainter(a_spPainter)
{
}
GraphicObjectBase::~GraphicObjectBase(void)
{
}

void GraphicObjectBase::Draw(CDC* a_pDC)
{
   if(m_spPainter)
   {
      m_spPainter->DrawObject(*this, a_pDC);
   }
}

void GraphicObjectBase::Reserve(size_t a_pointsCount)
{
   m_objectPoints.clear();
   m_objectPoints.assign(a_pointsCount, Point2D());
}

void GraphicObjectBase::UpdatePoint(size_t a_pointIndex, Coordinate a_xCoordinate, Coordinate a_yCoordinate)
{
   CONTROLSLIB_ASSERT(a_pointIndex < m_objectPoints.size());

   m_objectPoints[a_pointIndex]._x = a_xCoordinate;
   m_objectPoints[a_pointIndex]._y = a_yCoordinate;
}

size_t GraphicObjectBase::PointsCount() const
{
   return m_objectPoints.size();
}

const Point2D& GraphicObjectBase::GetAt(size_t a_pointIndex) const
{
   return m_objectPoints[a_pointIndex];
}

void GraphicObjectBase::Style(shared_ptr<GrxStyle> a_style)
{
   _grxStyle = a_style;
}

shared_ptr<GrxStyle> GraphicObjectBase::Style() const
{
   return _grxStyle;
}