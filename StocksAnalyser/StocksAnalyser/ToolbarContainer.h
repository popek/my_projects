#pragma once
#include "StockAnalyserToolbar.h"

class ToolbarContainer : public CWnd
{
	DECLARE_DYNAMIC(ToolbarContainer)

public:
	ToolbarContainer();
	virtual ~ToolbarContainer();

   BOOL     Create(CWnd* a_parent);

   void     Show();

   BOOL     Visible() const;

protected:

   static LRESULT APIENTRY ToolsWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
   static WNDPROC S_originalProc;

	DECLARE_MESSAGE_MAP()

public:
   StockAnalyserToolbar   m_toolbar;
   afx_msg void OnPaint();
   afx_msg void OnNcLButtonDown(UINT nHitTest, CPoint point);
   afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};


