#include "StdAfx.h"
#include "ResultsLog.h"
#include "Asset.h"
#include "Resource.h"
#include "TextFile.h"
#include <Shlwapi.h>
#include <atlpath.h>
#include "Misc.h"

// action parameters class
ResultsLog::ActionData::ActionData()
{
}
ResultsLog::ActionData::~ActionData()
{
}

void ResultsLog::ActionData::WriteToFile(TextFile& a_openedFile, int indent)
{
   CString strTextFileEntry;

   CString strIndent;
   while(indent)
   {
      strIndent +=_T("\t");
      --indent;
   }

   TParamsMap::iterator itName2Value = m_mamName2Value.begin();
   while(itName2Value != m_mamName2Value.end())
   {
      strTextFileEntry.Format(_T("%s<param name=\"%s\", value=\"%2.2f\"/>\n"), strIndent, itName2Value->first, itName2Value->second);
      a_openedFile.Write(strTextFileEntry);

      ++itName2Value;
   }
}

void ResultsLog::ActionData::SetParam(CString a_paramName, double a_paramValue)
{
   m_mamName2Value[a_paramName] = a_paramValue;
}

bool ResultsLog::ActionData::operator < (const ResultsLog::ActionData& a_dataToCompare) const
{
   // first comapare maps sizes
   if(m_mamName2Value.size()<a_dataToCompare.m_mamName2Value.size())
   {
      return true;
   }
   else if(m_mamName2Value.size()>a_dataToCompare.m_mamName2Value.size())
   {
      return false;
   }
   else
   {
      TParamsMap::const_iterator itName2Param            = m_mamName2Value.begin();
      TParamsMap::const_iterator itName2ParamToCompare   = a_dataToCompare.m_mamName2Value.begin();
      
      while(itName2Param != m_mamName2Value.end())
      {
         // then compare names
         if(itName2Param->first < itName2ParamToCompare->first)
         {
            return true;
         }
         else if(itName2Param->first > itName2ParamToCompare->first)
         {
            return false;
         }
         else
         {
            // if names are equal, compare values
            if(itName2Param->second < itName2ParamToCompare->second)
            {
               return true;
            }
            else if(itName2Param->second > itName2ParamToCompare->second)
            {
               return false;
            }
         }

         ++itName2Param;
         ++itName2ParamToCompare;
      }
   }

   //if it gets here, it means values are equal
   return false;
}


// single action result
ResultsLog::SingleResult::SingleResult()
{
   m_timestamp = COleDateTime::GetCurrentTime();
}
ResultsLog::SingleResult::~SingleResult()
{
}

void ResultsLog::SingleResult::WriteToFile(TextFile& a_openedFile, int a_indent, int a_resultIdx)
{
   CString strTextFileEntry;
   CString strAssetsListEnding;
   CString strResultEnding;
   int     actionDataContentIndent = a_indent+1;

   CString strIndent;
   while(a_indent)
   {
      strIndent +=_T("\t");
      --a_indent;
   }
   
   // write result header
   strTextFileEntry.Format(_T("%s<Result_%d Timestamp=\"%d:%d:%d\">\n"), strIndent, a_resultIdx, m_timestamp.GetHour(), m_timestamp.GetMinute(), m_timestamp.GetSecond());
   strResultEnding.Format(_T("%s</Result_%d>\n"), strIndent, a_resultIdx);
   a_openedFile.Write(strTextFileEntry);

   // write parameters
   m_actionData.WriteToFile(a_openedFile, actionDataContentIndent);

   // increase indent
   strIndent +=_T("\t");
   strTextFileEntry.Format(_T("%s<AssetsList>\n"), strIndent);

   // write asset list header
   strAssetsListEnding.Format(_T("%s</AssetsList>\n"), strIndent);
   a_openedFile.Write(strTextFileEntry);

   // increase indent
   strIndent +=_T("\t");
   // write assets list
   for(size_t assetIdx=0; assetIdx<m_vAssets.size(); ++assetIdx)
   {

      strTextFileEntry.Format(_T("%s<name=\"%s\"/>\n"), strIndent, m_vAssets[assetIdx]->Label().AssetName());

      a_openedFile.Write(strTextFileEntry);
   }

   // write assets list block end
   a_openedFile.Write(strAssetsListEnding);
   // write result block ending
   a_openedFile.Write(strResultEnding);
}

bool ResultsLog::SingleResult::operator < (const SingleResult& a_resultToCompare) const
{
   return m_actionData < a_resultToCompare.m_actionData;
}

void ResultsLog::SingleResult::UpdateParamsMap(CString a_paramName, double a_paramValue)
{
   m_actionData.SetParam(a_paramName, a_paramValue);
}

void ResultsLog::SingleResult::AddAsset(shared_ptr<Asset> a_spAsset)
{
   m_vAssets.push_back(a_spAsset);
}

//
ResultsLog::ResultsLog(void)
{
}

ResultsLog::~ResultsLog(void)
{
   Save();
}

void ResultsLog::AddActionResults(int a_actionType, shared_ptr<ResultsLog::SingleResult> a_spSingleResult)
{
   m_mapActionResults[a_actionType].insert(a_spSingleResult);
}

BOOL ResultsLog::Save()
{
   BOOL bSaved = FALSE;

   CString strDirectory = Misc::GetBinFolder();

   if(!strDirectory.IsEmpty())
   {
      strDirectory.Append(GenerateLogFileName());

//      path.Append( GenerateLogFileName() );

      TextFile logFile;
      if(logFile.OpenForWritting(strDirectory))
      {
         // write log file header
         logFile.Write(_T("<?xml version=\"1.0\"?>\n"));
         logFile.Write(_T("<GMMA_Actions_Results>\n"));

         TResultsMap::iterator itActionId2Results = m_mapActionResults.begin();
         while(itActionId2Results!=m_mapActionResults.end())
         {
            CString strTextEntry;
            int     resultIdx       = 0;

            // write action header
            strTextEntry.Format(_T("\t<Action name=\"%s\">\n"), ActionId2ActionName(itActionId2Results->first));
            logFile.Write(strTextEntry);

            TResultsSet::iterator itSingleResult = itActionId2Results->second.begin();
            while(itSingleResult != itActionId2Results->second.end())
            {
               (*itSingleResult)->WriteToFile(logFile, 2, resultIdx);
               
               ++itSingleResult;
               ++resultIdx;
            }

            // write action footer
            logFile.Write(_T("\t</Action>\n"));
            
            ++itActionId2Results;
         }

         // write log file footer
         logFile.Write(_T("</GMMA_Actions_Results>\n"));

         logFile.Close();

         bSaved = TRUE;
      }
   }

   ASSERT(bSaved);

   return bSaved;
}

CString ResultsLog::ActionId2ActionName(int a_actionType) const
{
   switch(a_actionType)
   {
   case IDC_NEAR_BUY_SIGNAL:
      return _T("Blisko sygna�u kupna");

   case IDC_BEGINING_OF_TREND:
      return _T("Pocz�tek trendu wzrostowego");

   default:
      return _T("AKCJA NIEZDEFINIOWANA");
   }
}

CString ResultsLog::GenerateLogFileName() const
{
   COleDateTime date;
   date = COleDateTime::GetCurrentTime();

   CString strFileName;
   strFileName.Format(_T("GMMA result %d%2.2d%2.2d_%2.2d%2.2d%2.2d.txt"), date.GetYear(), date.GetMonth(), date.GetDay(),
                                                                      date.GetHour(), date.GetMinute(), date.GetSecond());
   return strFileName;
}