#pragma once
#include "XMLItem.h"
#include "IXMLDataObserver.h"


class XMLAttrib;


class FOUNDATION_LIB XMLNode : public XMLItem
{
public:
   XMLNode(LPCTSTR name);
   virtual ~XMLNode(void);

   void addParam(LPCTSTR paramaName, LPCTSTR paramValue);

   void addNode(shared_ptr<XMLNode> node);

   shared_ptr<XMLNode> addNode(LPCTSTR nodeName, LPCTSTR nodeText);

   void setText(LPCTSTR nodeText);

   const CString& getText() const;

   void traverseNode(IXMLDataObserver* observer) const;

private:
   vector<shared_ptr<XMLNode>> _nodes;
   vector<shared_ptr<XMLAttrib>> _attribs;
   CString _text;
};



