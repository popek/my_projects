#include "LexFileReader.h"

namespace regexlib
{

LexFileReader::LexFileReader(eRegExModel modelType) : m_ModelType(modelType)
{
}

LexFileReader::~LexFileReader(void)
{
}

bool LexFileReader::Read(const char* pFilePath)
{
   FILE* pFile;
   char* pBuf;
   unsigned int uiMark = 0;

   //initialize members
   _uiAuxCnt = -1;
   m_ComplexSymbolsDefs.clear();
   m_RegExDefs.clear();
   m_mapChar2SymbolID.clear();
   fopen_s(&pFile, pFilePath, "r");

   if (pFile)
   {
      pBuf = new char[1024];

      while (fgets(pBuf, 1024, pFile))
      {
         if (*pBuf == '\n')
            continue;

         if (pBuf[0] == '%' && pBuf[1] == '%')
         {
            uiMark++;
            continue;
         }

         if (uiMark == 1)//reading first part (definitions of symbols)
            ReadSymbolDefinition(pBuf);

         if (uiMark == 2)//reading second part (definitions of regular expression)
            ReadRegExDefinition(pBuf);
      }

      // add two "special symbols" which have to be predefined (since they are keywords
//         AddPredefinedSymbols();

      delete pBuf;
      return true;
   }
   else
      return false;
}

void LexFileReader::ReadSymbolDefinition(char *pSymDefString)
{
   unsigned int i = 0, j = 0, k = 0;
   char sSubDef[64];
   std::set<unsigned char> setChars;
   char sSymbolName[256];

   char cAuxLast = 0;
   bool bDefinitionStarted = false;
   do
   {
      if (*(pSymDefString + i) != ' ' && *(pSymDefString + i) != '\t') // omit spaces and tabs
      {
         if (bDefinitionStarted)
         {
            if (*(pSymDefString + i) == ',') //check if sub-definition finished
            {
               GenerateSymbols(sSubDef, j, setChars);
               j = 0;
            }
            else
               sSubDef[j++] = *(pSymDefString + i);
         }
         else
         {
            if (pSymDefString[i] == '>' && cAuxLast == '-') // check if definition started
            {
               sSymbolName[k - 1] = '\0';
               bDefinitionStarted = true;
            }
            else
            {
               cAuxLast = pSymDefString[i];
               sSymbolName[k++] = pSymDefString[i];//save next char of symbol name
            }
         }
      }

   } while ((pSymDefString[i++]) != '\0');

   m_ComplexSymbolsDefs[sSymbolName] = setChars;//add symbol definition
   m_mapComplexSymbolName2SymbolID[sSymbolName] = _uiAuxCnt;

   auto it = setChars.begin();
   while (it != setChars.end())
   {
      if (m_mapChar2SymbolID.find(*it) == m_mapChar2SymbolID.end())
         m_mapChar2SymbolID[*it] = m_ModelType == eREM_SimpleRegEx ? *it : _uiAuxCnt;

      it++;
   }

   _uiAuxCnt--;

}

void LexFileReader::GenerateSymbols(char* sSymbolSubDef, unsigned int uiAuxLen, std::set<unsigned char>& setChars)
{
   //check if user defined a special character
   if (uiAuxLen == 4 &&
      sSymbolSubDef[0] == '"' &&
      sSymbolSubDef[1] == '\\' &&
      sSymbolSubDef[3] == '"')
   {
      switch (*(sSymbolSubDef + 2))
      {
      case 'n':
         setChars.insert('\n');
         return;
      case 't':
         setChars.insert('\t');
         return;
      case 'r':
         setChars.insert('\r');
         return;
      case 's': // I can't put simple space like: " ", becuase spaces are removed in "father" method
         setChars.insert(' ');
         return;
      }
   }
   ///
   while (uiAuxLen)
   {
      if (*sSymbolSubDef == '-')
      {
         char cLower, cHigher, cCnt;
         if (*(sSymbolSubDef - 1) < *(sSymbolSubDef + 1))
         {
            cLower = *(sSymbolSubDef - 1);
            cHigher = *(sSymbolSubDef + 1);
         }
         else
         {
            cLower = *(sSymbolSubDef + 1);;
            cHigher = *(sSymbolSubDef - 1);;
         }

         for (cCnt = cLower; cCnt <= cHigher; cCnt++) //add new symbols/characters
            setChars.insert(cCnt);
      }
      else
         setChars.insert(*sSymbolSubDef);

      uiAuxLen--;
      sSymbolSubDef++;
   }
}

void LexFileReader::ReadRegExDefinition(char *pRegExDefString)
{
   unsigned int i = 0, k = 0;
   char sSymbolName[256];

   char cAuxLast = 0;
   bool bDefinitionStarted = false;
   do
   {
      if (*(pRegExDefString + i) != ' ' && *(pRegExDefString + i) != '\t') // omit spaces and tabs
      {
         if (bDefinitionStarted)
         {
            ParseRegExDefinition(sSymbolName, pRegExDefString + i);
            break;
         }
         else
         {
            if (pRegExDefString[i] == '>' && cAuxLast == '-') // check if definition started
            {
               sSymbolName[k - 1] = '\0';
               bDefinitionStarted = true;
            }
            else
            {
               cAuxLast = pRegExDefString[i];
               sSymbolName[k++] = pRegExDefString[i];//save next char of symbol name
            }
         }
      }

   } while ((pRegExDefString[i++]) != '\0');
}

void LexFileReader::ParseRegExDefinition(std::string strName, std::string strRegEx)
{
   unsigned int ui, uiSize, uiNextWordStart = 0;
   uiSize = strRegEx.size();
   std::vector<std::string> vecRegExSubstrings;
   bool bStartMarked = false;

   strRegEx = strRegEx.substr(0, uiSize - 1); // remove last '\n' character
   uiSize--;
   for (ui = 0; ui < uiSize; ui++)
   {
      switch (strRegEx[ui])
      {
         // 			case 10: //new line character should be ignored
         // 				break;

      case '(':
      case ')':
      case '*':
      case '|':
      case ' ':
         if (bStartMarked)
         {
            GenerateRegExSubstrings(strRegEx.substr(uiNextWordStart, ui - uiNextWordStart), vecRegExSubstrings);
            uiNextWordStart = 0;
         }
         if (strRegEx[ui] != ' ')
         {
            vecRegExSubstrings.push_back(" ");
            vecRegExSubstrings[vecRegExSubstrings.size() - 1][0] = strRegEx[ui];
         }

         bStartMarked = false;
         break;

      default:
         if (!bStartMarked)
         {
            uiNextWordStart = ui;
            bStartMarked = true;
         }
      }
   }

   if (bStartMarked)
      GenerateRegExSubstrings(strRegEx.substr(uiNextWordStart, ui - uiNextWordStart), vecRegExSubstrings);

   m_RegExDefs.push_back(t_RegExName2RegExSub(strName, vecRegExSubstrings)); //add parsed regex definition
}

void LexFileReader::GenerateRegExSubstrings(std::string strSimpleRegEx, std::vector<std::string>& vecRegExSubstrings)
{
   unsigned int ui;

   if (m_ComplexSymbolsDefs.find(strSimpleRegEx) == m_ComplexSymbolsDefs.end())// 
   {
      for (ui = 0; ui < strSimpleRegEx.size(); ui++)
      {
         vecRegExSubstrings.push_back(" ");
         vecRegExSubstrings[vecRegExSubstrings.size() - 1][0] = strSimpleRegEx[ui];

         if (m_mapChar2SymbolID.find(strSimpleRegEx[ui]) == m_mapChar2SymbolID.end())
            m_mapChar2SymbolID[strSimpleRegEx[ui]] = strSimpleRegEx[ui]; // in case of simple symbols (characters) char = ID
      }
   }
   else
      vecRegExSubstrings.push_back(strSimpleRegEx);
}

void LexFileReader::AddPredefinedSymbols()
{
   m_ComplexSymbolsDefs["#DASH#"].insert('-');
   m_ComplexSymbolsDefs["#COMMA#"].insert(',');
}

} // namespace regexlib