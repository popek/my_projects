#include "SymbolUniqueKey.h"
#include "Symbol.h"

namespace lrlib
{

SymbolUniqueKey::SymbolUniqueKey(const Symbol& a_symbol)
{
   m_symbolName = a_symbol.name();
}
SymbolUniqueKey::SymbolUniqueKey(const std::string& a_symbolName)
{
   m_symbolName = a_symbolName;
}
SymbolUniqueKey::~SymbolUniqueKey(void)
{
}

bool SymbolUniqueKey::operator < (const SymbolUniqueKey& a_keyToCompare) const
{
   return (strcmp(m_symbolName.c_str(), a_keyToCompare.m_symbolName.c_str()) < 0);
}

} // namespace lrlib