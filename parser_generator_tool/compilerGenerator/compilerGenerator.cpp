// compilerGenerator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <LRLib/Grammar.h>

using namespace std;

int main(int argc, char* argv[])
{
   if (argc != 3)
   {
      printf("ERROR: usage is LR.exe [grammarFilePath] [DirectoryToGenerateParserClasses]\n");
   }
   else
   {
      lrlib::Grammar g(argv[1], argv[2]);

      g.ShowErrors();
   }

   return 0;
}

