#pragma once
#include "ControlsLib.h"
#include "DrawItemDescriptor.h"
#include "GraphicItemWrapper.h"
#include "Asset.h"

class BackgroundPainter;
class KandlePainter;
class ValueScalePainter;
class TimeScalePainter;
class PointerWrapper;

class CONTROLS_LIB ChartControl : public CWnd
{
   class ChartItemSnapshot
   {
   public:
      int m_nItemIdx;
      
      int m_nItemLeftSidePos;
   };

	DECLARE_DYNAMIC(ChartControl)
   

public:
	ChartControl();
	virtual ~ChartControl();
   
   BOOL Create(int a_nID, CWnd* a_pParent);
   BOOL Create(CWnd* a_pParent);

   void SetAsset(CAsset& a_rAsset);

protected:

   BOOL Initialise(CRect& a_rcClientRect, int a_nID, CWnd* a_pParent);
   
   void RecalcChartArea(CRect& a_rcClientRect);

   void GenerateDrawDescriptors();

   void RecalculateDrawDescriptors();

   void Draw(CDC& a_cdc);

   void DrawEmptyChart(CDC& a_cdc);
   
   void UpdateDimensions();
   void UpdateWidth();
   void UpdateHeight();

   void MoveScroll(int a_nBarID, int a_nPixelsToMove);
   
   void UpdateScroll(int a_nSB);

   void FillItemSnapshotInfo(const CPoint& a_Point, ChartItemSnapshot& a_ItemSnapshot);
   
   int  ItemLeftPos(int a_nItemIdx);

	DECLARE_MESSAGE_MAP()
   afx_msg void OnPaint();
   afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
   afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
   afx_msg BOOL OnEraseBkgnd(CDC* pDC);
   afx_msg void OnSize(UINT nType, int cx, int cy);
   afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);

protected:

   boost::shared_ptr<BackgroundPainter> m_pBackgroundPainter; 
   boost::shared_ptr<KandlePainter>     m_pKandlePainter;                 
   boost::shared_ptr<ValueScalePainter> m_pValueScalePainter;
   boost::shared_ptr<TimeScalePainter>  m_pTimeScalePainter;

   vector<CDrawItemDescriptor> m_vDrawDescriptors;

   vector<GraphicItemWrapper>  m_vItemsWrapper;
   
   vector<boost::shared_ptr<PointerWrapper>> m_vPointerWrappers;

   CAsset*                     m_pAssetObj;

   int   m_nChartWidth;
   int   m_nChartHeight;

   int   m_nItemWidth;

   double m_dHeightChangeFactor;
   
   double m_dWidthChangeFactor;

   bool              m_bUpdateHorzScroll;
   ChartItemSnapshot m_oItemSnapshot;

   bool              m_bUpdateVertScroll;
   int               m_nVertBarOffset;

   int    m_nItemsWithExtendedWidthAmount;
};