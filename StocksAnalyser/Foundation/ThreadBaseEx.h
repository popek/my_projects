/**
 * @file
 * @author  Marcin Wrobel <marwrobel@gmail.com>
 *
 * @section DESCRIPTION
 * thread which handles it's own messages
 */
 
#pragma once
#include "FoundationLib.h"
#include "ThreadBase.h"
#include "IThreadMessageHandler.h"

class FOUNDATION_LIB ThreadBaseEx : public ThreadBase, public IThreadMessageHandler
{
public:
   ThreadBaseEx(void);
   virtual ~ThreadBaseEx(void);
};
