#pragma once
#include "AssetFinderBase.h"


class IDoubleVector;
class DataSeries;


class DoubleBottomFinder : public AssetFinderBase
{
public:
   class Context
   {
   public:
      Context()
      {
         _searchArea = 50;
         _bottomNeigboursMinCount = 2;
         _bottomDepth = 0.02;       // by default it is 2% of bottom value
         _bottomsValueDiff = 0.02;  // abs(bottom1-bottom2)/min(bottom1, bottom2)
      }

      size_t _searchArea;                 // maximum samples from the end which will be searched
      size_t _bottomNeigboursMinCount;    // minimum samples near bottom

      double _bottomDepth;

      double _bottomsValueDiff;
   };

public:
   DoubleBottomFinder(shared_ptr<Context> a_spContext);
   virtual ~DoubleBottomFinder(void);

   virtual bool VerifyAssetConditions();

protected:

   size_t FindBottom(const DataSeries& a_data, size_t& a_bottomLeftBoundry, bool a_checkBothSidesDepth, size_t a_mostRightIndex);

   bool IsBottom(size_t a_potentialBotomIndex, size_t& a_doubleBottomBoundryPoint, const DataSeries& a_data, bool a_checkBothSidesDepth);

   bool CheckPotentialBottomRightSide(size_t a_potentialBottomIndex, size_t a_lastIndex,
                                      const DataSeries& a_data, bool a_checkDepth);

   bool CheckPotentialBottomLeftSide(size_t a_potentialBottomIndex, size_t a_firstIndex,
                                     const DataSeries& a_data, size_t& a_leftBoundryIndex);

   bool IsDeepEnough(size_t a_bottomBottomIndex, size_t a_bottomTopIndex, const DataSeries& a_data);

   size_t DoesItFitToLeftBranch(size_t a_sampleIndex, const DataSeries& a_data);

   size_t DoesItFitToRightBranch(size_t a_sampleIndex, const DataSeries& a_data);

protected:

   shared_ptr<Context>     m_spContext;
};
