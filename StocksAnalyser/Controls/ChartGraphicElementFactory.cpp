#include "StdAfx.h"
#include "ChartGraphicElementFactory.h"
#include "LineGraphicObjectPainter.h"
#include "VolumeGraphicObjectPainter.h"
#include "GraphicObjectBase.h"
#include "LineStyle.h"



ChartGraphicElementFactory* ChartGraphicElementFactory::Sm_pSingleInstance = NULL;

GraphicObjectBase*        G_pEmptyGraphicObject = NULL;
GraphicObjectPainterBase* G_pEmptyGraphicObjectPainter = NULL;

ChartGraphicElementFactory::ChartGraphicElementFactory(void)
{
}
ChartGraphicElementFactory::~ChartGraphicElementFactory(void)
{
}

ChartGraphicElementFactory* ChartGraphicElementFactory::Instance()
{
   if(NULL == Sm_pSingleInstance)
   {
      Sm_pSingleInstance = new ChartGraphicElementFactory();
   }

   return Sm_pSingleInstance;
}

shared_ptr<GraphicObjectBase> ChartGraphicElementFactory::CreateGraphicObject(EChartElementId a_objectId, shared_ptr<GrxStyle> a_style) const
{
   shared_ptr<GraphicObjectBase> spGrx;

   switch(a_objectId)
   {
   case EPoint:
   case ELine:
   case ELineWithPoints:
   case EVolume:
      spGrx = shared_ptr<GraphicObjectBase>(new GraphicObjectBase( CreateGraphicObjectPainter(a_objectId) ));
      break;

   default:
      CONTROLSLIB_ASSERT(false);
      spGrx = shared_ptr<GraphicObjectBase>(G_pEmptyGraphicObject);
   }

   if(spGrx)
   {
      spGrx->Style(a_style);
   }

   return spGrx;
}

shared_ptr<GraphicObjectPainterBase> ChartGraphicElementFactory::CreateGraphicObjectPainter(EChartElementId a_objectId) const
{
   switch(a_objectId)
   {
   case EPoint:
   case ELineWithPoints:
      return shared_ptr<GraphicObjectPainterBase>(new LineGraphicObjectPainter(true));
   case ELine:
      return shared_ptr<GraphicObjectPainterBase>(new LineGraphicObjectPainter(false));
   case EVolume:
      return shared_ptr<GraphicObjectPainterBase>(new VolumeGraphicObjectPainter());
   default:
      CONTROLSLIB_ASSERT(false);
      return shared_ptr<GraphicObjectPainterBase>(G_pEmptyGraphicObjectPainter);
   }
}

shared_ptr<GrxStyle> ChartGraphicElementFactory::CreateDefaultGrxStyle(EChartElementId a_objectId) const
{
   switch(a_objectId)
   {
   case EPoint:
   case ELineWithPoints:
      return shared_ptr<GrxStyle>(new LineStyle());
   case ELine:
      return shared_ptr<GrxStyle>(new LineStyle());
   case EVolume:
      return shared_ptr<GrxStyle>(new LineStyle());
   case EOHLC:
      return shared_ptr<GrxStyle>(new LineStyle());
   default:
      CONTROLSLIB_ASSERT(false);
      return shared_ptr<GrxStyle>((GrxStyle*)NULL);
   }
}