#pragma once
#include "ControlsLib.h"
#include "Asset.h"
#include "IDataRangeObserver.h"
#include "IThreadMessageHandler.h"
#include "CriticalSection.h"
#include "MouseActions.h"


class BackgroundPainter;
class ValueScalePainter;
class TimeScalePainter;
class IDataRangeManager;
class ThreadBase;
class ValueScaleObject;
class TimeScaleContext;
class GraphicObjectBase;
class ChartElementsCollection;
class MinMaxGetter;


class CONTROLS_LIB ChartControl : public CWnd, public IDataRangeObserver, public IThreadMessageHandler
{
	DECLARE_DYNAMIC(ChartControl)

   NONCOPYABLE_TYPE(ChartControl)

public:
	ChartControl();
	virtual ~ChartControl();
   
   BOOL CreateFromPlaceholder(int a_nID, CWnd* a_pParent);

   void SetAsset(shared_ptr<Asset> a_spAsset);
   void SetElementsCollection(shared_ptr<ChartElementsCollection> a_spElements);
   void SetMinMaxGetter(shared_ptr<MinMaxGetter> a_spMinMaxGetter);

   void DataRangeManager(IDataRangeManager* a_pRangeManager);

   IDataRangeManager*  DataRangeManager();

   virtual void OnDataRangeChanged(double a_dNewStartNormalisedPos, double a_dNewFinishNormalisedPos);

   virtual BOOL ProcessThreadMessage(UINT a_messageId);

protected:

   void UpdateAreas();

   void DrawInMemory();

   void RecreateGraphicObjects();

   void RecalculateGraphicObjects(UINT a_eventId);

   void RecalculateOnChartSizeChange();

   void RecalculateOnDataRangeChange();

   void RecalculateOnValueScaleRangeChange();

   BOOL Initialise(CRect& a_rcClientRect, int a_nID, CWnd* a_pParent);

   void Draw(CDC& a_cdc);

   void DrawEmptyChart(CDC& a_cdc, CString a_notificationText);
   
	DECLARE_MESSAGE_MAP()
   afx_msg void OnPaint();
   afx_msg BOOL OnEraseBkgnd(CDC* pDC);
   afx_msg void OnSize(UINT nType, int cx, int cy);
   afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
   afx_msg void OnShowDaysChart();
   afx_msg void OnShowWeeksChart();
   afx_msg void OnShoMonthsChart();
   afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
   afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
   afx_msg void OnMouseMove(UINT nFlags, CPoint point);
   afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);

protected:

   shared_ptr<BackgroundPainter> m_pBackgroundPainter; 
   shared_ptr<ValueScalePainter> m_spValueScalePainter;
   shared_ptr<TimeScalePainter>  m_spTimeScalePainter;

   int                               m_scrollBarHeight;

   shared_ptr<Asset>                 m_spAssetObj;

   shared_ptr<MinMaxGetter>          m_spMinMaxGetter;

   CRect                             m_rcChartArea;

   IDataRangeManager*                m_pRangeManager;

   MouseActions                      m_mouseActions;

   shared_ptr<ThreadBase>            m_spGraphicObjectRefreshingThread;

   CDC                               m_chartDC;
   CBitmap                           m_chartBitmap;

   shared_ptr<ValueScaleObject>      m_spValueScaleObject;
   shared_ptr<TimeScaleContext>      m_spTimeScaleContext;

   shared_ptr<ChartElementsCollection>   m_spElements;

   vector<shared_ptr<GraphicObjectBase>> m_graphicElements;

   bool                                  m_imageIsBeingProcessed;

   CriticalSection                       m_csBufferDCLock;
};