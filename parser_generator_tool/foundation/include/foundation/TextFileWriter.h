#pragma once
#include <string>

class TextFileWriter
{
public:
   TextFileWriter(std::string a_textFilePath);
   virtual ~TextFileWriter(void);

   void WriteText(const char* a_format, ...);

private:
   FILE* _file;
};

