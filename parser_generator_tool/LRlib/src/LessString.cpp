#include "LessString.h"

namespace lrlib
{

bool LessString::operator()(const std::string& s1, const std::string& s2) const
{
   return (strcmp(s1.c_str(), s2.c_str()) < 0);
}

} // namespace lrlib