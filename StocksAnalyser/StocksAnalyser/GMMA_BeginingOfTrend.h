#pragma once
#include "GMMA_Signal.h"

class GMMA_BeginingOfTrend : public GMMA_Signal
{
public:

   class Context
   {
   public:
      enum EAlgorithType
      {
         ECheckOnlyBuyPeriod,
         ECheckAllPeriodsSimple,
         ECheckAllPeriods
      };

   public:
      Context();

      void Update(size_t a_shortMeansAboveLongMeansPeriodMin, 
                  size_t a_shortMeansAboveLongMeansPeriodMax,
                  size_t a_breakingPeriod,
                  size_t a_shortMeansBelowLongMeansPeriod,
                  bool   a_weeklyAssetConfirmation,
                  bool   a_checkWeekSignal,
                  EAlgorithType a_algType);

      size_t ShortMeansAboveLongMeansPeriod(bool a_getRangeStart) const;

      size_t BreakingPeriod() const;

      size_t ShortMeansBelowLongMeansPeriod() const;


      bool WeeklyAssetConfirmationRequired() const;

      bool CheckWeekSignal() const;

      EAlgorithType AlgorithType() const;

   protected:
      size_t _shortMeansAboveLongMeansPeriodMin;
      size_t _shortMeansAboveLongMeansPeriodMax;
      size_t _breakingPeriod;
      size_t _shortMeansBelowLongMeansPeriod;
      bool   _weeklyAssetConfirmation;
      bool   _checkWeekSignal;
      EAlgorithType _algType;
   };

public:
   GMMA_BeginingOfTrend(shared_ptr<AssetContainer> a_spAssetContainer, shared_ptr<Context> a_spContext);

   virtual ~GMMA_BeginingOfTrend(void);

   virtual bool VerifyAssetConditions();

protected:

   bool     VerifyDailyAssetCondition();

   bool     VerifyWeeklyAggregatedAssetCondition() const;

   bool     VerifyOnlyBuyPeriod();
   bool     VerifyAllPeriods(bool a_simpleWay);

   size_t   ShortMeansComingThroughLongMeans(const size_t a_offsetStart, const size_t a_offsetEnd) const;
   size_t   SampleIsBrakingThrough(const size_t a_offset) const;

   bool     ShortMeanSampleBelowLongMeanSample(const size_t a_offset) const;

   bool     ShortMeanSampleAboveLongMeanSample(const size_t& a_ffset) const;

   bool     ShortMeanSampleNearLongMeanSampleAndValueSampleAboveMaxMean(const size_t& a_offset) const;

protected:
   shared_ptr<Context> m_spContext;
};
