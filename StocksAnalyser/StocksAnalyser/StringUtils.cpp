#include "StdAfx.h"
#include "StringUtils.h"

StringUtils::StringUtils(const CString& a_strText)
: m_strText(a_strText)
{
}
StringUtils::~StringUtils(void)
{
}

StringUtils::operator size_t ()
{
   if(!ValidCharsForNumeric())
   {
      throw std::exception();
   }

   int value = _ttoi(m_strText);

   if(0 == value)
   {
      throw std::exception();
   }

   return (size_t)value;
}

StringUtils::operator double ()
{
   if(!ValidCharsForDouble())
   {
      throw std::exception();
   }

   double value = _tstof(m_strText);

   if(0.0 == value)
   {
      throw std::exception();
   }

   return value;
}

bool StringUtils::ValidCharsForDouble() const
{
   static CString strValidDoubleChars = _T("-+0123456789.");

   return ValidateChars(strValidDoubleChars);
}

bool StringUtils::ValidCharsForNumeric() const
{
   static CString strValidNumericChars = _T("-+0123456789");
   
   return ValidateChars(strValidNumericChars);
}

bool StringUtils::ValidateChars(const CString& a_availableChars) const
{
   for(int charIdx=0; charIdx<m_strText.GetLength(); ++charIdx)
   {
      if( a_availableChars.Find(m_strText[charIdx]) < 0 )
      {
         return false;
      }
   }

   return true;
}


