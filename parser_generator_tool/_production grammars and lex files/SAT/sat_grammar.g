SAT_FORMULA -> EXPR_LIST
			|  EXPR_LIST MULTIPLE_SEMICOLON;
			   
EXPR_LIST ->  EXPR
			| EXPR_LIST MULTIPLE_SEMICOLON EXPR;

MULTIPLE_SEMICOLON -> semicolon
					| MULTIPLE_SEMICOLON semicolon;
					
EXPR_NAME_PART -> identifier
				| const_int
				| const_double;
				
EXPR_NAME -> EXPR_NAME_PART
		   | EXPR_NAME EXPR_NAME_PART;
		   
EXPR -> EXPR_NAME assign CMP_EXPR
	  | CMP_EXPR;

SIGNED -> add_op
		| SIGNED add_op;

CMP_EXPR ->   AEXPR_ADD
		    | CMP_EXPR cmp_op AEXPR_ADD;

AEXPR_ADD ->  AEXPR_MUL
			| AEXPR_ADD add_op AEXPR_MUL;

AEXPR_MUL ->  SIGNED_EXPR
			| AEXPR_MUL mul_op SIGNED_EXPR;

SIGNED_EXPR -> BASE_EXPR
            | SIGNED BASE_EXPR;
			
BASE_EXPR ->  CONST_NUMERIC
			| ( EXPR ) 
			| FUNCTION;
			
CONST_NUMERIC -> const_int
			   | const_double
			   | identifier;

SCRIPT_ACCESS -> identifier access_op;
FUNCTION_CALL -> FUNCTION_NAME ( ARG_LIST )
			   | FUNCTION_NAME (  );
FUNCTION -> FUNCTION_CALL
		  | SCRIPT_ACCESS FUNCTION_CALL;

FUNCTION_NAME -> identifier;

ARG_LIST -> ARG
		  | NEXT_ARG_LIST ARG;

NEXT_ARG_LIST -> NEXT_ARG
			| NEXT_ARG_LIST NEXT_ARG;
			
NEXT_ARG -> ARG ,;

ARG -> EXPR
	 | text;
		

