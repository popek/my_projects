#pragma once
#include "FoundationLib.h"


class FOUNDATION_LIB NumericStringParser
{
   enum Token
   {
      ESign,
      EDigit,
      ESeparator,
      EMultiplier,

      EInvalidChar
   };

   enum State
   {
      EStartState,
      EAfterSignState,
      EAfterSeparatorState,
      EFinalState,

      EInvalidState
   };

public:
   NumericStringParser(void);
   virtual ~NumericStringParser(void);

   virtual bool Parse(CString a_text, double& a_value);

protected:

   Token NextToken();
   bool  IsInAcceptingState();

protected:

   TCHAR   m_zero;
   TCHAR   m_nine;
   TCHAR   m_separator;
   TCHAR   m_minus;
   TCHAR   m_plus;

   State   m_state;

   double  m_value;
   double  m_auxValueDivider;

   bool m_negativeValue;
   double m_lastDigit;
   double m_multiplier;

   CString m_strValue;
   int     m_nextTokenIndex;
};
