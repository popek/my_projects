#pragma once
#include "ControlsLib.h"

class CONTROLS_LIB SplitterControl : public CWnd
{
   enum ELayout
   {
      EHorizontal,
      EVertical
   };

	DECLARE_DYNAMIC(SplitterControl)

public:
	SplitterControl();
	virtual ~SplitterControl();

   void SetSplitBarPos(double a_normalisedSplitBarPos);

   BOOL Create(DWORD a_style, CRect a_rcArea, CWnd* a_pParent, UINT a_id);

   void AttachFirst(CWnd* a_pFirstChild);

   void AttachSecond(CWnd* a_pSecondChild);

protected:

   void RefreshChildsPos();

   BOOL OverSplitBar(const CPoint& a_point);

	DECLARE_MESSAGE_MAP()
   afx_msg void OnMouseMove(UINT nFlags, CPoint point);
   afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
   afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
   afx_msg void OnPaint();
   afx_msg void OnSize(UINT nType, int cx, int cy);
   afx_msg BOOL OnEraseBkgnd(CDC* pDC);


protected:

   int m_frameWidth;
   int m_barWidth;

   ELayout m_layout;

   CWnd* m_p1stChild;
   CWnd* m_p2ndChild;

   bool m_draggingOn;

   double m_barPosNrmalised;
};


