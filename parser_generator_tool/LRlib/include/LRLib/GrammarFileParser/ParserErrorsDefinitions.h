#pragma once

#define PE_LEX_ATOM_ERROR             "lexical error while reading  '%s' symbol"
#define PE_GENERIC_ERROR              "generic parse error"
#define PE_INVALID_SYMBOL_DEFINITION  "forbidden symbol definition: '%s'. Terminals should be small letters, or single character, nonterminals should be capital letters"
#define PE_SHOULD_BE_NONTERMINAL      "rule result must be nonterminal. Symbol '%s' is terminal"
#define PE_SYMBOL_LIST_ALREADY_ASSIGNED "production '%s' is already assigned to nonterminal '%s'"
#define PE_NONTERMINAL_NOT_DEFINED      "nonterminal '%s' is not defined"
#define PE_NONTERMINAL_NOT_USED         "nonterminal '%s' is not used"
#define PE_ROOT_TERMINAL_NOT_FOUND      "root nonterminal could not be found"