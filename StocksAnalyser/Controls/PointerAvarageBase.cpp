#include "StdAfx.h"
#include "PointerAvarageBase.h"
#include "DataSeries.h"
#include "ControlsLibUtils.h"

PointerAvarageBase::PointerAvarageBase(int a_nMeanValueCalculationSamplesAmount)
: m_nMeanValueCalculationSamplesAmount(a_nMeanValueCalculationSamplesAmount)
{
   m_spDataSeries = ControlsLibUtils::CreateEmptySeries(m_nMeanValueCalculationSamplesAmount-1);
}
PointerAvarageBase::~PointerAvarageBase(void)
{
}

const DataSeries& PointerAvarageBase::Series(size_t a_seriesId) const
{
   // since avarages has only one series
   return *m_spDataSeries;
}

const size_t PointerAvarageBase::period() const
{
   return m_nMeanValueCalculationSamplesAmount;
}