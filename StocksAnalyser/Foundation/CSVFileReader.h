#pragma once
#include "FoundationLib.h"

class FOUNDATION_LIB CCSVFileReader
{
public:
   typedef vector<string>       TSingleLine;
   typedef vector<TSingleLine>  TStringLines;

   CCSVFileReader(void);
   virtual  ~CCSVFileReader(void);
   void           ReadFromFile(const MY_CHAR* a_pFilePath);
   TSingleLine&  operator [] (size_t a_nLineIdx);
   size_t   Size();
   void           Erase(size_t a_nIdx);

private:
   bool     openCSVFile(const MY_CHAR* a_pFilePath);
   void     closeCSVFile();
   void     readNextLine();
   string   readStringValue(char*& a_pValueStartIdx);
   void     allocateAuxLineBuffer();
   void     freeAuxLineBuffer();
   void     resetInternalData();
   
protected:
   TStringLines m_csvLines;
   FILE*         m_csvFile;
   bool m_bEOL;
   bool m_bEOF;
   bool m_bError;
   char * m_pcAuxLineBuffer;
};
