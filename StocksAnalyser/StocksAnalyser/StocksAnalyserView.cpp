#include "stdafx.h"
#include "StocksAnalyser.h"

#include "StocksAnalyserDoc.h"
#include "StocksAnalyserView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CStocksAnalyserView, ChartView)

BEGIN_MESSAGE_MAP(CStocksAnalyserView, ChartView)
	ON_COMMAND(ID_FILE_PRINT, &ChartView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &ChartView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &ChartView::OnFilePrintPreview)
END_MESSAGE_MAP()


CStocksAnalyserView::CStocksAnalyserView()
{
}

CStocksAnalyserView::~CStocksAnalyserView()
{
}

BOOL CStocksAnalyserView::PreCreateWindow(CREATESTRUCT& cs)
{
	return ChartView::PreCreateWindow(cs);
}

void CStocksAnalyserView::OnDraw(CDC* /*pDC*/)
{
	CStocksAnalyserDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
}


BOOL CStocksAnalyserView::OnPreparePrinting(CPrintInfo* pInfo)
{
	return DoPreparePrinting(pInfo);
}

void CStocksAnalyserView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
}

void CStocksAnalyserView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
}

void CStocksAnalyserView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView)
{
   CView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}

#ifdef _DEBUG
void CStocksAnalyserView::AssertValid() const
{
	ChartView::AssertValid();
}
void CStocksAnalyserView::Dump(CDumpContext& dc) const
{
	ChartView::Dump(dc);
}
CStocksAnalyserDoc* CStocksAnalyserView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CStocksAnalyserDoc)));
	return (CStocksAnalyserDoc*)m_pDocument;
}
#endif //_DEBUG
