#include "Grammar.h"
#include "Rules.h"
#include "SymbolsContainer.h"
#include "Utils.h"
#include "internal/GrammarGenerationobserver.h"

#include <algorithm>

namespace lrlib
{

int    Grammar::m_iDotID = -100;
int    Grammar::m_iEofID = -2;
int    Grammar::m_iExtNontermID = -1;

/*------------------------------------------------------
Method name: Grammar constructor
-------------------------------------------------------*/
/**
* @brief    constructor for reading creating grammar from a file
*
* @params   pG [in] - path to the grammar file
*
*//*--------------------------------------------------*/
Grammar::Grammar(const char* pGrammarDefinitionFile, const char* pPathForCodeGeneration)
   : m_oCodeGenerator(this)
   , grammarFileParser_(std::make_unique<CompileContext>(
      symbolsContainer_, rulesContainer_))
   , createObserver_([]() { return std::make_unique<helper::GrammarGenerationTimings>(); })
{
   if (grammarFileParser_.Init(pGrammarDefinitionFile))
   {
      if (true == grammarFileParser_.Parse())
      {
         ExtendGrammar();
         // generates parsers (right now only onle SLR(1), in the future LALR(1) is also planned)
         GenerateParsers();
         // creates cpp classes
         m_oCodeGenerator.GenerateAll(pPathForCodeGeneration);
      }
   }
}

Grammar::~Grammar(void)
{
}

void Grammar::ShowErrors()
{
   for (size_t nErrorIdx = 0; nErrorIdx < m_deqErrors.size(); ++nErrorIdx)
   {
      printf("%d Error in Grammar: %s\n", nErrorIdx, m_deqErrors[nErrorIdx].c_str());
   }

   m_oCodeGenerator.ShowErrors();
   grammarFileParser_.ShowErrors();
}

/*------------------------------------------------------
Method name: PropagateSymbols
-------------------------------------------------------*/
/**
* @brief    auxiliary method for getting out containers from object
*           TODO: in the future containers should be removed and
*           object SymbolsContainer should be used as member of Grammar
*//*--------------------------------------------------*/
void Grammar::PropagateSymbols()
{
   m_setGrammarSymbols = symbolsContainer_.symbols();

   // fill auxiliary maps
   std::for_each(m_setGrammarSymbols.begin(), m_setGrammarSymbols.end(),
      [this](t_setSymbol::value_type& key2symbol)
   {
      PropagateSymbol(Utils::SymbolRef(key2symbol));
   });
}
void Grammar::PropagateSymbol(Symbol& symbol)
{
   m_mapAuxSymbID2Sym[symbol.id()] = &symbol;
   m_mapAuxSymbName2Sym[symbol.name()] = &symbol;
}

/*------------------------------------------------------
Method name: PropagateRules
-------------------------------------------------------*/
/**
* @brief    auxiliary method for getting out containers from object
*           TODO: in the future containers should be removed and
*           object Rules should be used as member of Grammar
*//*--------------------------------------------------*/
void Grammar::PropagateRules()
{
   m_mmapRules = rulesContainer_.RulesByIDs();
}

/*------------------------------------------------------
Method name: ExtendGrammar
-------------------------------------------------------*/
/**
* @brief  auxiliary method for extending grammar (creates extended root nonterminal and eof terminal)
*
*//*--------------------------------------------------*/
void Grammar::ExtendGrammar()
{
   // find root symbol
   auto rootNonterm = std::find_if(symbolsContainer_.Begin(), symbolsContainer_.End(),
      [](t_setSymbol::value_type& key2symbol)
   {
      return Utils::SymbolRef(key2symbol).rootSymbol();
   });

   std::string exRootName = Utils::SymbolRef(rootNonterm).name() + "'";

   // insert extended nonterminal
   symbolsContainer_.Insert(Symbol(exRootName, m_iExtNontermID, false, -1));

   // insert eof terminal
   symbolsContainer_.Insert(Symbol("�", m_iEofID, true, -1));

   PropagateSymbols();

   PropagateRules();

   // insert extended non terminal rule
   std::vector<int> exRootRuleRightSide{ Utils::SymbolRef(rootNonterm).id(), m_iEofID };
   m_mmapRules.insert(t_mmapInt2VecInt::value_type(m_iExtNontermID, exRootRuleRightSide));
}

/*------------------------------------------------------
Method name: GenerateParsers
-------------------------------------------------------*/
/**
* @brief    generates SLR(1) parser. In the future also LALR(1) parser will be added
*
*//*--------------------------------------------------*/
void Grammar::GenerateParsers()
{
   t_setOfMMap allStates;
   std::vector<int> vecAuxRulesIDs;

   std::map<t_mmapInt2VecInt, int> mmapState2Idx;

   auto observer = createObserver_();

   observer->onGrammarGenerationStarted();

   //clear states
   m_vecLR0States.clear();
   //clear actions
   m_vecStateActions.clear();

   observer->onStateGenerationStarted();

   //generate q0 state
   m_vecLR0States.push_back(Closure(m_iExtNontermID, m_mmapRules.begin()->second, 0));
   allStates.insert(m_vecLR0States.back());

   observer->onStateGenerationFinished();

   observer->onStateGenerationStarted();

   //   generate other states
   for (size_t i = 0; i < m_vecLR0States.size(); ++i)
   {
      m_vecStateActions.push_back(std::make_unique<StateActions>(m_setGrammarSymbols.size() - 1, this)); // -1 because extended grammar nonterminal is not counted

      for (auto GSit = m_setGrammarSymbols.begin(); GSit != m_setGrammarSymbols.end(); ++GSit)
      {
         if (Utils::SymbolPtr(GSit)->id() == m_iExtNontermID) // we don't want to check what to do when extended grammar nonterminal appears
            continue;

         observer->onGotoGenerationStarted();

         auto targetState = Goto(m_vecLR0States[i], Utils::SymbolRef(GSit));

         observer->onGotoGenerationFinished();

         if (targetState.size()) // means that shift to some state appears
         {
            if (CheckAcceptance(targetState)) //if acceptance appears, don't shift. Just reduce with rule 0
            {
               vecAuxRulesIDs.push_back(0);
               m_vecStateActions[i]->AddReduceAction(Utils::SymbolPtr(GSit)->id(), vecAuxRulesIDs);

               vecAuxRulesIDs.clear();
               continue;
            }

            allStates.insert(targetState);

            if (allStates.size() > m_vecLR0States.size()) // if new state was "generated" then add it
            {
               auto stateIdx = m_vecLR0States.size();

               mmapState2Idx.insert(std::map<t_mmapInt2VecInt, int>::value_type(targetState, stateIdx));
               
               //add new symbol  
               m_vecLR0States.push_back(targetState);

               observer->onStateGenerationFinished();

               observer->onStateGenerationStarted();
            }

            //add shift action
            m_vecStateActions[i]->AddShiftAction(Utils::SymbolPtr(GSit)->id(), mmapState2Idx[targetState]);
         }

         //check if reduction appears
         if (CheckSLR1Reduction(m_vecLR0States[i], vecAuxRulesIDs, Utils::SymbolRef(GSit)))
            m_vecStateActions[i]->AddReduceAction(Utils::SymbolPtr(GSit)->id(), vecAuxRulesIDs);
         vecAuxRulesIDs.clear();
      }
   }
   observer->onGrammarGenerationFinished();
}

/*------------------------------------------------------
Method name: CheckAcceptance
-------------------------------------------------------*/
/**
* @brief    auxiliary method for checking if state is accept state
*
* @params   mmapState [in] - state to be examinde
*
* @return  true - state is acceptance state
*          false - state is not acceptance state
*
*//*--------------------------------------------------*/
bool Grammar::CheckAcceptance(t_mmapInt2VecInt& mmapState)
{
   bool bAcceptanceState = false;
   if (mmapState.size() == 1)
   {
      t_mmapInt2VecInt::iterator it = mmapState.begin();
      if (it->first == m_iExtNontermID)
      {
         std::vector<int>& vec = it->second;
         if (vec.size() == 3)
         {
            if (vec[0] == 0 &&
               vec[1] == m_iEofID &&
               vec[2] == m_iDotID)
               bAcceptanceState = true;
         }
      }
   }

   return bAcceptanceState;
}


/*------------------------------------------------------
Method name: Closure
-------------------------------------------------------*/
/**
* @brief    Closure operation for one production/rule
*           (if iDotIdx = -1 it means that vecWord
*           has got dot inside. Other way it means dot isn't there)
*
* @params   iProductionID [in] - left-side symbol id
*           vecWord [in] - right-side grammar word
*           iDotIdx [in] - auxiliary variable for marking dot position in the word
*           bClearAuxSet [in] - auxiliary flag for clearing auxiliary set
*
* @return  t_mmapInt2VecInt - set of closure items
*
*//*--------------------------------------------------*/
t_mmapInt2VecInt Grammar::Closure(int iProductionID, std::vector<int> vecWord, int iDotIdx/* = -1*/, bool bClearAuxSet/* = true*/)
{
   t_mmapInt2VecInt mmapRetState;
   t_mmapInt2VecInt mmapAux;
   t_mmapInt2VecInt::iterator mmapAuxIter;
   int iAux;

   if (bClearAuxSet)
      m_setAuxRuleAlreadyChecked.clear();

   if (iDotIdx < 0) //means dot is already inside vecWord
   {
      iDotIdx = GetDotIdx(vecWord);
      vecWord.erase(vecWord.begin() + iDotIdx);
   }

   if (iDotIdx < vecWord.size()) //dot not on the end of word
   {
      if (!(m_mapAuxSymbID2Sym[vecWord[iDotIdx]]->term()))
      {
         mmapAuxIter = m_mmapRules.find(vecWord[iDotIdx]);
         iAux = Iter2Index(mmapAuxIter);

         if (m_setAuxRuleAlreadyChecked.find(iAux) == m_setAuxRuleAlreadyChecked.end()) // was not checked yet
            while (mmapAuxIter != m_mmapRules.end() &&
               mmapAuxIter->first == vecWord[iDotIdx])
            {
               m_setAuxRuleAlreadyChecked.insert(iAux);
               mmapAux = Closure(mmapAuxIter->first, mmapAuxIter->second, 0, false);
               for (t_mmapInt2VecInt::iterator it = mmapAux.begin(); it != mmapAux.end(); it++)
                  mmapRetState.insert(*it);

               mmapAuxIter++;
            }
      }
   }

   vecWord.insert(vecWord.begin() + iDotIdx, m_iDotID);
   mmapRetState.insert(t_mmapInt2VecInt::value_type(iProductionID, vecWord));

   return mmapRetState;
}

/*------------------------------------------------------
Method name: Closure
-------------------------------------------------------*/
/**
* @brief    Closure of set of items
*
* @params   mmapProductions [in] - set of grammar items (rules)
*
* @return  t_mmapInt2VecInt - set of Closure items
*
*//*--------------------------------------------------*/
t_mmapInt2VecInt Grammar::Closure(t_mmapInt2VecInt mmapProductions)
{
   t_mmapInt2VecInt mmapRetState;
   t_mmapInt2VecInt mmapAux;
   t_mmapInt2VecInt::iterator mmapAuxIter;

   if (!mmapProductions.size())// if there is no production then return empty state
      return mmapRetState;

   mmapAuxIter = mmapProductions.begin();

   mmapAux = Closure(mmapAuxIter->first, mmapAuxIter->second);
   for (t_mmapInt2VecInt::iterator it = mmapAux.begin(); it != mmapAux.end(); it++)
      mmapRetState.insert(*it);
   mmapAuxIter++;

   for (; mmapAuxIter != mmapProductions.end(); mmapAuxIter++)
   {
      mmapAux = Closure(mmapAuxIter->first, mmapAuxIter->second, -1, false);
      for (t_mmapInt2VecInt::iterator it = mmapAux.begin(); it != mmapAux.end(); it++)
         mmapRetState.insert(*it);
   }

   return mmapRetState;
}

/*------------------------------------------------------
Method name: Succ
-------------------------------------------------------*/
/**
* @brief    Succed function
*
* @params   mmapProductions [in] - set of items on which Succed will be processed
*           GrammarSymbol [in] - grammar symbol
*
* @return  Succed set of items (rules)
*
*//*--------------------------------------------------*/
t_mmapInt2VecInt Grammar::Succ(t_mmapInt2VecInt& mmapProductions, Symbol& GrammarSymbol)
{
   return Succ(mmapProductions, GrammarSymbol.id());
}

/*------------------------------------------------------
Method name: Succ
-------------------------------------------------------*/
/**
* @brief    Succed function
*
* @params   mmapProductions [in] - set of items on which Succed will be processed
*           iSymbolID [in] - grammar symbol id
*
* @return  Succed set of items (rules)
*
*//*--------------------------------------------------*/
t_mmapInt2VecInt Grammar::Succ(t_mmapInt2VecInt& mmapProductions, int iSymbolID)
{
   t_mmapInt2VecInt mmapRet;
   t_mmapInt2VecInt::iterator mmapAuxIter;
   std::vector<int> vecAuxElement;

   if (!mmapProductions.size())
      return mmapRet;

   mmapAuxIter = mmapProductions.begin();
   for (; mmapAuxIter != mmapProductions.end(); mmapAuxIter++)
   {
      vecAuxElement = FindSuccElement(mmapAuxIter->second, iSymbolID);
      if (vecAuxElement.size())
         mmapRet.insert(t_mmapInt2VecInt::value_type(mmapAuxIter->first, vecAuxElement));
   }

   return mmapRet;
}

/*------------------------------------------------------
Method name: FindSuccElement
-------------------------------------------------------*/
/**
* @brief    auxiliary method for computing Succed function
*
* @params   vecProduction [in] - right-side of rule/production
*           iSymbolID [in] - grammar symbol id
*
* @return   right-side of succed item
*
*//*--------------------------------------------------*/
std::vector<int> Grammar::FindSuccElement(std::vector<int>& vecProduction, int iSymbolID)
{
   unsigned int uiSize = vecProduction.size(), i;
   bool bElementFound = false;
   std::vector<int> vecRet;
   int iDotIdx;

   for (i = 0; i < uiSize; i++)
   {
      if (vecProduction[i] == m_iDotID &&
         i < uiSize - 1) //dot is not at last position
         if (vecProduction[i + 1] == iSymbolID)
         {
            iDotIdx = i;
            bElementFound = true;
            break;
         }
   }
   //move dot
   if (bElementFound)
   {
      vecRet = vecProduction;
      vecRet[iDotIdx] = iSymbolID;
      vecRet[iDotIdx + 1] = m_iDotID;
   }

   return vecRet;
}

/*------------------------------------------------------
Method name: Goto
-------------------------------------------------------*/
/**
* @brief    computes Goto function
*
* @params   mmapProductions [in] - set of items on which goto will be processed
*           GrammarSymbol [in] - grammar symbol
*
* @return  set of Goto items
*
*//*--------------------------------------------------*/
t_mmapInt2VecInt Grammar::Goto(t_mmapInt2VecInt& mmapProductions, Symbol& GrammarSymbol)
{
   return Closure(Succ(mmapProductions, GrammarSymbol.id()));
}

/*------------------------------------------------------
Method name: Goto
-------------------------------------------------------*/
/**
* @brief    computes Goto function
*
* @params   mmapProductions [in] - set of items on which goto will be processed
*           iSymbolID [in] - grammar symbol id
*
* @return  set of Goto items
*
*//*--------------------------------------------------*/
t_mmapInt2VecInt Grammar::Goto(t_mmapInt2VecInt& mmapProductions, int iSymbolID)
{
   return Closure(Succ(mmapProductions, iSymbolID));
}

/*------------------------------------------------------
Method name: Iter2Index
-------------------------------------------------------*/
/**
* @brief    auxiliary method for acquiring index of rule iterator
*
* @params   t_mmapInt2VecInt::iterator [in] - iterator of specific rule
*
* @return  int [out] -
*
*//*--------------------------------------------------*/
int Grammar::Iter2Index(t_mmapInt2VecInt::iterator it)
{
   int iAuxCnt = 0;
   t_mmapInt2VecInt::iterator auxIt = m_mmapRules.begin();

   while (auxIt != it)
   {
      auxIt++;
      iAuxCnt++;
   }

   return iAuxCnt;
}

/*------------------------------------------------------
Method name: GetDotIdx
-------------------------------------------------------*/
/**
* @brief    auxiliary method for acquiring dot index in right-side of an item
*
* @params   t_mmapInt2VecInt::iterator [in] - item
*
* @return  int [out] - dot indx (-1 if dot wasn't found)
*
*//*--------------------------------------------------*/
int Grammar::GetDotIdx(t_mmapInt2VecInt::iterator it)
{
   return GetDotIdx(it->second);
}

/*------------------------------------------------------
Method name: GetDotIdx
-------------------------------------------------------*/
/**
* @brief    auxiliary method for acquiring dot index in right-side of an item
*
* @params   vector<int> [in] - right-side of an item
*
* @return  int [out] - dot indx (-1 if dot wasn't found)
*
*//*--------------------------------------------------*/
int Grammar::GetDotIdx(std::vector<int>& vecWord)
{
   unsigned int iSize = vecWord.size(),
      i = 0;
   while (i < iSize && vecWord[i] != m_iDotID) i++;

   return ((i < iSize) ? i : -1);
}

/*------------------------------------------------------
Method name: FIRST
-------------------------------------------------------*/
/**
* @brief    computes FIRST function
*
* @params   GrammarSymbol [in] - symbol on which FIRST will be computed
*           setSymID [in, out] - container for symbols that are FIRST in GrammarSymbol
*           bJustStarted [in] - auxiliary flag (for resetting auxiliary container)
*
*//*--------------------------------------------------*/
void Grammar::FIRST(Symbol& GrammarSymbol, std::set<int>& setSymID, bool bJustStarted /* = true*/)
{
   //auxiliary set to remember already checked symbols (nonterminals actually)
   static std::set<int> ssetAuxAlreadyChecked;
   if (bJustStarted) ssetAuxAlreadyChecked.clear();

   int iSymID = GrammarSymbol.id();
   if (GrammarSymbol.term())
      setSymID.insert(iSymID);
   else
   {
      //auxiliary set needed so we won't hang when there will be rules e.g. R ->LabS , L->RtbT
      if (ssetAuxAlreadyChecked.find(iSymID) == ssetAuxAlreadyChecked.end())
      {
         //remember symbol (so the FIRST function won't go there for the second time)
         ssetAuxAlreadyChecked.insert(iSymID);

         t_mmapInt2VecInt::iterator it;
         it = m_mmapRules.find(iSymID);
         //here I assume that there won't be any nonterminal that evaluates to empty string
         // that is why only first symbol in word is checked
         while (it != m_mmapRules.end() && it->first == iSymID)
         {
            FIRST(*(m_mapAuxSymbID2Sym[(it->second)[0]]), setSymID, false);
            it++;
         }
      }
   }
}

/*------------------------------------------------------
Method name: FOLLOW
-------------------------------------------------------*/
/**
* @brief    computes FOLLOW function
*
* @params   GrammarSymbol [in] - symbol on which FOLLOW will be computed
*           setSymID [in, out] - container for symbols that FOLLOW GrammarSymbol
*           bJustStarted [in] - auxiliary flag (for resetting auxiliary container)
*
*//*--------------------------------------------------*/
void Grammar::FOLLOW(Symbol& symbol, std::set<int>& setSymID, bool bJustStarted /*=true*/)
{
   //auxiliary set to remember already checked symbols (nonterminals actually)
   static std::set<int> ssetAuxAlreadyChecked;
   if (bJustStarted) ssetAuxAlreadyChecked.clear();

   t_mmapInt2VecInt::iterator it;
   int iSymID = symbol.id(), iAuxSize, i;


   /*assuming that only "�" follows head nonterminal was a serious bug. See example below were EXPR is head symbol
   EXPR ->	EXPR_MUL
   | EXPR + EXPR_MUL
   | EXPR - EXPR_MUL
   EXPR_MUL -> EXPR_POW
   | EXPR_MUL * EXPR_POW
   | EXPR_MUL / EXPR_POW
   EXPR_POW -> BASE_EXPR
   | EXPR_POW ^ BASE_EXPR
   BASE_EXPR -> number
   | ( EXPR )
   */
   if (!symbol.term())
   {
      //auxiliary set needed so we won't hang when there will be rules e.g. R ->LabS , L->RtbT
      if (ssetAuxAlreadyChecked.find(iSymID) == ssetAuxAlreadyChecked.end())
      {
         //here I assume that there won't be any nonterminal that evaluates to empty string
         for (it = m_mmapRules.begin(); it != m_mmapRules.end(); it++)
         {
            //remember symbol (so the FOLLOW function won't go there for the second time)
            ssetAuxAlreadyChecked.insert(iSymID);
            std::vector<int>& vecRef = it->second;
            //first case (when R -> abGD, then all in FIRST(D) is in FOLLOW(G))
            iAuxSize = vecRef.size();
            for (i = 0; i < iAuxSize - 1; i++) // we are not interested in last symbol (that is why iAux-1)
               if (vecRef[i] == iSymID)
                  FIRST(*(m_mapAuxSymbID2Sym[vecRef[i + 1]]), setSymID);
            //second case (if R -> abdD, then FOLLOW(R) "belongs" to FOLLOW(D)
            if (vecRef[iAuxSize - 1] == iSymID)
               FOLLOW(*(m_mapAuxSymbID2Sym[it->first]), setSymID, false);
         }
      }
   }
}

/*------------------------------------------------------
Method name: CheckSLR1Reduction
-------------------------------------------------------*/
/**
* @brief    method for checking if operation in LR(0) state is reduction
*
* @params   mmapState [in] - state
*           vecReductionRulesIDs [in, out] - container for reduction rules
*           LookAheadSymbol [in] - lookahead symbol
*
*//*--------------------------------------------------*/
bool Grammar::CheckSLR1Reduction(t_mmapInt2VecInt& mmapState, std::vector<int>& vecReductionRulesIDs, Symbol& LookAheadSymbol)
{
   unsigned int uiSize = mmapState.size(), uiAuxRCnt = 0;
   t_mmapInt2VecInt::iterator it;
   for (it = mmapState.begin(); it != mmapState.end(); it++)
   {
      std::vector<int>& vec = it->second;
      //if dot on the end...
      if (vec[vec.size() - 1] == m_iDotID)
      {
         //check if LooAheadSymbol is in FOLLOW(it->first)
         if (CheckIfSymbolIsInFOLLOW(LookAheadSymbol.id(), it->first))
         {
            vecReductionRulesIDs.push_back(GetRuleIdx(it));
            uiAuxRCnt++;
         }
      }
   }

   return (uiAuxRCnt ? true : false);
}

/*------------------------------------------------------
Method name: CheckIfSymbolIsInFOLLOW
-------------------------------------------------------*/
/**
* @brief    auxiliary method for checking if lookahead symbol can
*           FOLLOW grammar symbol
*
* @params   iLookAheadID [in] - lookahead symbol id
*           iSymbolToFoolowID [in] - grammar symbol
*
* @return   true - lookahead FOLLOWs grammar symbol
*           false - lookahead doesn not FOLLOW grammar symbol
*
*//*--------------------------------------------------*/
bool Grammar::CheckIfSymbolIsInFOLLOW(int iLookAheadID, int iSymbolToFoolowID)
{
   bool bLookAhedIfFoolow = true;
   GenerateFOLLOW();

   if (m_mapSymbol2Follow[iSymbolToFoolowID].find(iLookAheadID) == m_mapSymbol2Follow[iSymbolToFoolowID].end())
      bLookAhedIfFoolow = false;

   return bLookAhedIfFoolow;
}

/*------------------------------------------------------
Method name: GenerateFOLLOW
-------------------------------------------------------*/
/**
* @brief    auxiliary method for computing FOLLOW for all grammar symbols
*
*//*--------------------------------------------------*/
void Grammar::GenerateFOLLOW()
{
   static bool bAlreadyGenerated = false;

   if (!bAlreadyGenerated)
   {
      int iSize = m_mapAuxSymbID2Sym.size() - 2, i;//-2 because I start from second element (first -1), and second element is -1 (second -1)
      std::set<int> SymIDs;

      for (i = -1; i < iSize; i++)
      {
         FOLLOW(*(m_mapAuxSymbID2Sym[i]), SymIDs);
         m_mapSymbol2Follow.insert(t_mapInt2SetInt::value_type(i, SymIDs));
         SymIDs.clear();
      }
      bAlreadyGenerated = true;
   }
}

/*------------------------------------------------------
Method name: GetRuleIdx
-------------------------------------------------------*/
/**
* @brief    auxiliary method for acquiring rule index.
*           Difference between this method and Iter2Index is
*           that here item is an argument ( dot can be found inside of
*           right-side)
*
* @params   it [it] - item
*
* @return   int [out] - index of a rule
*
*//*--------------------------------------------------*/
int Grammar::GetRuleIdx(t_mmapInt2VecInt::iterator it)
{
   std::vector<int> vecWord = it->second;
   unsigned int uiSize = vecWord.size(), i;
   t_mmapInt2VecInt::iterator auxIt;
   int iRuleID = 0;
   bool bFounded = false;

   //first remove dot
   for (i = 0; i < uiSize; i++)
      if (vecWord[i] == m_iDotID)
      {
         vecWord.erase(vecWord.begin() + i);
         break;
      }

   for (auxIt = m_mmapRules.begin(); auxIt != m_mmapRules.end(); auxIt++)
   {
      if (auxIt->first == it->first &&
         auxIt->second == vecWord)
      {
         bFounded = true;
         break;
      }
      iRuleID++;
   }

   return (bFounded ? iRuleID : -1);
}

std::string Grammar::GetSymbolName(int iSymID)
{
   return m_mapAuxSymbID2Sym[iSymID]->name();
}

} // namespace lrlib