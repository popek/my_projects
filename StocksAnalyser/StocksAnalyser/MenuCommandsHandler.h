#pragma once


class MenuCommandsHandler : public CCmdTarget
{
	DECLARE_DYNAMIC(MenuCommandsHandler)

public:
	MenuCommandsHandler();
	virtual ~MenuCommandsHandler();

protected:
	DECLARE_MESSAGE_MAP()
public:
   afx_msg void OnShowToolsWindow();
   afx_msg void OnUpdateShowToolsWindow(CCmdUI *pCmdUI);
   afx_msg void OnShowStocksToBuy();
   afx_msg void OnUpdateShowStocksToBuy(CCmdUI *pCmdUI);
   afx_msg void OnAssetslistUpdate();
   afx_msg void OnUpdateAssetslistUpdate(CCmdUI *pCmdUI);
   afx_msg void OnShowStrategyTester();
   afx_msg void OnUpdateShowStrategyTester(CCmdUI *pCmdUI);
   afx_msg void OnShowMarketScanner();
   afx_msg void OnUpdateShowMarketScanner(CCmdUI *pCmdUI);
};


