#pragma once
#include "ControlsLib.h"
#include "IPointer.h"


class EMA_pointer;


class CONTROLS_LIB MACD_pointer : public IPointer
{
public:
   enum ESeriesId
   {
      ELongEMA,
      EShortEMA,
      EMACD,
      ESignal
   };

public:
   MACD_pointer();
   virtual ~MACD_pointer(void);

private:
   void Init(size_t a_shortEmaPeriod, size_t a_longEmaPeriod, size_t a_singalPeriod);

public:
   virtual void   Create(const DataSeries& a_inputData);

   virtual const DataSeries& Series(size_t a_seriesId) const;

   size_t Period(ESeriesId a_seriesId) const;

private:
   shared_ptr<EMA_pointer> _shortEMA;
   shared_ptr<EMA_pointer> _longEMA;

   shared_ptr<DataSeries>  _spMacdSeries;
   shared_ptr<EMA_pointer> _signal;
};
