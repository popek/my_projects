#pragma once

#include <string>

namespace lrlib
{

class Symbol;

class SymbolUniqueKey
{
public:
   SymbolUniqueKey(const Symbol& a_symbol);
   SymbolUniqueKey(const std::string& a_symbolName);
   virtual ~SymbolUniqueKey(void);

   bool operator < (const SymbolUniqueKey& a_keyToCompare) const;

private:
   std::string m_symbolName;
};

} // namespace lrlib