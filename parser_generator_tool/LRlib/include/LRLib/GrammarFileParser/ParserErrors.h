#pragma once

#include <deque>

namespace lrlib 
{

class ParserErrors
{
   class CError
   {
   public:
      CError(std::string, int);

      std::string m_strErrorDescription;
      int m_nLineNo;
   };

public:
   ParserErrors(void);
   virtual ~ParserErrors(void);

   void AddError(std::string, int);
   void AddFormatedError0(std::string a_strErrorDescription, int a_nLineNo, std::string a_AuxMiddleString);
   bool DoesErrorExist();   
   
   void Clear();

   void ShowErrors();

protected:
   std::deque<CError>  m_deqErrors;
};

} // namespace lrlib
