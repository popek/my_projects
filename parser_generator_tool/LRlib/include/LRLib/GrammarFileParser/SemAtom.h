#pragma once
#include "LexAtom.h"

namespace lrlib
{

class SemAtom
{
public:
   SemAtom(void);
   SemAtom(LexAtom lexAtom);
   virtual ~SemAtom(void);

   void operator = (const SemAtom& sa);
   LexAtom m_LexAtom;
};

} // namespace lrlib