#include "StateActions.h"
#include "Grammar.h"


#define A_SHIFT  0x01
#define A_REDUCE 0x10

namespace lrlib
{

StateActions::StateActions(int iSymbolsAmount, Grammar* pG)
   :m_pGrammar(pG)
{
   std::vector<int> vecEmptyVector; // for initializing reduction rules
   m_iSymbolsAmount = iSymbolsAmount;

   while (iSymbolsAmount--)
   {
      m_vecGotoStateIDs.push_back(-1); // initializing
      m_vecReductions.push_back(vecEmptyVector);
      m_vecActionIDs.push_back(0); //initializing (no action defined)
   }
}

StateActions::~StateActions(void)
{
}

/*------------------------------------------------------
Method name: AddShiftAction
-------------------------------------------------------*/
/**
* @brief    adds shift action for a grammar symbol
*
* @params   iSymbolID [in] - grammar symbol id
*           iGotoStateID [in] - index of state to which parser will shift
*
*//*--------------------------------------------------*/
void StateActions::AddShiftAction(int iSymbolID, int iGotoStateID)
{
   int iIdx = GetSymbolIDIdx(iSymbolID);
   m_vecGotoStateIDs[iIdx] = iGotoStateID;
   m_vecActionIDs[iIdx] |= A_SHIFT;
}

/*------------------------------------------------------
Method name: AddReduceAction
-------------------------------------------------------*/
/**
* @brief    adds reduce action for a grammar symbol
*
* @params   iSymbolID [in] - grammar symbol id
*           vecReductionsIDs [in] - container with rules ids to which parser will reduce
*                                   Usually vector contain only one elemet. But in case
*                                   reduce/reduce conflict appears vector is needed)
*
*//*--------------------------------------------------*/
void StateActions::AddReduceAction(int iSymbolID, std::vector<int> vecReductionsIDs)
{
   int iIdx = GetSymbolIDIdx(iSymbolID);
   m_vecReductions[iIdx] = vecReductionsIDs;
   m_vecActionIDs[iIdx] |= A_REDUCE;
}

/*------------------------------------------------------
Method name: GetSymbolIDIdx
-------------------------------------------------------*/
/**
* @brief    auxiliary method for getting symbol ID index
*
*//*--------------------------------------------------*/
int StateActions::GetSymbolIDIdx(int iSymbolID)
{
   return (iSymbolID < 0 ? m_iSymbolsAmount - 1 : iSymbolID);
}

std::string StateActions::GetActionDebugInfo(int idx)
{
   std::string strOut = "";
   char * pAuxBuf = new char[20];

   if (!m_vecActionIDs[idx])
      strOut = "x";
   else
   {
      if (m_vecActionIDs[idx] & A_SHIFT)
      {
         sprintf(pAuxBuf, "s%d", m_vecGotoStateIDs[idx]);
         strOut = pAuxBuf;
      }
      if (m_vecActionIDs[idx] & A_REDUCE)
      {
         std::vector<int>& vec = m_vecReductions[idx];
         unsigned int uiSize = vec.size(), i;
         for (i = 0; i < uiSize; i++)
         {
            sprintf(pAuxBuf, "r%d", vec[i]);
            strOut += pAuxBuf;
         }
      }
   }
   delete pAuxBuf;
   return strOut;
}

int StateActions::GetShiftStateID(int idx)
{
   if (m_vecActionIDs[idx] & A_SHIFT)
      return m_vecGotoStateIDs[idx];

   return -1;
}

int StateActions::GetReductionID(int idx)
{
   if (m_vecActionIDs[idx] & A_REDUCE)
      return (m_vecReductions[idx])[0];

   return -1;
}

std::string StateActions::GetConflictInfo()
{
   std::string strOut = "";
   char * pbuf = new char[1024];

   for (int i = 0; i < m_iSymbolsAmount; i++)
   {
      if (m_vecActionIDs[i] & A_REDUCE &&
         m_vecActionIDs[i] & A_SHIFT)
      {
         sprintf(pbuf, "shift/reduce conflict on '%s' terminal : shift to S%d, reduce with R%d\n",
            m_pGrammar->GetSymbolName(i == m_iSymbolsAmount - 1 ? Grammar::m_iEofID : i).c_str(),
            m_vecGotoStateIDs[i], (m_vecReductions[i])[0]);
         strOut += pbuf;
         continue;
      }

      if (m_vecActionIDs[i] & A_REDUCE &&
         m_vecReductions[i].size() > 1)
      {
         sprintf(pbuf, "reduce/reduce conflict on '%s' terminal : reduce with R%d, reduce with R%d\n",
            m_pGrammar->GetSymbolName(i == m_iSymbolsAmount - 1 ? Grammar::m_iEofID : i).c_str(),
            (m_vecReductions[i])[0], (m_vecReductions[i])[1]);
         strOut += pbuf;
      }
   }

   return strOut;
}

} // namespace lrlib