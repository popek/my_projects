#pragma once
#include "ControlsLib.h"
#include "KandleDate.h"
#include "CSVFileReader.h"

class CONTROLS_LIB Kandle
{
   enum
   {
      eDateIdx = 1,
      eOpenIdx,
      eHighIdx,
      eLowIdx,
      eCloseIdx,
      eVolumeIdx
   };

public:
   Kandle(CCSVFileReader::TSingleLine & a_CVSKandleRepresentation, size_t a_kandleIndex);
   virtual ~Kandle(void);

   shared_ptr<KandleDate> Date() const;

   double Open() const;

   double Close() const;

   double High() const;

   double Low() const;

   size_t Volume() const;

   double TurnOver() const;

   double MediumPrice() const;

   void Concatenate(const Kandle& a_kandleToConcatenate);

   shared_ptr<Kandle> Clone(size_t a_cloneIndex) const;

   static shared_ptr<Kandle> CreateNullObject(const KandleDate& a_spDate);

protected:
   Kandle(shared_ptr<KandleDate> a_spDate);
   Kandle(const Kandle& a_kandleToCopy);
   Kandle& operator = (const Kandle& a_kandleToCopy);

   double String2Double(string& a_strDouble);
   
   int    String2Int(string& a_strInt);

protected:   
   double m_dOpen;
   double m_dClose;
   double m_dHigh;
   double m_dLow;

   double m_mediumPrice;

   size_t m_nVolume;

   double m_dTheoreticalTurnover;

   shared_ptr<KandleDate> m_spDate;
};