#include "stdafx.h"
#include "StocksAnalyser.h"
#include "BeginingOfTrendParamsDlg.h"
#include "StringUtils.h"
#include "Misc.h"

const BOOL G_radioIndex_checkBuyPeriod = 0;
const BOOL G_radioIndex_checkAllPeriodsSimple = 1;
const BOOL G_radioIndex_checkAllPeriods = 2;

IMPLEMENT_DYNAMIC(BeginingOfTrendParamsDlg, PropertyDialogBase)

BeginingOfTrendParamsDlg::BeginingOfTrendParamsDlg()
   : PropertyDialogBase(BeginingOfTrendParamsDlg::IDD)
   , m_spContext(new GMMA_BeginingOfTrend::Context())
   , m_shortAboveLongMinimalPeriod(_T(""))
{
   switch(m_spContext->AlgorithType())
   {
   case GMMA_BeginingOfTrend::Context::ECheckOnlyBuyPeriod:
      m_algorithTypeIndex = G_radioIndex_checkBuyPeriod;
      break;

   case GMMA_BeginingOfTrend::Context::ECheckAllPeriodsSimple:
      m_algorithTypeIndex = G_radioIndex_checkAllPeriodsSimple;
      break;

   case GMMA_BeginingOfTrend::Context::ECheckAllPeriods:
      m_algorithTypeIndex = G_radioIndex_checkAllPeriods;
      break;
   }

   m_shortAboveLongMinimalPeriod = Misc::ToString((int)m_spContext->ShortMeansAboveLongMeansPeriod(false));
   m_shortAboveLongPeriodRange.Format(_T("%d-%d"), (int)m_spContext->ShortMeansAboveLongMeansPeriod(true), 
                                                   (int)m_spContext->ShortMeansAboveLongMeansPeriod(false) );
   m_breakingThorughPeriod = Misc::ToString((int)m_spContext->BreakingPeriod());
   m_shortBelowLongPeriod  = Misc::ToString((int)m_spContext->ShortMeansBelowLongMeansPeriod());

   m_weeklyConfirmation = m_spContext->WeeklyAssetConfirmationRequired() ? TRUE : FALSE;

   m_checkWeekSignal = m_spContext->CheckWeekSignal();
}
BeginingOfTrendParamsDlg::~BeginingOfTrendParamsDlg()
{
}

BOOL BeginingOfTrendParamsDlg::Create(CWnd* a_parent)
{
   return __super::Create(BeginingOfTrendParamsDlg::IDD, a_parent);
}

void BeginingOfTrendParamsDlg::DoDataExchange(CDataExchange* pDX)
{
   PropertyDialogBase::DoDataExchange(pDX);
   DDX_Text(pDX, IDC_MIN_SHORT_ABOVE_LONG, m_shortAboveLongMinimalPeriod);
   DDX_Text(pDX, IDC_SHORT_ABOVE_LONG, m_shortAboveLongPeriodRange);
   DDX_Text(pDX, IDC_BREAK_THROUGH, m_breakingThorughPeriod);
   DDX_Text(pDX, IDC_SHORT_BELOW_LONG, m_shortBelowLongPeriod);
   DDX_Check(pDX, IDC_WEEKLY_CONFIRMATION, m_weeklyConfirmation);
   DDX_Radio(pDX, IDC_CHECK_ONLY_BUY_PERIOD, m_algorithTypeIndex);
   DDX_Check(pDX, IDC_CHECK_WEEK_SIGNAL, m_checkWeekSignal);

   GMMA_BeginingOfTrend::Context::EAlgorithType currentAlgType = CurrentAlgorithType();

   if(TRUE == pDX->m_bSaveAndValidate)
   {
      int linePos = m_shortAboveLongPeriodRange.Find(_T('-'));

      m_spContext->Update( 
         Misc::ToInt(m_shortAboveLongPeriodRange.Mid(0, linePos + 1)),
         
         Misc::ToInt(
         currentAlgType == GMMA_BeginingOfTrend::Context::ECheckOnlyBuyPeriod ? 
            m_shortAboveLongMinimalPeriod : 
            m_shortAboveLongPeriodRange.Mid(linePos + 1)
                     ),
         
         Misc::ToInt(m_breakingThorughPeriod),
         Misc::ToInt(m_shortBelowLongPeriod),
         m_weeklyConfirmation ? true : false, 
         m_checkWeekSignal ? true : false, 
         CurrentAlgorithType()
         );
   }

   if(FALSE == pDX->m_bSaveAndValidate)
   {
      switch(currentAlgType)
      {
      case GMMA_BeginingOfTrend::Context::ECheckOnlyBuyPeriod:
         GetDlgItem(IDC_MIN_SHORT_ABOVE_LONG)->EnableWindow(TRUE);
         GetDlgItem(IDC_SHORT_ABOVE_LONG)->EnableWindow(FALSE);
         GetDlgItem(IDC_BREAK_THROUGH)->EnableWindow(FALSE);
         GetDlgItem(IDC_SHORT_BELOW_LONG)->EnableWindow(FALSE);
         break;

      case GMMA_BeginingOfTrend::Context::ECheckAllPeriodsSimple:
         GetDlgItem(IDC_MIN_SHORT_ABOVE_LONG)->EnableWindow(FALSE);
         GetDlgItem(IDC_SHORT_ABOVE_LONG)->EnableWindow(TRUE);
         GetDlgItem(IDC_BREAK_THROUGH)->EnableWindow(FALSE);
         GetDlgItem(IDC_SHORT_BELOW_LONG)->EnableWindow(FALSE);
         break;

      case GMMA_BeginingOfTrend::Context::ECheckAllPeriods:
         GetDlgItem(IDC_MIN_SHORT_ABOVE_LONG)->EnableWindow(FALSE);
         GetDlgItem(IDC_SHORT_ABOVE_LONG)->EnableWindow(TRUE);
         GetDlgItem(IDC_BREAK_THROUGH)->EnableWindow(TRUE);
         GetDlgItem(IDC_SHORT_BELOW_LONG)->EnableWindow(TRUE);
         break;
      }
   }
}

const shared_ptr<GMMA_BeginingOfTrend::Context> BeginingOfTrendParamsDlg::Context(bool a_update)
{
   if(true == a_update)
   {
      UpdateData(TRUE);
   }

   return m_spContext;
}




BEGIN_MESSAGE_MAP(BeginingOfTrendParamsDlg, PropertyDialogBase)
   ON_BN_CLICKED(IDC_CHECK_ONLY_BUY_PERIOD, &BeginingOfTrendParamsDlg::OnBnClickedCheckOnlyBuyPeriod)
   ON_BN_CLICKED(IDC_CHECK_ALL_SIMPLE_WAY, &BeginingOfTrendParamsDlg::OnBnClickedCheckAllSimpleWay)
   ON_BN_CLICKED(IDC_CHECK_ALL_WITH_REGARDS_TO_ALL_PERIODS, &BeginingOfTrendParamsDlg::OnBnClickedCheckAllWithRegardsToAllPeriods)
END_MESSAGE_MAP()




void BeginingOfTrendParamsDlg::OnBnClickedCheckOnlyBuyPeriod()
{
   UpdateData(TRUE);
   UpdateData(FALSE);
}

void BeginingOfTrendParamsDlg::OnBnClickedCheckAllSimpleWay()
{
   UpdateData(TRUE);
   UpdateData(FALSE);
}

void BeginingOfTrendParamsDlg::OnBnClickedCheckAllWithRegardsToAllPeriods()
{
   UpdateData(TRUE);
   UpdateData(FALSE);
}


GMMA_BeginingOfTrend::Context::EAlgorithType BeginingOfTrendParamsDlg::CurrentAlgorithType() const
{
   GMMA_BeginingOfTrend::Context::EAlgorithType algType;
   switch(m_algorithTypeIndex)
   {
   case G_radioIndex_checkBuyPeriod:
      algType = GMMA_BeginingOfTrend::Context::ECheckOnlyBuyPeriod;
      break;

   case G_radioIndex_checkAllPeriodsSimple:
      algType = GMMA_BeginingOfTrend::Context::ECheckAllPeriodsSimple;
      break;

   case G_radioIndex_checkAllPeriods:
      algType = GMMA_BeginingOfTrend::Context::ECheckAllPeriods;
      break;
   }

   return algType;
}