/**
 * @file
 * @author  Marcin Wrobel <marwrobel@gmail.com>
 *
 * @section DESCRIPTION
 * class responsible for downloading zipped assets CSV files
 */
#pragma once
#include "ThreadBase.h"
#include "IThreadMessageHandler.h"

class IInstrumentListUpdateObserver;

class AssetsListDownloader : public IThreadMessageHandler
{
public:
   AssetsListDownloader(IInstrumentListUpdateObserver* a_pUpdateObserver);
   virtual ~AssetsListDownloader(void);

   static void  Init();

   virtual BOOL ProcessThreadMessage(UINT a_messageId);

   void         UpdateAssetsList();

   void         CancelUpdateProcess();

protected:

   bool HandleUpdateAssetsList();

   bool DownloadAssetsZIPFileUsingHTTPSession();

   // -------obsolete--
   bool DownloadAssetsZIPFileUsingDefaultHTTPStreamOpener();
   void DownloadAssetsZIPFileUsingDefaultFTPStreamOpener();
   bool UNZIPFromNet();
   bool Unzip();
   // ------------------
   void CopyStream(std::istream& istr, std::ostream& ostr);

protected:

   IInstrumentListUpdateObserver*   m_pUpdateObserver;

   ThreadBase  m_workingThread;
   
   string      m_strHostURI;
   string      m_strURIPath;

   string      m_strFTPFullURI;

   CString     m_strError;

   bool        m_bCancelUpdate;

   CString _zipFilePath;
   CString _downloadFolder;
};
