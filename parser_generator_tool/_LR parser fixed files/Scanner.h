#pragma once
#include "ETerminalId.h"
#include "typeDefs.h"



// lexical analyzer class
class Scanner
{
public:

   Scanner(string_t a_scriptFilePath);

   //read next token
   ETerminalId GetAtom(LexAtom* a_pLexAtom);

protected:

	//reads current character
	virtual char_t readCharacter() = 0;

	//writes back last character
	virtual void moveToPrevChar() = 0;

   //checks if end of file has been reached
   virtual bool eof() const = 0;

private:
   char_t _token[1024];         // auxiliary container for token string
};
