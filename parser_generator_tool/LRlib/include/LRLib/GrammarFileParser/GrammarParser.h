#pragma once
#include "Scanner.h"
#include "CompileContext.h"
#include "..\auxiliary_types.h"

#include <memory>

namespace lrlib
{

class GrammarParser
{
public:
   GrammarParser(std::unique_ptr<CompileContext>&& context);
   ~GrammarParser() = default;

   bool Init(const char* a_pGrammarDefinitionFile);

   bool Parse();

   void ShowErrors();

private:
   Scanner scanner_;
   std::unique_ptr<CompileContext> context_;
};

} // namespace lrlib