#pragma once
#include "ControlsLib.h"
#include "Kandle.h"
#include "AssetLabel.h"


class ChartElementsCollection;


class CONTROLS_LIB Asset
{
   NONCOPYABLE_TYPE(Asset)

public:
   Asset();
   virtual ~Asset(void);
   void           ReadFromFile(const MY_CHAR* pcFilePath);
   vector<shared_ptr<Kandle>> GrabbKandles(size_t a_nFirstIdx, size_t a_nLastIdx);
   
   void           GetMinMax(double& a_rdMin, double& a_rdMax) const;
   void           GetMinMax(size_t a_nFirstIdx, size_t a_nLastIdx, double& a_rdMin, double& a_rdMax) const;
   
   void           GetVolumeMinMax(double& a_rdMin, double& a_rdMax) const;
   void           GetVolumeMinMax(size_t a_nFirstIdx, size_t a_nLastIdx, double& a_rdMin, double& a_rdMax) const;

   size_t         Size()                           const;
   
   const Kandle&  GetKandle(size_t a_kandleIndex) const;

   Kandle&       operator [] (size_t a_nIdx);
   const Kandle& operator [] (size_t a_nIdx)      const;

   bool           Loaded()                         const;

   void           Loaded(bool a_loaded);

   // finds nearest kandle (grater or equal to reqested one)
   size_t         FindNearestKandle(const KandleDate& a_dateToFind);

   void           AppendKandle(shared_ptr<Kandle> a_spKandle);

   void ExtractSamplesDates(vector<shared_ptr<KandleDate>>& a_datesVector) const;

   AssetLabel&    Label();

   void AttachElementsCollection(shared_ptr<ChartElementsCollection> a_spElementsCollection);

   shared_ptr<ChartElementsCollection> ElementsCollection() const;

protected:
   vector<shared_ptr<Kandle>> m_vKandles;

   mutable double m_dMinVal;
   mutable double m_dMaxVal;

   mutable double m_dMinVolume;
   mutable double m_dMaxVolume;

   bool           m_bLoaded;

   AssetLabel     m_label;

   shared_ptr<ChartElementsCollection> m_spChartElementsCollection; // elements which will be shown on chart
};
