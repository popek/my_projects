#pragma once
#include "ThreadBase.h"
#include "IThreadMessageHandler.h"
#include "ThreadSafeContainer.h"
#include "StrategyParams.h"
#include "BacktestThread.h"


class TradeStrategiesTesterDlg : public CDialog, public BacktestThread::IBacdtestObserver
{
   struct ui_message
   {
      ui_message(int a_id, CString a_text)
      {
         _id = a_id;
         _text = a_text;
      }

      int _id;
      int _value;

      CString _text;
      CString _additionalText;
   };

	DECLARE_DYNAMIC(TradeStrategiesTesterDlg)

public:
	TradeStrategiesTesterDlg();
	virtual ~TradeStrategiesTesterDlg();

	enum { IDD = IDD_TRADE_STRATEGY };

protected:
   virtual void onAssetsAmountEstablished(int assetsToBacktestAmount);

   virtual void onAssetTested(const CString& assetFilePath, shared_ptr<StrategyResult> result);

   virtual void onBactestFinished();

   bool ReadyToProcessAssets();
   void BeforeProcessing();
   bool AssetTurnoverOK(shared_ptr<Asset> a_spAsset) const;

   void ProcessUiMessages();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP()
   afx_msg void OnBnClickedChooseAssetsPath();
   afx_msg void OnBnClickedTestStrategy();
   afx_msg void OnBnClickedStopProcessing();

public:
   CDateTimeCtrl _fromDate;
   CDateTimeCtrl _toDate;
   CEdit      _assetsDirectory;
   CListCtrl  _strategiesList;
   CListCtrl  _resultsList;

   vector<CString> _assetsListMeetingRequirements;
   vector<CString> _assetsListMeetingRequirementsResult;

   BacktestThread          _thread;
   ThreadSafeContainer<shared_ptr<ui_message>> _updateUIMessagesQueue;

   int _assetCounter;
   int _assetToTestAmount;

protected:
   virtual BOOL OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult);
public:
   virtual BOOL OnInitDialog();
   CProgressCtrl _progress;
   afx_msg void OnLvnItemchangedAssetsList(NMHDR *pNMHDR, LRESULT *pResult);
   CEdit _strategyResultLog;
   afx_msg void OnNMDblclkAssetsList(NMHDR *pNMHDR, LRESULT *pResult);
   CDateTimeCtrl _confirmStrategyEndDate;
};
