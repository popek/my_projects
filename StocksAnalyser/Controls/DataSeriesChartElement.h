#pragma once
#include "ControlsLib.h"
#include "ChartElement.h"


class CONTROLS_LIB DataSeriesChartElement : public ChartElement
{
public:
   DataSeriesChartElement(EChartElementId a_elementId, size_t a_pointsCount, size_t a_offset);
   DataSeriesChartElement(EChartElementId a_elementId, size_t a_pointsCount);
   virtual ~DataSeriesChartElement(void);

   virtual shared_ptr<GraphicObjectBase> CreateGraphicObject(const TimeScaleContext& a_timeScaleContext, const ValueScaleContext& a_valueScaleContext) const;

protected:
   size_t FindFirstIndex(const TimeScaleContext& a_timescaleContext) const;

   size_t FindLastIndex(const TimeScaleContext& a_timescaleContext) const;

protected:
   size_t m_offset;     // offset used to calculate first and last visible index
};
