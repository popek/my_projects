#include "StdAfx.h"
#include "SetTimeRangeAction.h"
#include "ChartControl.h"
#include "ClientRect.h"
#include "CommonDefines.h"
#include "IDataRangeManager.h"

SetTimeRangeAction::SetTimeRangeAction(ChartControl* a_pOwner)
   : m_bSetRangeStarted(false)
   , m_XStart(0)
   , m_XFinish(0)
   , m_pOwner(a_pOwner)
   , m_pOldCapture(NULL)
   , m_chartWidth(0)
   , m_selectionColor(60, 128, 128, 255)
{
   ASSERT(m_pOwner);
}
SetTimeRangeAction::~SetTimeRangeAction(void)
{
}

void SetTimeRangeAction::OnLButtonDown(UINT a_nFlags, const CPoint& a_point)
{
   if(!(a_nFlags&MK_SHIFT))
      return;

   if(a_point.x <= m_chartWidth && a_point.x >=0 && false == m_bSetRangeStarted)
   {
      m_pOldCapture = m_pOwner->SetCapture();
      m_bSetRangeStarted = true;

      m_XStart = a_point.x;
   }
}

void SetTimeRangeAction::OnLButtonUp(UINT a_nFlags, const CPoint& a_point)
{
   // reset state if shift is not pressed
   if( FALSE == (a_nFlags&MK_SHIFT) )
   {
      Reset();
      return;
   }

   if(m_bSetRangeStarted)
   {
      Reset();

      UpdateRange( );

      m_pOwner->Invalidate();
   }
}

void SetTimeRangeAction::OnMouseMove(UINT a_nFlags, const CPoint& a_point)
{
   // reset state if shift is not pressed
   if( FALSE == (a_nFlags&MK_SHIFT) )
   {
      Reset();
      return;
   }

   if(m_bSetRangeStarted)
   {
      if(a_point.x < m_chartWidth)
      {
         m_XFinish = a_point.x > 0 ? a_point.x : 0;
      }
      else
      {
         m_XFinish = m_chartWidth;
      }

      m_pOwner->Invalidate();
   }
}

void SetTimeRangeAction::OnSize(int a_chartWidth)
{
   m_chartWidth = a_chartWidth;
}

void SetTimeRangeAction::Draw(CDC& a_dc)
{
   if(!m_bSetRangeStarted)
      return;

   ClientRect  ownerClient(m_pOwner);

   Gdiplus::Graphics graphicDev(a_dc.GetSafeHdc());

   Gdiplus::Rect     currentSelection(m_XFinish >= m_XStart ? m_XStart : m_XFinish, ownerClient.top,
                                      abs(m_XFinish - m_XStart), ownerClient.Height());

   graphicDev.FillRectangle(new Gdiplus::SolidBrush(m_selectionColor), currentSelection);
}

bool SetTimeRangeAction::DrawingRequired() const
{
   return m_bSetRangeStarted;
}

void SetTimeRangeAction::Reset()
{
   if(m_bSetRangeStarted)
   {
      if(GetCapture() == m_pOwner->GetSafeHwnd())
      {
         ReleaseCapture();
         if(m_pOldCapture)
         {
            m_pOldCapture->SetCapture();
            m_pOldCapture = NULL; 
         }
      }

      m_bSetRangeStarted = false;
      m_pOwner->Invalidate();
   }
}

void SetTimeRangeAction::UpdateRange()
{
   IDataRangeManager* pRangeManager = m_pOwner->DataRangeManager();
   ASSERT(pRangeManager);

   if(!pRangeManager)
      return;

   // prapare range start and finish pos
   double dNormalisedSubrangeStartPos  = (double)m_XStart /(double)m_chartWidth;
   double dNormalisedSubrangeFinishPos = (double)m_XFinish/(double)m_chartWidth;
   
   // switch start and finish if statr > finish
   if(dNormalisedSubrangeStartPos > dNormalisedSubrangeFinishPos)
   {
      double startBuffer = dNormalisedSubrangeStartPos;

      dNormalisedSubrangeStartPos  = dNormalisedSubrangeFinishPos;
      dNormalisedSubrangeFinishPos = startBuffer;
   }

   double dNormalizedRange = pRangeManager->GetFinishNormalisedPos() - 
                             pRangeManager->GetStartNormalisedPos();

   // update sub-range start and finish pos
   double dNewNormalizedStartPos = pRangeManager->GetStartNormalisedPos()  + 
                                   dNormalisedSubrangeStartPos*dNormalizedRange;

   double dNewNormalizedStopPos  = pRangeManager->GetStartNormalisedPos() + 
                                   dNormalisedSubrangeFinishPos*dNormalizedRange;

   pRangeManager->ForceSetRange(dNewNormalizedStartPos, dNewNormalizedStopPos);
}