#pragma once
#include "ControlsLib.h"
#include "AssetContainer.h"
#include "CommonTypes.h"


class IBeforeShowChartEvent;


class CONTROLS_LIB ChartDocument : public CDocument
{
protected:
	ChartDocument();
	DECLARE_DYNCREATE(ChartDocument)

public:
	virtual BOOL OnNewDocument();
   
   virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);

	virtual void Serialize(CArchive& ar);

   virtual void CreateAnotherViewForDocument(EAggregationType a_aggregationType);

   static void SetBeforeShowChartEvent(IBeforeShowChartEvent* a_pEvent);

public:
	virtual ~ChartDocument();

protected:
	DECLARE_MESSAGE_MAP()

protected:
   shared_ptr<AssetContainer>    m_spAssetsContainer;

   static IBeforeShowChartEvent* Sm_pBeforeShowAssetEvent;
};


