#pragma once


class IDoubleVector;


class LineCoefficientCalculator
{

public:
   LineCoefficientCalculator(void);
   virtual ~LineCoefficientCalculator(void);

   void Calculate(vector<double> a_arguments, vector<double> & a_values);

   double ValueDeviation(size_t a_valueIndex) const;
   
   double Value(size_t a_valueIndex) const;

   double Dev2Value(size_t a_valueIndex) const;

   double A() const;

   double B() const;

   

protected:

   void Calculate(const IDoubleVector& a_arguments, const IDoubleVector& a_values);

   void Reset();

protected:

   double _a;
   double _b;

   double _S;
   double _Sx;
   double _Sy;
   double _Sxy;
   double _Sxx;
   double _Syy;

   double _meanDeviation;
   vector<double> _valueDeviations;
   vector<double> _values;

   vector<double> _dev2value;
};
