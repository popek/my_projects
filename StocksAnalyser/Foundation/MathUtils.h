#pragma once
#include "FoundationLib.h"


class FOUNDATION_LIB MathUtils
{
public:
   static double Abs(const double& a_value);

   static double Max(const double& a_value1, const double& a_value2);

   static double Min(const double& a_value1, const double& a_value2);
};

