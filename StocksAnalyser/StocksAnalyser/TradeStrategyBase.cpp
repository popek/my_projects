#include "StdAfx.h"
#include "TradeStrategyBase.h"
#include "Asset.h"
#include "StrategyResult.h"
#include "TransactionDetails.h"
#include "templateFunctions.h"
#include "ChartElement.h"
#include "ChartElementsCollection.h"
#include "LineStyle.h"
#include "ColorUtils.h"
#include "StrategyParams.h"


TradeStrategyBase::TradeStrategyBase(shared_ptr<Asset> a_spAsset)
: _spAsset(a_spAsset)
, _maxLost(0.50)
, _tradeNextDayAfterSignal(false)
, _currentState(ESold)
{
   _spParams = make_shared<StrategyParams>(0.06, 0.06, 0.08, 30000);
}
TradeStrategyBase::~TradeStrategyBase(void)
{
}

shared_ptr<StrategyResult> TradeStrategyBase::RunTest( const KandleDate& a_start, const KandleDate& a_finish)
{
   auto spElements = _spAsset->ElementsCollection() ? 
                        _spAsset->ElementsCollection() : 
                        make_shared<ChartElementsCollection>();

   shared_ptr<StrategyResult> spResult(new StrategyResult());
   shared_ptr<TransactionDetails> spBuy;

   _currentState = ESold;

   size_t sampleIndex = _spAsset->FindNearestKandle(a_start);
   size_t endIndex = _spAsset->FindNearestKandle(a_finish);

   double buyPrice = 0;
   double sellPrice = 0;

   for(; sampleIndex<endIndex; ++sampleIndex)
   {
      if(false == _spParams->isParamFulfilled(StrategyParams::EMaxChange, (*_spAsset)[sampleIndex]))
      {
         return nullptr;
      }

      if(false == _spParams->isParamFulfilled(StrategyParams::EValueStability, (*_spAsset)[sampleIndex]))
      {
         return nullptr;
      }

      if(false == _spParams->isParamFulfilled(StrategyParams::EMinTurnover, (*_spAsset)[sampleIndex]))
      {
         return nullptr;
      }

      if(sampleIndex > 0)
      {
         if(false == _spParams->isParamFulfilled(StrategyParams::EMaxDropDown, (*_spAsset)[sampleIndex-1],
                                                                               (*_spAsset)[sampleIndex]))
         {
            return nullptr;
         }
      }



      if(ESold == _currentState)
      {
         if(ShouldBuy(sampleIndex, buyPrice))
         {
            if(_tradeNextDayAfterSignal && sampleIndex < _spAsset->Size() - 1)
            {
               // buy with opening price of next day
               ++sampleIndex;
               buyPrice = (*_spAsset)[sampleIndex].Open();
            }

            spBuy = make_shared<TransactionDetails>((*_spAsset)[sampleIndex].Date(), buyPrice);
            _currentState = EBought;

            spElements->PushBack(CreateSignalChartElement(true, buyPrice, (*_spAsset)[sampleIndex].Date()));
         }
      }
      else if(EBought == _currentState)
      {
         if(ShouldSell(sampleIndex, sellPrice))
         {
            spResult->RegisterEntry( spBuy, shared_ptr<TransactionDetails>
                                            (new TransactionDetails((*_spAsset)[sampleIndex].Date(), sellPrice)) );
            _currentState = ESold;
            
            spElements->PushBack(CreateSignalChartElement(false, sellPrice, (*_spAsset)[sampleIndex].Date()));
         }
         else if( (buyPrice - (*_spAsset)[sampleIndex].Low())/buyPrice >= _maxLost  )
         {
            sellPrice = (1-_maxLost)*buyPrice;
            spResult->RegisterEntry( spBuy, shared_ptr<TransactionDetails>
                                           (new TransactionDetails((*_spAsset)[sampleIndex].Date(), sellPrice)) );
            _currentState = ELossReached;
            spElements->PushBack(CreateSignalChartElement(false, sellPrice, (*_spAsset)[sampleIndex].Date()));
         }
      }
      else if(ELossReached == _currentState)
      {
         // if we encountered lost then we rebuy when price will raise above last buy level
         if((*_spAsset)[sampleIndex].High() > buyPrice)
         {
            double buyFromStrategy = 0;
            if(ShouldBuy(sampleIndex, buyFromStrategy))
            {
               // we leave buy as it was before (we don't take the new level taken from strategy)
               spBuy = shared_ptr<TransactionDetails>
                        (new TransactionDetails((*_spAsset)[sampleIndex].Date(), buyPrice));
               _currentState = EBought;

               spElements->PushBack(CreateSignalChartElement(true, buyPrice, (*_spAsset)[sampleIndex].Date()));
            }
            else
            {
               _currentState = ESold;
            }
         }
      }
   }

   // if asset is bought then wee need to sell it on the end of test
   if(EBought == _currentState)
   {
      sellPrice = (*_spAsset)[sampleIndex].Close();
      spResult->RegisterEntry( spBuy, shared_ptr<TransactionDetails>
                  (new TransactionDetails((*_spAsset)[sampleIndex].Date(), sellPrice)) );
      
      _currentState = ESold;
      spElements->PushBack(CreateSignalChartElement(false, sellPrice, (*_spAsset)[sampleIndex].Date()));
   }

   if(spResult->EntriesCount())
   {
      AddGraphicPresentation(spElements);
      _spAsset->AttachElementsCollection(spElements);
   }

   return spResult;
}

bool TradeStrategyBase::TakePosition(position a_positionType)
{
   double dummy;
   size_t preLatestSampleIndex = 0;
   size_t latestSampleIndex = _spAsset->Size();
   if(latestSampleIndex > 1)
   {
      --latestSampleIndex;
      preLatestSampleIndex = latestSampleIndex - 1;
   }
   
   bool takePosition = false;

   if(ELong == a_positionType)
   {
      takePosition = !ShouldBuy(preLatestSampleIndex, dummy) && ShouldBuy(latestSampleIndex, dummy);
   }
   else if(EShort == a_positionType)
   {
      takePosition = !ShouldSell(preLatestSampleIndex, dummy) && ShouldSell(latestSampleIndex, dummy);
   }
   if(takePosition)
   {
      shared_ptr<ChartElementsCollection> spElements = make_shared<ChartElementsCollection>();
      AddGraphicPresentation(spElements);
      _spAsset->AttachElementsCollection(spElements);
   }

   return takePosition;
}

void TradeStrategyBase::AttachParams(shared_ptr<StrategyParams> a_spParams)
{
   _spParams = a_spParams;
}

shared_ptr<ChartElement> TradeStrategyBase::CreateSignalChartElement(bool a_buySignal, double a_price, shared_ptr<KandleDate> a_spDate)
{
   COLORREF color = ColorUtils::MixColors(a_buySignal ? ColorUtils::EGreen : ColorUtils::ERed, 
                                          ColorUtils::EBlack, 0.7);

   shared_ptr<ChartElement> signalPoint(new ChartElement(EPoint, 1));
   signalPoint->AddPoint(a_spDate, a_price);

   signalPoint->Style()->SetNumericParam(LineStyle::EPointColor, color);
   return signalPoint;
}