#pragma once;

#define  STR_INSTRUMENT_LIST_DOWNLOAD_STATE        _T("Downloading instruments list archive (%d/%d)...")
#define  STR_INSTRUMENT_LIST_UNZIPPING             _T("Unzipping instruments list archive...")
#define  STR_INSTRUMENT_LIST_UPDATE_SUCCESS        _T("Instrument list updated. Press Ok to close dialog")

#define  STR_INSTRUMENT_UNZIPPING_NEXT_INSTRUMENT  _T("(%d/%d) unzipping %s")

#define  STR_INSTRUMENT_LIST_UPDATE_ERROR          _T("Error during instrument list update:\n\n%s\n\nList is OUT of date")
#define  STR_INSTRUMENT_LIST_UPDATE_UNKNOWN_ERROR  _T("unknown error")

#define STR_INSTRUMENT_LIST_UPDATE_CANCELED        _T("Instrument list update canceled.")
