#include "StdAfx.h"
#include "XercescUtils.h"
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XercesDefs.hpp>


XERCES_CPP_NAMESPACE_USE

bool g_XercescInitialised = false;


void XercescUtils::initialize()
{
   if(!g_XercescInitialised)
   {
      XMLPlatformUtils::Initialize();
      g_XercescInitialised = true;
   }
}

void XercescUtils::release()
{
   if(g_XercescInitialised)
   {
      XMLPlatformUtils::Terminate();
      g_XercescInitialised = false;
   }
}