#pragma once

class IInstrumentListUpdateObserver
{
public:
   virtual ~IInstrumentListUpdateObserver(void){};

   virtual void SetRange(int a_assetListSize)                  = 0;

   virtual void SetZippedFilesCount(int a_assetsCount)         = 0;

   virtual void NextFileUnzippedNotification(CString a_strFileName) = 0;

   virtual void OnDataDownloaded(int a_totalDataDownloaded)    = 0;

   virtual void UnzippingStarted()                             = 0;

   virtual void UnzippingFinished()                            = 0;

   virtual void UpdateCanceled()                               = 0;

   virtual  void NotifyError(CString a_strErrorDesc)           = 0;
};
