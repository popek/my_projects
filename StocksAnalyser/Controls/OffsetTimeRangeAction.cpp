#include "StdAfx.h"
#include "OffsetTimeRangeAction.h"
#include "ChartControl.h"
#include "CommonDefines.h"
#include "IDataRangeManager.h"

OffsetTimeRangeAction::OffsetTimeRangeAction(ChartControl* a_pOwner)
: m_pOwner(a_pOwner)
, m_pOldCapture(NULL)
, m_chartWidth(0)
, m_startPos(0)
, m_bActionStarted(false)
{
}
OffsetTimeRangeAction::~OffsetTimeRangeAction(void)
{
}

void OffsetTimeRangeAction::OnLButtonDown(UINT a_nFlags, const CPoint& a_point)
{
   if(a_nFlags&(MK_SHIFT|MK_CONTROL))
   {
      return;
   }

   if(a_point.x < m_chartWidth && a_point.x >= 0)
   {
      m_startPos = a_point.x;

      if(GetCapture() != m_pOwner->GetSafeHwnd())
      {
         m_pOldCapture = m_pOwner->SetCapture();
      }

      m_bActionStarted = true;
   }
}

void OffsetTimeRangeAction::OnLButtonUp(UINT a_nFlags, const CPoint& a_point)
{
   Reset();
}

void OffsetTimeRangeAction::OnMouseMove(UINT a_nFlags, const CPoint& a_point)
{
   if(a_nFlags&(MK_SHIFT|MK_CONTROL))
   {
      Reset();
      return;
   }

   if(m_bActionStarted)
   {
      UpdateRange(a_point.x);
      m_startPos = a_point.x;
   }
}

void OffsetTimeRangeAction::OnSize(int a_chartWidth)
{
   m_chartWidth = a_chartWidth;
}

void OffsetTimeRangeAction::Draw(CDC& a_dc)
{
}

bool OffsetTimeRangeAction::DrawingRequired() const
{
   return false;
}

void OffsetTimeRangeAction::Reset()
{
   if(m_bActionStarted)
   {
      if(GetCapture() == m_pOwner->GetSafeHwnd())
      {
         ReleaseCapture();
      }

      if(m_pOldCapture)
      {
         m_pOldCapture->SetCapture();
         m_pOldCapture = NULL;
      }
   }

   m_bActionStarted = false;
}

void OffsetTimeRangeAction::UpdateRange(const int& a_mouseXpos)
{
   IDataRangeManager* pRangeManager = m_pOwner->DataRangeManager();
   ASSERT(pRangeManager);

   if(!pRangeManager)
      return;

   double dVirtualChartWidht = (double)m_chartWidth/(pRangeManager->GetFinishNormalisedPos() - pRangeManager->GetStartNormalisedPos());

   // prapare range offset ("-" on the begining since when mouse moves right, offsets go to the left)
   double dOffset = -(double)(a_mouseXpos - m_startPos)/dVirtualChartWidht;
   
   double dNewStartPos  = pRangeManager->GetStartNormalisedPos() + dOffset;
   double dNewFinishPos = pRangeManager->GetFinishNormalisedPos() + dOffset;
   
   // verify start and finish pos if they are out of range
   if(dNewStartPos < 0.0)
   {
      dNewStartPos  = 0.0;
      dNewFinishPos = pRangeManager->GetFinishNormalisedPos() - 
                      pRangeManager->GetStartNormalisedPos();
   }
   if(dNewFinishPos > 1.0)
   {
      dNewStartPos  = 1.0 - (pRangeManager->GetFinishNormalisedPos() - 
                             pRangeManager->GetStartNormalisedPos());
      dNewFinishPos = 1.0;
   }

   pRangeManager->ForceSetRange(dNewStartPos, 
                                dNewFinishPos);
}