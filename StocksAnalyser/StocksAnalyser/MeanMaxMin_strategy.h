#pragma once
#include "TradeStrategyBase.h"


class SMA_pointer;
class MAX_pointer;
class MIN_pointer;
class ChartElementsCollection;
class TransactionDetails;


class MeanMaxMin_strategy : public TradeStrategyBase
{
private:
   enum state
   {
      EIdle, 

      EBoughtAboveMean,
      EBoughtAboveLastMinimum,
      ESoldBelowMean,
      ESoldOnCutoffValue,
   };

public:
   MeanMaxMin_strategy(shared_ptr<Asset> spAsset);
   virtual ~MeanMaxMin_strategy(void);

   virtual shared_ptr<StrategyResult> RunTest( const KandleDate& a_start, const KandleDate& a_finish);

protected:
   virtual void AddGraphicPresentation(shared_ptr<ChartElementsCollection> elements);

private:

   shared_ptr<TransactionDetails> handleIdle(size_t candleIndex);

   shared_ptr<TransactionDetails> handleSoldBelowMean(size_t candleIndex);

   shared_ptr<TransactionDetails> handleSoldOnCutoffValue(size_t candleIndex);

   shared_ptr<TransactionDetails> handleBoughtAboveMean(size_t candleIndex);

   shared_ptr<TransactionDetails> handleBoughtAboveLastMinimum(size_t candleIndex);

private:
   state _state;

   shared_ptr<ChartElementsCollection> _elements;

   double _lastTransactionPrice;
   double _maxDecreseFromMaxValue;

   shared_ptr<SMA_pointer> _sma;
   size_t _smaPeriod;

   shared_ptr<MAX_pointer> _maxIndicator;
   shared_ptr<MIN_pointer> _minIndicator;
   size_t _maxAndMinIndicatorPeriod;
};



