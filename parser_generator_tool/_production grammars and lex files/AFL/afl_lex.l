%%
LETTER -> a-z,A-Z,
DIGIT  -> 0-9,
OR	   -> |,
WS     -> "\t", "\s",
CR	   -> "\r",
LF	   -> "\n",
STAR   -> *,
LPAR   -> (,
RPAR   -> ),

%%
l_par					-> LPAR
r_par					-> RPAR
coma					-> ,
dot						-> .
colon				 	-> :
l_square_br				-> [
r_square_br				-> ]
add_op 					-> + | -
assign_op				-> ( = ) | (STAR =) | (/ =) | (% =) | (+ =) | (- =) | (& =) | (OR =)
bit_and_op				-> &
bit_or_op				-> OR
log_and_op				-> (& &)
log_or_op				-> (OR OR)
cmp_eq_op				-> (==) | (!=)
cmp_more_less_op		-> > | (>=) | < | (<=)
const_double 			-> ((DIGIT)* . (DIGIT)(DIGIT)*)
const_int   			-> ((DIGIT)(DIGIT)*)
identifier				-> (LETTER|_)(LETTER|DIGIT|_)*
incr_op					-> (+ +) | (- -)
mul_op					-> STAR | / | %
pow_op					-> ^
semicolon				-> ;
l_br					-> {
r_br					-> }
eol		 		  		-> (CR LF)|LF
blank 	 	      		-> WS WS*
script_code_start		-> < %
script_code_end			-> % >
directive				-> # ((LETTER)(LETTER|DIGIT|_)*)
%%