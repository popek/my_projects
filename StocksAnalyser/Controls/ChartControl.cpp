#include "stdafx.h"
#include "ChartControl.h"
#include "BackgroundPainter.h"
#include "ValueScalePainter.h"
#include "TimeScalePainter.h"
#include "CommonDefines.h"
#include "MemoryDC.h"
#include "SMA_pointer.h"
#include "EMA_pointer.h"
#include "IDataRangeManager.h"
#include "ThreadBase.h"
#include "Resource.h"
#include "WindowMessages.h"
#include "AssetContainer.h"
#include "CommonTypes.h"
#include "SetTimeRangeAction.h"
#include "OffsetTimeRangeAction.h"
#include "ClientRect.h"
#include "ScopeLock.h"
#include "ValueScaleContext.h"
#include "TimeScaleContext.h"
#include "ChartElement.h"
#include "GraphicObjectBase.h"
#include "ChartElementsCollection.h"
#include "MinMaxGetter.h"
#include "ValueScaleObject.h"


const UINT MSG_ON_DATA_RANGE_CHANGED = 1;

const UINT MSG_ON_VALUE_SCALE_RANGE_CHANGED = 2;

const UINT MSG_ON_CHART_SIZE_CHANGED = 3;

const int G_nDefaultValueScaleWidth = 45;

const int G_nDefaultTimeScaleHeight = 25;

IMPLEMENT_DYNAMIC(ChartControl, CWnd)

ChartControl::ChartControl()
: m_pRangeManager(NULL)
, m_spTimeScaleContext(new TimeScaleContext())
, m_spMinMaxGetter(new MinMaxGetter(MinMaxGetter::EGetPriceMinMax))
, m_imageIsBeingProcessed(false)
{
   m_spValueScaleObject = shared_ptr<ValueScaleObject>(new ValueScaleObject(this));

   m_pBackgroundPainter = shared_ptr<BackgroundPainter>( new BackgroundPainter() );
   m_spValueScalePainter = shared_ptr<ValueScalePainter>( new ValueScalePainter() );
   m_spTimeScalePainter  = shared_ptr<TimeScalePainter>( new TimeScalePainter() );

   m_spGraphicObjectRefreshingThread = shared_ptr<ThreadBase>(new ThreadBase(this));
   m_spGraphicObjectRefreshingThread->Create();

   m_spGraphicObjectRefreshingThread->Resume();
}

ChartControl::~ChartControl()
{
}

BOOL ChartControl::CreateFromPlaceholder(int a_nID, CWnd* a_pParent)
{
   if(!a_pParent->GetSafeHwnd())
   {
      return FALSE;
   }

   CRect rcWindow;
   CWnd* pPlaceholder   = a_pParent->GetDlgItem(a_nID);

   if(pPlaceholder == NULL)
   {
      return FALSE;
   }

   pPlaceholder->GetWindowRect(&rcWindow);
   pPlaceholder->DestroyWindow();


   return Initialise(rcWindow, a_nID, a_pParent);

}

void ChartControl::SetAsset(shared_ptr<Asset> a_spAsset)
{
   m_spAssetObj = a_spAsset;

   m_spTimeScaleContext->InitiateTimestampsContainer(*a_spAsset);

   // setup mouse actions
   m_mouseActions.AddAction(shared_ptr<IMouseInteraction>(new SetTimeRangeAction(this)));
   m_mouseActions.AddAction(shared_ptr<IMouseInteraction>(new OffsetTimeRangeAction(this)));

   if( GetSafeHwnd() )
   {
      UpdateAreas();
      m_spGraphicObjectRefreshingThread->PostIfNotInQueue(MSG_ON_DATA_RANGE_CHANGED);
   }
}

void ChartControl::SetElementsCollection(shared_ptr<ChartElementsCollection> a_spElements)
{
   m_spElements = a_spElements;
}

void ChartControl::SetMinMaxGetter(shared_ptr<MinMaxGetter> a_spMinMaxGetter)
{
   m_spMinMaxGetter = a_spMinMaxGetter;
}

void ChartControl::DataRangeManager(IDataRangeManager* a_pRangeManager)
{
   m_pRangeManager = a_pRangeManager;
}

IDataRangeManager*  ChartControl::DataRangeManager()
{
   return m_pRangeManager;
}

void ChartControl::OnDataRangeChanged(double a_dNewStartNormalisedPos, double a_dNewFinishNormalisedPos)
{
   m_spGraphicObjectRefreshingThread->PostIfNotInQueue(MSG_ON_DATA_RANGE_CHANGED);
}

BOOL ChartControl::ProcessThreadMessage(UINT a_messageId)
{
   if(MSG_ON_DATA_RANGE_CHANGED        == a_messageId || 
      MSG_ON_VALUE_SCALE_RANGE_CHANGED == a_messageId || 
      MSG_ON_CHART_SIZE_CHANGED        == a_messageId )
   {
      m_imageIsBeingProcessed = true;

      RecalculateGraphicObjects(a_messageId);
      
      DrawInMemory();

      m_imageIsBeingProcessed = false;

      Invalidate();

      return TRUE;
   }

   return FALSE;
}

BOOL ChartControl::Initialise(CRect& a_rcClientRect, int a_nID, CWnd* a_pParent)
{
   if( CWnd::Create(NULL, NULL, WS_VISIBLE|WS_CHILD, a_rcClientRect, a_pParent, a_nID) )
   {
      return TRUE;
   }
   else
   {
      return FALSE;
   }
}

void ChartControl::UpdateAreas()
{
   GetClientRect(&m_rcChartArea);

   m_spTimeScalePainter->Height( min(m_rcChartArea.Height(), G_nDefaultTimeScaleHeight) );
   m_spTimeScalePainter->VertOffset(m_rcChartArea.Height());
   m_rcChartArea.bottom -= m_spTimeScalePainter->Height();

   m_spValueScalePainter->Height(m_rcChartArea.Height());
   m_spValueScalePainter->Width( m_spValueScalePainter->CalculateMinWidth() );
   m_rcChartArea.right -= m_spValueScalePainter->Width();

   CRect rcValueScaleArea(0, 0, m_spValueScalePainter->Width(), m_rcChartArea.Height());
   rcValueScaleArea.MoveToXY(m_rcChartArea.right, m_rcChartArea.top);
   m_spValueScaleObject->UpdateArea(rcValueScaleArea);
   
   m_mouseActions.OnSize(m_rcChartArea.Width());
}

void ChartControl::DrawInMemory()
{
   if(NULL == m_chartDC.GetSafeHdc())
   {
      VERIFY(  m_chartDC.CreateCompatibleDC(CDC::FromHandle(::GetDC(NULL)))  );
   }

   ClientRect rcClient(this);
   CBitmap* pOldBitmap = NULL;
   {
      LOCK_SCOPE(m_csBufferDCLock);

      // first delete previous "bitmap state" in case it was created before
      if(m_chartBitmap.GetSafeHandle())
      {
         m_chartBitmap.DeleteObject();
      }

      // create appropriate bitmap
      VERIFY(  m_chartBitmap.CreateCompatibleBitmap(CDC::FromHandle(::GetDC(NULL)), rcClient.Width(), rcClient.Height())  );
      // set bitmap into device context
      pOldBitmap = m_chartDC.SelectObject(&m_chartBitmap);

      // draw on into prepared device context
      Draw(m_chartDC);
   }

   // delete old bitmap
   if(pOldBitmap && pOldBitmap->GetSafeHandle())
   {
      pOldBitmap->DeleteObject();
   }
}

void ChartControl::RecreateGraphicObjects()
{
   if(m_spElements)
   {
      m_graphicElements.clear();
      for(size_t elementIndex=0; elementIndex<m_spElements->ElementsCount(); ++elementIndex)
      {
         auto spElementOwnValueScale = m_spElements->GetElement(elementIndex)->OwnValueScaleContext();
         if(spElementOwnValueScale)
         {
            double maxValue = 0;
            double minValue = 0;
            spElementOwnValueScale->CurrentMinMaxValues(maxValue, minValue);
            spElementOwnValueScale->Update(maxValue, minValue, 0, m_rcChartArea.bottom);
         }

         m_graphicElements.push_back( m_spElements->GetElement(elementIndex)->CreateGraphicObject(*m_spTimeScaleContext, *m_spValueScaleObject->Context()) );
      }
   }
}

void ChartControl::RecalculateGraphicObjects(UINT a_eventId)
{
   switch(a_eventId)
   {
   case MSG_ON_CHART_SIZE_CHANGED:
      RecalculateOnChartSizeChange();
      break;

   case MSG_ON_DATA_RANGE_CHANGED:
      RecalculateOnDataRangeChange();
      break;

   case MSG_ON_VALUE_SCALE_RANGE_CHANGED:
      RecalculateOnValueScaleRangeChange();
      break;
   }
}

void ChartControl::RecalculateOnChartSizeChange()
{
   if( NULL != m_spAssetObj && m_spAssetObj->Loaded() )
   {
      double normalisedStartPos = 0;
      double normalisedEndPos = 1;

      if(m_pRangeManager)
      {
         ASSERT(m_pRangeManager->GetStartNormalisedPos()>=0.0);
         ASSERT(m_pRangeManager->GetFinishNormalisedPos()<=1.0);

         normalisedStartPos = m_pRangeManager->GetStartNormalisedPos();
         normalisedEndPos = m_pRangeManager->GetFinishNormalisedPos();
      }

      // update time scale context
      m_spTimeScaleContext->UpdateCoordinates(normalisedStartPos, normalisedEndPos, m_rcChartArea.Width());
      // update value scale context         
      m_spValueScaleObject->Context()->Update( m_spValueScaleObject->CurrentMaximum(), m_spValueScaleObject->CurrentMinimum(), 
                                               0, m_rcChartArea.bottom );

      RecreateGraphicObjects();
   }
}

void ChartControl::RecalculateOnDataRangeChange()
{
   if( NULL != m_spAssetObj && m_spAssetObj->Loaded() )
   {
      double normalisedStartPos = 0;
      double normalisedEndPos = 1;

      if(m_pRangeManager)
      {
         ASSERT(m_pRangeManager->GetStartNormalisedPos()>=0.0);
         ASSERT(m_pRangeManager->GetFinishNormalisedPos()<=1.0);

         normalisedStartPos = m_pRangeManager->GetStartNormalisedPos();
         normalisedEndPos = m_pRangeManager->GetFinishNormalisedPos();
      }
         
      // update time scale context
      m_spTimeScaleContext->UpdateCoordinates(normalisedStartPos, normalisedEndPos, m_rcChartArea.Width());

      // find new min and max values on visible asset range
      double maxValue = 0;
      double minValue = 0;

      m_spMinMaxGetter->GetMinMax( *m_spAssetObj, 
                                    m_spTimeScaleContext->FirstVisibleTimestampIndex(),
                                    m_spTimeScaleContext->LastVisibleTimestampIndex(), 
                                    minValue, maxValue);
      // update value scale context         
      m_spValueScaleObject->Context()->Update( maxValue, minValue, 0, m_rcChartArea.bottom );
      m_spValueScaleObject->SetMostReasonableMinMax(minValue, maxValue);
      // update value scale painter
      m_spValueScalePainter->UpdateMinMax(minValue, maxValue);

      RecreateGraphicObjects();
   }
}

void ChartControl::RecalculateOnValueScaleRangeChange()
{
   ASSERT( NULL != m_spAssetObj && m_spAssetObj->Loaded() );

   // update value scale painter
   m_spValueScalePainter->UpdateMinMax(m_spValueScaleObject->CurrentMinimum(), m_spValueScaleObject->CurrentMaximum());

   RecreateGraphicObjects();
}

void ChartControl::Draw(CDC& a_cdc)
{
   CRect        rcClient;
   GetClientRect(&rcClient);
   // draw background
   m_pBackgroundPainter->Draw( a_cdc, rcClient );
   
   // draw value scale if there is enough space
   if(rcClient.Width() > m_spValueScalePainter->Width())
   {
      m_spValueScalePainter->Draw( a_cdc, rcClient.right - m_spValueScalePainter->Width());
   }

   // draw time scale
   m_spTimeScalePainter->Draw(a_cdc, *m_spTimeScaleContext);

   CRgn rgnChart;
   VERIFY(rgnChart.CreateRectRgnIndirect(m_rcChartArea));
   a_cdc.SelectClipRgn(&rgnChart);

   for(size_t elementIndex=0; elementIndex<m_graphicElements.size(); ++elementIndex)
   {
      m_graphicElements[elementIndex]->Draw(&a_cdc);
   }

   a_cdc.SelectClipRgn(NULL);
}

void ChartControl::DrawEmptyChart(CDC& a_cdc, CString a_notificationText)
{
   CRect        rcClient;
   GetClientRect(&rcClient);

   // draw background
   m_pBackgroundPainter->Draw( a_cdc, rcClient );

   a_cdc.DrawText(a_notificationText, &rcClient, DT_CENTER|DT_VCENTER|DT_SINGLELINE);
}


BEGIN_MESSAGE_MAP(ChartControl, CWnd)
   ON_WM_PAINT()
   ON_WM_ERASEBKGND()
   ON_WM_SIZE()
   ON_WM_CONTEXTMENU()
   ON_COMMAND(ID_DAILY,  ChartControl::OnShowDaysChart)
   ON_COMMAND(ID_WEEKLY, ChartControl::OnShowWeeksChart)
   ON_COMMAND(ID_MONTHLY, ChartControl::OnShoMonthsChart)
   ON_WM_LBUTTONDOWN()
   ON_WM_LBUTTONUP()
   ON_WM_MOUSEMOVE()
   ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()

void ChartControl::OnPaint()
{
   CPaintDC dc(this);
   ClientRect clientArea(this);

   // draw empty chart if no instrument loaded
   if( NULL == m_spAssetObj || false == m_spAssetObj->Loaded() )
   {
      MemoryDC memDC( &dc , this );
      DrawEmptyChart(memDC.BufferedDC(), _T("Nie za�adowano �adnego instrumentu"));
      return;
   }

   if(m_imageIsBeingProcessed)
   {
      MemoryDC memDC( &dc , this );
      DrawEmptyChart(memDC.BufferedDC(), _T("Generowanie wykresu..."));
      return;
   }

   bool bDoubleBufferRequired = m_mouseActions.DrawingRequired();

   MemoryDC* pMemDC = NULL;
   CDC*      pDC    = &dc;

   // if double buffer required then create memory dc to which drawing will be done
   if(bDoubleBufferRequired)
   {
      pMemDC = new MemoryDC(&dc, this);
      pDC    = &pMemDC->BufferedDC();
   }

   // copy chart bitmap to choosen device context
   {
      LOCK_SCOPE(m_csBufferDCLock);
      pDC->BitBlt(0, 0, clientArea.Width(), clientArea.Height(), &m_chartDC, 0, 0, SRCCOPY);
   }

   // draw mouse action on choosen device context

   m_mouseActions.Draw(*pDC);

   // delete mem dc in case it was created
   if(pMemDC)
   {
      delete pMemDC;
   }
}

BOOL ChartControl::OnEraseBkgnd(CDC* pDC)
{
   return TRUE;
}

void ChartControl::OnSize(UINT nType, int cx, int cy)
{
   CWnd::OnSize(nType, cx, cy);
   
   UpdateAreas();

   m_spGraphicObjectRefreshingThread->PostIfNotInQueue(MSG_ON_CHART_SIZE_CHANGED);
}

void ChartControl::OnContextMenu(CWnd* pWnd, CPoint point)
{
   if(!m_spAssetObj || m_spAssetObj->Loaded())
   {
      return;
   }

   CMenu menu;
   VERIFY(menu.LoadMenu(IDR_CHOOSE_PERIOD));

   CMenu* pPopup = menu.GetSubMenu(0);
   ASSERT(pPopup != NULL);

   pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, 
      point.x, 
      point.y,
      this);
}

void ChartControl::OnShowDaysChart()
{
   GetParent()->SendMessage(WM_SHOW_NEW_CHART, eDay, 0);
}

void ChartControl::OnShowWeeksChart()
{
   GetParent()->SendMessage(WM_SHOW_NEW_CHART, eWeek, 0);
}

void ChartControl::OnShoMonthsChart()
{
   GetParent()->SendMessage(WM_SHOW_NEW_CHART, eMonth, 0);
}

void ChartControl::OnLButtonDown(UINT nFlags, CPoint point)
{
   if(ValueScaleObject::EMinMaxChanged == m_spValueScaleObject->OnLButtonDown(nFlags, point))
   {
      m_spGraphicObjectRefreshingThread->PostIfNotInQueue(MSG_ON_VALUE_SCALE_RANGE_CHANGED);
   }
   else
   {
      m_mouseActions.OnLButtonDown(nFlags, point);
   }

   __super::OnLButtonDown(nFlags, point);
}

void ChartControl::OnLButtonUp(UINT nFlags, CPoint point)
{
   if(ValueScaleObject::EMinMaxChanged == m_spValueScaleObject->OnLButtonUp(nFlags, point))
   {
      m_spGraphicObjectRefreshingThread->PostIfNotInQueue(MSG_ON_VALUE_SCALE_RANGE_CHANGED);
   }
   else
   {
      m_mouseActions.OnLButtonUp(nFlags, point);
   }

   __super::OnLButtonUp(nFlags, point);
}

void ChartControl::OnMouseMove(UINT nFlags, CPoint point)
{
   if(ValueScaleObject::EMinMaxChanged == m_spValueScaleObject->OnMouseMove(nFlags, point))
   {
      m_spGraphicObjectRefreshingThread->PostIfNotInQueue(MSG_ON_VALUE_SCALE_RANGE_CHANGED);
   }
   else
   {
      m_mouseActions.OnMouseMove(nFlags, point);
   }

   __super::OnMouseMove(nFlags, point);
}

void ChartControl::OnLButtonDblClk(UINT nFlags, CPoint point)
{
   if(ValueScaleObject::EMinMaxChanged == m_spValueScaleObject->OnLButtonDblClk(nFlags, point))
   {
      m_spGraphicObjectRefreshingThread->PostIfNotInQueue(MSG_ON_VALUE_SCALE_RANGE_CHANGED);
   }

   __super::OnLButtonDblClk(nFlags, point);
}
