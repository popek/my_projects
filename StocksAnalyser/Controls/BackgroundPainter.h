#pragma once

class BackgroundPainter
{
public:
   BackgroundPainter(void);
   virtual ~BackgroundPainter(void);

   virtual void Draw(CDC& a_dc, CRect& a_rcBackGroundArea);

protected:
   
   COLORREF   m_BackgroundColor;
};
