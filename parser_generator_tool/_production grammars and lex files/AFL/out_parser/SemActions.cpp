#include "SemanticHandler.h"
#include "CommonDef.h"

SemActions::SemActions(void)
{
}
SemActions::~SemActions(void)
{
}

void SemActions::DoReduceAction(int a_ruleId)
{
  typedef void (SemActions::*RuleFunction)();

  static RuleFunction Rules[RULES_COUNT]=
  {
    &SemActions::Rule_acceptance,
    &SemActions::Rule_AFL_SCRIPT__to__STMT_LIST,
    &SemActions::Rule_STMT_LIST__to__STMT,
    &SemActions::Rule_STMT_LIST__to__STMT_LIST__STMT,
    &SemActions::Rule_STMT__to__FUN_DEF,
    &SemActions::Rule_STMT__to__RUNTIME_STMT,
    &SemActions::Rule_FUN_DEF__to__FUN_DEF_PREFIX__FUN_DECL__STMT_COMPOUND,
    &SemActions::Rule_RUNTIME_STMT__to__STMT_WHILE,
    &SemActions::Rule_RUNTIME_STMT__to__STMT_FOR_LOOP,
    &SemActions::Rule_RUNTIME_STMT__to__STMT_IF,
    &SemActions::Rule_RUNTIME_STMT__to__STMT_SWITCH,
    &SemActions::Rule_RUNTIME_STMT__to__STMT_COMPOUND,
    &SemActions::Rule_RUNTIME_STMT__to__STMT_SIMPLE,
    &SemActions::Rule_FUN_DEF_PREFIX__to__function,
    &SemActions::Rule_FUN_DEF_PREFIX__to__procedure,
    &SemActions::Rule_FUN_DECL__to__identifier__lpar__ARGS_DECL_LIST__rpar,
    &SemActions::Rule_FUN_DECL__to__identifier__lpar__rpar,
    &SemActions::Rule_STMT_COMPOUND__to__lbracebr__RUNTIME_STMT_LIST__rbracebr,
    &SemActions::Rule_STMT_COMPOUND__to__lbracebr__rbracebr,
    &SemActions::Rule_ARGS_DECL_LIST__to__identifier,
    &SemActions::Rule_ARGS_DECL_LIST__to__ARGS_DECL_LIST__comma__identifier,
    &SemActions::Rule_RUNTIME_STMT_LIST__to__RUNTIME_STMT,
    &SemActions::Rule_RUNTIME_STMT_LIST__to__RUNTIME_STMT_LIST__RUNTIME_STMT,
    &SemActions::Rule_STMT_WHILE__to__WHILE_HEADER__WHILE_LOOP_BODY,
    &SemActions::Rule_STMT_FOR_LOOP__to__FOR_LOOP_HEADER__FOR_LOOP_BODY,
    &SemActions::Rule_STMT_IF__to__IF_HEADER__IF_BLOCK,
    &SemActions::Rule_STMT_IF__to__IF_HEADER__IF_BLOCK__else__IF_BLOCK,
    &SemActions::Rule_STMT_SWITCH__to__SWITCH_HEADER__SWITCH_BODY,
    &SemActions::Rule_STMT_SIMPLE__to__STMT_SIMPLE_LEFT__semicolon,
    &SemActions::Rule_STMT_SIMPLE__to__semicolon,
    &SemActions::Rule_WHILE_HEADER__to__WHILE_HEADER_START__lpar__LEXPR__rpar,
    &SemActions::Rule_WHILE_LOOP_BODY__to__RUNTIME_STMT,
    &SemActions::Rule_WHILE_HEADER_START__to__while,
    &SemActions::Rule_LEXPR__to__LEXPR_OR,
    &SemActions::Rule_LEXPR_OR__to__LEXPR_AND,
    &SemActions::Rule_LEXPR_OR__to__LEXPR_OR__log_or_op__LEXPR_AND,
    &SemActions::Rule_LEXPR_AND__to__LEXPR_NOT,
    &SemActions::Rule_LEXPR_AND__to__LEXPR_AND__log_and_op__LEXPR_NOT,
    &SemActions::Rule_LEXPR_NOT__to__LEXPR_BITOR,
    &SemActions::Rule_LEXPR_NOT__to__log_not_op__LEXPR_BITOR,
    &SemActions::Rule_LEXPR_BITOR__to__LEXPR_BITAND,
    &SemActions::Rule_LEXPR_BITOR__to__LEXPR_BITOR__bit_or_op__LEXPR_BITAND,
    &SemActions::Rule_LEXPR_BITAND__to__LEXPR_CMP_EQ,
    &SemActions::Rule_LEXPR_BITAND__to__LEXPR_BITAND__bit_and_op__LEXPR_CMP_EQ,
    &SemActions::Rule_LEXPR_CMP_EQ__to__LEXPR_CMP_MORE_LESS,
    &SemActions::Rule_LEXPR_CMP_EQ__to__LEXPR_CMP_EQ__cmp_eq_op__LEXPR_CMP_MORE_LESS,
    &SemActions::Rule_LEXPR_CMP_MORE_LESS__to__AEXPR_ADD,
    &SemActions::Rule_LEXPR_CMP_MORE_LESS__to__LEXPR_CMP_MORE_LESS__cmp_more_less_op__AEXPR_ADD,
    &SemActions::Rule_AEXPR_ADD__to__AEXPR_MUL,
    &SemActions::Rule_AEXPR_ADD__to__AEXPR_ADD__add_op__AEXPR_MUL,
    &SemActions::Rule_AEXPR_MUL__to__AEXPR_SIGN,
    &SemActions::Rule_AEXPR_MUL__to__AEXPR_MUL__mul_op__AEXPR_SIGN,
    &SemActions::Rule_AEXPR_SIGN__to__AEXPR_POW,
    &SemActions::Rule_AEXPR_SIGN__to__add_op__AEXPR_POW,
    &SemActions::Rule_AEXPR_POW__to__AEXPR_INCR,
    &SemActions::Rule_AEXPR_POW__to__AEXPR_POW__pow_op__AEXPR_INCR,
    &SemActions::Rule_AEXPR_INCR__to__BASE_EXPR,
    &SemActions::Rule_AEXPR_INCR__to__incr_op__BASE_EXPR,
    &SemActions::Rule_AEXPR_INCR__to__BASE_EXPR__incr_op,
    &SemActions::Rule_BASE_EXPR__to__lpar__LEXPR__rpar,
    &SemActions::Rule_BASE_EXPR__to__FUN_CALL,
    &SemActions::Rule_BASE_EXPR__to__const_string,
    &SemActions::Rule_BASE_EXPR__to__CONST_NUMERIC,
    &SemActions::Rule_BASE_EXPR__to__LVALUE,
    &SemActions::Rule_BASE_EXPR__to__LVALUE__assign__LEXPR,
    &SemActions::Rule_FUN_CALL__to__COM_OBJECT__FUN_CALL_RIGHT,
    &SemActions::Rule_FUN_CALL__to__FUN_CALL_RIGHT,
    &SemActions::Rule_CONST_NUMERIC__to__const_int,
    &SemActions::Rule_CONST_NUMERIC__to__const_double,
    &SemActions::Rule_LVALUE__to__identifier,
    &SemActions::Rule_LVALUE__to__identifier__lbr__LEXPR__rbr,
    &SemActions::Rule_COM_OBJECT__to__identifier__dot,
    &SemActions::Rule_FUN_CALL_RIGHT__to__FUN_NAME__lpar__ARG_LIST__rpar,
    &SemActions::Rule_FUN_CALL_RIGHT__to__FUN_NAME__lpar__rpar,
    &SemActions::Rule_FUN_NAME__to__identifier,
    &SemActions::Rule_ARG_LIST__to__LEXPR,
    &SemActions::Rule_ARG_LIST__to__ARG_LIST__comma__LEXPR,
    &SemActions::Rule_FOR_LOOP_HEADER__to__FOR_LOOP_BEGIN__FOR_LOOP_MID__FOR_LOOP_END,
    &SemActions::Rule_FOR_LOOP_BODY__to__RUNTIME_STMT,
    &SemActions::Rule_FOR_LOOP_BEGIN__to__for__lpar__semicolon,
    &SemActions::Rule_FOR_LOOP_BEGIN__to__for__lpar__FOR_LOOP_LIST__semicolon,
    &SemActions::Rule_FOR_LOOP_MID__to__LEXPR__semicolon,
    &SemActions::Rule_FOR_LOOP_MID__to__semicolon,
    &SemActions::Rule_FOR_LOOP_END__to__FOR_LOOP_LIST__rpar,
    &SemActions::Rule_FOR_LOOP_END__to__rpar,
    &SemActions::Rule_FOR_LOOP_LIST__to__LEXPR,
    &SemActions::Rule_FOR_LOOP_LIST__to__FOR_LOOP_LIST__comma__LEXPR,
    &SemActions::Rule_IF_HEADER__to__if__lpar__LEXPR__rpar,
    &SemActions::Rule_IF_BLOCK__to__RUNTIME_STMT,
    &SemActions::Rule_SWITCH_HEADER__to__switch__lpar__LEXPR__rpar,
    &SemActions::Rule_SWITCH_BODY__to__lbracebr__SWITCH_CASES_LIST__rbracebr,
    &SemActions::Rule_SWITCH_BODY__to__lbracebr__rbracebr,
    &SemActions::Rule_SWITCH_CASES_LIST__to__SWITCH_CASE,
    &SemActions::Rule_SWITCH_CASES_LIST__to__SWITCH_CASES_LIST__SWITCH_CASE,
    &SemActions::Rule_SWITCH_CASE__to__SWITCH_CASE_HEADER,
    &SemActions::Rule_SWITCH_CASE__to__SWITCH_CASE_HEADER__SWITCH_CASE_BODY,
    &SemActions::Rule_SWITCH_CASE_HEADER__to__case__LEXPR__colon,
    &SemActions::Rule_SWITCH_CASE_HEADER__to__default__colon,
    &SemActions::Rule_SWITCH_CASE_BODY__to__RUNTIME_STMT_LIST,
    &SemActions::Rule_STMT_SIMPLE_LEFT__to__LEXPR,
    &SemActions::Rule_STMT_SIMPLE_LEFT__to__STMT_FUNCTION_SPECIFIC,
    &SemActions::Rule_STMT_SIMPLE_LEFT__to__STMT_DO_WHILE,
    &SemActions::Rule_STMT_SIMPLE_LEFT__to__STMT_BREAK,
    &SemActions::Rule_STMT_SIMPLE_LEFT__to__STMT_CONTINUE,
    &SemActions::Rule_STMT_FUNCTION_SPECIFIC__to__STMT_EXPLICIT_VAR_DECL,
    &SemActions::Rule_STMT_FUNCTION_SPECIFIC__to__STMT_RETURN,
    &SemActions::Rule_STMT_DO_WHILE__to__DO_WHILE_LEFT__WHILE_LOOP_BODY__while__lpar__LEXPR__rpar,
    &SemActions::Rule_STMT_BREAK__to__break,
    &SemActions::Rule_STMT_CONTINUE__to__continue,
    &SemActions::Rule_STMT_EXPLICIT_VAR_DECL__to__local__identifier,
    &SemActions::Rule_STMT_EXPLICIT_VAR_DECL__to__global__identifier,
    &SemActions::Rule_STMT_RETURN__to__return,
    &SemActions::Rule_STMT_RETURN__to__return__LEXPR,
    &SemActions::Rule_DO_WHILE_LEFT__to__do,
  };

  (this->*(Rules[a_ruleId]))();
}

//------------------ rule actions --------------------
void SemActions::Rule_acceptance()
{
}

void SemActions::Rule_AFL_SCRIPT__to__STMT_LIST()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT_LIST__to__STMT()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT_LIST__to__STMT_LIST__STMT()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_STMT__to__FUN_DEF()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT__to__RUNTIME_STMT()
{
	m_semStack.Pop();
}

void SemActions::Rule_FUN_DEF__to__FUN_DEF_PREFIX__FUN_DECL__STMT_COMPOUND()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_RUNTIME_STMT__to__STMT_WHILE()
{
	m_semStack.Pop();
}

void SemActions::Rule_RUNTIME_STMT__to__STMT_FOR_LOOP()
{
	m_semStack.Pop();
}

void SemActions::Rule_RUNTIME_STMT__to__STMT_IF()
{
	m_semStack.Pop();
}

void SemActions::Rule_RUNTIME_STMT__to__STMT_SWITCH()
{
	m_semStack.Pop();
}

void SemActions::Rule_RUNTIME_STMT__to__STMT_COMPOUND()
{
	m_semStack.Pop();
}

void SemActions::Rule_RUNTIME_STMT__to__STMT_SIMPLE()
{
	m_semStack.Pop();
}

void SemActions::Rule_FUN_DEF_PREFIX__to__function()
{
	m_semStack.Pop();
}

void SemActions::Rule_FUN_DEF_PREFIX__to__procedure()
{
	m_semStack.Pop();
}

void SemActions::Rule_FUN_DECL__to__identifier__lpar__ARGS_DECL_LIST__rpar()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FUN_DECL__to__identifier__lpar__rpar()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_STMT_COMPOUND__to__lbracebr__RUNTIME_STMT_LIST__rbracebr()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_STMT_COMPOUND__to__lbracebr__rbracebr()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_ARGS_DECL_LIST__to__identifier()
{
	m_semStack.Pop();
}

void SemActions::Rule_ARGS_DECL_LIST__to__ARGS_DECL_LIST__comma__identifier()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_RUNTIME_STMT_LIST__to__RUNTIME_STMT()
{
	m_semStack.Pop();
}

void SemActions::Rule_RUNTIME_STMT_LIST__to__RUNTIME_STMT_LIST__RUNTIME_STMT()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_STMT_WHILE__to__WHILE_HEADER__WHILE_LOOP_BODY()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_STMT_FOR_LOOP__to__FOR_LOOP_HEADER__FOR_LOOP_BODY()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_STMT_IF__to__IF_HEADER__IF_BLOCK()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_STMT_IF__to__IF_HEADER__IF_BLOCK__else__IF_BLOCK()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_STMT_SWITCH__to__SWITCH_HEADER__SWITCH_BODY()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_STMT_SIMPLE__to__STMT_SIMPLE_LEFT__semicolon()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_STMT_SIMPLE__to__semicolon()
{
	m_semStack.Pop();
}

void SemActions::Rule_WHILE_HEADER__to__WHILE_HEADER_START__lpar__LEXPR__rpar()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_WHILE_LOOP_BODY__to__RUNTIME_STMT()
{
	m_semStack.Pop();
}

void SemActions::Rule_WHILE_HEADER_START__to__while()
{
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR__to__LEXPR_OR()
{
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_OR__to__LEXPR_AND()
{
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_OR__to__LEXPR_OR__log_or_op__LEXPR_AND()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_AND__to__LEXPR_NOT()
{
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_AND__to__LEXPR_AND__log_and_op__LEXPR_NOT()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_NOT__to__LEXPR_BITOR()
{
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_NOT__to__log_not_op__LEXPR_BITOR()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_BITOR__to__LEXPR_BITAND()
{
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_BITOR__to__LEXPR_BITOR__bit_or_op__LEXPR_BITAND()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_BITAND__to__LEXPR_CMP_EQ()
{
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_BITAND__to__LEXPR_BITAND__bit_and_op__LEXPR_CMP_EQ()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_CMP_EQ__to__LEXPR_CMP_MORE_LESS()
{
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_CMP_EQ__to__LEXPR_CMP_EQ__cmp_eq_op__LEXPR_CMP_MORE_LESS()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_CMP_MORE_LESS__to__AEXPR_ADD()
{
	m_semStack.Pop();
}

void SemActions::Rule_LEXPR_CMP_MORE_LESS__to__LEXPR_CMP_MORE_LESS__cmp_more_less_op__AEXPR_ADD()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_ADD__to__AEXPR_MUL()
{
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_ADD__to__AEXPR_ADD__add_op__AEXPR_MUL()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_MUL__to__AEXPR_SIGN()
{
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_MUL__to__AEXPR_MUL__mul_op__AEXPR_SIGN()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_SIGN__to__AEXPR_POW()
{
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_SIGN__to__add_op__AEXPR_POW()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_POW__to__AEXPR_INCR()
{
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_POW__to__AEXPR_POW__pow_op__AEXPR_INCR()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_INCR__to__BASE_EXPR()
{
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_INCR__to__incr_op__BASE_EXPR()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_AEXPR_INCR__to__BASE_EXPR__incr_op()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_BASE_EXPR__to__lpar__LEXPR__rpar()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_BASE_EXPR__to__FUN_CALL()
{
	m_semStack.Pop();
}

void SemActions::Rule_BASE_EXPR__to__const_string()
{
	m_semStack.Pop();
}

void SemActions::Rule_BASE_EXPR__to__CONST_NUMERIC()
{
	m_semStack.Pop();
}

void SemActions::Rule_BASE_EXPR__to__LVALUE()
{
	m_semStack.Pop();
}

void SemActions::Rule_BASE_EXPR__to__LVALUE__assign__LEXPR()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FUN_CALL__to__COM_OBJECT__FUN_CALL_RIGHT()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FUN_CALL__to__FUN_CALL_RIGHT()
{
	m_semStack.Pop();
}

void SemActions::Rule_CONST_NUMERIC__to__const_int()
{
	m_semStack.Pop();
}

void SemActions::Rule_CONST_NUMERIC__to__const_double()
{
	m_semStack.Pop();
}

void SemActions::Rule_LVALUE__to__identifier()
{
	m_semStack.Pop();
}

void SemActions::Rule_LVALUE__to__identifier__lbr__LEXPR__rbr()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_COM_OBJECT__to__identifier__dot()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FUN_CALL_RIGHT__to__FUN_NAME__lpar__ARG_LIST__rpar()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FUN_CALL_RIGHT__to__FUN_NAME__lpar__rpar()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FUN_NAME__to__identifier()
{
	m_semStack.Pop();
}

void SemActions::Rule_ARG_LIST__to__LEXPR()
{
	m_semStack.Pop();
}

void SemActions::Rule_ARG_LIST__to__ARG_LIST__comma__LEXPR()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FOR_LOOP_HEADER__to__FOR_LOOP_BEGIN__FOR_LOOP_MID__FOR_LOOP_END()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FOR_LOOP_BODY__to__RUNTIME_STMT()
{
	m_semStack.Pop();
}

void SemActions::Rule_FOR_LOOP_BEGIN__to__for__lpar__semicolon()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FOR_LOOP_BEGIN__to__for__lpar__FOR_LOOP_LIST__semicolon()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FOR_LOOP_MID__to__LEXPR__semicolon()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FOR_LOOP_MID__to__semicolon()
{
	m_semStack.Pop();
}

void SemActions::Rule_FOR_LOOP_END__to__FOR_LOOP_LIST__rpar()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_FOR_LOOP_END__to__rpar()
{
	m_semStack.Pop();
}

void SemActions::Rule_FOR_LOOP_LIST__to__LEXPR()
{
	m_semStack.Pop();
}

void SemActions::Rule_FOR_LOOP_LIST__to__FOR_LOOP_LIST__comma__LEXPR()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_IF_HEADER__to__if__lpar__LEXPR__rpar()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_IF_BLOCK__to__RUNTIME_STMT()
{
	m_semStack.Pop();
}

void SemActions::Rule_SWITCH_HEADER__to__switch__lpar__LEXPR__rpar()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_SWITCH_BODY__to__lbracebr__SWITCH_CASES_LIST__rbracebr()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_SWITCH_BODY__to__lbracebr__rbracebr()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_SWITCH_CASES_LIST__to__SWITCH_CASE()
{
	m_semStack.Pop();
}

void SemActions::Rule_SWITCH_CASES_LIST__to__SWITCH_CASES_LIST__SWITCH_CASE()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_SWITCH_CASE__to__SWITCH_CASE_HEADER()
{
	m_semStack.Pop();
}

void SemActions::Rule_SWITCH_CASE__to__SWITCH_CASE_HEADER__SWITCH_CASE_BODY()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_SWITCH_CASE_HEADER__to__case__LEXPR__colon()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_SWITCH_CASE_HEADER__to__default__colon()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_SWITCH_CASE_BODY__to__RUNTIME_STMT_LIST()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT_SIMPLE_LEFT__to__LEXPR()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT_SIMPLE_LEFT__to__STMT_FUNCTION_SPECIFIC()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT_SIMPLE_LEFT__to__STMT_DO_WHILE()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT_SIMPLE_LEFT__to__STMT_BREAK()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT_SIMPLE_LEFT__to__STMT_CONTINUE()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT_FUNCTION_SPECIFIC__to__STMT_EXPLICIT_VAR_DECL()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT_FUNCTION_SPECIFIC__to__STMT_RETURN()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT_DO_WHILE__to__DO_WHILE_LEFT__WHILE_LOOP_BODY__while__lpar__LEXPR__rpar()
{
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_STMT_BREAK__to__break()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT_CONTINUE__to__continue()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT_EXPLICIT_VAR_DECL__to__local__identifier()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_STMT_EXPLICIT_VAR_DECL__to__global__identifier()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_STMT_RETURN__to__return()
{
	m_semStack.Pop();
}

void SemActions::Rule_STMT_RETURN__to__return__LEXPR()
{
	m_semStack.Pop();
	m_semStack.Pop();
}

void SemActions::Rule_DO_WHILE_LEFT__to__do()
{
	m_semStack.Pop();
}

