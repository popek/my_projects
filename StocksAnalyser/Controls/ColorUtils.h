#pragma once
#include "ControlsLib.h"


class CONTROLS_LIB ColorUtils
{
public:
   enum EColorId
   {
      EBlack,
      EWhite,
      ERed,
      EGreen,
      EBlue,
   };

public:
   static COLORREF Color(EColorId a_standardColorId);

   static COLORREF MixColors(EColorId a_standardColor1Id, EColorId a_standardColor2Id, double a_color1Part);

   static COLORREF MixColors(COLORREF a_color1, COLORREF a_color2, double a_color1Part);
};
