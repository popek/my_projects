#pragma once
#include "FoundationLib.h"

class FOUNDATION_LIB IXMLDataObserver
{
public:
   virtual ~IXMLDataObserver(void);

   virtual void onEnterNewNode(LPCTSTR nodeName, LPCTSTR nodeText) = 0;
   virtual void onLeaveNode() = 0;

   virtual void onAttribute(LPCTSTR attrName, LPCTSTR attrText) = 0;
};

