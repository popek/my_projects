#include "StdAfx.h"
#include "ColorUtils.h"


// global function. Used only in this cpp module
BYTE mixSubColors(BYTE a_subcolor1, BYTE a_subcolor2, double a_subcolor1Part)
{
   return (BYTE)(a_subcolor1Part*(double)a_subcolor1 + (1-a_subcolor1Part)*(double)a_subcolor2);
}

const COLORREF g_arrStandardColors[] = 
{
   RGB(0, 0, 0),
   RGB(255, 255, 255),
   RGB(255, 0, 0),
   RGB(0, 255, 0),
   RGB(0, 0, 255)
};


COLORREF ColorUtils::Color(ColorUtils::EColorId a_standardColorId)
{
  return g_arrStandardColors[a_standardColorId];
}

COLORREF ColorUtils::MixColors(ColorUtils::EColorId a_standardColor1Id, 
                               ColorUtils::EColorId a_standardColor2Id, double a_color1Part)
{
   return ColorUtils::MixColors(ColorUtils::Color(a_standardColor1Id), 
                                ColorUtils::Color(a_standardColor2Id), a_color1Part);
}

COLORREF ColorUtils::MixColors(COLORREF a_color1, COLORREF a_color2, double a_color1Part)
{
   return RGB(mixSubColors(GetRValue(a_color1), GetRValue(a_color2), a_color1Part),
              mixSubColors(GetGValue(a_color1), GetGValue(a_color2), a_color1Part),
              mixSubColors(GetBValue(a_color1), GetBValue(a_color2), a_color1Part));
}