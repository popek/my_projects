#pragma once
#include "ControlsLib.h"

class CONTROLS_LIB IDataRangeObserver
{
public:

   IDataRangeObserver(){};
   virtual ~IDataRangeObserver(){};

   virtual void OnDataRangeChanged(double a_dNewStartNormalisedPos, double a_dNewFinishNormalisedPos) = 0;
};