#pragma once
#include "KandleDate.h"


class Asset;
class IPointer;


class Misc
{
public:

   static CString GetBinFolder();

   static CString ClearFolderContent(CString a_strFolderPath);

   static void    RemoveFile(CString a_strFilePath);

   static std::string ToAnsi(CString a_string);

   static CString ToTSTR(std::string a_string);

   static void PreparePointers(const Asset& a_asset, vector<shared_ptr<IPointer>>& a_vPointers, size_t& a_shortPointerCnt);

   static void TryToAttachStantardCharElementsSet(Asset& a_assetToWhichAttach);

   static int ToInt(CString a_strValue);

   static double ToDouble(CString a_strValue);

   static CString ToString(double a_value);

   static CString ToString(int a_value);

   static void CutUnnecessaryDecimalPlaces(CString& a_strDoubleValue);

   static shared_ptr<KandleDate> FromOleDate(const COleDateTime& a_date);

   static shared_ptr<KandleDate> ExtractKandleDate(CDateTimeCtrl& a_dateControl);


   static CString ChooseAssetsFilesDirectory();
};
