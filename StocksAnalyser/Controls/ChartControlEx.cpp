#include "stdafx.h"
#include "ChartControlEx.h"
#include "ChartControl.h"
#include "ScrollBarExt.h"
#include "WindowMessages.h"

const UINT CHART_CONTROL_ID   = 1;
const UINT SCROLL_CONTROL_ID  = 2;

IMPLEMENT_DYNAMIC(ChartControlEx, CWnd)

ChartControlEx::ChartControlEx(bool a_bUseScroll)
: m_bUseScrollbar(a_bUseScroll)
, m_nScrollbarHeight(20)
{
}
ChartControlEx::~ChartControlEx()
{
}

void ChartControlEx::SetAsset(shared_ptr<Asset> a_spAsset)
{
   m_spChart->SetAsset(a_spAsset);
}

BEGIN_MESSAGE_MAP(ChartControlEx, CWnd)
   ON_WM_CREATE()
   ON_WM_SIZE()
END_MESSAGE_MAP()

int ChartControlEx::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
   if (CWnd::OnCreate(lpCreateStruct) == -1)
   {
      return -1;
   }

   CRect rcChart;
   CRect rcScroll;

   GetClientRect(&rcChart);

   if(m_bUseScrollbar)
   {
      rcScroll        = rcChart;

      if(rcChart.Height() > m_nScrollbarHeight)
      {
         rcChart.bottom -= m_nScrollbarHeight;
         rcScroll.top    = rcScroll.bottom - m_nScrollbarHeight;
      }
   }

   m_spChart = shared_ptr<ChartControl>( new ChartControl() );
   if(FALSE == m_spChart->Create(NULL, NULL, WS_CHILD|WS_CLIPSIBLINGS|WS_VISIBLE, rcChart, this, CHART_CONTROL_ID))
   {
      ASSERT(FALSE);
      return -1;
   }

   if(m_bUseScrollbar)
   {
      m_spScroll = shared_ptr<ScrollBarExt>( new ScrollBarExt(m_spChart.get()) );
      if( FALSE == m_spScroll->Create(NULL, NULL, WS_CHILD|WS_CLIPSIBLINGS|WS_VISIBLE, rcScroll, this, SCROLL_CONTROL_ID) )
      {
         ASSERT(FALSE);
         return -1;
      }

      // set range manager 
      m_spChart->DataRangeManager(m_spScroll.get());
   }

   return 0;
}

void ChartControlEx::OnSize(UINT nType, int cx, int cy)
{
   CWnd::OnSize(nType, cx, cy);

   CRect rcChart;
   CRect rcScroll;

   GetClientRect(&rcChart);

   if(m_bUseScrollbar)
   {
      rcScroll        = rcChart;
      if(rcChart.Height() > m_nScrollbarHeight)
      {
         rcChart.bottom -= m_nScrollbarHeight;
         rcScroll.top    = rcScroll.bottom - m_nScrollbarHeight;
      }

      if(m_spScroll->GetSafeHwnd())
      {
         m_spScroll->MoveWindow(&rcScroll);
      }
   }

   if(m_spChart->GetSafeHwnd())
   {
      m_spChart->MoveWindow(&rcChart);
   }
}

LRESULT ChartControlEx::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
   if(WM_SHOW_NEW_CHART == message)
   {
      GetParent()->SendMessage(message, wParam, lParam);
      return 0;
   }

   return CWnd::WindowProc(message, wParam, lParam);
}
