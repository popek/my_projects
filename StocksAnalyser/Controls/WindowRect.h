#pragma once
#include "ControlsLib.h"

class CONTROLS_LIB WindowRect : public CRect
{
public:
   WindowRect(CWnd* a_pWindow);
   WindowRect(CWnd& a_Window);
   virtual ~WindowRect(void);
};
