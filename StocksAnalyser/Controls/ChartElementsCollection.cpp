#include "StdAfx.h"
#include "ChartElementsCollection.h"
#include "ChartElement.h"


ChartElementsCollection::ChartElementsCollection(void)
{
}
ChartElementsCollection::~ChartElementsCollection(void)
{
}

void ChartElementsCollection::PushBack(shared_ptr<ChartElement> a_spElement)
{
   m_elements.push_back(a_spElement);
}

void ChartElementsCollection::PushFront(shared_ptr<ChartElement> a_spElement)
{
   m_elements.push_front(a_spElement);
}

shared_ptr<ChartElement> ChartElementsCollection::GetElement(size_t a_elementIndex)
{
   return m_elements[a_elementIndex];
}

size_t ChartElementsCollection::ElementsCount() const
{
   return m_elements.size();
}