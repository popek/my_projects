#pragma once
#include "ETerminalId.h"
#include "typeDefs.h"


// lex atom class
class LexAtom
{
public:
  int         _line;
  ETerminalId _id;
  string_t    _text;
  double      _dValue;
  int         _iValue;
};


// lexical analyzer class
class LexicalAnalyzer
{
public:

	//read next token
	ETerminalId GetAtom(LexAtom* a_pLexAtom);

protected:
	//read next character from input stream
	virtual char_t readNextChar() = 0;

	//writes back last character
	virtual void  writeBackChar() = 0;

	char_t _token[1024];         // auxiliary container for token string
	bool  _EOF;                 // flag set during reading character. Set when end of file have beed reached

};
