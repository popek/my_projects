#include "StdAfx.h"
#include "PathUtils.h"
#include <atlpath.h>


CString PathUtils::concatenateDirAndFile(const CString& directory, const CString& fileName)
{
   CString strFullFilePath = directory;

   if(strFullFilePath[strFullFilePath.GetLength()-1] != _T('\\'))
   {
      strFullFilePath.AppendChar(_T('\\'));
   }

   strFullFilePath.Append(fileName);

   return strFullFilePath;
}

CString PathUtils::extractFileName(const CString& filePath)
{
   CPath path(filePath);

   path.RemoveExtension();

   return path.m_strPath.Mid(path.FindFileName());
}

bool PathUtils::isDirectory(const CString& path)
{
   CPath pathObj(path);
   return pathObj.IsDirectory();
}

bool PathUtils::pathExists(const CString& path)
{
   CPath pathObj(path);
   return pathObj.FileExists();
}

bool PathUtils::doesDirectoryExist(const CEdit& editWithPath)
{
   CString path;
   editWithPath.GetWindowText(path);

   return doesDirectoryExist(path);
}

bool PathUtils::doesDirectoryExist(const CString& path)
{
   CPath pathObj(path);
   return pathObj.IsDirectory() && pathObj.FileExists();
}
