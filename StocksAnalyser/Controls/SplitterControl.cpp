#include "stdafx.h"
#include "SplitterControl.h"
#include "WindowUtils.h"
#include "MemoryDC.h"
#include "ClientRect.h"
#include "WindowRect.h"
#include "WindowUtils.h"
#include "ControlsLibUtils.h"


static const TCHAR* SPLITTER_CLASS_NAME = _T("controlsLib_splitter");

IMPLEMENT_DYNAMIC(SplitterControl, CWnd)

SplitterControl::SplitterControl()
: m_layout(EVertical)
, m_p1stChild(NULL)
, m_p2ndChild(NULL)
, m_frameWidth(1)
, m_barWidth(4)
, m_draggingOn(false)
, m_barPosNrmalised(0.8)
{
   static BOOL s_alreadyRegistered = FALSE;
   if(FALSE == s_alreadyRegistered)
   {
      s_alreadyRegistered = WindowUtils::RegisterWindowClass(SPLITTER_CLASS_NAME, NULL, CS_HREDRAW|CS_DBLCLKS|CS_VREDRAW);
      ASSERT(s_alreadyRegistered);
   }
}
SplitterControl::~SplitterControl()
{
}

void SplitterControl::SetSplitBarPos(double a_normalisedSplitBarPos)
{
   m_barPosNrmalised = a_normalisedSplitBarPos;
}

BOOL SplitterControl::Create(DWORD a_style, CRect a_rcArea, CWnd* a_pParent, UINT a_id)
{
   return __super::Create(SPLITTER_CLASS_NAME, NULL, a_style, a_rcArea, a_pParent, a_id);
}

void SplitterControl::AttachFirst(CWnd* a_pFirstChild)
{
   if(m_p1stChild && m_p1stChild->GetSafeHwnd())
   {
      m_p1stChild->DestroyWindow();
   }
   m_p1stChild = a_pFirstChild;

   RefreshChildsPos();
}

void SplitterControl::AttachSecond(CWnd* a_pSecondChild)
{
   if(m_p2ndChild && m_p2ndChild->GetSafeHwnd())
   {
      m_p2ndChild->DestroyWindow();
   }
   m_p2ndChild = a_pSecondChild;
   
   RefreshChildsPos();
}

void SplitterControl::RefreshChildsPos()
{
   ClientRect rcClient(this);
   int barPos = (int)(m_barPosNrmalised*(double)(EVertical == m_layout ? rcClient.Height() : rcClient.Width()));

   rcClient.DeflateRect(m_frameWidth, m_frameWidth);

   CRect rc1stChild = rcClient;
   CRect rc2ndChild = rcClient;

   if(EVertical == m_layout)
   {
      rc1stChild.bottom = barPos - m_barWidth/2;
      rc2ndChild.top = rc1stChild.bottom + m_barWidth;
   }
   else
   {
      rc1stChild.right = barPos - m_barWidth/2;
      rc2ndChild.left = rc1stChild.right + m_barWidth;
   }

   WindowUtils::SetWindowPos(m_p1stChild, NULL, rc1stChild, SWP_NOZORDER);
   WindowUtils::SetWindowPos(m_p2ndChild, NULL, rc2ndChild, SWP_NOZORDER);
}

BOOL SplitterControl::OverSplitBar(const CPoint& a_point)
{
   ClientRect rcClient(this);

   int barPos = (int)(m_barPosNrmalised*(double)(EVertical == m_layout ? rcClient.Height() : rcClient.Width()));
   
   if(EVertical == m_layout)
   {
      rcClient.top = barPos - m_barWidth/2;
      rcClient.bottom = rcClient.top + m_barWidth;
   }
   else
   {
      rcClient.left = barPos - m_barWidth/2;
      rcClient.right = rcClient.left + m_barWidth;
   }

   return rcClient.PtInRect(a_point);
}

BEGIN_MESSAGE_MAP(SplitterControl, CWnd)
   ON_WM_MOUSEMOVE()
   ON_WM_LBUTTONDOWN()
   ON_WM_LBUTTONUP()
   ON_WM_PAINT()
   ON_WM_SIZE()
   ON_WM_ERASEBKGND()
END_MESSAGE_MAP()



// SplitterControl message handlers

void SplitterControl::OnMouseMove(UINT nFlags, CPoint point)
{
   ClientRect rcClient(this);

   if(m_draggingOn)
   {
      BOOL refreshRequired = FALSE;

      bool betweenBoundries = EVertical == m_layout ? (point.y<=rcClient.bottom && point.y>=rcClient.top) : 
                                                      (point.x<=rcClient.right && point.x>=0);
      if(betweenBoundries)
      {
         m_barPosNrmalised = EVertical == m_layout ? (double)point.y/rcClient.Height() : 
                                                     (double)point.x/rcClient.Width();
         refreshRequired = TRUE;
      }
      else
      {
         double newBarpos = EVertical == m_layout ? (point.y < 0 ? 0 : 1) : 
                                                    (point.x < 0 ? 0 : 1);
         if(newBarpos != m_barPosNrmalised)
         {
            m_barPosNrmalised = newBarpos;
            refreshRequired = TRUE;
         }
      }

      if(refreshRequired)
      {
         RefreshChildsPos();
      }
   }
   else if(OverSplitBar(point))
   {
      SetCursor(ControlsLibUtils::Cursor(EVertical == m_layout ? IDC_SIZENS : IDC_SIZEWE));
   }
   __super::OnMouseMove(nFlags, point);
}

void SplitterControl::OnLButtonDown(UINT nFlags, CPoint point)
{
   if(OverSplitBar(point))
   {
      m_draggingOn = true;

      SetCursor(ControlsLibUtils::Cursor(EVertical == m_layout ? IDC_SIZENS : IDC_SIZEWE));

      SetCapture();
   }

   __super::OnLButtonDown(nFlags, point);
}

void SplitterControl::OnLButtonUp(UINT nFlags, CPoint point)
{
   m_draggingOn = false;

   SetCursor(ControlsLibUtils::Cursor(IDC_ARROW));

   if(GetCapture() == this)
   {
      ReleaseCapture();
   }

   CWnd::OnLButtonUp(nFlags, point);
}

void SplitterControl::OnPaint()
{
   CPaintDC dc(this); 
   MemoryDC memDC(&dc, this);

   ClientRect rcClient(this);

   memDC.BufferedDC().FillSolidRect(rcClient, RGB(109, 88, 167));
}

void SplitterControl::OnSize(UINT nType, int cx, int cy)
{
   __super::OnSize(nType, cx, cy);
   RefreshChildsPos();
}

BOOL SplitterControl::OnEraseBkgnd(CDC* pDC)
{
   return TRUE;
}
