#pragma once
#include "TradeStrategyBase.h"


class SMA_pointer;
class MAX_pointer;
class ChartElementsCollection;
class TransactionDetails;


class MeanMax_strategy : public TradeStrategyBase
{
private:
   enum state
   {
      EIdle, 

      EBoughtAboveMean,
      EBoughtAboveLastMaximum,
      ESoldBelowMean,
      ESoldOnCutoffValue,
   };

public:
   MeanMax_strategy(shared_ptr<Asset> spAsset);
   virtual ~MeanMax_strategy(void);

   virtual shared_ptr<StrategyResult> RunTest( const KandleDate& a_start, const KandleDate& a_finish);

protected:
   virtual void AddGraphicPresentation(shared_ptr<ChartElementsCollection> elements);
   
private:

   shared_ptr<TransactionDetails> handleIdle(size_t candleIndex);

   shared_ptr<TransactionDetails> handleSoldBelowMean(size_t candleIndex);

   shared_ptr<TransactionDetails> handleSoldOnCutoffValue(size_t candleIndex);

   shared_ptr<TransactionDetails> handleBoughtAboveMean(size_t candleIndex);

   shared_ptr<TransactionDetails> handleBoughtAboveLastMaximum(size_t candleIndex);

private:
   state _state;

   shared_ptr<ChartElementsCollection> _elements;

   double _lastTransactionPrice;
   double _lastMaxBasedOnWhichCutofSellWasGenerated;
   double _maxDecreseFromMaxValue;

   shared_ptr<SMA_pointer> _sma;
   size_t _smaPeriod;
   
   shared_ptr<MAX_pointer> _maxIndicator;
   size_t _maxIndicatorPeriod;
};


