#pragma once
#include "Poco/Zip/ZipLocalFileHeader.h"

class AssetDecompressionErrorObserver
{
public:
   AssetDecompressionErrorObserver(void);
   virtual ~AssetDecompressionErrorObserver(void);

   void OnDecompressError(const void* pSender, std::pair<const Poco::Zip::ZipLocalFileHeader, const std::string>& info);
};
