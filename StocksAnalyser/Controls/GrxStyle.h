#pragma once
#include "ControlsLib.h"


typedef size_t paramId_t;


class CONTROLS_LIB GrxStyle
{
public:
   GrxStyle(void);
   virtual ~GrxStyle(void);

   virtual LONG_PTR GetNumericParam(paramId_t a_paramId) const = 0;
   
   virtual void SetNumericParam(paramId_t a_paramId, LONG_PTR a_value) = 0;
};
