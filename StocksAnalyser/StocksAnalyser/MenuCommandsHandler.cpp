#include "stdafx.h"
#include "StocksAnalyser.h"
#include "MenuCommandsHandler.h"
#include "ToolbarContainer.h"
#include "SearchGMMAsignalsDlg.h"

IMPLEMENT_DYNAMIC(MenuCommandsHandler, CCmdTarget)
MenuCommandsHandler::MenuCommandsHandler()
{
}
MenuCommandsHandler::~MenuCommandsHandler()
{
}

BEGIN_MESSAGE_MAP(MenuCommandsHandler, CCmdTarget)
   ON_COMMAND(ID_SHOW_TOOLS_WINDOW, &MenuCommandsHandler::OnShowToolsWindow)
   ON_UPDATE_COMMAND_UI(ID_SHOW_TOOLS_WINDOW, &MenuCommandsHandler::OnUpdateShowToolsWindow)
   ON_COMMAND(ID_SHOW_STOCKS_TO_BUY, &MenuCommandsHandler::OnShowStocksToBuy)
   ON_UPDATE_COMMAND_UI(ID_SHOW_STOCKS_TO_BUY, &MenuCommandsHandler::OnUpdateShowStocksToBuy)
   ON_COMMAND(ID_ASSETSLIST_UPDATE, &MenuCommandsHandler::OnAssetslistUpdate)
   ON_UPDATE_COMMAND_UI(ID_ASSETSLIST_UPDATE, &MenuCommandsHandler::OnUpdateAssetslistUpdate)
   ON_COMMAND(ID_SHOW_STRATEGY_TESTER, &MenuCommandsHandler::OnShowStrategyTester)
   ON_UPDATE_COMMAND_UI(ID_SHOW_STRATEGY_TESTER, &MenuCommandsHandler::OnUpdateShowStrategyTester)
   ON_COMMAND(ID_SHOW_MARKET_SCANNER, &MenuCommandsHandler::OnShowMarketScanner)
   ON_UPDATE_COMMAND_UI(ID_SHOW_MARKET_SCANNER, &MenuCommandsHandler::OnUpdateShowMarketScanner)
END_MESSAGE_MAP()




void MenuCommandsHandler::OnShowToolsWindow()
{
   G_stockAnalyserApp.ToolsWindow().Show();
}

void MenuCommandsHandler::OnUpdateShowToolsWindow(CCmdUI *pCmdUI)
{
   pCmdUI->SetCheck(G_stockAnalyserApp.ToolsWindow().Visible());
}

void MenuCommandsHandler::OnShowStocksToBuy()
{
   G_stockAnalyserApp.ShowGMMADlg();
}

void MenuCommandsHandler::OnUpdateShowStocksToBuy(CCmdUI *pCmdUI)
{
   pCmdUI->SetCheck(G_stockAnalyserApp.GMMADlgVisible());
}

void MenuCommandsHandler::OnAssetslistUpdate()
{
   G_stockAnalyserApp.UpdateCSVFiles();
}

void MenuCommandsHandler::OnUpdateAssetslistUpdate(CCmdUI *pCmdUI)
{
   pCmdUI->Enable(TRUE);
}

void MenuCommandsHandler::OnShowStrategyTester()
{
   G_stockAnalyserApp.ShowTradeStrategyTesterDlg();  
}

void MenuCommandsHandler::OnUpdateShowStrategyTester(CCmdUI *pCmdUI)
{
   pCmdUI->SetCheck(G_stockAnalyserApp.TradeStrategyTesterDlgVisible());
}


void MenuCommandsHandler::OnShowMarketScanner()
{
   G_stockAnalyserApp.ShowMarketScannerDlg();
}


void MenuCommandsHandler::OnUpdateShowMarketScanner(CCmdUI *pCmdUI)
{
   pCmdUI->Enable(TRUE);
   pCmdUI->SetCheck(G_stockAnalyserApp.MarketScannerDlgVisible());
}
