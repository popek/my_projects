#pragma once
#include "ControlsLib.h"
#include "DataSeries.h"


class Asset;


// base class for all asset wrappers
class CONTROLS_LIB AssetDoubleVectorWrapper : public DataSeries
{
   NONCOPYABLE_TYPE(AssetDoubleVectorWrapper)

public:
   AssetDoubleVectorWrapper(const Asset& a_assetRef);
   virtual ~AssetDoubleVectorWrapper(void);

   virtual size_t size() const;

protected:
   const Asset& m_assetRef;
};

//
class CONTROLS_LIB AssetOpenValueGetter : public AssetDoubleVectorWrapper
{
public:
   AssetOpenValueGetter(const Asset& a_assetRef);
   virtual double operator [](size_t a_elementIndex) const;
};

//
class CONTROLS_LIB AssetCloseValueGetter : public AssetDoubleVectorWrapper
{
public:
   AssetCloseValueGetter(const Asset& a_assetRef);
   virtual double operator [](size_t a_elementIndex) const;
};

//
class CONTROLS_LIB AssetHighValueGetter : public AssetDoubleVectorWrapper
{
public:
   AssetHighValueGetter(const Asset& a_assetRef);
   virtual double operator [](size_t a_elementIndex) const;
};

//
class CONTROLS_LIB AssetLowValueGetter : public AssetDoubleVectorWrapper
{
public:
   AssetLowValueGetter(const Asset& a_assetRef);
   virtual double operator [](size_t a_elementIndex) const;
};