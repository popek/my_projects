#include "StdAfx.h"
#include "AssetAuxiliaryWrappers.h"
#include "Asset.h"


//
AssetDoubleVectorWrapper::AssetDoubleVectorWrapper(const Asset& a_assetRef)
   : DataSeries(NULL, 0)
   , m_assetRef(a_assetRef)
{
}
AssetDoubleVectorWrapper::~AssetDoubleVectorWrapper(void)
{
}
size_t AssetDoubleVectorWrapper::size() const
{
   return m_assetRef.Size();
}

//
AssetOpenValueGetter::AssetOpenValueGetter(const Asset& a_assetRef)
: AssetDoubleVectorWrapper(a_assetRef)
{
}
double AssetOpenValueGetter::operator [](size_t a_elementIndex) const
{
   return m_assetRef[a_elementIndex].Open();
}

//
AssetCloseValueGetter::AssetCloseValueGetter(const Asset& a_assetRef)
: AssetDoubleVectorWrapper(a_assetRef)
{
}
double AssetCloseValueGetter::operator [](size_t a_elementIndex) const
{
   return m_assetRef[a_elementIndex].Close();
}

//
AssetHighValueGetter::AssetHighValueGetter(const Asset& a_assetRef)
: AssetDoubleVectorWrapper(a_assetRef)
{
}
double AssetHighValueGetter::operator [](size_t a_elementIndex) const
{
   return m_assetRef[a_elementIndex].High();
}

//
AssetLowValueGetter::AssetLowValueGetter(const Asset& a_assetRef)
: AssetDoubleVectorWrapper(a_assetRef)
{
}
double AssetLowValueGetter::operator [](size_t a_elementIndex) const
{
   return m_assetRef[a_elementIndex].Low();
}
