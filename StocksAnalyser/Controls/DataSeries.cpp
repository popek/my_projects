#include "StdAfx.h"
#include "DataSeries.h"


DataSeries::DataSeries(size_t a_offset)
: m_parentToRootOffset(0)
, m_offset(a_offset)
{
}
DataSeries::DataSeries(const DataSeries* a_pParent, size_t a_offset)
: m_parentToRootOffset(a_pParent ? a_pParent->offset(false) : 0)
, m_offset(a_offset)
{
}
DataSeries::~DataSeries(void)
{
}

double DataSeries::GetDataBasedOnRootSeriesIndex(size_t a_rootSeriesIndex) const
{
   return this->operator[](a_rootSeriesIndex - offset(false));
}

// returns difference between size of parent and this (flat = true), or 
//         difference between size of root and this (flat = false)
size_t DataSeries::offset(bool a_flatOffset) const
{
   return a_flatOffset ? m_offset : m_offset + m_parentToRootOffset;
}




//
VectorBasedSeries::VectorBasedSeries(shared_ptr<vector<double>> a_spData, size_t a_offset)
: DataSeries(NULL, a_offset)
, m_spData(a_spData)
{
}
VectorBasedSeries::VectorBasedSeries(const DataSeries* a_pParent, shared_ptr<vector<double>> a_spData, size_t a_offset)
: DataSeries(a_pParent, a_offset)
, m_spData(a_spData)
{
}
VectorBasedSeries::~VectorBasedSeries(void)
{
}

double VectorBasedSeries::operator [](size_t a_elementIndex) const
{

   return (*m_spData)[a_elementIndex];
}

size_t VectorBasedSeries::size() const
{
   return m_spData->size();
}
