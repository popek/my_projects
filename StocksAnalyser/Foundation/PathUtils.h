#pragma once
#include "FoundationLib.h"

class FOUNDATION_LIB PathUtils
{
public:
   static CString concatenateDirAndFile(const CString& directory, const CString& fileName);

   static CString extractFileName(const CString& filePath);

   static bool isDirectory(const CString& path);

   static bool pathExists(const CString& path);

   static bool doesDirectoryExist(const CEdit& editWithPath);

   static bool doesDirectoryExist(const CString& path);
};

