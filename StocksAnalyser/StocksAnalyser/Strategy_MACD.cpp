#include "StdAfx.h"
#include "Strategy_MACD.h"
#include "Asset.h"
#include "MACD_pointer.h"
#include "AssetAuxiliaryWrappers.h"
#include "ChartElementsCollection.h"
#include "ChartElement.h"
#include "ControlsLibUtils.h"
#include "EMA_pointer.h"
#include "LineStyle.h"
#include "ColorUtils.h"
#include "DataSeries.h"


Strategy_MACD::Strategy_MACD(shared_ptr<Asset> a_spAsset)
: TradeStrategyBase(a_spAsset)
, _initialised(false)
{
   _spMacd = shared_ptr<MACD_pointer>(new MACD_pointer());
}
Strategy_MACD::~Strategy_MACD(void)
{
}

bool Strategy_MACD::ShouldBuy(size_t a_sampleIndex, double& a_buyPrice)
{
   Init();



   if(0 == _spMacd->Series(MACD_pointer::ELongEMA).size() ||
      a_sampleIndex < _spMacd->Series(MACD_pointer::ELongEMA).offset(false))
   {
      return false;
   }

   if( _spMacd->Series(MACD_pointer::EShortEMA).GetDataBasedOnRootSeriesIndex(a_sampleIndex) - 
      _spMacd->Series(MACD_pointer::ELongEMA).GetDataBasedOnRootSeriesIndex(a_sampleIndex) >= 0 )
   {
      double openValue = _spAsset->GetKandle(a_sampleIndex).Open();

      double valueOfShortEma = _spMacd->Series(MACD_pointer::EShortEMA).GetDataBasedOnRootSeriesIndex(a_sampleIndex);

      a_buyPrice =  (openValue > valueOfShortEma ? openValue : valueOfShortEma);

      return true;
   }
   return false;





   if(0 == _spMacd->Series(MACD_pointer::ESignal).size() ||
      a_sampleIndex < _spMacd->Series(MACD_pointer::ESignal).offset(false))
   {
      return false;
   }

   if( _spMacd->Series(MACD_pointer::EMACD).GetDataBasedOnRootSeriesIndex(a_sampleIndex) - 
       _spMacd->Series(MACD_pointer::ESignal).GetDataBasedOnRootSeriesIndex(a_sampleIndex) >= 0 )
   {
      double openValue = _spAsset->GetKandle(a_sampleIndex).Open();

      double valueOfShortEma = _spMacd->Series(MACD_pointer::EShortEMA).GetDataBasedOnRootSeriesIndex(a_sampleIndex);

      a_buyPrice =  (openValue > valueOfShortEma ? openValue : valueOfShortEma);

      return true;
   }
   return false;
}

bool Strategy_MACD::ShouldSell(size_t a_sampleIndex, double& a_sellPrice)
{
   Init();




   if(0 == _spMacd->Series(MACD_pointer::ELongEMA).size() ||
      a_sampleIndex < _spMacd->Series(MACD_pointer::ELongEMA).offset(false))
   {
      return false;
   }

   if( _spMacd->Series(MACD_pointer::EShortEMA).GetDataBasedOnRootSeriesIndex(a_sampleIndex) - 
      _spMacd->Series(MACD_pointer::ELongEMA).GetDataBasedOnRootSeriesIndex(a_sampleIndex) < 0 )
   {
      a_sellPrice = _spAsset->GetKandle(a_sampleIndex).Close();
      return true;
   }
   return false;





   if(0 == _spMacd->Series(MACD_pointer::ESignal).size() ||
      a_sampleIndex < _spMacd->Series(MACD_pointer::ESignal).offset(false))
   {
      return false;
   }

   if( _spMacd->Series(MACD_pointer::EMACD).GetDataBasedOnRootSeriesIndex(a_sampleIndex) - 
      _spMacd->Series(MACD_pointer::ESignal).GetDataBasedOnRootSeriesIndex(a_sampleIndex) < 0 )
   {
      a_sellPrice = _spAsset->GetKandle(a_sampleIndex).Close();
      return true;
   }
   return false;
}

void Strategy_MACD::Init()
{
   if(!_initialised)
   {
      _spMacd->Create( AssetCloseValueGetter(*_spAsset));
      _initialised = true;
   }
}

void Strategy_MACD::AddGraphicPresentation(shared_ptr<ChartElementsCollection> a_elements)
{
   a_elements->PushBack(ControlsLibUtils::CreatePointerSeriesElement(_spMacd->Series(MACD_pointer::ELongEMA), *_spAsset));
   a_elements->GetElement(a_elements->ElementsCount()-1)->Style()->SetNumericParam(LineStyle::EColor, 
                                                                                   ColorUtils::MixColors(ColorUtils::EBlue, ColorUtils::EWhite, 0.4));

   a_elements->PushBack(ControlsLibUtils::CreatePointerSeriesElement(_spMacd->Series(MACD_pointer::EShortEMA), *_spAsset));
   a_elements->GetElement(a_elements->ElementsCount()-1)->Style()->SetNumericParam(LineStyle::EColor, 
                                                                                   ColorUtils::MixColors(ColorUtils::EBlue, ColorUtils::EWhite, 0.65));

   a_elements->PushBack(ControlsLibUtils::CreatePointerSeriesElement(_spMacd->Series(MACD_pointer::EMACD), *_spAsset));
   a_elements->GetElement(a_elements->ElementsCount()-1)->Style()->SetNumericParam(LineStyle::EColor, 
                                                                                   ColorUtils::MixColors(ColorUtils::EBlue, ColorUtils::EWhite, 0.77));

   a_elements->PushBack(ControlsLibUtils::CreatePointerSeriesElement(_spMacd->Series(MACD_pointer::ESignal), *_spAsset));
   a_elements->GetElement(a_elements->ElementsCount()-1)->Style()->SetNumericParam(LineStyle::EColor, 
                                                                                   ColorUtils::MixColors(ColorUtils::EGreen, ColorUtils::EWhite, 0.9));
}