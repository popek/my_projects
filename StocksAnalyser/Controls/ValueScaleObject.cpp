#include "StdAfx.h"
#include "ValueScaleObject.h"
#include "ValueScaleContextEx.h"


ValueScaleObject::ValueScaleObject(CWnd* a_pChartControl)
: m_pChartControl(a_pChartControl)
, m_dragStartPos(-1)
, m_changingMaxValue(false)
, m_spContext(new ValueScaleContextEx())
{
}
ValueScaleObject::~ValueScaleObject(void)
{
}

void ValueScaleObject::UpdateArea(const CRect& a_newValueScaleObjectArea)
{
   m_rcValueScaleArea = a_newValueScaleObjectArea;
}

BOOL ValueScaleObject::IsPointOverObjectArea(const CPoint& a_point) const
{
   return m_rcValueScaleArea.PtInRect(a_point);
}

ValueScaleObject::EResult ValueScaleObject::OnLButtonDown(UINT a_flags, const CPoint& a_point)
{
   if(FALSE == m_rcValueScaleArea.PtInRect(a_point))
   {
      return ENothing;
   }

   m_dragStartPos = a_point.y;
   m_pChartControl->SetCapture();

   m_changingMaxValue = (m_dragStartPos <= m_rcValueScaleArea.CenterPoint().y);

   m_spContext->Buffer();

   return ENothing;
}

ValueScaleObject::EResult ValueScaleObject::OnLButtonUp(UINT a_flags, const CPoint& a_point)
{
   if(-1 != m_dragStartPos)
   {
      m_dragStartPos = -1;
      if(CWnd::GetCapture() == m_pChartControl)
      {
         ReleaseCapture();
      }

      return EMinMaxChanged;
   }

   return ENothing;
}

ValueScaleObject::EResult ValueScaleObject::OnMouseMove(UINT a_flags, const CPoint& a_point)
{
   if(-1 != m_dragStartPos)
   {
      int vertPos = a_point.y;
      if(vertPos > m_rcValueScaleArea.bottom)
      {
         vertPos = m_rcValueScaleArea.bottom;
      }
      else if(vertPos < m_rcValueScaleArea.top)
      {
         vertPos = m_rcValueScaleArea.top;
      }

      if(m_changingMaxValue)
      {
         m_spContext->RecalculateMaxValue( (Coordinate)m_dragStartPos, (Coordinate)vertPos );
      }
      else
      {
         m_spContext->RecalculateMinValue( (Coordinate)m_dragStartPos, (Coordinate)vertPos );
      }

      return EMinMaxChanged;
   }

   return ENothing;
}

ValueScaleObject::EResult ValueScaleObject::OnLButtonDblClk(UINT nFlags, CPoint point)
{
   m_spContext->UpdateFromMostReasanobleValues();
   
   return EMinMaxChanged;
}

void ValueScaleObject::SetMostReasonableMinMax(double a_minimum, double a_maximum)
{
   m_spContext->BufferMostReasonableMinMax(a_minimum, a_maximum);
}

double ValueScaleObject::CurrentMinimum() const
{
   return m_spContext->Minimum();
}

double ValueScaleObject::CurrentMaximum() const
{
   return m_spContext->Maximum();
}

shared_ptr<ValueScaleContext> ValueScaleObject::Context() const
{
   return m_spContext;
}