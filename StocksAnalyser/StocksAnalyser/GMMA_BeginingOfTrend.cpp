#include "StdAfx.h"
#include "GMMA_BeginingOfTrend.h"
#include "IPointer.h"
#include "Kandle.h"
#include "Asset.h"
#include "AssetContainer.h"
#include "Misc.h"


//
GMMA_BeginingOfTrend::Context::Context()
   : _shortMeansAboveLongMeansPeriodMin(1)
   , _shortMeansAboveLongMeansPeriodMax(4)
   , _breakingPeriod(10)
   , _shortMeansBelowLongMeansPeriod(5)
   , _weeklyAssetConfirmation(false)
   , _checkWeekSignal(true)
   , _algType(ECheckAllPeriods)
{
}

void GMMA_BeginingOfTrend::Context::Update(size_t a_shortMeansAboveLongMeansPeriodMin, 
                                           size_t a_shortMeansAboveLongMeansPeriodMax,
                                           size_t a_breakingPeriod,
                                           size_t a_shortMeansBelowLongMeansPeriod,
                                           bool   a_weeklyAssetConfirmation,
                                           bool   a_checkWeekSignal,
                                           EAlgorithType a_algType)
{
   _shortMeansAboveLongMeansPeriodMin = a_shortMeansAboveLongMeansPeriodMin;
   _shortMeansAboveLongMeansPeriodMax = a_shortMeansAboveLongMeansPeriodMax;
   _breakingPeriod = a_breakingPeriod;
   _shortMeansBelowLongMeansPeriod = a_shortMeansBelowLongMeansPeriod;
   _weeklyAssetConfirmation = a_weeklyAssetConfirmation;
   _checkWeekSignal = a_checkWeekSignal;
   _algType = a_algType;
}

size_t GMMA_BeginingOfTrend::Context::ShortMeansAboveLongMeansPeriod(bool a_getRangeStart) const
{
   return a_getRangeStart ? _shortMeansAboveLongMeansPeriodMin : _shortMeansAboveLongMeansPeriodMax;
}

size_t GMMA_BeginingOfTrend::Context::BreakingPeriod() const
{
   return _breakingPeriod;
}

size_t GMMA_BeginingOfTrend::Context::ShortMeansBelowLongMeansPeriod() const
{
   return _shortMeansBelowLongMeansPeriod;
}

bool GMMA_BeginingOfTrend::Context::WeeklyAssetConfirmationRequired() const
{
   return _weeklyAssetConfirmation;
}

bool GMMA_BeginingOfTrend::Context::CheckWeekSignal() const
{
   return _checkWeekSignal;
}

GMMA_BeginingOfTrend::Context::EAlgorithType GMMA_BeginingOfTrend::Context::AlgorithType() const
{
   return _algType;
}


//
GMMA_BeginingOfTrend::GMMA_BeginingOfTrend(shared_ptr<AssetContainer> a_spAssetContainer, 
                                           shared_ptr<GMMA_BeginingOfTrend::Context> a_spContext)
    : GMMA_Signal(a_spAssetContainer)
    , m_spContext(a_spContext)
{
}
GMMA_BeginingOfTrend::~GMMA_BeginingOfTrend(void)
{
}

bool GMMA_BeginingOfTrend::VerifyAssetConditions()
{
   // prepare temporary variables
   m_spTemporaryAsset = (*m_spAsset)[eDay];
   Misc::PreparePointers(*m_spTemporaryAsset, m_vTemporaryPointers, m_shortPeriodsPointersAmount);

   // if daily asset is verified, then also weekly asset must fulfill conditions
   if( false == VerifyDailyAssetCondition() )
   {
      return false;
   }

   if(true == m_spContext->WeeklyAssetConfirmationRequired())
   {
      return VerifyWeeklyAggregatedAssetCondition();
   }

   return true;
}

bool GMMA_BeginingOfTrend::VerifyDailyAssetCondition()
{
   switch(m_spContext->AlgorithType())
   {
   case Context::ECheckOnlyBuyPeriod:
      return VerifyOnlyBuyPeriod();

   case Context::ECheckAllPeriodsSimple:
      return VerifyAllPeriods(true);

   case Context::ECheckAllPeriods:
      return VerifyAllPeriods(false);
   default:
      ASSERT(FALSE);
      return false;
   }
}

bool GMMA_BeginingOfTrend::VerifyWeeklyAggregatedAssetCondition() const
{
   const size_t weekAssetShortMeansAboveLongMeansPeriod = 1;

   // update asset and pointers
   m_spTemporaryAsset = (*m_spAsset)[eWeek];
   Misc::PreparePointers(*m_spTemporaryAsset, m_vTemporaryPointers, m_shortPeriodsPointersAmount);

   if(0 == MinimalSamplesCount())
   {
      return false;
   }

   return ShortMeanSampleAboveLongMeanSample(1) || 
          ShortMeanSampleNearLongMeanSampleAndValueSampleAboveMaxMean(1);
}

bool GMMA_BeginingOfTrend::VerifyOnlyBuyPeriod()
{
   if(false == CheckPointersSamplesAmount(m_spContext->ShortMeansAboveLongMeansPeriod(false)))
   {
      return false;
   }

   // initialize offset
   size_t offset = 1;
   size_t samplesWhichFullfillesConditionCount = 0;
   // find samples amount which fulfill condition
   while( true == ShortMeanSampleAboveLongMeanSample(offset) || 
          true == ShortMeanSampleNearLongMeanSampleAndValueSampleAboveMaxMean(offset) )
   {
      ++samplesWhichFullfillesConditionCount;
      ++offset;

      if(samplesWhichFullfillesConditionCount >= m_spContext->ShortMeansAboveLongMeansPeriod(false))
      {
         return true;
      }
   }

   return false;
}


bool GMMA_BeginingOfTrend::VerifyAllPeriods(bool a_simpleWay)
{
   size_t maximumSamplesConfirmingSignal = m_spContext->ShortMeansAboveLongMeansPeriod(false);

   if(0 == MinimalSamplesCount())
   {
      return false;
   }
   // calculate max sample
   size_t maxOffset = MinimalSamplesCount() - 1;
   // initialize offset
   size_t offset = 1;

   // check boundry condition
   if( maxOffset < 3)
   {
      return false;
   }

   // find samples amount which fulfill condition
   size_t samplesWhichFullfillesConditionCount = 0;
   while( true == ShortMeanSampleAboveLongMeanSample(offset) || 
          true == ShortMeanSampleNearLongMeanSampleAndValueSampleAboveMaxMean(offset) )
   {
      ++samplesWhichFullfillesConditionCount;
      ++offset;

      if(samplesWhichFullfillesConditionCount > maximumSamplesConfirmingSignal ||
         offset > maxOffset)
      {
         return false;
      }
   }
   // if samples count which fullfills buy signal is less then minimal samples count then return false
   if(samplesWhichFullfillesConditionCount < m_spContext->ShortMeansAboveLongMeansPeriod(true))
   {
      return false;
   }

   // get samples amount which fulfills "getting throught" condition
   size_t samplesWhichAreGettingThrough = 0;
   while(offset < maxOffset && SampleIsBrakingThrough(offset))
   {
      ++offset;
      ++samplesWhichAreGettingThrough;
   }

   // if we are calculating not simple algorithm, then samples which are getting trhough must be less
   // then available breaking through period
   if(false == a_simpleWay &&
      samplesWhichAreGettingThrough > m_spContext->BreakingPeriod())
   {
      return false;
   }

   // calculate numbers of samples which fullfilles sell signal (short means are below long means
   bool sellSignalSamplesCountAtLeastTheSameAsBreakingThroughSamplesCount = false;
   size_t samplesWhichFulfillSellSignal = 0;
   size_t minSamplesWhichFulfillSellSignalCount = a_simpleWay ? 
                                                      // if there is no samples which are in "gettign through" state then minimal cound of samples in sell signal is 1
                                                      (samplesWhichAreGettingThrough > 0 ? samplesWhichAreGettingThrough : 1) :
                                                      // if the algorithm is not simple then minimum sell signal samples count must be greater or equal value from context
                                                      m_spContext->ShortMeansBelowLongMeansPeriod();

   while(offset < maxOffset && ShortMeanSampleBelowLongMeanSample(offset))
   {
      ++offset;
      ++samplesWhichFulfillSellSignal;

      if(samplesWhichFulfillSellSignal >= minSamplesWhichFulfillSellSignalCount)
      {
         sellSignalSamplesCountAtLeastTheSameAsBreakingThroughSamplesCount = true;
      }
   }

   return sellSignalSamplesCountAtLeastTheSameAsBreakingThroughSamplesCount;
}

size_t GMMA_BeginingOfTrend::ShortMeansComingThroughLongMeans(const size_t a_offsetStart, const size_t a_offsetEnd) const
{
   size_t offset = 0;

   for(size_t offset = a_offsetStart; offset <= a_offsetEnd; ++offset)
   {
      if(false == SampleIsBrakingThrough(offset))
      {
         return offset;
      }
   }

   // 0 means failure
   return 0;
}

size_t GMMA_BeginingOfTrend::SampleIsBrakingThrough(const size_t a_offset) const
{
   if(m_spContext->CheckWeekSignal())
   {
      return CalcShortTermPointersMean(a_offset) >= CalcLongTermPointersMean(a_offset);
   }
   else
   {
      return GetShortTermsMeansMaximum(a_offset) >= GetLongTermsMeansMinimum(a_offset);
   }
}

bool GMMA_BeginingOfTrend::ShortMeanSampleBelowLongMeanSample(const size_t a_offset) const
{
   if(m_spContext->CheckWeekSignal())
   {
      return CalcShortTermPointersMean(a_offset) <= CalcLongTermPointersMean(a_offset);
   }
   else
   {
      return GetShortTermsMeansMaximum(a_offset) <= GetLongTermsMeansMinimum(a_offset);
   }
}

bool GMMA_BeginingOfTrend::ShortMeanSampleAboveLongMeanSample(const size_t& a_ffset) const
{
   if(m_spContext->CheckWeekSignal())
   {
      return CalcShortTermPointersMean(a_ffset) >= CalcLongTermPointersMean(a_ffset);
   }
   else
   {
      return GetShortTermsMeansMinimum(a_ffset) >= GetLongTermsMeansMaximum(a_ffset);
   }
}

bool GMMA_BeginingOfTrend::ShortMeanSampleNearLongMeanSampleAndValueSampleAboveMaxMean(const size_t& a_offset) const
{
   if(false == m_spContext->CheckWeekSignal())
   {
      return false;
   }

   const double dValueRange = 0.03;

   double dMeansMax        = max(GetShortTermsMeansMaximum(a_offset), GetLongTermsMeansMaximum(a_offset));

   double dShortMeansValue = CalcShortTermPointersMean(a_offset);
   double dLongMeansValue  = CalcLongTermPointersMean(a_offset);
   double dMean            = (dShortMeansValue+dLongMeansValue)/2;

   double dLowerBound      = dMean*(1-dValueRange/2);
   double dUpperBound      = dMean*(1+dValueRange/2);

   if( (dShortMeansValue>=dLowerBound && dShortMeansValue<=dUpperBound) && 
      (dLongMeansValue>=dLowerBound && dLongMeansValue<=dUpperBound) )
   {
      if((*m_spTemporaryAsset)[m_spTemporaryAsset->Size()- a_offset].Close() > dMeansMax)
      {
         return true;
      }
   }

   return false;
}
