#pragma once

class StringUtils
{
public:
   StringUtils(const CString& a_strText);
   virtual ~StringUtils(void);

   operator size_t ();

   operator double ();

protected:
   bool     ValidCharsForDouble() const;

   bool     ValidCharsForNumeric() const;

   bool     ValidateChars(const CString& a_availableChars) const;

protected:

   const CString& m_strText;
};
