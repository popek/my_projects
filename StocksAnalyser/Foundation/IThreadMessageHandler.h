#pragma once 
#include "FoundationLib.h"

class FOUNDATION_LIB IThreadMessageHandler
{
public:
   virtual ~IThreadMessageHandler(){};

   virtual BOOL    ProcessThreadMessage(UINT a_messageId) = 0;

};