#pragma once
#include "LexAtom.h"


class SemAtom
{
public:
  SemAtom(void);
  SemAtom(const LexAtom& a_lexAtom);
  virtual ~SemAtom(void);

private:
  LexAtom _lexAtom;
};
