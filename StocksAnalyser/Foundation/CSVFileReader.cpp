#include "StdAfx.h"
#include "CSVFileReader.h"

CCSVFileReader::CCSVFileReader(void)
{
}

CCSVFileReader::~CCSVFileReader(void)
{
}

void CCSVFileReader::ReadFromFile(const MY_CHAR* a_pFilePath)
{
   resetInternalData();

   if(openCSVFile(a_pFilePath))
   {
      allocateAuxLineBuffer();
      while(!m_bEOF && !m_bError)
      {
         readNextLine();
      }

      closeCSVFile();
      freeAuxLineBuffer();
   }
   else
   {
      m_bError = true;
   }
}

CCSVFileReader::TSingleLine&  CCSVFileReader::operator [](size_t a_nLineIdx)
{
   if(a_nLineIdx >= m_csvLines.size())
   {
      throw std::exception("CCSVFileReader::operator [] index excedes dimension");
   }

   return m_csvLines[a_nLineIdx];
}

size_t CCSVFileReader::Size()
{
   return m_csvLines.size();
}

void CCSVFileReader::Erase(size_t a_nIdx)
{
   size_t nAux = 0;
   TStringLines::const_iterator it = m_csvLines.begin();
   if(a_nIdx < m_csvLines.size())
   {
      while(nAux < a_nIdx)
      {
         nAux++;
         it++;
      }
      m_csvLines.erase(it);
   }
}

bool CCSVFileReader::openCSVFile(const MY_CHAR* a_pFilePath)
{
   FOPEN(&m_csvFile, a_pFilePath, _T("r"));
   return (m_csvFile ? true : false);
}

void CCSVFileReader::closeCSVFile()
{
   fclose(m_csvFile);
}

void CCSVFileReader::readNextLine()
{
   char *pNextStringStart;
   pNextStringStart = m_pcAuxLineBuffer;
   bool bSuccess = true;
   vector<string> vecStrings;
   m_bEOL = false;

   if(fgets(m_pcAuxLineBuffer, 1024, m_csvFile))
   {
      while(!m_bEOL)
      {
         vecStrings.push_back(readStringValue(pNextStringStart));
      }

      m_csvLines.push_back(vecStrings);
   }
   else
   {
      if(feof(m_csvFile))
         m_bEOF = true;
      else
         m_bError = true;
   }
}

string CCSVFileReader::readStringValue(char*& a_prValueStartIdx)
{
   char* pAuxIdx;
   pAuxIdx = a_prValueStartIdx;
   string strRetVal;

   while(*pAuxIdx != ',')
   {
      if(*pAuxIdx == 10)
      {
         m_bEOL = true;
         break;
      }

      if(*pAuxIdx == '\0')
      {
         // and in this case even EOF is true
         m_bEOL = true;
         break;
      }

      ++pAuxIdx;
   }
   
   *pAuxIdx = '\0';
   
   strRetVal = string(a_prValueStartIdx);
   a_prValueStartIdx = pAuxIdx+1;
   
   return strRetVal;
}

void CCSVFileReader::allocateAuxLineBuffer()
{
   m_pcAuxLineBuffer = new char[1024];
}

void CCSVFileReader::freeAuxLineBuffer()
{
   delete [] m_pcAuxLineBuffer;
}

void CCSVFileReader::resetInternalData()
{
   m_bError = false;
   m_bEOF   = false;
   m_csvLines.clear();
}