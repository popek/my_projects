#include "StdAfx.h"
#include "LineGraphicObjectPainter.h"
#include "GraphicObjectBase.h"
#include "LineStyle.h"


LineGraphicObjectPainter::LineGraphicObjectPainter(bool a_drawPoints)
: m_drawPoints(a_drawPoints)
{
}
LineGraphicObjectPainter::~LineGraphicObjectPainter(void)
{
}

void LineGraphicObjectPainter::DrawObject(const GraphicObjectBase& a_object, CDC* a_pDC)
{
   const LineStyle* pLineStyle = dynamic_cast<const LineStyle*>(a_object.Style().get());
   ASSERT(pLineStyle);

   if(0 == a_object.PointsCount())
   {
      return;
   }

   CPen pen((int)pLineStyle->GetNumericParam(LineStyle::EStyle),
            (int)pLineStyle->GetNumericParam(LineStyle::EWidth),
            (COLORREF)pLineStyle->GetNumericParam(LineStyle::EColor));

   CPen* pOldPen = a_pDC->SelectObject(&pen);

   CPoint startPoint((int)a_object.GetAt(0)._x, (int)a_object.GetAt(0)._y);
   CPoint endPoint = startPoint;

   for(size_t pointIndex=1; pointIndex<a_object.PointsCount(); ++pointIndex)
   {
      endPoint.x = (int)a_object.GetAt(pointIndex)._x;
      endPoint.y = (int)a_object.GetAt(pointIndex)._y;

      a_pDC->MoveTo(startPoint);
      a_pDC->LineTo(endPoint);

      if(m_drawPoints)
      {
         DrawPoint(startPoint, a_pDC, (COLORREF)pLineStyle->GetNumericParam(LineStyle::EPointColor));
      }

      startPoint = endPoint;
   }

   if(m_drawPoints)
   {
      DrawPoint(endPoint, a_pDC, (COLORREF)pLineStyle->GetNumericParam(LineStyle::EPointColor));
   }

   a_pDC->SelectObject(pOldPen);

   /*
   Gdiplus::Graphics graphicObject(a_DC.GetSafeHdc());
   Gdiplus::Pen      pen(Gdiplus::Color(175, 0, 0, 0));

   for(size_t nPointIdx=0; nPointIdx<m_vPoints.size()-1; ++nPointIdx)
   {

   graphicObject.DrawLine(&pen, m_vPoints[nPointIdx]._xCoord, m_vPoints[nPointIdx]._yCoord,
   m_vPoints[nPointIdx+1]._xCoord, m_vPoints[nPointIdx+1]._yCoord );
   }
   */
}

void LineGraphicObjectPainter::DrawPoint(const CPoint& a_point, CDC* a_pDC, COLORREF a_color)
{
   const int rectLen = 8;

   CRect rcPoint(0, 0, rectLen, rectLen);
   rcPoint.MoveToXY(a_point.x - rectLen/2, a_point.y - rectLen/2);

   a_pDC->FillSolidRect(rcPoint, a_color);

   CPoint arrPoints[] = 
   {
      CPoint(rcPoint.left, rcPoint.top),
      CPoint(rcPoint.right, rcPoint.top),
      CPoint(rcPoint.right, rcPoint.bottom),
      CPoint(rcPoint.left, rcPoint.bottom),
      CPoint(rcPoint.left, rcPoint.top)
   };

   a_pDC->Polyline(arrPoints, 5);
}
