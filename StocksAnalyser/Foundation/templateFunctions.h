#pragma once


// template<class _TYPE>
// shared_ptr<_TYPE> make_shared()
// {
//    return shared_ptr<_TYPE>(new _TYPE());
// };
// 
// template<class _TYPE, class _Arg0>
// shared_ptr<_TYPE> make_shared(_Arg0 arg0)
// {
//    return shared_ptr<_TYPE>(new _TYPE(arg0));
// };
// 
// template<class _TYPE, class _Arg0, class _Arg1>
// shared_ptr<_TYPE> make_shared(_Arg0 arg0, _Arg1 arg1)
// {
//    return shared_ptr<_TYPE>(new _TYPE(arg0, arg1));
// };
// 
// template<class _TYPE, class _Arg0, class _Arg1, class _Arg2>
// shared_ptr<_TYPE> make_shared(_Arg0 arg0, _Arg1 arg1, _Arg2 arg2)
// {
//    return shared_ptr<_TYPE>(new _TYPE(arg0, arg1, arg2));
// };
// 
// template<class _TYPE, class _Arg0, class _Arg1, class _Arg2, class _Arg3>
// shared_ptr<_TYPE> make_shared(_Arg0 arg0, _Arg1 arg1, _Arg2 arg2, _Arg3 arg3)
// {
//    return shared_ptr<_TYPE>(new _TYPE(arg0, arg1, arg2, arg3));
// };
// 
// template<class _TYPE, class _Arg0, class _Arg1, class _Arg2, class _Arg3, class _Arg4>
// shared_ptr<_TYPE> make_shared(_Arg0 arg0, _Arg1 arg1, _Arg2 arg2, _Arg3 arg3, _Arg4 arg4)
// {
//    return shared_ptr<_TYPE>(new _TYPE(arg0, arg1, arg2, arg3, arg4));
// };