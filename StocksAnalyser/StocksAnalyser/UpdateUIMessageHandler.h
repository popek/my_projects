/**
 * @file
 * @author  Marcin Wrobel <marwrobel@gmail.com>
 *
 * @section DESCRIPTION
 * class responsible for handling UI messages which are sent by GMMA processing thread
 */
 
#pragma once
#include "CriticalSection.h"

class SearchGMMAsignalsDlg;

class UpdateUIMessageHandler
{
   struct QueueData
   {
      UINT        m_messageID;
      CString     m_text;
      int         m_number;
   };

public:
   UpdateUIMessageHandler(SearchGMMAsignalsDlg* a_pOwner);
   virtual ~UpdateUIMessageHandler(void);

   BOOL TryToHandle(const UINT& a_msgId);

   void PostUpdateProgress(CString a_strLabel, int a_progressPos);

   void PostSetProgressRange(int a_range);

   void PostAddNewAsset(CString a_assetFilePath);

   void PostAfterProcessing();

protected:

   void HandleQueuedMessages();

   void HandleData(QueueData& a_dataToHandle);

protected:

   SearchGMMAsignalsDlg*  m_pOwner;

   CriticalSection        m_csQueueLock;

   deque<QueueData>       m_deqMessages;
};
