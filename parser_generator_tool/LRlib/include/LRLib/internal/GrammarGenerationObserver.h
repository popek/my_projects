#pragma once
#include <chrono>

namespace lrlib
{
namespace helper
{

class IGrammarGenerationObserver
{
public:
   virtual ~IGrammarGenerationObserver() = default;

   virtual void onGrammarGenerationStarted() = 0;
   virtual void onGrammarGenerationFinished() = 0;

   virtual void onStateGenerationStarted() = 0;
   virtual void onStateGenerationFinished() = 0;

   virtual void onGotoGenerationStarted() = 0;
   virtual void onGotoGenerationFinished() = 0;
};

//
class GrammarGenerationTimings : public IGrammarGenerationObserver
{
public:
   GrammarGenerationTimings();

   void onGrammarGenerationStarted() override;
   void onGrammarGenerationFinished() override;

   void onStateGenerationStarted() override;
   void onStateGenerationFinished() override;

   void onGotoGenerationStarted() override;
   void onGotoGenerationFinished() override;

private:
   int stateCnt_;
   int gotoCnt_;

   std::chrono::system_clock::time_point startTime_;

   std::chrono::system_clock::time_point stateStartTime_;

   std::chrono::system_clock::time_point gotoStartTime_;
   double gotosAccumulator_;
};

}
}
