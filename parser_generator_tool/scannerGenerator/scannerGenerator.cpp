// scannerGenerator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <REGEXLib\RegExManager.h>
#include <REGEXLib\LexManager.h>

using namespace std;


int main(int argc, char* argv[])
{
   if (argc != 4)
   {
      printf("ERROR: usage regEx.exe [model type (s(imple),c(omplex)] [LexinputFile] [outputDirecotry]\n");
      return 0;
   }

   regexlib::LexManager LM(*(argv[1]) == 'c' ? regexlib::eREM_ComplexRegEx : regexlib::eREM_SimpleRegEx);
   if (LM.CreateLexer(argv[2]))
   {
      if (!LM.GenerateCode(argv[3]))
      {
         printf("ERROR: while trying to generate code into '%s' directory\n", argv[3]);
         return 0;
      }
   }
   else
   {
      printf("ERROR: while trying to create lexer from '%s' lex file definition\n", argv[2]);
      return 0;
   }

   printf("DONE: %s model of lexer from lex file '%s', created succesfully in '%s' directory\n", *(argv[1]) == 'c' ? "complex" : "simple", argv[2], argv[3]);
   return 0;
}