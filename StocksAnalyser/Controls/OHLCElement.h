#pragma once
#include "ControlsLib.h"
#include "DataSeriesChartElement.h"


class Asset;


class CONTROLS_LIB OHLCElement : public DataSeriesChartElement
{
public:
   OHLCElement(const Asset& a_dataSource);
   virtual ~OHLCElement(void);

   virtual shared_ptr<GraphicObjectBase> CreateGraphicObject(const TimeScaleContext& a_timeScaleContext, const ValueScaleContext& a_valueScaleContext) const;

private:
   // close series values are kept in chart points vector

   vector<double> m_openSeries;
   vector<double> m_lowSeries;
   vector<double> m_highSeries;
};
