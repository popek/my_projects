#include "stdafx.h"
#include "XercescSAXHandler.h"
#include "XMLNode.h"
#include <xercesc/util/XMLUniDefs.hpp>
#include <xercesc/util/XMLUni.hpp>
#include <xercesc/sax/AttributeList.hpp>


// ---------------------------------------------------------------------------
//  XercescSAXHandler: Constructors and Destructor
// ---------------------------------------------------------------------------
XercescSAXHandler::XercescSAXHandler()
{
}
XercescSAXHandler::~XercescSAXHandler()
{
}

shared_ptr<XMLNode> XercescSAXHandler::detachNode()
{
   auto rootBuffer = _rootNode;
   _rootNode = nullptr;

   return rootBuffer;
}

// ---------------------------------------------------------------------------
//  XercescSAXHandler: Overrides of the SAX DocumentHandler interface
// ---------------------------------------------------------------------------
void XercescSAXHandler::startElement(const   XMLCh* const   name, xercesc::AttributeList&  attributes)
{
   auto newNode = make_shared<XMLNode>((LPCTSTR)name);
   if(_nodes.size())
   {
      _nodes.top()->addNode(newNode);
   }
   else
   {
      _rootNode = newNode;
   }

   for(XMLSize_t attribIndex = 0; attribIndex < attributes.getLength(); ++attribIndex)
   {
      newNode->addParam((LPCTSTR)attributes.getName(attribIndex), (LPCTSTR)attributes.getValue(attribIndex));
   }

   _nodes.push(newNode);
}

void XercescSAXHandler::endElement(const XMLCh* const name)
{
   _nodes.pop();
}

void XercescSAXHandler::characters(  const   XMLCh* const chars, const XMLSize_t length)
{
}

void XercescSAXHandler::resetDocument()
{
}


// ---------------------------------------------------------------------------
//  XercescSAXHandler: Overrides of the SAX ErrorHandler interface
// ---------------------------------------------------------------------------
void XercescSAXHandler::error(const xercesc::SAXParseException& /*e*/)
{
}

void XercescSAXHandler::fatalError(const xercesc::SAXParseException& /*e*/)
{
}

void XercescSAXHandler::warning(const xercesc::SAXParseException& /*e*/)
{
}

