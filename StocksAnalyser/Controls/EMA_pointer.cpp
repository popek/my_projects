#include "StdAfx.h"
#include "EMA_pointer.h"
#include "CommonDefines.h"
#include "DataSeries.h"
#include "ControlsLibUtils.h"


EMA_pointer::EMA_pointer(int a_nMeanValueCalculationSamplesAmount)
: PointerAvarageBase(a_nMeanValueCalculationSamplesAmount)
{
}
EMA_pointer::~EMA_pointer()
{
}

void EMA_pointer::Create(const DataSeries& a_inputData)
{
   // check if samples amount needed to prepare mean value is grater then 0
   if(m_nMeanValueCalculationSamplesAmount <= 0)
      return;
   // make calculation only in case asset samples amount is grater then samples amount needed to prepare mean value
   if((int)a_inputData.size() < m_nMeanValueCalculationSamplesAmount)
      return;

   double dSamplesSum  = 0;
   double factor       = 2/(1 + (double)m_nMeanValueCalculationSamplesAmount);
   
   shared_ptr<vector<double>> spData(new vector<double>());
   // prepare samples container
   vector<double>& meanValues = *spData;

   meanValues.assign(a_inputData.size() - m_nMeanValueCalculationSamplesAmount+1, 0.0);

   // calculate first value (simple SMA)
   for(size_t elementIndex=0; elementIndex<(size_t)m_nMeanValueCalculationSamplesAmount; ++elementIndex)
   {
      meanValues[0] += a_inputData[elementIndex];
   }
   meanValues[0] /= m_nMeanValueCalculationSamplesAmount;
      
   // calculate rest samples
   size_t prev_sample_idx = 0;
   for(size_t elementIndex=(size_t)m_nMeanValueCalculationSamplesAmount; elementIndex<a_inputData.size(); ++elementIndex, ++prev_sample_idx)
   {
      meanValues[prev_sample_idx+1] = (a_inputData[elementIndex]- meanValues[prev_sample_idx])*factor + meanValues[prev_sample_idx];
   }

   m_spDataSeries = shared_ptr<DataSeries>(new VectorBasedSeries(&a_inputData, spData, m_nMeanValueCalculationSamplesAmount - 1));
}