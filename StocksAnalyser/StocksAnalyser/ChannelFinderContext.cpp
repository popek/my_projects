#include "StdAfx.h"
#include "ChannelFinderContext.h"
#include "ParallelChannelFinder.h"


ChannelFinderContext::ChannelFinderContext(void)
{
   m_period = 100;
   m_algorithmType = ParallelChannelFinder::EChannel;

   m_minimumLinePointsAmount = 3;

   m_maxDeviation2LineValue = 0.01;

   m_maxDataSource2LineValueDistance = 0.01;

   m_showConfirmedSignal = true;
}
ChannelFinderContext::~ChannelFinderContext(void)
{
}

void ChannelFinderContext::Update(int a_period,
                             UINT a_algorithmType,
                             int a_minimumLinePointsAmount,
                             double a_maxDeviation2LineValue,
                             double a_maxDataSource2LineValueDistance,
                             bool a_showConfirmedSignal)
{
   m_period = a_period;
   m_algorithmType = a_algorithmType;

   m_minimumLinePointsAmount = a_minimumLinePointsAmount;

   m_maxDeviation2LineValue = a_maxDeviation2LineValue;

   m_maxDataSource2LineValueDistance = a_maxDataSource2LineValueDistance;

   m_showConfirmedSignal = a_showConfirmedSignal;
}

int ChannelFinderContext::Period() const
{
   return m_period;
}

UINT ChannelFinderContext::AlgType() const
{
   return m_algorithmType;
}

int ChannelFinderContext::MinLinePointsCount() const
{
   return m_minimumLinePointsAmount;
}

double ChannelFinderContext::MaxDev2LineValue() const
{
   return m_maxDeviation2LineValue;
}

double ChannelFinderContext::MaxData2LineValueDistance() const
{
   return m_maxDataSource2LineValueDistance;
}

bool ChannelFinderContext::ShowConfirmedSignal() const
{
   return m_showConfirmedSignal;
}

bool ChannelFinderContext::IsValid() const
{
   return m_period > 0;
};