#pragma once
#include "ParserErrors.h"
#include "ParserWarnings.h"

#include <memory>

namespace lrlib
{

class SymbolsContainer;
class Rules;

class CompileContext
{
public:
   CompileContext(SymbolsContainer& symbols, Rules& rules)
      : symbols_(symbols), rules_(rules) {}

   SymbolsContainer& symbols() { return symbols_; };
   Rules& rules() { return rules_; };

   ParserErrors& errors() { return errors_; };
   ParserWarnings& warnings() { return warnings_; };

private:
   SymbolsContainer& symbols_;
   Rules& rules_;

   ParserErrors errors_;
   ParserWarnings warnings_;
};

} // namespace lrlib