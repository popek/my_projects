#pragma once
#include "TradeStrategyBase.h"


class Asset;
class MACD_pointer;

class ChartElementsCollection;


class Strategy_MACD : public TradeStrategyBase
{
public:
   Strategy_MACD(shared_ptr<Asset> a_spAsset);
   virtual ~Strategy_MACD(void);

private:
   void Init();

protected:

   virtual bool ShouldBuy(size_t a_sampleIndex, double& a_buyPrice);

   virtual bool ShouldSell(size_t a_sampleIndex, double& a_sellPrice);

   virtual void AddGraphicPresentation(shared_ptr<ChartElementsCollection> a_elements);

private:
   shared_ptr<MACD_pointer> _spMacd;
   bool _initialised;
};
