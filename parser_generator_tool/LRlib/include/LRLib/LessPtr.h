#pragma once 
//

namespace lrlib
{

template<class _Ty>
class LessPtr
{
public:
   bool operator()(const _Ty* _Left, const _Ty* _Right) const
   {
      return ((*_Left) < (*_Right));
   }
};

} // namespace lrlib