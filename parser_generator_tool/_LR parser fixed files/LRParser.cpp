#include "stdafx.h"
#include "LRParser.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


LRParser::LRParser()
{
}

bool LRParser::Compile(string_t a_scriptFile)
{
  LexAtom atom;
  int iTopState;
  bool bAccept = false,
       bParseError  = false;

  stack<int> stateStack;

  auto spSemanticErrors = make_shared<SemanticErrors>();
  _spActions->SetSemanticErrorObserver(spSemanticErrors);

  stateStack.push(0);// initialize state stack
  _spScaner->GetNextSignificantAtom(&atom);
  if(Eerror == atom.id())
  {
    bParseError = true;
    ErrorRecovery();
  }

  while( !(bAccept || bParseError || spSemanticErrors->errorOccured()) )
  {
    iTopState = stateStack.top();

    if(TermShiftArray[iTopState][atom.id()] > -1) //shift actions appears
    {
      if(ReduceArray[iTopState][atom.id()] > -1)
        throw; //shift reduce conflict

      stateStack.push(TermShiftArray[iTopState][atom.id()]);
      _spActions->PushSymbonOnStack( make_shared<SemAtom>(atom) );
      _spScaner->GetNextSignificantAtom(&atom);
      if(Eerror == atom.id())
      {
        bParseError = true;
        ErrorRecovery();
      }
    }
    else if(ReduceArray[iTopState][atom.id()] > 0) //if reduction appears
    {
      int iRuleID = ReduceArray[iTopState][atom.id()];
      int iLen = RulesLength[iRuleID];
      int iNontermID = Productions[iRuleID];

      _spActions->DoReduceAction(iRuleID); //do action

      while(iLen--)
	  {
        stateStack.pop();
	  }

      stateStack.push(NontermShiftArray[ stateStack.top() ][iNontermID]);
    }
    else if(ReduceArray[iTopState][atom.id()] == 0) // if acceptance appears
    {
      spSemActions->DoReduceAction(0); //do accept action
      bAccept = true;
    }
    else // if no reduce, no shift, no acceptance, then error appears
    {
      bParseError = true;
      ErrorRecovery(); // print out compilation error message
    }
  }

  // recover from error
  if(spSemanticErrors->errorOccured())
  {
     ErrorRecovery();
  }

  return false == bParseError && true == bAccept && false == spSemanticErrors->errorOccured();
}

void LRParser::ErrorRecovery()
{
}