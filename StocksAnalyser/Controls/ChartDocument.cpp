#include "stdafx.h"
#include "ChartDocument.h"
#include "CSVFileReader.h"
#include "Asset.h"
#include "ChartView.h"
#include "AssetsListBuffer.h"
#include "IBeforeShowChartEvent.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IBeforeShowChartEvent* ChartDocument::Sm_pBeforeShowAssetEvent = NULL;

IMPLEMENT_DYNCREATE(ChartDocument, CDocument)

BEGIN_MESSAGE_MAP(ChartDocument, CDocument)
END_MESSAGE_MAP()


ChartDocument::ChartDocument()
{
}

ChartDocument::~ChartDocument()
{
}

BOOL ChartDocument::OnNewDocument()
{
   if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

BOOL ChartDocument::OnOpenDocument(LPCTSTR lpszPathName)
{
   m_spAssetsContainer = AssetsListBuffer::Instance()->GetAssetContainer(lpszPathName);

   // if asset is not buffered then we need to load it
   if(NULL == m_spAssetsContainer.get())
   {
      m_spAssetsContainer = shared_ptr<AssetContainer>(new AssetContainer());
      m_spAssetsContainer->LoadDaysAssetFromFile(lpszPathName);
   }

   if(NULL != Sm_pBeforeShowAssetEvent)
   {
      Sm_pBeforeShowAssetEvent->OnBeforeShowingAsset(*((*m_spAssetsContainer)[eDay]));
   }

   POSITION pos = GetFirstViewPosition();
   while (pos != NULL)
   {
      ChartView* pView = (ChartView*)( GetNextView(pos) );
      pView->SetAsset((*m_spAssetsContainer)[eDay]);
   }  

   return TRUE;
}

void ChartDocument::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

void ChartDocument::CreateAnotherViewForDocument(EAggregationType a_aggregationType)
{
   BOOL bAutoDelete = m_bAutoDelete;
   m_bAutoDelete = FALSE;
   CFrameWnd* pFrame = GetDocTemplate()->CreateNewFrame(this, NULL);
   m_bAutoDelete = bAutoDelete;

   if(NULL == pFrame)
   {
      AfxMessageBox(_T("Nie uda�o si� stworzy� nowego widoku dla aktualnego dokumentu"));
      return;
   }
   
   ChartView* pNewView = NULL;
   POSITION pos = GetFirstViewPosition();
   while (pos != NULL)
   {
      pNewView = (ChartView*)(GetNextView(pos));
   }  

   if(NULL != Sm_pBeforeShowAssetEvent)
   {
      Sm_pBeforeShowAssetEvent->OnBeforeShowingAsset(*((*m_spAssetsContainer)[a_aggregationType]));
   }

   if(pNewView)
   {
      ChartView* pView = (ChartView*)( pNewView );
      pView->SetAsset((*m_spAssetsContainer)[a_aggregationType]);
   }  

   GetDocTemplate()->InitialUpdateFrame(pFrame, this);
   //pFrame->UpdateFrameTitleForDocument(GetTitle() + AssetContainer::Type2String(a_assetType));
}

void ChartDocument::SetBeforeShowChartEvent(IBeforeShowChartEvent* a_pEvent)
{
   Sm_pBeforeShowAssetEvent = a_pEvent;
}