#pragma once
#include "ValueScaleContext.h"


class ValueScaleContextEx : public ValueScaleContext
{
   NONCOPYABLE_TYPE(ValueScaleContextEx)

public:
   ValueScaleContextEx(void);
   virtual ~ValueScaleContextEx(void);

   void Buffer();

   void RecalculateMaxValue(Coordinate a_previousPos, Coordinate a_currentPos);

   void RecalculateMinValue(Coordinate a_previousPos, Coordinate a_currentPos);

   void UpdateFromMostReasanobleValues();

   void BufferMostReasonableMinMax(double a_minimum, double a_maximum);

   double Minimum() const;
   
   double Maximum() const;

protected:
   double m_maxValueBuffer;
   double m_minValueBuffer;
   double m_coordinate2valueBuffer;

   double m_mostReasonableMinimum;
   double m_mostReasonableMaximum;
};
