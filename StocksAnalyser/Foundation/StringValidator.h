#pragma once
#include "FoundationLib.h"

class FOUNDATION_LIB StringValidator
{
public:
	StringValidator();
	virtual ~StringValidator();

   virtual bool ProperLeftSubstring(const CString& a_text) const = 0;

   virtual bool ProperString(const CString& a_text) const = 0;

   virtual bool Valid(const TCHAR a_char) const = 0;
};


