%%
LETTER -> a-z,A-Z,
DIGIT  -> 0-9,
WS     -> "\t", "\s",
CR	   -> "\r",
LF	   -> "\n",
STAR   -> *,
LPAR   -> (,
RPAR   -> ),

%%
l_par					-> LPAR
r_par					-> RPAR
coma					-> ,
semicolon				-> ;
add_op 					-> + | -
const_double 			-> ((DIGIT)* . (DIGIT)(DIGIT)*)
const_int   			-> ((DIGIT)(DIGIT)*)
identifier				-> ((LETTER)(LETTER|DIGIT|_)*)|%|$
mul_op					-> STAR | /
cmp_op					-> = | (<>) | (<=) | (>=) | < | >
assign					-> (: =)
eol		 		  		-> (CR LF)|LF
blank 	 	      		-> WS WS*
%%