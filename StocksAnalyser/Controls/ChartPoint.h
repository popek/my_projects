#pragma once
#include "ControlsLib.h"
#include "controlsLibTypes.h"
#include "KandleDate.h"


class KandleDate;


class CONTROLS_LIB ChartPoint
{
public:
   ChartPoint(shared_ptr<KandleDate> a_spTimestamp, Coordinate a_value);
   virtual ~ChartPoint(void);

   shared_ptr<KandleDate> Timestamp() const;
   
   double Value() const;

protected:

   shared_ptr<KandleDate> m_spTimestamp;
   double                 m_value;
};
