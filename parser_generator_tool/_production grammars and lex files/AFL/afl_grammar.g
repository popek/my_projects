// statements
	AFL_SCRIPT -> STMT_LIST;

	STMT_LIST ->  STMT
				| STMT_LIST STMT;

	STMT ->   FUN_DEF
			| RUNTIME_STMT;
	
	RUNTIME_STMT_LIST ->  RUNTIME_STMT
						| RUNTIME_STMT_LIST RUNTIME_STMT;
						
	RUNTIME_STMT ->   STMT_WHILE
					| STMT_FOR_LOOP
					| STMT_IF
					| STMT_SWITCH
					| STMT_COMPOUND
					| STMT_SIMPLE;
				  
	FUN_DEF -> FUN_DEF_PREFIX FUN_DECL STMT_COMPOUND;
	FUN_DECL -> identifier ( ARGS_DECL_LIST )
			  | identifier ( );
	ARGS_DECL_LIST -> identifier
					| ARGS_DECL_LIST, identifier;
	FUN_DEF_PREFIX -> function									// 'function' terminal
					| procedure;								// 'procedure' terminal
	
	STMT_COMPOUND -> { RUNTIME_STMT_LIST }
					| { };

	STMT_SIMPLE -> STMT_SIMPLE_LEFT semicolon					// semicolon: ;
				 | semicolon;	/*empty statement*/	

	STMT_SIMPLE_LEFT -> LEXPR
					  | STMT_FUNCTION_SPECIFIC 
					  | STMT_DO_WHILE
					  | STMT_BREAK
					  | STMT_CONTINUE;
					
	STMT_FUNCTION_SPECIFIC -> STMT_EXPLICIT_VAR_DECL
							| STMT_RETURN;
	
	STMT_EXPLICIT_VAR_DECL -> local identifier						// 'local' terminal
							| global identifier;					// 'global' terminal
					   
	STMT_RETURN -> return 											// 'return' terminal
				 | return LEXPR;

// loops				
	STMT_DO_WHILE -> DO_WHILE_LEFT WHILE_LOOP_BODY while( LEXPR ) ;				// 'do' terminal, 'while' terminal
	DO_WHILE_LEFT -> do;
	
	STMT_WHILE	  -> WHILE_HEADER WHILE_LOOP_BODY ;
	WHILE_HEADER -> WHILE_HEADER_START ( LEXPR );
	WHILE_HEADER_START -> while;
	WHILE_LOOP_BODY -> RUNTIME_STMT;
	
	STMT_FOR_LOOP -> FOR_LOOP_HEADER FOR_LOOP_BODY;
	FOR_LOOP_HEADER -> FOR_LOOP_BEGIN FOR_LOOP_MID FOR_LOOP_END;
	FOR_LOOP_BEGIN -> for( semicolon
					| for( FOR_LOOP_LIST semicolon ;
	FOR_LOOP_BODY -> RUNTIME_STMT;
	FOR_LOOP_MID -> LEXPR semicolon 
				  | semicolon;
	FOR_LOOP_END -> FOR_LOOP_LIST )
				  | ) ;
	FOR_LOOP_LIST -> LEXPR
				   | FOR_LOOP_LIST, LEXPR;
				  
	STMT_BREAK -> break;											// 'break' terminal	
	STMT_CONTINUE -> continue;										// 'continue' terminal
				
//if statement. Dangling else will be resolved by choosing shift over reduce
	STMT_IF -> IF_HEADER IF_BLOCK
			 | IF_HEADER IF_BLOCK else IF_BLOCK;
			
	IF_BLOCK -> RUNTIME_STMT;
	
	IF_HEADER -> if ( LEXPR );

// switch statement
	STMT_SWITCH -> SWITCH_HEADER SWITCH_BODY;
	
	SWITCH_HEADER -> switch ( LEXPR );
	
	SWITCH_BODY -> { SWITCH_CASES_LIST }
				 | { }; 
	
	SWITCH_CASES_LIST -> SWITCH_CASE
					   | SWITCH_CASES_LIST SWITCH_CASE;
					   
	SWITCH_CASE -> SWITCH_CASE_HEADER
				 | SWITCH_CASE_HEADER SWITCH_CASE_BODY;
	
	SWITCH_CASE_BODY -> RUNTIME_STMT_LIST;
	
	SWITCH_CASE_HEADER -> case LEXPR : 			// 'case' terminal
						| default : ;			// 'default' terminal 	


// logical expression head
	LEXPR -> LEXPR_OR;
				 
// logical expressions

	LEXPR_OR -> LEXPR_AND
			  | LEXPR_OR log_or_op LEXPR_AND;			// log_or_op: OR, ||

	LEXPR_AND -> LEXPR_NOT
			   | LEXPR_AND log_and_op LEXPR_NOT;	 // log_and_op: AND, &&
			   
	LEXPR_NOT -> LEXPR_BITOR
			   | log_not_op LEXPR_BITOR;			// log_not_op: NOT
			   
	LEXPR_BITOR -> LEXPR_BITAND
				 | LEXPR_BITOR bit_or_op LEXPR_BITAND;	// bit_or_op: |
				
	LEXPR_BITAND -> LEXPR_CMP_EQ
				  | LEXPR_BITAND bit_and_op LEXPR_CMP_EQ;	// bit_and_op: &

	LEXPR_CMP_EQ -> LEXPR_CMP_MORE_LESS
				  | LEXPR_CMP_EQ cmp_eq_op LEXPR_CMP_MORE_LESS;			// cmp_eq_op: ==, !=

	LEXPR_CMP_MORE_LESS -> AEXPR_ADD
						 | LEXPR_CMP_MORE_LESS cmp_more_less_op AEXPR_ADD;	// cmp_more_less_op: >, >=, <, <=

// arithmethic expressions

	AEXPR_ADD -> AEXPR_MUL 
		   | AEXPR_ADD add_op AEXPR_MUL;				// add_op: +, -

	AEXPR_MUL -> AEXPR_SIGN 
			   | AEXPR_MUL mul_op AEXPR_SIGN;		// mul_op: *, /, %

	AEXPR_SIGN ->  AEXPR_POW
				| add_op AEXPR_POW;					// add_op: +, -

	AEXPR_POW -> AEXPR_INCR
			   | AEXPR_POW pow_op AEXPR_INCR;		// pow_op: ^
    
	AEXPR_INCR -> BASE_EXPR
			    | incr_op BASE_EXPR
				| BASE_EXPR incr_op;    			// incr_op: ++, --
				
	BASE_EXPR -> ( LEXPR )
				| FUN_CALL 
				| const_string
				| CONST_NUMERIC
				| LVALUE
				| LVALUE assign LEXPR;
				
	LVALUE -> identifier
			| identifier [ LEXPR ];
	
	FUN_CALL -> COM_OBJECT FUN_CALL_RIGHT
			  | FUN_CALL_RIGHT;
	
	FUN_CALL_RIGHT -> FUN_NAME ( ARG_LIST )
					| FUN_NAME ( );
	
	COM_OBJECT -> identifier . ;					// '.' terminal
	
	FUN_NAME -> identifier;

	ARG_LIST ->   LEXPR
				| ARG_LIST , LEXPR;					// ,
	
	CONST_NUMERIC ->  const_int						// const_int: constant integer value
					| const_double;					// constant float value
					


/*
semantic rules which will be checkeed later (they are not defined in grammar):

functions semantic rules:
a) return can be only at the end
b) function body can't be empty
c) procedure can't return value

explicit variables declaration (variable can be declared global or local):
a) declaration can appear (and makes sence) only inside of a function body

loops:
a) checking for endless loops (e.g. var = 34; for(; var>23; ++var) {} - this is endless loop since var will always be grater then 23)

switch-case statement:
a) switch-case can't be empty
b) default: case can be placed one time and on the end only
c) case condition must be constant value

break statement:
a) can be used only in loops and switch-case

continue statement:
a) can be used only in loops

return statement:
a) can be used only in functions
*/					
		