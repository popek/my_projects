#pragma once
#include "IMouseInteraction.h"
#include <GdiPlus.h>

class ChartControl;

class SetTimeRangeAction : public IMouseInteraction
{
public:
   SetTimeRangeAction(ChartControl* a_pOwner);
   virtual ~SetTimeRangeAction(void);

   virtual void OnLButtonDown(UINT a_nFlags, const CPoint& a_point);
   virtual void OnLButtonUp(UINT a_nFlags, const CPoint& a_point);
   virtual void OnMouseMove(UINT a_nFlags, const CPoint& a_point);

   virtual void OnSize(int a_chartWidth);

   virtual void Draw(CDC& a_dc);

   virtual bool DrawingRequired() const;

protected:

   void         Reset();

   void         UpdateRange();

protected:

   bool   m_bSetRangeStarted;

   INT    m_XStart;
   INT    m_XFinish;

   INT    m_chartWidth;

   ChartControl* m_pOwner;
   CWnd*         m_pOldCapture;

   Gdiplus::Color m_selectionColor;
};
