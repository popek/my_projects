#pragma once
#include "SemStack.h"

class SemActions
{
public:
  SemActions(void);
  virtual ~SemActions(void);

	void DoReduceAction(int a_ruleId);

  virtual void Rule_acceptance();
  virtual void Rule_SAT_FORMULA__to__EXPR_LIST();
  virtual void Rule_SAT_FORMULA__to__EXPR_LIST__MULTIPLE_SEMICOLON();
  virtual void Rule_EXPR_LIST__to__EXPR();
  virtual void Rule_EXPR_LIST__to__EXPR_LIST__MULTIPLE_SEMICOLON__EXPR();
  virtual void Rule_MULTIPLE_SEMICOLON__to__semicolon();
  virtual void Rule_MULTIPLE_SEMICOLON__to__MULTIPLE_SEMICOLON__semicolon();
  virtual void Rule_EXPR__to__EXPR_NAME__assign__CMP_EXPR();
  virtual void Rule_EXPR__to__CMP_EXPR();
  virtual void Rule_EXPR_NAME__to__EXPR_NAME_PART();
  virtual void Rule_EXPR_NAME__to__EXPR_NAME__EXPR_NAME_PART();
  virtual void Rule_CMP_EXPR__to__AEXPR_ADD();
  virtual void Rule_CMP_EXPR__to__CMP_EXPR__cmp_op__AEXPR_ADD();
  virtual void Rule_EXPR_NAME_PART__to__identifier();
  virtual void Rule_EXPR_NAME_PART__to__const_int();
  virtual void Rule_EXPR_NAME_PART__to__const_double();
  virtual void Rule_AEXPR_ADD__to__AEXPR_MUL();
  virtual void Rule_AEXPR_ADD__to__AEXPR_ADD__add_op__AEXPR_MUL();
  virtual void Rule_AEXPR_MUL__to__SIGNED_EXPR();
  virtual void Rule_AEXPR_MUL__to__AEXPR_MUL__mul_op__SIGNED_EXPR();
  virtual void Rule_SIGNED_EXPR__to__BASE_EXPR();
  virtual void Rule_SIGNED_EXPR__to__SIGNED__BASE_EXPR();
  virtual void Rule_BASE_EXPR__to__CONST_NUMERIC();
  virtual void Rule_BASE_EXPR__to__lpar__EXPR__rpar();
  virtual void Rule_BASE_EXPR__to__FUNCTION();
  virtual void Rule_SIGNED__to__add_op();
  virtual void Rule_SIGNED__to__SIGNED__add_op();
  virtual void Rule_CONST_NUMERIC__to__const_int();
  virtual void Rule_CONST_NUMERIC__to__const_double();
  virtual void Rule_CONST_NUMERIC__to__identifier();
  virtual void Rule_FUNCTION__to__FUNCTION_CALL();
  virtual void Rule_FUNCTION__to__SCRIPT_ACCESS__FUNCTION_CALL();
  virtual void Rule_FUNCTION_CALL__to__FUNCTION_NAME__lpar__ARG_LIST__rpar();
  virtual void Rule_FUNCTION_CALL__to__FUNCTION_NAME__lpar__rpar();
  virtual void Rule_SCRIPT_ACCESS__to__identifier__access_op();
  virtual void Rule_FUNCTION_NAME__to__identifier();
  virtual void Rule_ARG_LIST__to__ARG();
  virtual void Rule_ARG_LIST__to__NEXT_ARG_LIST__ARG();
  virtual void Rule_ARG__to__EXPR();
  virtual void Rule_ARG__to__text();
  virtual void Rule_NEXT_ARG_LIST__to__NEXT_ARG();
  virtual void Rule_NEXT_ARG_LIST__to__NEXT_ARG_LIST__NEXT_ARG();
  virtual void Rule_NEXT_ARG__to__ARG__comma();

protected:
	CSemStack m_semStack;
};