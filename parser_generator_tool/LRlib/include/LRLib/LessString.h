#pragma once

#include <string>

namespace lrlib
{

struct LessString
{
public:
   bool operator()(const std::string&, const std::string&) const;
};

} // namespace lrlib