#include "StdAfx.h"
#include "FoundationUtils.h"
#include "XercescXMLObjectsFactory.h"


shared_ptr<IXMLObjectsFactory> FoundationUtils::xmlFactory()
{
   return make_shared<XercescXMLObjectsFactory>();
}