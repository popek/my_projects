#pragma once

#include "SemAtom.h"

#include <vector>

namespace lrlib
{

class SemStack
{
public:
   SemStack(void);
   virtual ~SemStack(void);

   void Push(SemAtom& semAtom);
   void Pop();
   void Top(SemAtom* pSemAtom);
   std::vector<SemAtom> m_Stack;
};

} // namespace lrlib