#pragma once
#include "CommonDefs.h"

class TextFileWriter;

namespace regexlib
{
class CodeGenerator
{
public:
   CodeGenerator(t_Char2SymbolID&, t_DFAdiagram&, t_DFAAcceptingState2RegExNo&, t_RegExDefs&, unsigned int);
   virtual ~CodeGenerator(void);
   bool GenerateCode(const char* path);

private:
   void WriteDFAtable(TextFileWriter& a_fileWritter);
   void WriteChar2ColIDtable(TextFileWriter& a_fileWritter);
   void WriteAcceptStatesTable(TextFileWriter& a_fileWritter);
   void WriteCheckIfProperTokenMethod(FILE* fid);
   void WriteGetNextLexAtomMethod(TextFileWriter& a_fileWritter);

   void WriteLexHeaderFile(const char* a_folderPath);
   void WriteTerminalIdsHeader(const char* a_folderPath);
   void WriteLexerTypesHeader(const char* a_folderPath);

   short symbolID2ColID(short symID);
   inline std::string regexID2regexEnum(unsigned int uiRegexNo);
   inline std::string regexName2regexEnum(std::string strRegExName);
   t_Char2SymbolID& _Char2SymID;
   t_DFAdiagram& _DFA;
   t_DFAAcceptingState2RegExNo& _Accept;
   t_RegExDefs& _reName2reDef;
   std::set<short> _setSymIDs;
   unsigned int _uiDFAStatesAmount;
};

} // namespace regexlib