#include "StdAfx.h"
#include "TimeScaleContext.h"
#include "KandleDate.h"
#include "Asset.h"


TimeScaleContext::TimeScaleContext(void)
: m_1stVisibleTimestampIndex(0)
, m_lastVisibleTimestampIndex(0)
{
}
TimeScaleContext::~TimeScaleContext(void)
{
}

void TimeScaleContext::InitiateTimestampsContainer(const Asset& a_assetContainingTimestamps)
{
   a_assetContainingTimestamps.ExtractSamplesDates(m_datesContainer);
   
   m_datesCoordinatesContainer.clear();
   m_datesCoordinatesContainer.reserve(m_datesContainer.size());
   m_datesCoordinatesContainer.assign(m_datesContainer.size(), 0);
}

void TimeScaleContext::UpdateCoordinates(double a_rangeStart, double a_rangeEnd, Coordinate a_availableWidth)
{
   m_1stVisibleTimestampIndex  = (size_t)(a_rangeStart*(m_datesContainer.size()-1));
   m_lastVisibleTimestampIndex = (size_t)(a_rangeEnd*(m_datesContainer.size()-1));   
   double elementsAmount  = (double)(m_lastVisibleTimestampIndex - m_1stVisibleTimestampIndex + 1);

   double elementWidht    = (double)a_availableWidth/elementsAmount;

   double timestampXCoord = -(double)m_1stVisibleTimestampIndex * elementWidht + elementWidht/2;
   for(size_t sampleIndex=0; sampleIndex<m_datesContainer.size(); ++sampleIndex)
   {
      m_datesCoordinatesContainer[sampleIndex] = timestampXCoord;
      timestampXCoord += elementWidht;
   }
}

Coordinate TimeScaleContext::ToCoordinate(shared_ptr<KandleDate>& a_spDate) const
{
   return m_datesCoordinatesContainer[a_spDate->Index()];
}

Coordinate TimeScaleContext::GetCoordinate(size_t a_timestampIndex) const
{
   return m_datesCoordinatesContainer[a_timestampIndex];
}

shared_ptr<KandleDate> TimeScaleContext::GetTimestamp(size_t a_timestampIndex) const
{
   return m_datesContainer[a_timestampIndex];
}

size_t TimeScaleContext::FirstVisibleTimestampIndex() const
{
   return m_1stVisibleTimestampIndex;
}

size_t TimeScaleContext::LastVisibleTimestampIndex() const
{
   return m_lastVisibleTimestampIndex;
}

Coordinate TimeScaleContext::NeighbourDistance() const
{
   return m_datesCoordinatesContainer[1] - m_datesCoordinatesContainer[0];
}