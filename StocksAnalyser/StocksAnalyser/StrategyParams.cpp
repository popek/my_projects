#include "StdAfx.h"
#include "StrategyParams.h"
#include "Kandle.h"
#include "MathUtils.h"


StrategyParams::StrategyParams(double a_maxChange, double a_maxDropDown, double a_valueStability, double a_minTurnover)
   : _maxChange(a_maxChange)
   , _maxDropDown(a_maxDropDown)
   , _valueStability(a_valueStability)
   , _minTurnover(a_minTurnover)
{
}
StrategyParams::~StrategyParams(void)
{
}

bool StrategyParams::isParamFulfilled(paramId a_paramId, const Kandle& a_candle) const
{
   return isParamFulfilledInternal(a_paramId, a_candle);
}

bool StrategyParams::isParamFulfilled(StrategyParams::paramId a_paramId, const Kandle& a_candle1, const Kandle& a_candle2) const
{
   return isParamFulfilledInternal(a_paramId, a_candle1, a_candle2);
}

bool StrategyParams::isParamFulfilledInternal(paramId a_paramId, const Kandle& a_candle) const
{
   switch(a_paramId)
   {
   case EMaxChange:
      return MathUtils::Abs( (a_candle.Close() - a_candle.Open())/a_candle.Open() ) <= _maxChange;

   case EValueStability:
      return MathUtils::Abs( (a_candle.High() - a_candle.Low())/((a_candle.Close() + a_candle.Open())/2) ) <= _valueStability;  

   case EMinTurnover:
      return a_candle.TurnOver() >= _minTurnover;

   default:
      ASSERT(FALSE);
      return false;
   }
}

bool StrategyParams::isParamFulfilledInternal(StrategyParams::paramId a_paramId, const Kandle& a_candle1, const Kandle& a_candle2) const
{
   switch(a_paramId)
   {
   case EMaxDropDown:
      return (a_candle2.Open() - a_candle1.Close())/a_candle1.Close() >= -_maxDropDown;

   default:
      ASSERT(FALSE);
      return false;
   }
}