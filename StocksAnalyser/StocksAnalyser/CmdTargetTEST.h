#pragma once

class CmdTargetTEST : public CCmdTarget
{
	DECLARE_DYNAMIC(CmdTargetTEST)

public:
	CmdTargetTEST();
	virtual ~CmdTargetTEST();

protected:
	DECLARE_MESSAGE_MAP()
   afx_msg void OnTestGetdata();
   afx_msg void OnUpdateTestGetdata(CCmdUI *pCmdUI);
};


