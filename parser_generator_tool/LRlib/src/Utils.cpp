#include "Utils.h"

namespace lrlib
{

Symbol& Utils::SymbolRef(t_setSymbol::value_type& value)
{
   return value.second;
}

Symbol& Utils::SymbolRef(t_setSymbol::iterator& a_iterator)
{
   return a_iterator->second;
}

Symbol* Utils::SymbolPtr(t_setSymbol::iterator& a_iterator)
{
   return &SymbolRef(a_iterator);
}


} // namespace lrlib