#pragma once
#include "ThreadBaseEx.h"
#include "StrategiesUtils.h"


class IMarketScannerObserver;


class MarketScannerWorkerThread : public ThreadBaseEx
{
public:
   MarketScannerWorkerThread(void);
   virtual ~MarketScannerWorkerThread(void);

   virtual BOOL ProcessThreadMessage(UINT a_messageId);

   void RunMarketScan(IMarketScannerObserver* a_pObserver, StrategiesUtils::strategyId a_strategyId, CString a_assetsFilesDir);

   void StopMarketScan();

private:
   void ProcessAllAssets(CString a_assetsFilesFolder);

   void ProcessAsset(CString a_fileFullPath);

   bool AssetTurnoverOK(shared_ptr<Asset> a_spAsset) const;

private:
   bool m_stop;
   StrategiesUtils::strategyId m_strategyId;
   CString m_assetsFilesDirectory;

   IMarketScannerObserver* m_pScannerObserver;
};

