#pragma once

class AssetContainer;

class AssetFinderBase
{
public:
   AssetFinderBase();
   virtual ~AssetFinderBase(void);

   void SetAsset(shared_ptr<AssetContainer> a_spAsset);

   // check if asset meets concrete finder conditions
   virtual bool VerifyAssetConditions() = 0;

protected:
   shared_ptr<AssetContainer> m_spAsset;
};
