#pragma once
#include "IXMLDataObserver.h"
#include <stack>
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMElement.hpp>


class XercescXMLDataObserver : public IXMLDataObserver
{
public:
   XercescXMLDataObserver(void);
   virtual ~XercescXMLDataObserver(void);

   virtual void onEnterNewNode(LPCTSTR nodeName, LPCTSTR nodeText);
   virtual void onLeaveNode();

   virtual void onAttribute(LPCTSTR attrName, LPCTSTR attrText);

   const XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument* document() const;

private:
   xercesc::DOMDocument* _xercescDoc;

   stack<xercesc::DOMElement*> _nodes;
};

