#pragma once


class Asset;


class MinMaxGetter
{
public:
   enum EContext
   {
      EGetVolumenMinMax,
      EGetPriceMinMax,
   };

public:
   MinMaxGetter(EContext a_context);
   virtual ~MinMaxGetter(void);

   void GetMinMax(const Asset& a_asset, const size_t& a_firstSampleIndex, const size_t& a_lastSampleIndex,
                  double& a_min, double& a_max);
   
   void GetMinMax(const Asset& a_asset, double& a_min, double& a_max);

protected:
   EContext m_context;
};
