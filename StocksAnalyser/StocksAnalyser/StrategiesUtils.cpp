#include "StdAfx.h"
#include "StrategiesUtils.h"
#include "Strategy_MACD.h"


shared_ptr<TradeStrategyBase> StrategiesUtils::CreateStrategy(strategyId a_id, shared_ptr<Asset> a_asset)
{
   switch(a_id)
   {
   case EMacd:
      return make_shared<Strategy_MACD>(a_asset);

   default:
      ASSERT(FALSE);
      return nullptr;
   }
}