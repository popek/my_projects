#pragma once
#include "IDataRangeManager.h"

class IDataRangeObserver;

class ScrollBarExt : public CWnd, public IDataRangeManager
{
	DECLARE_DYNAMIC(ScrollBarExt)

public:
	ScrollBarExt(IDataRangeObserver* a_pRangeObserver);
	virtual ~ScrollBarExt();

   void Reset();

   virtual double GetStartNormalisedPos() const;

   virtual double GetFinishNormalisedPos() const;

   virtual void   ForceSetRange(double a_thumbStart, double a_thumbStop);

protected:

   void Handle_DragBorder(const CPoint& a_mousePos);
   
   void Handle_DragThumb(const CPoint& a_mousePos);

   

protected:
	DECLARE_MESSAGE_MAP()

   afx_msg void OnMouseMove(UINT nFlags, CPoint point);
   afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
   afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
   afx_msg void OnPaint();
   afx_msg void OnSize(UINT nType, int cx, int cy);
   afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
   afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);

protected:

   int      m_thumbStartPos;
   int      m_thumbFinishPos;

   double   m_thumbStart;
   double   m_thumbFinish;
   double   m_windowWidth;

   int      m_minThumbLen;
   COLORREF m_bkColor;
   COLORREF m_thumbColor;

   bool     m_dragBorderEnabled;
   bool     m_dragBorderOnGoing;
   
   bool     m_dragThumbEnabled;
   bool     m_dragThumbOnGoing;
   int      m_thumbDragStartPos;

   bool     m_dragingLeft;

   IDataRangeObserver* m_pRangeObserver;
};


