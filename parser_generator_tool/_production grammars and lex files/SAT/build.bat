set THIS_DIR=%cd%
set BIN_DIR=%THIS_DIR%\..\..\_out\Release\bin\

echo "reset parser out dir"
rd out_parser /s /q
mkdir out_parser

echo "reset scanner out dir"
rd out_lexer /s /q
mkdir out_lexer

"%BIN_DIR%compilerGenerator.exe" sat_grammar.g out_parser
"%BIN_DIR%scannerGenerator.exe" c sat_lex.l out_lexer