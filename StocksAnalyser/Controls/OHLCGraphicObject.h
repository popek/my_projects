#pragma once
#include "GraphicObjectBase.h"


class OHLCGraphicObject : public GraphicObjectBase
{
public:
   OHLCGraphicObject(void);
   virtual ~OHLCGraphicObject(void);

   virtual void Draw(CDC* a_pDC);

   virtual void Reserve(size_t a_pointsCount);

   void UpdateOHLCoordinates(size_t a_pointIndex, Coordinate a_openCoord, Coordinate a_highCoord, Coordinate a_lowCoord);

private:

   // close values coordinates are kept points vector
   vector<Coordinate>   m_openValueCoordinates;
   vector<Coordinate>   m_lowValueCoordinates;
   vector<Coordinate>   m_highValueCoordinates;

   CPen  m_increasePen;
   CPen  m_decresePen;
};
