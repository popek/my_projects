#include "StdAfx.h"
#include "MemoryDC.h"

MemoryDC::MemoryDC(CDC* a_pSourceDC, CWnd* a_pWindowObj)
: m_pSourceDC(a_pSourceDC)
, m_bInitialised(false)
{
   if(m_BufferedDC.CreateCompatibleDC(m_pSourceDC))
   {
      a_pWindowObj->GetClientRect(&m_rcBufferedArea);
      
      if( m_Bitmap.CreateCompatibleBitmap(m_pSourceDC, m_rcBufferedArea.Width(), m_rcBufferedArea.Height()) )
      {
         m_BufferedDC.SelectObject(&m_Bitmap);

         m_bInitialised = true;
      }
   }
}

MemoryDC::~MemoryDC(void)
{
   if(m_bInitialised)
   {
      m_pSourceDC->BitBlt(0, 0, m_rcBufferedArea.Width(), m_rcBufferedArea.Height(), &m_BufferedDC, 0, 0, SRCCOPY); 
      m_BufferedDC.DeleteDC();
   }
}


bool MemoryDC::Initialised()
{
   return m_bInitialised;
}

CDC& MemoryDC::BufferedDC()
{
   return m_BufferedDC;
}