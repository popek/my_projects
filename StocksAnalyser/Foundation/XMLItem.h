#pragma once
#include "FoundationLib.h"


class FOUNDATION_LIB XMLItem
{
public:
   XMLItem(LPCTSTR name);
   virtual ~XMLItem(void);

   const CString& getName() const;

private:
   CString _name;
};

