#include "StdAfx.h"
#include "StrategyResult.h"
#include "SingleMarketEntryDetails.h"
#include "TransactionDetails.h"


StrategyResult::StrategyResult(void)
: _brokerFee(0.0038)
{
}
StrategyResult::~StrategyResult()
{
}
void StrategyResult::RegisterEntry(shared_ptr<TransactionDetails> a_spBuy, 
                                   shared_ptr<TransactionDetails> a_spSell)
{
   _marketEntries.push_back(shared_ptr<SingleMarketEntryDetails>
                            (new SingleMarketEntryDetails(a_spBuy, a_spSell)));
}

double StrategyResult::Income(const double& a_initialBankroll, 
                              StrategyResult::ISubIncomeObserver* a_pSingleEntriesObserver) const
{
   double bankroll = a_initialBankroll;

   for(size_t entryIndex=0; entryIndex<_marketEntries.size(); ++entryIndex)
   {
      bankroll += _marketEntries[entryIndex]->Income(bankroll, _brokerFee);

      if(a_pSingleEntriesObserver)
      {
         a_pSingleEntriesObserver->Notify(bankroll);
      }
   }

   return bankroll - a_initialBankroll;
}

bool StrategyResult::EntriesCount() const
{
   return _marketEntries.size();
}