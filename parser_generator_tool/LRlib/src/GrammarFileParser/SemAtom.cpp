#include "GrammarFileParser/SemStack.h"

namespace lrlib
{

//----------------------------------
//------------ SemAtom ------------
//----------------------------------
SemAtom::SemAtom(void)
{
}

SemAtom::SemAtom(LexAtom lexAtom)
{
   m_LexAtom = lexAtom;
}

SemAtom::~SemAtom(void)
{
}

void SemAtom::operator = (const SemAtom& sa)
{
   m_LexAtom = sa.m_LexAtom;
}

} // namespace lrlib