#pragma once
#include "ChartDocument.h"

class CStocksAnalyserDoc : public ChartDocument
{
protected: // create from serialization only
	CStocksAnalyserDoc();
	DECLARE_DYNCREATE(CStocksAnalyserDoc)

// Implementation
public:
	virtual ~CStocksAnalyserDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	DECLARE_MESSAGE_MAP()
};


