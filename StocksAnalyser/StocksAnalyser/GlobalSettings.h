#pragma once
#include "CommonDefines.h"


class GlobalSettings
{
   NONCOPYABLE_TYPE(GlobalSettings);

private:
   GlobalSettings();

public:
   virtual ~GlobalSettings();

   static GlobalSettings* instance();
   
   static void release();

   const CString& DBFolderPath() const;

private:
   CString _databaseFolderPath;

   CString _settingsFolder;

   static GlobalSettings* _instance;
};

