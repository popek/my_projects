#include "stdafx.h"
#include "ChartControlSplitted.h"
#include "ChartControl.h"
#include "SplitterControl.h"
#include "ScrollBarExt.h"
#include "ClientRect.h"
#include "WindowMessages.h"
#include "WindowUtils.h"
#include "ControlsLibUtils.h"
#include "ChartElementsCollection.h"
#include "MinMaxGetter.h"

#include "ValueScaleContext.h"
#include "EMA_pointer.h"
#include "ChartElement.h"


const UINT G_priceChartId = 1;
const UINT G_volumeChartId = 2;
const UINT G_splitterControlId = 3;
const UINT G_scrollControlId = 4;


IMPLEMENT_DYNAMIC(ChartControlSplitted, CWnd)

ChartControlSplitted::ChartControlSplitted()
: m_nScrollbarHeight(20)
{
}
ChartControlSplitted::~ChartControlSplitted()
{
}

void ChartControlSplitted::SetAsset(shared_ptr<Asset> a_spAsset)
{
   m_spPriceChart->SetElementsCollection(a_spAsset->ElementsCollection());
   m_spPriceChart->SetAsset(a_spAsset);

//    shared_ptr<EMA_pointer> spATR(new EMA_pointer(14));
//    spATR->Create(*ControlsLibUtils::CreateATRSeries(*a_spAsset));
//    shared_ptr<ChartElement> spATRElement = ControlsLibUtils::CreatePointerSeriesElement(spATR->Series(0), *a_spAsset);
//    spATRElement->OwnValueScaleContext(make_shared<ValueScaleContext>(15, 0, 0, 0));
//    a_spAsset->ElementsCollection()->PushBack(spATRElement);

   shared_ptr<ChartElementsCollection> spVolumeChartCollection(new ChartElementsCollection());
   spVolumeChartCollection->PushBack(ControlsLibUtils::CreateVolumeSeriesElement(*a_spAsset));
   
   m_spVolumeChart->SetElementsCollection(spVolumeChartCollection);
   m_spVolumeChart->SetMinMaxGetter(make_shared<MinMaxGetter>(MinMaxGetter::EGetVolumenMinMax));
   m_spVolumeChart->SetAsset(a_spAsset);
}

void ChartControlSplitted::OnDataRangeChanged(double a_dNewStartNormalisedPos, double a_dNewFinishNormalisedPos)
{
   m_spPriceChart->OnDataRangeChanged(a_dNewStartNormalisedPos, a_dNewFinishNormalisedPos);
   m_spVolumeChart->OnDataRangeChanged(a_dNewStartNormalisedPos, a_dNewFinishNormalisedPos);
}

BEGIN_MESSAGE_MAP(ChartControlSplitted, CWnd)
   ON_WM_CREATE()
   ON_WM_SIZE()
END_MESSAGE_MAP()

int ChartControlSplitted::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
   if (__super::OnCreate(lpCreateStruct) == -1)
   {
      return -1;
   }

   ClientRect rcClient(this);
   CRect rcChart  = rcClient;
   CRect rcScroll = rcClient;

   if(rcChart.Height() > m_nScrollbarHeight)
   {
      rcChart.bottom -= m_nScrollbarHeight;
      rcScroll.top    = rcScroll.bottom - m_nScrollbarHeight;
   }

   DWORD style = WS_CHILD|WS_CLIPSIBLINGS|WS_VISIBLE;

   m_spSplitter = shared_ptr<ChartControlMessagesProxy<SplitterControl>>( new ChartControlMessagesProxy<SplitterControl>() );
   m_spSplitter->SetSplitBarPos(0.8);
   if( FALSE == m_spSplitter->Create(style|WS_CLIPCHILDREN, rcClient, this, G_splitterControlId) )
   {
      ASSERT(FALSE);
      return -1;
   }

   m_spScroll = shared_ptr<ScrollBarExt>( new ScrollBarExt(this) );
   if( FALSE == m_spScroll->Create(NULL, NULL, style, rcScroll, this, G_scrollControlId) )
   {
      ASSERT(FALSE);
      return -1;
   }

   m_spPriceChart = shared_ptr<ChartControl>( new ChartControl() );
   if(FALSE == m_spPriceChart->Create(NULL, NULL, style, rcChart, m_spSplitter.get(), G_priceChartId))
   {
      ASSERT(FALSE);
      return -1;
   }
   m_spVolumeChart = shared_ptr<ChartControl>( new ChartControl() );
   if(FALSE == m_spVolumeChart->Create(NULL, NULL, style, rcChart, m_spSplitter.get(), G_volumeChartId))
   {
      ASSERT(FALSE);
      return -1;
   }

   // set range manager 
   m_spVolumeChart->DataRangeManager(m_spScroll.get());
   m_spPriceChart->DataRangeManager(m_spScroll.get());

   m_spSplitter->AttachFirst(m_spPriceChart.get());
   m_spSplitter->AttachSecond(m_spVolumeChart.get());

   return 0;
}

void ChartControlSplitted::OnSize(UINT nType, int cx, int cy)
{
   __super::OnSize(nType, cx, cy);

   ClientRect rcChart(this);
   CRect rcScroll = rcChart;

   if(rcChart.Height() > m_nScrollbarHeight)
   {
      rcChart.bottom -= m_nScrollbarHeight;
      rcScroll.top    = rcScroll.bottom - m_nScrollbarHeight;
   }

   WindowUtils::SetWindowPos(m_spScroll.get(), NULL, rcScroll, SWP_NOZORDER);
   WindowUtils::SetWindowPos(m_spSplitter.get(), NULL, rcChart, SWP_NOZORDER|SWP_NOREDRAW);
}