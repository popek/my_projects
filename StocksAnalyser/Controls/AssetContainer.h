#pragma once
#include "ControlsLib.h"
#include "CommonTypes.h"
//
class Asset;

class CONTROLS_LIB AssetContainer
{
   NONCOPYABLE_TYPE(AssetContainer)

protected:
   typedef map<EAggregationType, ::shared_ptr<Asset>> TAssetsMap;

public:
   AssetContainer(void);
   virtual ~AssetContainer(void);

   static CString  Type2String(EAggregationType a_aggregationType);

   void LoadDaysAssetFromFile(LPCTSTR a_CSVfileName);

   ::shared_ptr<Asset> operator[](const EAggregationType& a_aggregationType);

protected:

   ::shared_ptr<Asset> CreateFromDayAsset(const EAggregationType& a_aggregationType);

   ::shared_ptr<Asset> DayAsset2WeekAsset() const;

   ::shared_ptr<Asset> DayAsset2MonthAsset() const;

protected:
   TAssetsMap        m_mapAssets;
};
