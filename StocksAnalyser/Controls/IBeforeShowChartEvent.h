#pragma once
#include "ControlsLib.h"


class Asset;


class CONTROLS_LIB IBeforeShowChartEvent abstract
{
public:
   IBeforeShowChartEvent(void);
   virtual ~IBeforeShowChartEvent(void);

   virtual void OnBeforeShowingAsset(Asset& a_assetToShow) const = 0;
};
