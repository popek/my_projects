#pragma once
#include "resource.h"
#include "ChannelFinderContext.h"
#include "PropertyDialogBase.h"
#include "RegularTextEdit.h"
#include "afxwin.h"


class NumericStringValidator;


class ChannelAlgorithParametersDlg : public PropertyDialogBase
{

	DECLARE_DYNAMIC(ChannelAlgorithParametersDlg)

   const ChannelFinderContext& Context() const;

public:
	ChannelAlgorithParametersDlg();
	virtual ~ChannelAlgorithParametersDlg();

	enum { IDD = IDD_CHANNEL_PROPERTIES };


protected:
	virtual void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP()

protected:

   int m_algorithmType;

   CString m_strPeriod;
   CString m_strMinimumLinePointsAmount;
   CString m_strMaxDeviation2LineValueInPercents;
   CString m_strMaxDataSource2LineValueDistanceInPercents;

   CButton m_butShowConfirmedSignal;

   ChannelFinderContext m_context;

   RegularTextEdit m_editMaxDev2Value;
   RegularTextEdit m_editMaxData3LineDistance;

   shared_ptr<NumericStringValidator> m_spNumericStringValidator;
};
