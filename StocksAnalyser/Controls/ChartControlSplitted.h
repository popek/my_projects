#pragma once
#include "ControlsLib.h"
#include "IDataRangeObserver.h"
#include "ChartControlMessagesProxy.h"


class Asset;
class SplitterControl;
class ScrollBarExt;
class ChartControl;


class CONTROLS_LIB ChartControlSplitted : public CWnd, public IDataRangeObserver
{
	DECLARE_DYNAMIC(ChartControlSplitted)

public:
   ChartControlSplitted();
   virtual ~ChartControlSplitted();

   void SetAsset(shared_ptr<Asset> a_spAsset);

   virtual void OnDataRangeChanged(double a_dNewStartNormalisedPos, double a_dNewFinishNormalisedPos);

protected:

   DECLARE_MESSAGE_MAP()
   afx_msg int    OnCreate(LPCREATESTRUCT lpCreateStruct);
   afx_msg void   OnSize(UINT nType, int cx, int cy);

protected:

   shared_ptr<ChartControlMessagesProxy<SplitterControl> > m_spSplitter;
   shared_ptr<ChartControl> m_spPriceChart;
   shared_ptr<ChartControl> m_spVolumeChart;

   shared_ptr<ScrollBarExt> m_spScroll;

   int  m_nScrollbarHeight;
};


