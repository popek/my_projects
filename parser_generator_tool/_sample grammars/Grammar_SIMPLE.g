	SCRIPT -> EXPR
			| CLASS_DECL
			
	EXPR ->	EXPR_MUL 
			| EXPR + EXPR_MUL 
			| EXPR - EXPR_MUL
	
	EXPR_MUL -> EXPR_POW 
				| EXPR_MUL * EXPR_POW 
				| EXPR_MUL / EXPR_POW

	EXPR_POW -> BASE_EXPR 
				| EXPR_POW ^ BASE_EXPR

	BASE_EXPR -> number 
				| FUN_CALL 
				| ( EXPR )
				
	FUN_CALL -> fun ( ARG_LIST )
	
	ARG_LIST -> BASE_EXPR
			| ARG_LIST , BASE_EXPR 
			
			
	CLASS_DECL -> CLASS_HEAD CLASS_BODY
	
	CLASS_HEAD -> class identifier
	
	CLASS_BODY -> [ CLASS_DEF ] 
	
	CLASS_DEF -> CLASS_ENTRY '
				| CLASS_DEF  CLASS_ENTRY
	
	CLASS_ENTRY -> MODIFIER CLASS_ATT
				| CLASS_ATT
	MODIFIER -> private :
				| public :
				| protected :
	CLASS_ATT -> TYPE CLASS_ATT_NAME
	
	TYPE -> identifier
	
	CLASS_ATT_NAME -> identifier '
					| CLASS_METHOD
	
	CLASS_METHOD ->	method_name ( FUN_ARG_LIST ) '
	
	FUN_ARG_LIST -> TYPE identifier
				| FUN_ARG_LIST , TYPE identifier
	