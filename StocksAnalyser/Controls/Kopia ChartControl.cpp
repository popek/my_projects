#include "stdafx.h"
#include "ChartControl.h"
#include "BackgroundPainter.h"
#include "KandlePainter.h"
#include "ValueScalePainter.h"
#include "TimeScalePainter.h"
#include "CommonDefines.h"
#include "MemoryDC.h"
#include "SMA_pointer.h"
#include "EMA_pointer.h"
#include "PointerWrapper.h"

const int G_nScrollResolution = 4;

const int G_nDefaultValueScaleWidth = 60;

const int G_nDefaultTimeScaleHeight = 40;

IMPLEMENT_DYNAMIC(ChartControl, CWnd)

ChartControl::ChartControl()
: m_dHeightChangeFactor(0.05)
, m_dWidthChangeFactor(0.1)
, m_nChartWidth(0)
, m_nChartHeight(0)
, m_bUpdateHorzScroll(false)
, m_bUpdateVertScroll(false)
, m_nVertBarOffset(0)
, m_nItemsWithExtendedWidthAmount(0)
, m_pAssetObj(NULL)
{
   m_pBackgroundPainter = boost::shared_ptr<BackgroundPainter>( new BackgroundPainter() );
   m_pKandlePainter     = boost::shared_ptr<KandlePainter>( new KandlePainter() );
   m_pValueScalePainter = boost::shared_ptr<ValueScalePainter>( new ValueScalePainter() );
   m_pTimeScalePainter  = boost::shared_ptr<TimeScalePainter>( new TimeScalePainter() );
}

ChartControl::~ChartControl()
{
}

BOOL ChartControl::Create(int a_nID, CWnd* a_pParent)
{
   VERIFY_EXPRESSION1(a_pParent->GetSafeHwnd(), FALSE);

   CWnd* pPlaceholder   = a_pParent->GetDlgItem(a_nID);

   if(pPlaceholder == NULL)
   {
      return FALSE;
   }

   CRect rcWindow;
   pPlaceholder->GetWindowRect(&rcWindow);
   pPlaceholder->DestroyWindow();

   return Initialise(rcWindow, a_nID, a_pParent);

}

BOOL ChartControl::Create(CWnd* a_pParent)
{
   VERIFY_EXPRESSION1(a_pParent->GetSafeHwnd(), FALSE);
   
   CRect rcWindow;
   a_pParent->GetClientRect(&rcWindow);

   return Initialise(rcWindow, 0, a_pParent);
}

void ChartControl::SetAsset(CAsset& a_rAsset)
{
   m_pAssetObj = &a_rAsset;

   if( GetParent()->GetSafeHwnd() )
   {
      CRect rcChartArea;
      GetParent()->GetClientRect(&rcChartArea);

      RecalcChartArea(rcChartArea);
   }

   //<DEBUG>
   vector<int> vAvarageSamplesAmount;

   vAvarageSamplesAmount.push_back(3);
   vAvarageSamplesAmount.push_back(5);
   vAvarageSamplesAmount.push_back(8);
   vAvarageSamplesAmount.push_back(10);
   vAvarageSamplesAmount.push_back(12);
   vAvarageSamplesAmount.push_back(15);
   vAvarageSamplesAmount.push_back(30);
   vAvarageSamplesAmount.push_back(35);
   vAvarageSamplesAmount.push_back(40);
   vAvarageSamplesAmount.push_back(45);
   vAvarageSamplesAmount.push_back(50);
   vAvarageSamplesAmount.push_back(60);
   
   for(size_t nAvarageTypeIdx=0; nAvarageTypeIdx<vAvarageSamplesAmount.size(); ++nAvarageTypeIdx)
   {
      m_vPointerWrappers.push_back(boost::shared_ptr<PointerWrapper>( 
                                                      new PointerWrapper(boost::shared_ptr<IPointer>(new EMA_pointer(vAvarageSamplesAmount[nAvarageTypeIdx]))
                                                                     ) ) );

      m_vPointerWrappers.back()->Pointer()->Create(a_rAsset);
   }
   //</DEBUG>
   
   GenerateDrawDescriptors();
   m_pValueScalePainter->UpdateMinMax( m_pAssetObj->GetGlobalMin(), m_pAssetObj->GetGlobalMax() );
}

BOOL ChartControl::Initialise(CRect& a_rcClientRect, int a_nID, CWnd* a_pParent)
{
   if( CWnd::Create(NULL, NULL, WS_VISIBLE|WS_CHILD, a_rcClientRect, a_pParent, a_nID) )
   {
      RecalcChartArea(a_rcClientRect);
      return TRUE;
   }
   else
   {
      return FALSE;
   }
}

void ChartControl::RecalcChartArea(CRect& a_rcClientRect)
{
   m_nChartWidth = a_rcClientRect.Width();
   m_nChartHeight = a_rcClientRect.Height();

   m_pValueScalePainter->Width( min(m_nChartWidth, G_nDefaultValueScaleWidth) );
   m_pValueScalePainter->Height( m_nChartHeight );

   m_pTimeScalePainter->Height( min(m_nChartHeight, G_nDefaultTimeScaleHeight) );

   m_nChartWidth  -= m_pValueScalePainter->Width();
   m_nChartHeight -= m_pTimeScalePainter->Height();
}

void ChartControl::GenerateDrawDescriptors()
{
   // reserve memory to avoid memory realocation
   m_vDrawDescriptors.reserve(m_pAssetObj->Size());
   m_vItemsWrapper.reserve(m_pAssetObj->Size());

   for(size_t nItemIdx=0; nItemIdx < m_pAssetObj->Size(); ++nItemIdx)
   {
      m_vDrawDescriptors.push_back(CDrawItemDescriptor());
      m_vItemsWrapper.push_back( GraphicItemWrapper( &(*m_pAssetObj)[nItemIdx], &m_vDrawDescriptors.back() ) );
   }

   RecalculateDrawDescriptors();
}

void ChartControl::RecalculateDrawDescriptors()
{
   double dPixels2PriceCoefficient = m_nChartHeight/(m_pAssetObj->GetGlobalMax() - m_pAssetObj->GetGlobalMin());

   for(size_t nItemIdx=0; nItemIdx<m_pAssetObj->Size(); ++nItemIdx)
   {
      m_vDrawDescriptors[nItemIdx].Recalculate(dPixels2PriceCoefficient, (*m_pAssetObj)[nItemIdx], m_pAssetObj->GetGlobalMin(), m_nChartHeight);
   }

   // recalculate pointers coordinates
   for(size_t nPointerIdx=0; nPointerIdx<m_vPointerWrappers.size(); ++nPointerIdx)
   {
      m_vPointerWrappers[nPointerIdx]->DrawDescription().RecalculateYGraphicCoordinates(dPixels2PriceCoefficient, m_pAssetObj->GetGlobalMin(), m_nChartHeight);
   }

   UpdateDimensions();
}

void ChartControl::Draw(CDC& a_cdc)
{
   CRect        rcClient;
   GetClientRect(&rcClient);

   int          nItemLeft      = 0;
   int          nItemRight     = 0;
   int          nMostLeftPos   = GetScrollPos(SB_HORZ)*G_nScrollResolution;
   int          nMostRightPos  = nMostLeftPos + rcClient.Width() - m_pValueScalePainter->Width();
   
   int          nMostTopPos    = GetScrollPos(SB_VERT)*G_nScrollResolution;
   int          nMostBottomPos = nMostTopPos + rcClient.Height() - m_pTimeScalePainter->Height();
   bool         bDrawStarted   = false;

   // reset visibility flags
   for(size_t nItemIdx=0; nItemIdx<m_vItemsWrapper.size(); ++nItemIdx)
   {
      m_vItemsWrapper[nItemIdx].Visible(false);
      m_vItemsWrapper[nItemIdx].TimeStampVisible(false);
   }

   // reset pointers x graphic coordinates
   for(size_t nPointerIdx=0; nPointerIdx<m_vPointerWrappers.size(); ++nPointerIdx)
   {
      m_vPointerWrappers[nPointerIdx]->DrawDescription().ResetXGraphicCoordinates();
   }

   // update items horizontal coordinates and visibility
   for(size_t nItemIdx=0; nItemIdx<m_vItemsWrapper.size(); ++nItemIdx)
   {
      nItemRight += m_nItemWidth + (nItemIdx < (size_t)m_nItemsWithExtendedWidthAmount ? 1 : 0);
      
      if( (nItemLeft<=nMostLeftPos && nItemRight>nMostLeftPos) && false == bDrawStarted)
      {
         bDrawStarted = true;
      }
      
      if( nItemRight > nMostRightPos )
      {
         break;
      }

      if( bDrawStarted )
      {
         // set time stamp visibility flag
         m_vItemsWrapper[nItemIdx].TimeStampVisible(true);

         // update horizontal coordinates   
         m_vItemsWrapper[nItemIdx].HorzCoordinates().Update(nItemLeft - nMostLeftPos, nItemRight - nMostLeftPos);

         // update item visibility flag
         if( m_vDrawDescriptors[nItemIdx].IsItemInBoundries(nMostTopPos, nMostBottomPos) )
         {
            m_vItemsWrapper[nItemIdx].Visible(true);
         }

         // update pointers x coordinates
         for(size_t nPointerIdx=0; nPointerIdx<m_vPointerWrappers.size(); ++nPointerIdx)
         {
            m_vPointerWrappers[nPointerIdx]->DrawDescription().SetXCoordinate(nItemIdx, (nItemRight + nItemLeft)/2 - nMostLeftPos);
         }
      }

      nItemLeft = nItemRight;
   }

   // draw background
   m_pBackgroundPainter->Draw( a_cdc, rcClient );
   // draw value scale
   m_pValueScalePainter->Draw( a_cdc, rcClient.right - m_pValueScalePainter->Width(), nMostTopPos, nMostBottomPos );
   
   // prepare region do draw chart
   CRgn chartRegion;
   chartRegion.CreateRectRgn(rcClient.left, rcClient.top, rcClient.right - m_pValueScalePainter->Width(), rcClient.bottom - m_pTimeScalePainter->Height());
   a_cdc.SelectClipRgn(&chartRegion);

   // draw chart
   m_pKandlePainter->Draw(a_cdc, m_vItemsWrapper, nMostTopPos);

   // draw pointers
   for(size_t nPointerIdx=0; nPointerIdx<m_vPointerWrappers.size(); ++nPointerIdx)
   {
      m_vPointerWrappers[nPointerIdx]->Draw(a_cdc, nMostTopPos);
   }

   // prepare region to draw time scale
   CRgn timescaleRegion;
   timescaleRegion.CreateRectRgn(rcClient.left, rcClient.bottom - m_pTimeScalePainter->Height(), rcClient.right, rcClient.bottom);
   a_cdc.SelectClipRgn(&timescaleRegion);
   
   // draw time scale
   m_pTimeScalePainter->VertOffset(rcClient.Height());
   m_pTimeScalePainter->Draw(a_cdc, m_vItemsWrapper);
}

void ChartControl::DrawEmptyChart(CDC& a_cdc)
{
   CRect        rcClient;
   GetClientRect(&rcClient);
   CString strEmptyChartNotification = _T("Nie za�adowano �adnego instrumentu");

   // draw background
   m_pBackgroundPainter->Draw( a_cdc, rcClient );

   a_cdc.DrawText(strEmptyChartNotification, &rcClient, DT_CENTER|DT_VCENTER|DT_SINGLELINE);
}

void ChartControl::UpdateDimensions()
{
   VERIFY_EXPRESSION0(m_vDrawDescriptors.size() > 0);
   
   UpdateWidth();

   UpdateHeight();
}

void ChartControl::UpdateWidth()
{
   if( m_nChartWidth < (int)m_vDrawDescriptors.size() )
   {
      m_nItemWidth = 1;
      m_nItemsWithExtendedWidthAmount = 0;
      m_nChartWidth = m_vDrawDescriptors.size();
   }
   else
   {
      m_nItemWidth = (int)(m_nChartWidth/m_vDrawDescriptors.size());
      m_nItemsWithExtendedWidthAmount = m_nChartWidth - m_nItemWidth*m_vDrawDescriptors.size();
   }

   UpdateScroll(SB_HORZ);
}

void ChartControl::UpdateHeight()
{
   UpdateScroll(SB_VERT);
}

void ChartControl::MoveScroll(int a_nBarID, int a_nPixelsToMove)
{
   SCROLLINFO     oScrollInfo;
   oScrollInfo.cbSize = sizeof(oScrollInfo);

   GetScrollInfo(a_nBarID, &oScrollInfo, SIF_PAGE | SIF_POS);
   
   if(oScrollInfo.nPage > 0) // if scroll page is grater then 0 (means scroll is visible/needed)
   {
      oScrollInfo.fMask  = SIF_POS;
      oScrollInfo.nPos   += (a_nPixelsToMove/G_nScrollResolution)+1;

      SetScrollInfo(a_nBarID, &oScrollInfo);
   }
}

void ChartControl::UpdateScroll(int a_nSB)
{
   SCROLLINFO     oScrollInfo;
   CRect          rcClient;
   GetClientRect(rcClient);

   int nWindowLength  = ( a_nSB == SB_HORZ ? rcClient.Width() : rcClient.Height() );
   int nCurrentLength = ( a_nSB == SB_HORZ ? m_nChartWidth + m_pValueScalePainter->Width() : m_nChartHeight + m_pTimeScalePainter->Height() );

   if(nCurrentLength>nWindowLength)
   {
      oScrollInfo.cbSize = sizeof(oScrollInfo);
      oScrollInfo.fMask = SIF_PAGE | SIF_RANGE;
      oScrollInfo.nPage = nWindowLength/G_nScrollResolution + 1;
      oScrollInfo.nMin  = 0;
      oScrollInfo.nMax  = nCurrentLength/G_nScrollResolution + 1;

      ShowScrollBar(a_nSB, TRUE);
      SetScrollInfo(a_nSB, &oScrollInfo, TRUE);
   }
   else
   {
      ShowScrollBar(a_nSB, FALSE);
      SetScrollPos(a_nSB, 0);

      oScrollInfo.cbSize = sizeof(oScrollInfo);
      oScrollInfo.fMask = SIF_PAGE | SIF_RANGE;
      oScrollInfo.nPage = 0;
      oScrollInfo.nMin  = 0;
      oScrollInfo.nMax  = 0;

      SetScrollInfo(a_nSB, &oScrollInfo, TRUE);
   }
}

void ChartControl::FillItemSnapshotInfo(const CPoint& a_Point, ChartControl::ChartItemSnapshot& a_ItemSnapshot)
{
   int nLeftItemPos     = 0;

   int nPointXPos   = a_Point.x + GetScrollPos(SB_HORZ)*G_nScrollResolution;

   for(int nItemIdx=0; nItemIdx<(int)m_vDrawDescriptors.size(); ++nItemIdx)
   {
      if(nPointXPos > nLeftItemPos)
      {
         a_ItemSnapshot.m_nItemLeftSidePos = nLeftItemPos;
         a_ItemSnapshot.m_nItemIdx = nItemIdx;

         nLeftItemPos     += m_nItemWidth + (nItemIdx<m_nItemsWithExtendedWidthAmount ? 1 : 0);
      }
      else
      {
         break;
      }
   }
}

int ChartControl::ItemLeftPos(int a_nItemIdx)
{
   int nItemLeftPos = 0;

   if(a_nItemIdx<m_nItemsWithExtendedWidthAmount)
   {
      nItemLeftPos = a_nItemIdx*(m_nItemWidth+1);
   }
   else
   {
      nItemLeftPos = a_nItemIdx*m_nItemWidth;
      nItemLeftPos += m_nItemsWithExtendedWidthAmount;
   }

   return nItemLeftPos;
}

BEGIN_MESSAGE_MAP(ChartControl, CWnd)
   ON_WM_PAINT()
   ON_WM_HSCROLL()
   ON_WM_VSCROLL()
   ON_WM_ERASEBKGND()
   ON_WM_SIZE()
   ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

void ChartControl::OnPaint()
{
//   VERIFY_EXPRESSION0(m_vDrawDescriptors.size()>0);

   if(m_bUpdateHorzScroll || m_bUpdateVertScroll)
   {
      UpdateDimensions();

      if(m_bUpdateHorzScroll)
      {
         // move horizontal scroll with difference between current and remembered position
         MoveScroll(SB_HORZ, ItemLeftPos(m_oItemSnapshot.m_nItemIdx) - m_oItemSnapshot.m_nItemLeftSidePos );
         // reset flag
         m_bUpdateHorzScroll = false;
      }

      if(m_bUpdateVertScroll)
      {
         // move vertical scroll with difference between current and remembered position
         MoveScroll(SB_VERT, m_nVertBarOffset);
         // reset flag;
         m_bUpdateVertScroll = false;
      }
   }

   CPaintDC dc(this);
   MemoryDC memDC( &dc , this );

   if( NULL != m_pAssetObj && m_pAssetObj->Loaded() )
   {
      Draw(memDC.BufferedDC());
   }
   else
   {
      DrawEmptyChart(memDC.BufferedDC());
   }
}

void ChartControl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
   BOOL bRedraw = FALSE;
//   CRect rcClient;
//    int nMin = 0;
//    int nMax = 0;

//   GetClientRect(rcClient);

   switch(nSBCode)
   {
   case SB_THUMBPOSITION:
   case SB_THUMBTRACK:
      SetScrollPos(SB_HORZ, nPos);
      bRedraw = TRUE;
      break;

   case SB_LINEDOWN:
      SetScrollPos(SB_HORZ, GetScrollPos(SB_HORZ)+1);
      bRedraw = TRUE;
      break;

   case SB_LINEUP:
      SetScrollPos(SB_HORZ, GetScrollPos(SB_HORZ)-1);
      bRedraw = TRUE;
      break;

//    case SB_PAGEUP :
//       SetScrollPos(SB_VERT, GetScrollPos(SB_VERT)-rcClient.Height()/G_nTableScrollResolution);
//       bRedraw = TRUE;
//       break;
// 
//    case SB_PAGEDOWN :
//       SetScrollPos(SB_VERT, GetScrollPos(SB_VERT)+rcClient.Height()/G_nTableScrollResolution);
//       bRedraw = TRUE;
//       break;
// 
//    case SB_TOP :
//       SetScrollPos(SB_VERT, 0);
//       bRedraw = TRUE;
//       break;
// 
//    case SB_BOTTOM :
//       GetScrollRange(SB_VERT, &nMin, &nMax);
//       SetScrollPos(SB_VERT, nMax);
//       bRedraw = TRUE;
//       break;
   }

   if(bRedraw)
   {   
      Invalidate();
   }
}

void ChartControl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
   BOOL bRedraw = FALSE;

   switch(nSBCode)
   {
   case SB_THUMBPOSITION:
   case SB_THUMBTRACK:
      SetScrollPos(SB_VERT, nPos);
      bRedraw = TRUE;
      break;

   case SB_LINEDOWN:
      SetScrollPos(SB_VERT, GetScrollPos(SB_VERT)+1);
      bRedraw = TRUE;
      break;

   case SB_LINEUP:
      SetScrollPos(SB_VERT, GetScrollPos(SB_VERT)-1);
      bRedraw = TRUE;
      break;

      //    case SB_PAGEUP :
      //       SetScrollPos(SB_VERT, GetScrollPos(SB_VERT)-rcClient.Height()/G_nTableScrollResolution);
      //       bRedraw = TRUE;
      //       break;
      // 
      //    case SB_PAGEDOWN :
      //       SetScrollPos(SB_VERT, GetScrollPos(SB_VERT)+rcClient.Height()/G_nTableScrollResolution);
      //       bRedraw = TRUE;
      //       break;
      // 
      //    case SB_TOP :
      //       SetScrollPos(SB_VERT, 0);
      //       bRedraw = TRUE;
      //       break;
      // 
      //    case SB_BOTTOM :
      //       GetScrollRange(SB_VERT, &nMin, &nMax);
      //       SetScrollPos(SB_VERT, nMax);
      //       bRedraw = TRUE;
      //       break;
   }

   if(bRedraw)
   {   
      Invalidate();
   }
}


BOOL ChartControl::OnEraseBkgnd(CDC* pDC)
{
   return TRUE;
}

void ChartControl::OnSize(UINT nType, int cx, int cy)
{
   CWnd::OnSize(nType, cx, cy);

   m_pValueScalePainter->Width( min(cx, G_nDefaultValueScaleWidth) );

   m_pTimeScalePainter->Height( min(cy, G_nDefaultTimeScaleHeight) );

   UpdateScroll(SB_HORZ);

   UpdateScroll(SB_VERT);
}

BOOL ChartControl::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
   int  nUnitsCount      = zDelta/WHEEL_DELTA;
   bool bRedraw          = false;
   bool bMoveScroll      = true;

   if(nFlags & MK_CONTROL)       // if Ctrl is pressed change chart width
   {
      bMoveScroll = false;
      // change width only if necessary
      if(nUnitsCount < 0 && m_nItemWidth > 1 || 
         nUnitsCount > 0 && m_nItemWidth < 20   )
      {
         ScreenToClient(&pt);
         FillItemSnapshotInfo(pt, m_oItemSnapshot);
         m_bUpdateHorzScroll = true;

         m_nChartWidth +=  (int)(m_dWidthChangeFactor*nUnitsCount*m_nChartWidth);

         bRedraw = true;
      }
   }
   
   if(nFlags & MK_SHIFT)
   {
      bMoveScroll = false;

      ScreenToClient(&pt);
      int nCurrentYPos = GetScrollPos(SB_VERT)*G_nScrollResolution + pt.y;
      
      m_nVertBarOffset = (int)(m_dHeightChangeFactor*nUnitsCount*nCurrentYPos);

      //FillItemSnapshotInfo(pt, m_oItemSnapshot);
      m_bUpdateVertScroll = true;
      
      m_nChartHeight += (int)(m_dHeightChangeFactor*nUnitsCount*m_nChartHeight);

      m_pValueScalePainter->Height( m_nChartHeight );

      RecalculateDrawDescriptors();

      bRedraw = true;
   }
   
   if(bMoveScroll)
   {
      if(nFlags & MK_LBUTTON)
      {
         SetScrollPos(SB_VERT, GetScrollPos(SB_VERT)-nUnitsCount);
      }

      if(nFlags & MK_RBUTTON)
      {
         SetScrollPos(SB_HORZ, GetScrollPos(SB_HORZ)+nUnitsCount);
      }
     
      bRedraw = true;
   }
   
   if(bRedraw)
   {
      Invalidate();
   }

   return CWnd::OnMouseWheel(nFlags, zDelta, pt);
}