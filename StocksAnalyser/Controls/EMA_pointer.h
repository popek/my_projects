#pragma once
#include "ControlsLib.h"
#include "PointerAvarageBase.h"


class CONTROLS_LIB EMA_pointer : public PointerAvarageBase
{
public:
   EMA_pointer(int a_nMeanValueCalculationSamplesAmount);
   virtual ~EMA_pointer(void);

   virtual void   Create(const DataSeries& a_inputData);
};
