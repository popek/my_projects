#include <xercesc/sax/HandlerBase.hpp>
#include <stack>


class XMLNode;


class XercescSAXHandler : public xercesc::HandlerBase
{
public :
    // -----------------------------------------------------------------------
    //  Constructors
    // -----------------------------------------------------------------------
    XercescSAXHandler();
    ~XercescSAXHandler();

    shared_ptr<XMLNode> detachNode();

    // -----------------------------------------------------------------------
    //  Handlers for the SAX DocumentHandler interface
    // -----------------------------------------------------------------------
    virtual void startElement(const XMLCh* const name, xercesc::AttributeList& attributes);
    virtual void endElement(const XMLCh* const name);
    virtual void characters(const XMLCh* const chars, const XMLSize_t length);
    virtual void resetDocument();


    // -----------------------------------------------------------------------
    //  Implementations of the SAX ErrorHandler interface
    // -----------------------------------------------------------------------
    virtual void warning(const xercesc::SAXParseException& exc);
    virtual void error(const xercesc::SAXParseException& exc);
    virtual void fatalError(const xercesc::SAXParseException& exc);

private:
   shared_ptr<XMLNode> _rootNode;
   stack<shared_ptr<XMLNode>> _nodes;
};

