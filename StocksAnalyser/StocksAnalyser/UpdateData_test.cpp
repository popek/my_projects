#include "StdAfx.h"
#include "UpdateData_test.h"
#include "Misc.h"
#include "Poco/Net/HTTPStreamFactory.h"
#include "Poco/Net/FTPStreamFactory.h"
#include "Poco/URIStreamOpener.h"
#include "Poco/StreamCopier.h"
#include "Poco/URI.h"
#include "Poco/Exception.h"
#include <fstream>
#include <sstream>
#include "StocksAnalyser.h"
#include "PathUtils.h"


using namespace Poco;
using namespace Poco::Net;

UpdateData_test::UpdateData_test(void)
{
}
UpdateData_test::~UpdateData_test(void)
{
}

void UpdateData_test::Init()
{
   HTTPStreamFactory::registerFactory();
   FTPStreamFactory::registerFactory();
}

void UpdateData_test::GetData()
{
   string resourceURI = "http://stooq.pl/notowania/?kat=g2";

   std::ofstream testFile(Misc::ToAnsi( PathUtils::concatenateDirAndFile( Misc::GetBinFolder(), _T("test") )).c_str(), 
      std::ios_base::out/* | std::ios_base::binary*/);

   ASSERT(testFile.is_open());

   if(!testFile.is_open())
   {
      return;
   }

   try
   {
      URI uri(resourceURI);

      shared_ptr<std::istream> pStr(URIStreamOpener::defaultOpener().open(uri));
      StreamCopier::copyStream(*pStr.get(), testFile);
   }
   catch (Exception&)
   {
      ASSERT(FALSE);
   }
}
