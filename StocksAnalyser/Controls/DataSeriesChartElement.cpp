#include "StdAfx.h"
#include "DataSeriesChartElement.h"
#include "ChartGraphicElementFactory.h"
#include "GraphicObjectBase.h"
#include "TimeScaleContext.h"
#include "ValueScaleContext.h"


DataSeriesChartElement::DataSeriesChartElement(EChartElementId a_elementId, size_t a_pointsCount, size_t a_offset)
: ChartElement(a_elementId, a_pointsCount)
, m_offset(a_offset)
{
}
DataSeriesChartElement::DataSeriesChartElement(EChartElementId a_elementId, size_t a_pointsCount)
: ChartElement(a_elementId, a_pointsCount)
, m_offset(0)
{
}
DataSeriesChartElement::~DataSeriesChartElement(void)
{
}

shared_ptr<GraphicObjectBase> DataSeriesChartElement::CreateGraphicObject(const TimeScaleContext& a_timeScaleContext, const ValueScaleContext& a_valueScaleContext) const
{
   if(0 == m_points.size())
   {
      return ChartGraphicElementFactory::Instance()->CreateGraphicObject(m_elementId, _elementStyle);
   }

   size_t firstPointIndex = FindFirstIndex(a_timeScaleContext);
   size_t lastPointIndex  = FindLastIndex(a_timeScaleContext);

   shared_ptr<GraphicObjectBase> spGraphicObject = ChartGraphicElementFactory::Instance()->CreateGraphicObject(m_elementId, _elementStyle);

   spGraphicObject->Reserve(lastPointIndex - firstPointIndex + 1);

   const ValueScaleContext& valueScaleToUse = _spOwnValueScaleContex ? *_spOwnValueScaleContex : a_valueScaleContext;
   
   size_t graphicPointIndex = 0;
   for(size_t pointIndex = firstPointIndex; pointIndex <= lastPointIndex; ++pointIndex, ++ graphicPointIndex)
   {
      spGraphicObject->UpdatePoint( graphicPointIndex, 
                                    a_timeScaleContext.ToCoordinate( m_points[pointIndex].Timestamp()), 
                                    valueScaleToUse.ToCoordinate( m_points[pointIndex].Value()) );
   }

   return spGraphicObject;
}

size_t DataSeriesChartElement::FindFirstIndex(const TimeScaleContext& a_timescaleContext) const
{
   int firstVisibleIndex = (int)a_timescaleContext.FirstVisibleTimestampIndex() - 1 - (int)m_offset;

   return (firstVisibleIndex < 0 ? 0 : firstVisibleIndex);
}

size_t DataSeriesChartElement::FindLastIndex(const TimeScaleContext& a_timescaleContext) const
{
   int lastVisibleIndex = (int)a_timescaleContext.LastVisibleTimestampIndex() + 1 - (int)m_offset;

   if(lastVisibleIndex < 0)
   {
      return 0;
   }

   return (lastVisibleIndex < (int)m_points.size() ? lastVisibleIndex : m_points.size() - 1);
}
